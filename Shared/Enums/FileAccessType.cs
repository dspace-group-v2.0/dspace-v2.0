namespace Shared.Enums;

public enum FileAccessType
{
    FullAccess,
    Download,
    View
}