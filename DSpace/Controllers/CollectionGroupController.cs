﻿using Application.CollectionGroups;
using Infrastructure.Services;
using Microsoft.AspNetCore.Mvc;

namespace DSpace.Controllers
{
   [Route("api/[controller]")]
   [ApiController]
   public class CollectionGroupController : ControllerBase
   {
      private ICollectionGroupService _collectionGroupService;
      public CollectionGroupController(ICollectionGroupService collectionGroupService)
      {
         _collectionGroupService = collectionGroupService;
      }
      [HttpPost("[action]")]
      public async Task<IActionResult> AddCollectionGroup(List<CollectionGroupDTOForCreate> listCollectionGroupDTO)
      {
         var result = await _collectionGroupService.AddCollectionManageGroup(listCollectionGroupDTO);
         if (result.IsSuccess)
         {
            return Ok(result.Message);
         }

         return BadRequest(result.Message);

      }
      [HttpPut("[action]")]
      public async Task<IActionResult> UpdateCollectionGroup(CollectionGroupDTOForUpdate collectionGroupDTO)
      {
         var result = await _collectionGroupService.UpdateCollectionManageGroup(collectionGroupDTO);
         if (result.IsSuccess)
         {
            return Ok(result.Message);
         }

         return BadRequest(result.Message);

      }
      [HttpDelete("[action]")]
      public async Task<IActionResult> DeleteCollectionGroup(int id)
      {
         var result = await _collectionGroupService.DeleteCollectionManageInGroup(id);
         if (result.IsSuccess)
         {
            return Ok(result.Message);
         }

         return BadRequest(result.Message);

      }
      [HttpGet("[action]")]
      public async Task<IActionResult> GetAllCollectionGroup()
      {
         var result = await _collectionGroupService.GetListCollectionGroup();
         if (result.IsSuccess)
         {
            return Ok(result.ObjectResponse);
         }

         return BadRequest(result.Message);

      }
   }
}
