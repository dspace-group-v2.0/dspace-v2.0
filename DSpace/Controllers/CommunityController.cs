using Application.Communities;
using Infrastructure.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace DSpace.Controllers
{
   [Route("api/[controller]")]
   [ApiController]
   public class CommunityController : Controller
   {
      private ICommunityService _communityService;

      public CommunityController(ICommunityService communityService)
      {
         _communityService = communityService;
      }

      [HttpGet("[action]")]
      public async Task<IActionResult> getListOfCommunities()
      {
         var response = await _communityService.GetAllCommunity();
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response.Message);
         }
      }
      [HttpGet("[action]")]
      public async Task<IActionResult> GetCommunityTree()
      {
         var response = await _communityService.GetAllCommunityTree();
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response.Message);
         }
      }
      [HttpGet("[action]/{id}")]
      public async Task<IActionResult> GetCommunity(int id)
      {
         var response = await _communityService.GetCommunityByID(id);
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response.Message);
         }
      }
      [HttpPost("[action]")]
      [Authorize]
      public async Task<IActionResult> CreateCommunity([FromForm] CommunityDTOForCreateOrUpdate communityDTO)
      {
         var identity = HttpContext.User.Identity as ClaimsIdentity;

         if (identity == null)
         {
            return BadRequest();
         }
         var userClaims = identity.Claims;
         var idUser = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Authentication).Value;
         int userCreateId = int.Parse(idUser);
         if (communityDTO.File == null)
         {
            communityDTO.LogoUrl = "";
         }
         else
         {
            string folderPath = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(Directory.GetCurrentDirectory()), "client-dspace\\public\\UploadedFiles"));
            Directory.CreateDirectory(folderPath);
            string filePath = Path.Combine(folderPath, communityDTO.File.FileName);
            communityDTO.LogoUrl = filePath;
         }
         var response = await _communityService.CreateCommunity(communityDTO, userCreateId);
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response.Message);
         }
      }

      [HttpPut("[action]/{communityId}")]
      [Authorize]
      public async Task<IActionResult> UpdateCommunity(int communityId, [FromForm] CommunityDTOForCreateOrUpdate communityDTO)
      {
         var identity = HttpContext.User.Identity as ClaimsIdentity;

         if (identity == null)
         {
            return Unauthorized();
         }
         var userClaims = identity.Claims;
         var idUser = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Authentication).Value;
         var userUpdateId = int.Parse(idUser);
         if (communityDTO.File == null)
         {
            communityDTO.LogoUrl = "";
         }
         else
         {
            string folderPath = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(Directory.GetCurrentDirectory()), "client-dspace\\public\\UploadedFiles"));
            Directory.CreateDirectory(folderPath);
            string filePath = Path.Combine(folderPath, communityDTO.File.FileName);
            communityDTO.LogoUrl = filePath;
         }
         var response = await _communityService.UpdateCommunity(communityId, communityDTO, userUpdateId);
         if (response.IsSuccess)
         {
            return Ok(response.Message);
         }
         else
         {
            return BadRequest(response.Message);
         }
      }
      [HttpGet("[action]/{name}")]
      public async Task<IActionResult> GetCommunityByName(string name)
      {
         var response = await _communityService.GetCommunityByName(name);
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response.Message);
         }
      }

      [HttpDelete]
      [Route("[action]/{communityId}")]
      public async Task<IActionResult> DeleteCommunity(int communityId)
      {
         var response = await _communityService.DeleteCommunity(communityId);
         if (response.IsSuccess)
         {
            return Ok(response.Message);
         }
         else
         {
            return BadRequest(response.Message);
         }
      }
      [HttpGet("[action]")]
      public async Task<IActionResult> GetCommunityParent()
      {
         var response = await _communityService.GetAllCommunityParent();
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response.Message);
         }
      }
   }
}