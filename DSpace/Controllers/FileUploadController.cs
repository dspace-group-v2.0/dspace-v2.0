using Application.FileUploads;
using Infrastructure.Services;
using Microsoft.AspNetCore.Mvc;

namespace DSpace.Controllers;

[ApiController]
[Route("api/[controller]")]
public class FileUploadController : Controller
{
    private IFileUploadService _fileUploadService;
    public FileUploadController(IFileUploadService fileUploadService)
    {
        _fileUploadService = fileUploadService;
    }
    
    [HttpGet("downloadFile/{fileKeyId}")]
    public async Task<IActionResult> DownloadFileWithKeyId([FromRoute] string fileKeyId)
    {
        var response = await _fileUploadService.DownloadFileWithId(fileKeyId);
        if (response.IsSuccess)
        {
            return Ok(response.ObjectResponse);
        }
        else
        {
            return BadRequest(response.Message);
        }
    }
    [HttpDelete("[action]/{fileId}")]
    public async Task<IActionResult> DeleteFileWithFileId([FromRoute] int fileId)
    {
        var response = await _fileUploadService.DeleteFileWithFileId(fileId);
        if (response.IsSuccess)
        {
            return Ok(response.Message);
        }
        else
        {
            return BadRequest(response.Message);
        }
    }
    [HttpPut("[action]/{fileId}")]
    public async Task<IActionResult> UndiscoverableFileWithFileId([FromRoute] int fileId)
    {
        var response = await _fileUploadService.UndiscoverableFileWithFileId(fileId);
        if (response.IsSuccess)
        {
            return Ok(response.Message);
        }
        else
        {
            return BadRequest(response.Message);
        }
    }
    [HttpPut("[action]")]
    public async Task<IActionResult> ModifyFileWithFileId([FromForm] ModifyFileWithAccessDto modifyFileWithAccessDto)
    {
        var response = await _fileUploadService.ModifyFileWithFileId(modifyFileWithAccessDto);
        if (response.IsSuccess)
        {
            return Ok(response.ObjectResponse);
        }
        else
        {
            return BadRequest(response.Message);
        }
    }
    [HttpPost("[action]/{itemId}")]
    public async Task<IActionResult> AddNewFileToItem([FromRoute] int itemId, [FromForm] IList<IFormFile> multipleFiles, [FromForm] string role)
    {
        var response = await _fileUploadService.AddNewFileToItem(itemId, multipleFiles, role);
        if (response.IsSuccess)
        {
            return Ok(response.ObjectResponse);
        }
        else
        {
            return BadRequest(response.Message);
        }
    }
}