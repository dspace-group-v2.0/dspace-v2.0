﻿using Application.LoginRequest;
using Infrastructure.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shared.Constants;
using System.ComponentModel.DataAnnotations;

namespace DSpace.Controllers;

[Route("api/[controller]")]
[ApiController]
public class AuthenticationController : Controller
{
   private IUserService _userService;


   private string? ThisProvider { get; set; }
   public AuthenticationProperties? ThisProperties { get; set; }
   public AuthenticationController(
       IUserService userService)
   {

      _userService = userService;
   }

   public class RegisterModel
   {
      [Required]
      [EmailAddress]
      [Display(Name = "Email")]
      public string Email { get; set; }

      [Required]
      [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
      [DataType(DataType.Password)]
      [Display(Name = "Password")]
      public string Password { get; set; }

      [DataType(DataType.Password)]
      [Display(Name = "Confirm password")]
      [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
      public string ConfirmPassword { get; set; }
   }

   //public class LoginModel
   //{
   //   [Required]
   //   [Display(Name = "Username")]
   //   public string Username { get; set; }

   //   [Required]
   //   [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
   //   [DataType(DataType.Password)]
   //   [Display(Name = "Password")]
   //   public string Password { get; set; }

   //   [Display(Name = "Remember me?")]
   //   public bool RememberMe { get; set; }
   //}

   //public class LoginModel
   //{
   //   [Required]
   //   [EmailAddress]
   //   public string Email { get; set; }
   //   [Required]
   //   public string Given_Name { get; set; }
   //   [Required]
   //   public string Family_Name { get; set; }
   //   public string? Hd { get; set; }
   //}

   [AllowAnonymous]
   [HttpPost("Login")]
   public async Task<IActionResult> LoginWithGoogle(LoginRequest request)
   {
      var result = await _userService.Login(request);
      //string email = request.Email;
      ////if(email.Contains("@fpt.edu.vn"))
      ////{
      ////   return Unauthorized();
      ////}
      //var userCheck = await _userService.getUserByEmail(email);
      //if (userCheck == null)
      //{
      //   var userNew = _userService.CreateInstanceUser(); //CreateUser();
      //   userNew.Email = email;
      //   userNew.FirstName = request.Family_Name;
      //   userNew.LastName = request.Given_Name;

      //   userNew.isActive = true;
      //   userNew.UserName = email;

      //   var createUserResult = await _userService.CreateUser(userNew);
      //   if (createUserResult)
      //   {
      //      var emailInfomation = email.Split('@');
      //      var emailCheck = emailInfomation[0];
      //      if (emailCheck.Substring(emailCheck.Length - 4).All(char.IsDigit))
      //      {
      //         await _userService.AssignRole(userNew, RolesConstants.STUDENT);
      //      }
      //      else
      //      {
      //         await _userService.AssignRole(userNew, RolesConstants.LECTURER);
      //      }
      //   }
      //}
      //var user = await _userService.getUserByEmail(email);

      //var userRole = await _userService.GetRole(user);

      //var authClaims = _userService.getUserClaim(user, userRole);

      //var token = _userService.GenerateToken(authClaims);
      if (result.IsSuccess)
      {
         return Ok(result.ObjectResponse);
      }
      else
      {
         return BadRequest();
      }
      
   }
}