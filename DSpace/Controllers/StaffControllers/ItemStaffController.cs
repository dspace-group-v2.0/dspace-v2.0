﻿using Application.Items;
using Application.Metadatas;
using Infrastructure.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shared.Enums;
using System.Security.Claims;

namespace DSpace.Controllers.StaffControllers
{
   [Route("api/[controller]")]
   [ApiController]
   public class ItemStaffController : ControllerBase
   {
      private IItemService _itemService;
    

      public ItemStaffController(IItemService itemService)
      {
         _itemService = itemService;
         
      }

      [HttpGet("[action]/{pageIndex}/{pageSize}")]
      [Authorize]
      public async Task<IActionResult> GetAllItems(int pageIndex, int pageSize, SortType sortType)
      {
         var identity = HttpContext.User.Identity as ClaimsIdentity;

         if (identity == null)
         {
            return BadRequest();
         }
         var userClaims = identity.Claims;

         var idUser = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Authentication).Value;
         var userId = int.Parse(idUser);
         var result = await _itemService.GetAllItemForStaff(userId, pageIndex, pageSize, sortType);
         if (result.IsSuccess)
         {
            if (result.ObjectResponse != null)
            {
               return Ok(result.ObjectResponse);
            }
            return NotFound(result.Message);

         }
         else
         {
            return BadRequest(result.Message);
         }
      }
      [HttpPost("[action]")]
      [Authorize]
      public async Task<IActionResult> CreateSimpleItem([FromForm] ItemDTOForCreateSimple itemDTO)
      {
         var identity = HttpContext.User.Identity as ClaimsIdentity;

         if (identity == null)
         {
            return BadRequest();
         }
         var userClaims = identity.Claims;
         var idUser = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Authentication).Value;
         int userId = int.Parse(idUser);


         var response = await _itemService.CreateSimpleItemForStaff(itemDTO, userId);
         if (response.IsSuccess)
         {
            return Ok(response);
         }
         else
         {
            return BadRequest(response);
         }

      }
      [HttpGet("[action]/{itemId}")]
      [Authorize]
      public async Task<IActionResult> GetItemSimpleById(int itemId)
      {
         var identity = HttpContext.User.Identity as ClaimsIdentity;

         if (identity == null)
         {
            return BadRequest();
         }
         var userClaims = identity.Claims;
         var idUser = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Authentication).Value;
         int userId = int.Parse(idUser);
         var result = await _itemService.GetItemSimpleByIdForStaff(itemId, userId);
         if (result.IsSuccess)
         {
            return Ok(result.ObjectResponse);
         }
         else
         {
            return BadRequest(result);
         }
      }
      [HttpPut("[action]/{itemId}")]
      [Authorize]
      public async Task<IActionResult> ModifyItem(MetadataValueDTOForModified metadataValueDTOForModified, int itemId)
      {
         var identity = HttpContext.User.Identity as ClaimsIdentity;

         if (identity == null)
         {
            return BadRequest();
         }
         var userClaims = identity.Claims;
         var idUser = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Authentication).Value;
         int userCreateId = int.Parse(idUser);

         var result = await _itemService.ModifyItemForStaff(metadataValueDTOForModified, userCreateId, itemId);
         if (result.IsSuccess)
         {
            return Ok(result.ObjectResponse);
         }
         else
         {
            return BadRequest(result);
         }
      }
      [HttpGet("[action]/{itemId}")]
      [Authorize]
      public async Task<IActionResult> GetItemFullById(int itemId)
      {
         var identity = HttpContext.User.Identity as ClaimsIdentity;

         if (identity == null)
         {
            return BadRequest();
         }
         var userClaims = identity.Claims;
         var idUser = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Authentication).Value;
         int userId = int.Parse(idUser);
         var result = await _itemService.GetItemFullByIdForStaff(itemId, userId);
         if (result.IsSuccess)
         {
            return Ok(result.ObjectResponse);
         }
         else
         {
            return BadRequest(result);
         }
      }
      [HttpGet("itemsInCollection/{collectionId}/{pageIndex}/{pageSize}/{sortType}")]
      [Authorize]
      public async Task<ActionResult<List<ItemDto>>> GetAllItemInOneCollection([FromRoute] int collectionId, int pageIndex, int pageSize, SortType sortType)
      {
         var identity = HttpContext.User.Identity as ClaimsIdentity;

         if (identity == null)
         {
            return BadRequest();
         }
         var userClaims = identity.Claims;
         var idUser = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Authentication).Value;
         int userId = int.Parse(idUser);
         var response = await _itemService.GetAllItemInOneCollectionForStaff(collectionId, pageIndex, pageSize, userId, sortType);
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response.Message);
         }
      }
      [HttpPost("[action]/{pageIndex}/{pageSize}")]
      [Authorize]
      public async Task<IActionResult> SearchItem(ItemDtoForSearch itemDtoForSearch, int pageIndex, int pageSize)
      {
         var identity = HttpContext.User.Identity as ClaimsIdentity;

         if (identity == null)
         {
            return BadRequest();
         }
         var userClaims = identity.Claims;
         var idUser = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Authentication).Value;
         int userId = int.Parse(idUser);
         var response = await _itemService.SearchItemForStaff(itemDtoForSearch, userId, pageIndex, pageSize);
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response);
         }
      }
      [HttpGet("[action]/{search}/{pageIndex}/{pageSize}/{sortType}")]
      [Authorize]
      public async Task<IActionResult> SearchItemByAll(string search, int pageIndex, int pageSize, SortType sortType)
      {
         var identity = HttpContext.User.Identity as ClaimsIdentity;

         if (identity == null)
         {
            return BadRequest();
         }
         var userClaims = identity.Claims;
         var idUser = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Authentication).Value;
         int userId = int.Parse(idUser);
         var response = await _itemService.SearchItemByAllForStaff(search, userId, pageIndex, pageSize, sortType);
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response);
         }
      }

      [HttpPost("[action]/{pageIndex}/{pageSize}")]
      [Authorize]
      public async Task<IActionResult> SearchItemSimple(ItemDTOForSearchSimple itemDTOForSearchSimple, int pageIndex, int pageSize)
      {
         var identity = HttpContext.User.Identity as ClaimsIdentity;

         if (identity == null)
         {
            return BadRequest();
         }
         var userClaims = identity.Claims;
         var idUser = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Authentication).Value;
         int userId = int.Parse(idUser);
         var response = await _itemService.SearchItemSimpleForStaff(itemDTOForSearchSimple, userId, pageIndex, pageSize);
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response);
         }
      }
      [HttpPost("[action]/{pageIndex}/{pageSize}")]
      public async Task<IActionResult> SearchItemByAuthor(ItemDTOForSearchAuthor itemDTOForSearchAuthor, int pageIndex, int pageSize)
      {
         var identity = HttpContext.User.Identity as ClaimsIdentity;

         if (identity == null)
         {
            return BadRequest();
         }
         var userClaims = identity.Claims;
         var idUser = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Authentication).Value;
         int userId = int.Parse(idUser);
         var response = await _itemService.SearchItemByAuthorForStaff(itemDTOForSearchAuthor.ListAuthor, itemDTOForSearchAuthor.CollectionId,itemDTOForSearchAuthor.Discoverable,itemDTOForSearchAuthor.SortType, userId, pageIndex, pageSize);
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response);
         }
      }
      [HttpPost("[action]/{pageIndex}/{pageSize}")]
      public async Task<IActionResult> SearchItemBySubjectKeyword(ItemDTOForSearchSubjectKeyword itemDTOForSearchSubjectKeyword, int pageIndex, int pageSize)
      {
         var identity = HttpContext.User.Identity as ClaimsIdentity;

         if (identity == null)
         {
            return BadRequest();
         }
         var userClaims = identity.Claims;
         var idUser = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Authentication).Value;
         int userId = int.Parse(idUser);
         var response = await _itemService.SearchItemBySubjectKeyWordsForStaff(itemDTOForSearchSubjectKeyword.ListSubjectKeyword, itemDTOForSearchSubjectKeyword.CollectionId, itemDTOForSearchSubjectKeyword.Discoverable, itemDTOForSearchSubjectKeyword.SortType, userId, pageIndex, pageSize);
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response);
         }
      }
      [HttpPost("[action]/{pageIndex}/{pageSize}")]
      public async Task<IActionResult> SearchItemByTitle(ItemDTOForSearchTitle itemDTOForSearchTitle, int pageIndex, int pageSize)
      {
         var identity = HttpContext.User.Identity as ClaimsIdentity;

         if (identity == null)
         {
            return BadRequest();
         }
         var userClaims = identity.Claims;
         var idUser = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Authentication).Value;
         int userId = int.Parse(idUser);
         var response = await _itemService.SearchItemByTitleForStaff(itemDTOForSearchTitle.Title, itemDTOForSearchTitle.CollectionId, itemDTOForSearchTitle.Discoverable, itemDTOForSearchTitle.SortType,userId, pageIndex, pageSize);
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response);
         }
      }
      [HttpPost("[action]/{pageIndex}/{pageSize}")]
      public async Task<IActionResult> SearchItemByDate(ItemDTOForSearchDateIssue itemDTOForSearchDateIssue, int pageIndex, int pageSize)
      {
         var identity = HttpContext.User.Identity as ClaimsIdentity;

         if (identity == null)
         {
            return BadRequest();
         }
         var userClaims = identity.Claims;
         var idUser = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Authentication).Value;
         int userId = int.Parse(idUser);
         var response = await _itemService.SearchItemByDateForStaff(itemDTOForSearchDateIssue.Year, itemDTOForSearchDateIssue.Month, itemDTOForSearchDateIssue.Day,itemDTOForSearchDateIssue.CollectionId, itemDTOForSearchDateIssue.Discoverable, itemDTOForSearchDateIssue.SortType,userId, pageIndex, pageSize);
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response);
         }
      }


   }
}
