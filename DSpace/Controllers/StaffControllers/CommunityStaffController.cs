﻿using Application.Communities;
using Infrastructure.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace DSpace.Controllers.StaffControllers
{
   [Route("api/[controller]")]
   [ApiController]
   public class CommunityStaffController : ControllerBase
   {
      private ICommunityService _communityService;

      public CommunityStaffController(ICommunityService communityService)
      {
         _communityService = communityService;
      }
      [HttpGet("[action]")]
      [Authorize]
      public async Task<IActionResult> GetAllCommunities()
      {
         var identity = HttpContext.User.Identity as ClaimsIdentity;

         if (identity == null)
         {
            return BadRequest();
         }
         var userClaims = identity.Claims;

         var idUser = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Authentication).Value;
         var userId = int.Parse(idUser);
         var result = await _communityService.GetCommunityByStaff(userId);
         if (result.IsSuccess)
         {
            if (result.ObjectResponse != null)
            {
               return Ok(result.ObjectResponse);
            }
            return NotFound(result.Message);

         }
         else
         {
            return BadRequest(result.Message);
         }

      }
      [HttpGet("[action]/{communityId}")]
      [Authorize]
      public async Task<IActionResult> GetCommunityById(int communityId)
      {
         var identity = HttpContext.User.Identity as ClaimsIdentity;

         if (identity == null)
         {
            return BadRequest();
         }
         var userClaims = identity.Claims;

         var idUser = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Authentication).Value;
         var userId = int.Parse(idUser);
         var response = await _communityService.GetCommunityByIDForStaff(communityId, userId);
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response.Message);
         }
      }
      [HttpPost("[action]")]
      [Authorize]
      public async Task<IActionResult> CreateCommunity([FromForm] CommunityDTOForCreateOrUpdate communityDTO)
      {
         var identity = HttpContext.User.Identity as ClaimsIdentity;

         if (identity == null)
         {
            return BadRequest();
         }
         var userClaims = identity.Claims;
         var idUser = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Authentication).Value;
         int userCreateId = int.Parse(idUser);
         if (communityDTO.File == null)
         {
            communityDTO.LogoUrl = "";
         }
         else
         {
            string folderPath = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(Directory.GetCurrentDirectory()), "client-dspace\\public\\UploadedFiles"));
            Directory.CreateDirectory(folderPath);
            string filePath = Path.Combine(folderPath, communityDTO.File.FileName);
            communityDTO.LogoUrl = filePath;
         }
         var response = await _communityService.CreateCommunityForStaff(communityDTO, userCreateId);
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response.Message);
         }
      }

      [HttpPut("[action]/{communityId}")]
      //[Authorize]
      public async Task<IActionResult> UpdateCommunity(int communityId, [FromForm] CommunityDTOForCreateOrUpdate communityDTO)
      {
         var identity = HttpContext.User.Identity as ClaimsIdentity;

         if (identity == null)
         {
            return BadRequest();
         }
         var userClaims = identity.Claims;
         var idUser = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Authentication).Value;
         var userUpdateId = int.Parse(idUser);
         if (communityDTO.File == null)
         {
            communityDTO.LogoUrl = "";
         }
         else
         {
            string folderPath = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(Directory.GetCurrentDirectory()), "client-dspace\\public\\UploadedFiles"));
            Directory.CreateDirectory(folderPath);
            string filePath = Path.Combine(folderPath, communityDTO.File.FileName);
            communityDTO.LogoUrl = filePath;
         }
         var response = await _communityService.UpdateCommunityForStaff(communityId, communityDTO, userUpdateId);
         if (response.IsSuccess)
         {
            return Ok(response.Message);
         }
         else
         {
            return BadRequest(response.Message);
         }
      }
      [HttpGet("[action]/{name}")]
      public async Task<IActionResult> GetCommunityByName(string name)
      {
         var response = await _communityService.GetCommunityByName(name);
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response.Message);
         }
      }

      [HttpDelete]
      [Route("[action]/{communityId}")]
      public async Task<IActionResult> DeleteCommunity(int communityId)
      {
         var identity = HttpContext.User.Identity as ClaimsIdentity;

         if (identity == null)
         {
            return BadRequest();
         }
         var userClaims = identity.Claims;
         var idUser = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Authentication).Value;
         var userDeleteId = int.Parse(idUser);
         var response = await _communityService.DeleteCommunityForStaff(communityId,userDeleteId);
         if (response.IsSuccess)
         {
            return Ok(response.Message);
         }
         else
         {
            return BadRequest(response.Message);
         }
      }
   }
}
