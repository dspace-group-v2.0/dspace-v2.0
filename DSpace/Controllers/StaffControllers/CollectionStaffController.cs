﻿using Application.Collections;
using Infrastructure.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace DSpace.Controllers.StaffControllers
{
   [Route("api/[controller]")]
   [ApiController]
   public class CollectionStaffController : ControllerBase
   {
      private ICollectionService _collectionService;
      private ICollectionGroupService _collectionGroupService;

      public CollectionStaffController(ICollectionService collectionService, ICollectionGroupService collectionGroupService)
      {
         _collectionService = collectionService;
         _collectionGroupService = collectionGroupService;
      }
      [HttpGet("[action]")]
      public async Task<IActionResult> GetAllCollections()
      {
         var identity = HttpContext.User.Identity as ClaimsIdentity;

         if (identity == null)
         {
            return BadRequest();
         }
         var userClaims = identity.Claims;

         var idUser = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Authentication).Value;
         var userId = int.Parse(idUser);

         var result = await _collectionService.GetAllCollectionByStaff(userId);
         if (result.IsSuccess)
         {
            if (result.ObjectResponse != null)
            {
               return Ok(result.ObjectResponse);
            }
            return NotFound(result.Message);

         }
         else
         {
            return BadRequest(result.Message);
         }

      }
      [HttpGet("getCollection/{id}")]
      public async Task<IActionResult> GetCollectionById(int id)
      {
         var identity = HttpContext.User.Identity as ClaimsIdentity;

         if (identity == null)
         {
            return BadRequest();
         }
         var userClaims = identity.Claims;

         var idUser = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Authentication).Value;
         var userId = int.Parse(idUser);
         var response = await _collectionService.GetCollectionByCollectionIdAndPeopleId(id,userId);
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response.Message);
         }
      }

      [HttpGet("[action]/CollectionId")]
      public async Task<IActionResult> GetCollectionGroupByCollectionId(int collectionId)
      {
         var identity = HttpContext.User.Identity as ClaimsIdentity;

         if (identity == null)
         {
            return BadRequest();
         }
         var userClaims = identity.Claims;

         var idUser = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Authentication).Value;
         var userId = int.Parse(idUser);
         var result = await _collectionGroupService.GetCollectionGroupDetailByStaff(collectionId, userId);
         if (result.IsSuccess)
         {
            return Ok(result.ObjectResponse);
         }

         return BadRequest(result.Message);

      }
      [HttpPost("[action]")]
      [Authorize]
      public async Task<IActionResult> CreateCollection([FromForm] CollectionDTOForCreateOrUpdate collectionDTO)
      {
         var identity = HttpContext.User.Identity as ClaimsIdentity;

         if (identity == null)
         {
            return BadRequest();
         }
         var userClaims = identity.Claims;
         var idUser = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Authentication).Value;
         int userCreateId = int.Parse(idUser);
         if (collectionDTO.File == null)
         {
            collectionDTO.LogoUrl = "";
         }
         else
         {
            string folderPath = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(Directory.GetCurrentDirectory()), "client-dspace\\public\\UploadedFiles"));
            Directory.CreateDirectory(folderPath);
            string filePath = Path.Combine(folderPath, collectionDTO.File.FileName);
            collectionDTO.LogoUrl = filePath;
         }
         var response = await _collectionService.CreateCollectionForStaff(collectionDTO, userCreateId);
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response.Message);
         }
      }

      [HttpPut("updateCollection/{collectionId}")]

      public async Task<IActionResult> UpdateCollection([FromRoute] int collectionId, [FromForm] CollectionDTOForCreateOrUpdate collectionDTO)
      {

         var identity = HttpContext.User.Identity as ClaimsIdentity;

         if (identity == null)
         {
            return BadRequest();
         }
         var userClaims = identity.Claims;
         var idUser = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Authentication).Value;
         int userUpdateId = int.Parse(idUser);
         if (collectionDTO.File == null)
         {
            collectionDTO.LogoUrl = "";
         }
         else
         {
            string folderPath = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(Directory.GetCurrentDirectory()), "client-dspace\\public\\UploadedFiles"));
            Directory.CreateDirectory(folderPath);
            string filePath = Path.Combine(folderPath, collectionDTO.File.FileName);
            collectionDTO.LogoUrl = filePath;
         }
         var response = await _collectionService.UpdateCollectionForStaff(collectionId, collectionDTO, userUpdateId);
         if (response.IsSuccess)
         {
            return Ok(response.Message);
         }
         else
         {
            return BadRequest(response.Message);
         }


      }
      [HttpGet("getCollectionByName/{name}")]
      public async Task<IActionResult> GetCollectionByName(string name)
      {
         var response = await _collectionService.GetCollectionByName(name);
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response.Message);
         }
      }
      [HttpDelete]
      [Route("DeleteCollection/{collectionId}")]
      public async Task<IActionResult> DeleteCollection(int collectionId)
      {
         var identity = HttpContext.User.Identity as ClaimsIdentity;

         if (identity == null)
         {
            return BadRequest();
         }
         var userClaims = identity.Claims;
         var idUser = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Authentication).Value;
         int userId = int.Parse(idUser);
         var response = await _collectionService.DeleteCollectionForStaff(collectionId,userId);
         if (response.IsSuccess)
         {
            return Ok(response.Message);
         }
         else
         {
            return BadRequest(response.Message);
         }
      }
   }
}
