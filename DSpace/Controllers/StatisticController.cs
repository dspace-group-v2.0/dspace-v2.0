﻿using Application.Statistics;
using Infrastructure.Services;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace DSpace.Controllers
{
   [Route("api/[controller]")]
   [ApiController]
   public class StatisticController : ControllerBase
   {
      private IStatisticService _statisticService;

      public StatisticController(IStatisticService statisticService)
      {
         _statisticService = statisticService;
      }
      [HttpGet("[action]/{itemId}")]
      public async Task<IActionResult> GetStatisticByItemId(int itemId)
      {
         var result = await _statisticService.GetAllStatisticByItemId(itemId);
         if (result.IsSuccess)
         {
            return Ok(result.ObjectResponse);
         }
         else
         {
            return BadRequest(result.Message);
         }
      }
      [HttpGet("[action]")]
      public async Task<IActionResult> GetAllStatistic()
      {
         var result = await _statisticService.GetAllStatistics();
         if (result.IsSuccess)
         {
            return Ok(result.ObjectResponse);
         }
         else
         {
            return BadRequest(result.Message);
         }
      }

      [HttpPost("[action]")]
      public async Task<IActionResult> UploadAllStatisticInOneDay(List<StatisticDTOForCreate> listCounterViewer)
      {
         var result = await _statisticService.UploadStatistic(listCounterViewer);
         if (result.IsSuccess)
         {
            return Ok(result.ObjectResponse);
         }
         else
         {
            return BadRequest(result.Message);
         }
      }
      [HttpPost("[action]")]
      public async Task<IActionResult> UploadStatisticIndividual(StatisticDTOForCreateIndividual CounterViewer)
      {
         var identity = HttpContext.User.Identity as ClaimsIdentity;

         if (identity == null)
         {
            return BadRequest();
         }
         var userClaims = identity.Claims;
         var userId = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.NameIdentifier).Value;
         var result = await _statisticService.UploadStatisticIndividual(CounterViewer, userId);
         if (result.IsSuccess)
         {
            return Ok(result.ObjectResponse);
         }
         else
         {
            return BadRequest(result.Message);
         }
      }

      [HttpGet("[action]/{itemId}")]
      public async Task<IActionResult> GetStatisticForItem([FromRoute] int itemId)
      {
         var result = await _statisticService.GetStatisticForItem(itemId);
         if (result.IsSuccess)
         {
            return Ok(result.ObjectResponse);
         }
         else
         {
            return BadRequest(result.Message);
         }
      }
      [HttpGet("[action]/{collectionId}")]
      public async Task<IActionResult> GetStatisticForCollection([FromRoute] int collectionId)
      {
         var result = await _statisticService.GetStatisticForCollection(collectionId);
         if (result.IsSuccess)
         {
            return Ok(result.ObjectResponse);
         }
         else
         {
            return BadRequest(result.Message);
         }
      }
      [HttpGet("[action]/{communityId}")]
      public async Task<IActionResult> GetStatisticForCommunity([FromRoute] int communityId)
      {
         var result = await _statisticService.GetStatisticForCommunity(communityId);
         if (result.IsSuccess)
         {
            return Ok(result.ObjectResponse);
         }
         else
         {
            return BadRequest(result.Message);
         }
      }
   }
}
