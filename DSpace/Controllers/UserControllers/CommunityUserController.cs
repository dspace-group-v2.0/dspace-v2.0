﻿using Infrastructure.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace DSpace.Controllers.UserControllers
{
   [Route("api/[controller]")]
   [ApiController]
   public class CommunityUserController : ControllerBase
   {
      private ICommunityService _communityService;

      public CommunityUserController(ICommunityService communityService)
      {
         _communityService = communityService;
      }

      [HttpGet("[action]")]
      public async Task<IActionResult> getListOfCommunities()
      {
         var response = await _communityService.GetAllCommunityForUser();
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response.Message);
         }
      }
      [HttpGet("[action]")]
      public async Task<IActionResult> GetCommunityTree()
      {
         var response = await _communityService.GetAllCommunityTreeForUser();
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response.Message);
         }
      }
      [HttpGet("[action]/{id}")]
      [Authorize]
      public async Task<IActionResult> GetCommunity(int id)
      {
         var identity = HttpContext.User.Identity as ClaimsIdentity;

         if (identity == null)
         {
            return BadRequest();
         }
         var userClaims = identity.Claims;
         var email = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Email).Value;
         var response = await _communityService.GetCommunityByIDForUser(id, email);
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response.Message);
         }
      }
      [HttpGet("[action]/{name}")]
      public async Task<IActionResult> GetCommunityByName(string name)
      {
         var response = await _communityService.GetCommunityByNameForUser(name);
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response.Message);
         }
      }
   }
}
