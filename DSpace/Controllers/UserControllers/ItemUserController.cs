﻿using Application.Items;
using Infrastructure.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shared.Enums;
using System.Security.Claims;

namespace DSpace.Controllers.UserControllers
{
   [Route("api/[controller]")]
   [ApiController]
   public class ItemUserController : ControllerBase
   {
      private readonly IItemService _itemService;

      public ItemUserController(IItemService itemService)
      {
         _itemService = itemService;
      }

      [HttpGet("[action]/{itemId}")]
      public async Task<IActionResult> GetItemFullById(int itemId)
      {
         var result = await _itemService.GetItemFullByIdForUser(itemId);
         if (result.IsSuccess)
         {
            return Ok(result.ObjectResponse);
         }
         else
         {
            return BadRequest(result);
         }
      }
      [HttpGet("[action]/{itemId}")]
      public async Task<IActionResult> GetItemSimpleById(int itemId)
      {
         var result = await _itemService.GetItemSimpleByIdForUser(itemId);
         if (result.IsSuccess)
         {
            return Ok(result.ObjectResponse);
         }
         else
         {
            return BadRequest(result);
         }
      }
      [HttpGet("[action]/{collectionId}/{pageIndex}/{pageSize}/{sortType}")]

      public async Task<IActionResult> GetListItemByCollectionId(int collectionId, int pageIndex, int pageSize, SortType sortType)
      {
         var result = await _itemService.GetAllItemByCollectionIdForUser(collectionId, pageIndex, pageSize, sortType);
         if (result.IsSuccess)
         {
            return Ok(result.ObjectResponse);
         }
         else
         {
            return BadRequest(result);
         }
      }
      [HttpGet("[action]")]
      [Authorize]
      public async Task<IActionResult> GetListItemRecentSubmited()
      {

         var identity = HttpContext.User.Identity as ClaimsIdentity;

         if (identity == null)
         {
            return BadRequest();
         }
         var userClaims = identity.Claims;
         var email = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Email).Value;
         var result = await _itemService.GetListItemRecentForUser(email);
         if (result.IsSuccess)
         {
            return Ok(result.ObjectResponse);
         }
         else
         {
            return BadRequest(result);
         }
      }

      [HttpPost("[action]/{pageIndex}/{pageSize}")]
      public async Task<IActionResult> SearchItem(ItemDTOForSearchForUser itemDtoForSearch, int pageIndex, int pageSize)
      {
         var response = await _itemService.SearchItemForUser(itemDtoForSearch, pageIndex, pageSize);
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response);
         }
      }
      [HttpGet("[action]/{search}/{pageIndex}/{pageSize}/{sortType}")]
      public async Task<IActionResult> SearchItemByAll(string search, int pageIndex, int pageSize, SortType sortType)
      {
         var response = await _itemService.SearchItemByAllForUser(search, pageIndex, pageSize, sortType);
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response);
         }
      }
      [HttpGet("[action]/{collectionId}")]
      public async Task<IActionResult> Get5ItemRecentInAcollection(int collectionId)
      {
         var response = await _itemService.Get5ItemRecentlyInOneCollectionForUser(collectionId);
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response);
         }
      }
      [HttpGet("[action]/{pageIndex}/{pageSize}/{sortType}")]
      public async Task<IActionResult> GetAllItems(int pageIndex, int pageSize, SortType sortType)
      {
         var result = await _itemService.GetAllItemForUser(pageIndex, pageSize, sortType);
         if (result.IsSuccess)
         {
            if (result.ObjectResponse != null)
            {
               return Ok(result.ObjectResponse);
            }
            return NotFound(result.Message);

         }
         else
         {
            return BadRequest(result.Message);
         }
      }

      [HttpPost("[action]/{pageIndex}/{pageSize}")]
      public async Task<IActionResult> SearchItemSimple(ItemDTOForSearchSimpleForUser itemDTOForSearchSimple, int pageIndex, int pageSize)
      {
         var response = await _itemService.SearchItemSimpleForUser(itemDTOForSearchSimple, pageIndex, pageSize);
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response);
         }
      }
      [HttpPost("[action]/{pageIndex}/{pageSize}")]
      public async Task<IActionResult> SearchItemByAuthor(ItemDTOForSearchAuthorForUser itemDTOForSearchAuthor, int pageIndex, int pageSize)
      {
         var response = await _itemService.SearchItemByAuthorForUser(itemDTOForSearchAuthor.ListAuthor, itemDTOForSearchAuthor.CollectionId, itemDTOForSearchAuthor.SortType, pageIndex, pageSize);
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response);
         }
      }
      [HttpPost("[action]/{pageIndex}/{pageSize}")]
      public async Task<IActionResult> SearchItemBySubjectKeyword(ItemDTOForSearchSubjectKeyword itemDTOForSearchSubjectKeyword, int pageIndex, int pageSize)
      {
         var response = await _itemService.SearchItemBySubjectKeyWordsForUser(itemDTOForSearchSubjectKeyword.ListSubjectKeyword, itemDTOForSearchSubjectKeyword.CollectionId, itemDTOForSearchSubjectKeyword.SortType, pageIndex, pageSize);
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response);
         }
      }
      [HttpPost("[action]/{pageIndex}/{pageSize}")]
      public async Task<IActionResult> SearchItemByTitle(ItemDTOForSearchTitle itemDTOForSearchTitle, int pageIndex, int pageSize)
      {
         var response = await _itemService.SearchItemByTitleForUser(itemDTOForSearchTitle.Title, itemDTOForSearchTitle.CollectionId, itemDTOForSearchTitle.SortType, pageIndex, pageSize);
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response);
         }
      }
      [HttpPost("[action]/{pageIndex}/{pageSize}")]
      public async Task<IActionResult> SearchItemByDate(ItemDTOForSearchDateIssue itemDTOForSearchDateIssue, int pageIndex, int pageSize)
      {
         var response = await _itemService.SearchItemByDateForUser(itemDTOForSearchDateIssue.Year, itemDTOForSearchDateIssue.Month, itemDTOForSearchDateIssue.Day, itemDTOForSearchDateIssue.CollectionId, itemDTOForSearchDateIssue.SortType, pageIndex, pageSize);
         if (response.IsSuccess)
         {
            return Ok(response.ObjectResponse);
         }
         else
         {
            return BadRequest(response);
         }
      }
   }
}
