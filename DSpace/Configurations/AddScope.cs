using Infrastructure.Repositories.AuthorRepositories;
using Infrastructure.Repositories.CollectionRepositories;
using Infrastructure.Repositories.Interfaces;
using Infrastructure.Services.Implements;
using Infrastructure.Repositories.CollectionGroupRepositories;
using Infrastructure.Repositories.CommunityGroupRepositories;
using Infrastructure.Repositories.CommunityRepositories;
using Infrastructure.Repositories.FileUploadRepositories;
using Infrastructure.Repositories.GroupPeopleRepositories;
using Infrastructure.Repositories.GroupRepositories;
using Infrastructure.Repositories.ItemRepositories;
using Infrastructure.Repositories.MetadataFieldRepositories;
using Infrastructure.Repositories.MetadataValueRepositories;
using Infrastructure.Repositories.PeopleRepositories;
using Infrastructure.Repositories.StatisticRepositories;
using Infrastructure.Repositories.SubscribeRepositories;
using Infrastructure.Repositories.UserRepositories;
using Infrastructure.Services;
using Infrastructure.Repositories.FileModifyAccessRepositories;

namespace DSpace.Configurations;
public static class AddScope
{
   public static IServiceCollection AddScopedCollection(this IServiceCollection serviceCollection)
   {
      //Repository
      serviceCollection.AddScoped(typeof(IRepositoryBase<>), typeof(RepositoryBase<>));
      serviceCollection.AddScoped(typeof(IUnitOfWork), typeof(UnitOfWork));
      serviceCollection.AddScoped<IAuthorRepository, AuthorRepository>();
      serviceCollection.AddScoped<IMetadataValueRepository, MetadataValueRepository>();
      
      serviceCollection.AddScoped<ICollectionRepository, CollectionRepository>();

      serviceCollection.AddScoped<ICommunityRepository, CommunityRepository>();
      serviceCollection.AddScoped<IFileModifyAccessRepository, FileModifyAccessRepository>();
      serviceCollection.AddScoped<IFileUploadRepository, FileUploadRepository>();
      serviceCollection.AddScoped<IGroupRepository, GroupRepository>();

      serviceCollection.AddScoped<IItemRepository, ItemRepository>();
      serviceCollection.AddScoped<IMetadataFieldRegistryRepository, MetadataFieldRegistryRepository>();

      serviceCollection.AddScoped<IPeopleRepository, PeopleRepository>();
      serviceCollection.AddScoped<IUserRepository, UserRepository>();
      serviceCollection.AddScoped<IStatisticRepository, StatisticRepository>();
      serviceCollection.AddScoped<ISubscribeRepository, SubscribeRepository>();
      serviceCollection.AddScoped<ICommunityGroupRepository, CommunityGroupRepository>();
      serviceCollection.AddScoped<ICollectionGroupRepository, CollectionGroupRepository>();
      serviceCollection.AddScoped<IGroupPeopleRepository, GroupPeopleRepository>();


      //SERVICE
      serviceCollection.AddScoped<IAuthorService, AuthorService>();


      serviceCollection.AddScoped<IItemService, ItemService>();
      serviceCollection.AddScoped<IFileUploadService, FileUploadService>();
      serviceCollection.AddScoped<IGroupPeopleService, GroupPeopleService>();
      serviceCollection.AddScoped<IEmailService, EmailService>();
      serviceCollection.AddScoped<ICommunityService, CommunityService>();

      serviceCollection.AddScoped<ICollectionService, CollectionService>();
      serviceCollection.AddScoped<IUserService, UserService>();
      serviceCollection.AddScoped<IMetadataFieldRegistryService, MetadataFieldRegistryService>();
      serviceCollection.AddScoped<IPeopleService, PeopleService>();
      serviceCollection.AddScoped<IGroupService, GroupService>();

      serviceCollection.AddScoped<IStatisticService, StatisticService>();
      serviceCollection.AddScoped<ISubscribeService, SubscribeService>();
      serviceCollection.AddScoped<ICommunityGroupService, CommunityGroupService>();
      serviceCollection.AddScoped<ICollectionGroupService, CollectionGroupService>();




      return serviceCollection;
   }
}