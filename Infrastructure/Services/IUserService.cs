﻿using Application.LoginRequest;
using Application.Responses;
using Domain;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Infrastructure.Services
{
   public interface IUserService
   {
      public User CreateInstanceUser();
      public Task<string> GetRole(User user);
      public Task<bool> AssignRole(User user,string role);

      public Task<bool> CreateUser(User user);

      public Task<User> getUserByEmail(string email);

      public List<Claim> getUserClaim(User user, string roles);


      public Task<User> getUserById(string id);

      public Task<bool> UpdateUser(User user);

      public string GenerateToken(List<Claim> claims);

      public Task<ResponseDTO> Login(LoginRequest loginRequest);





   }
}
