using Application.Items;
using Application.Metadatas;
using Application.Responses;
using Shared.Enums;

namespace Infrastructure.Services;

public interface IItemService
{
   //admin
    public Task<ResponseDTO> GetAllItem(int pageIndex,int pageSize, SortType sortType);
    public Task<ResponseDTO> GetAllItemInOneCollection(int collectionId, int pageIndex, int pageSize, SortType sortType);

   public Task<ResponseDTO> Get5ItemRecentlyInOneCollection(int collectionId);
   public Task<ResponseDTO> SearchItemByAll(string sreach,int pageIndex,int pageSize, SortType sortType);
   public Task<ResponseDTO> DeleteItem(int itemId);
   public Task<ResponseDTO> CreateSimpleItem(ItemDTOForCreateSimple itemDTO, int submmitterId);
   public Task<ResponseDTO> GetItemSimpleById(int itemId);
   public Task<ResponseDTO> GetItemFullById(int itemId);
   public Task<ResponseDTO> ModifyItem(MetadataValueDTOForModified metadataValueDTOForModified,int submmitter,int itemId);

   public Task<ResponseDTO> UpdateStatus(int itemId, bool discoverable, int peopleId);
   public Task<ResponseDTO> UpdateCollection(int itemId, int collectionId, int peopleId);
   public Task<ResponseDTO> SearchItem(ItemDtoForSearch itemDtoForSearch, int pageIndex, int pageSize);
   
   public Task<ResponseDTO> SearchItemSimple(ItemDTOForSearchSimple itemDTOForSearchSimple, int pageIndex, int pageSize);

   public Task<ResponseDTO> SearchItemByAuthor(List<string> listAuthor, int collectionId, bool? discoverable, SortType sortType,int pageIndex,int pageSize);
   public Task<ResponseDTO> SearchItemByTitle(string author,int collectionId, bool? discoverable, SortType sortType, int pageIndex, int pageSize);
   
   public Task<ResponseDTO> SearchItemBySubjectKeyWords(List<string> listSubjectKeywords, int collectionId, bool? discoverable, SortType sortType, int pageIndex, int pageSize);

   public Task<ResponseDTO> SearchItemByDate(string? year,string? month,string? date, int collectionId, bool? discoverable, SortType sortType, int pageIndex, int pageSize);





   // user

   public Task<ResponseDTO> GetAllItemByCollectionIdForUser(int collectionId, int pageIndex, int pageSize, SortType sortType);
   public Task<ResponseDTO> GetItemSimpleByIdForUser(int itemId);
   public Task<ResponseDTO> GetItemFullByIdForUser(int itemId);
   public Task<ResponseDTO> GetListItemRecentForUser(string userEmail);
   public Task<ResponseDTO> SearchItemForUser(ItemDTOForSearchForUser itemDtoForSearch, int pageIndex, int pageSize);

   public Task<ResponseDTO> SearchItemByAllForUser(string search,int pageIndex,int pageSize, SortType sortType);

   public Task<ResponseDTO> Get5ItemRecentlyInOneCollectionForUser(int collectionId);

   public Task<ResponseDTO> GetAllItemForUser(int pageIndex, int pageSize, SortType sortType);
   public Task<ResponseDTO> SearchItemSimpleForUser(ItemDTOForSearchSimpleForUser itemDTOForSearchSimple, int pageIndex, int pageSize);

   public Task<ResponseDTO> SearchItemByAuthorForUser(List<string> listAuthor, int collectionId, SortType sortType, int pageIndex, int pageSize);
   public Task<ResponseDTO> SearchItemByTitleForUser(string author, int collectionId, SortType sortType, int pageIndex, int pageSize);

   public Task<ResponseDTO> SearchItemBySubjectKeyWordsForUser(List<string> listSubjectKeywords, int collectionId, SortType sortType, int pageIndex, int pageSize);

   public Task<ResponseDTO> SearchItemByDateForUser(string? year, string? month, string? date, int collectionId, SortType sortType, int pageIndex, int pageSize);

   //staff
   public Task<ResponseDTO> GetAllItemInOneCollectionForStaff(int collectionId, int pageIndex, int pageSize, int peopleId, SortType sortType);
   public Task<ResponseDTO> GetAllItemForStaff(int peopleId, int pageIndex, int pageSize, SortType sortType);
   public Task<ResponseDTO> CreateSimpleItemForStaff(ItemDTOForCreateSimple itemDTO, int submmitterId);
   public Task<ResponseDTO> GetItemSimpleByIdForStaff(int itemId,int peopleId);
   public Task<ResponseDTO> GetItemFullByIdForStaff(int itemId,int peopleId);
   public Task<ResponseDTO> ModifyItemForStaff(MetadataValueDTOForModified metadataValueDTOForModified, int submmitter, int itemId);

   public Task<ResponseDTO> SearchItemForStaff(ItemDtoForSearch itemDtoForSearch, int peopleId, int pageIndex, int pageSize);

   public Task<ResponseDTO> SearchItemByAllForStaff(string title,int peopleId,int pageIndex,int pageSize, SortType sortType);

   public Task<ResponseDTO> SearchItemSimpleForStaff(ItemDTOForSearchSimple itemDTOForSearchSimple,int peopleId, int pageIndex, int pageSize);

   public Task<ResponseDTO> SearchFileContent(string fileContent,int pageIndex,int pageSize);

   public Task<ResponseDTO> SearchItemByAuthorForStaff(List<string> listAuthor, int collectionId, bool? discoverable, SortType sortType,int staffId, int pageIndex, int pageSize);
   public Task<ResponseDTO> SearchItemByTitleForStaff(string author, int collectionId, bool? discoverable, SortType sortType, int staffId, int pageIndex, int pageSize);

   public Task<ResponseDTO> SearchItemBySubjectKeyWordsForStaff(List<string> listSubjectKeywords, int collectionId, bool? discoverable, SortType sortType, int staffId, int pageIndex, int pageSize);

   public Task<ResponseDTO> SearchItemByDateForStaff(string? year, string? month, string? date, int collectionId, bool? discoverable, SortType sortType, int staffId, int pageIndex, int pageSize);
}