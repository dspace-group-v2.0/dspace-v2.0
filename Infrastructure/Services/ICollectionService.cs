﻿using Application.Collections;
using Application.Responses;

namespace Infrastructure.Services
{
   public interface ICollectionService
   {
      public Task<ResponseDTO> GetCollectionByName(string title);
      public Task<ResponseDTO> GetCollectionByID(int collectionId);

      public Task<ResponseDTO> GetAllCollection();
      public Task<ResponseDTO> CreateCollectionForStaff(CollectionDTOForCreateOrUpdate collectionDTO, int userCreateId);

      public Task<ResponseDTO> UpdateCollectionForStaff(int collectionId, CollectionDTOForCreateOrUpdate collectionDTO, int userUpdateId);

      public Task<ResponseDTO> DeleteCollectionForStaff(int collectionId, int userId);
      public Task<ResponseDTO> CreateCollection(CollectionDTOForCreateOrUpdate collectionDTO, int userCreateId);

      public Task<ResponseDTO> UpdateCollection(int collectionId, CollectionDTOForCreateOrUpdate collectionDTO, int userUpdateId);

      public Task<ResponseDTO> DeleteCollection(int collectionId);

      public Task<ResponseDTO> GetCollectionByNameForUser(string title,string email);
      public Task<ResponseDTO> GetCollectionByIDForUser(int collectionId,string email);

      public Task<ResponseDTO> GetAllCollectionForUser(string email);

      public Task<ResponseDTO> GetAllCollectionByStaff(int peopleId);
      public Task<ResponseDTO> GetCollectionByCollectionIdAndPeopleId(int collectionId,  int peopleId);

    

   }
}