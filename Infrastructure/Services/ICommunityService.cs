﻿using Application.Communities;
using Application.Responses;

namespace Infrastructure.Services
{
    public interface ICommunityService
    {
        //public Task<List<int>> GetAllSubCommunity(int communityId);
        public Task<ResponseDTO> GetCommunityByName(string title);
        public Task<ResponseDTO> GetCommunityByID(int communityId);

        public Task<ResponseDTO> GetAllCommunity();

        public Task<ResponseDTO> CreateCommunityForStaff(CommunityDTOForCreateOrUpdate communityDTO, int userCreateId);

        public Task<ResponseDTO> UpdateCommunityForStaff(int communityId, CommunityDTOForCreateOrUpdate communityDTO, int userUpdateId);

        public Task<ResponseDTO> DeleteCommunityForStaff(int communityId, int userId);
        public Task<ResponseDTO> CreateCommunity(CommunityDTOForCreateOrUpdate communityDTO, int userCreateId);

        public Task<ResponseDTO> UpdateCommunity(int communityId, CommunityDTOForCreateOrUpdate communityDTO, int userUpdateId);

        public Task<ResponseDTO> DeleteCommunity(int communityId);



        public Task<ResponseDTO> GetAllCommunityForUser();



        public Task<ResponseDTO> GetCommunityByNameForUser(string title);
        public Task<ResponseDTO> GetCommunityByIDForUser(int communityId, string email);

        public Task<ResponseDTO> GetCommunityByStaff(int peopleId);

        public Task<ResponseDTO> GetAllCommunityParent();

        public Task<ResponseDTO> GetAllCommunityTree();
        public Task<ResponseDTO> GetAllCommunityTreeForUser();

        public Task<ResponseDTO> GetCommunityByIDForStaff(int communityId, int peopleId);



    }
}