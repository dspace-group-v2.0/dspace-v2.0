using Application.Authors;
using Application.FileUploads;
using Application.Items;
using Application.Metadatas;
using Application.Responses;
using AutoMapper;
using Domain;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using Infrastructure.Repositories.AuthorRepositories;
using Infrastructure.Repositories.CollectionGroupRepositories;
using Infrastructure.Repositories.CollectionRepositories;
using Infrastructure.Repositories.CommunityGroupRepositories;
using Infrastructure.Repositories.FileModifyAccessRepositories;
using Infrastructure.Repositories.FileUploadRepositories;
using Infrastructure.Repositories.GroupPeopleRepositories;
using Infrastructure.Repositories.ItemRepositories;
using Infrastructure.Repositories.MetadataValueRepositories;
using Infrastructure.Repositories.StatisticRepositories;
using Infrastructure.Repositories.SubscribeRepositories;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas.Parser;
using iText.Kernel.Pdf.Canvas.Parser.Listener;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Shared.Constants;
using Shared.Enums;
using System.Globalization;
using System.Text;
using Infrastructure.Repositories.UserRepositories;
using File = Google.Apis.Drive.v3.Data.File;
using Path = System.IO.Path;

namespace Infrastructure.Services.Implements;

public class ItemService : IItemService
{
    private readonly IMapper _mapper;
    protected ResponseDTO _response;
    private ICollectionRepository _collectionRepository;
    private IMetadataValueRepository _metadataValueRepository;
    private ICollectionGroupRepository _collectionGroupRepository;
    private IGroupPeopleRepository _groupPeopleRepository;
    private IStatisticRepository _statisticRepository;
    private ISubscribeRepository _subscribeRepository;
    private IEmailService _emailService;
    private IAuthorRepository _authorRepository;
    private ICommunityGroupRepository _communityGroupRepository;
    private IFileModifyAccessRepository _fileModifyAccessRepository;
    private IUserRepository _userRepository;
    private IFileUploadRepository _fileUploadRepository;
    private IItemRepository _itemRepository;
    private static readonly string[] Scopes = new[] { DriveService.Scope.DriveFile };

    public ItemService(
        IMapper mapper,
        IMetadataValueRepository metadataValueRepository,
        IGroupPeopleRepository groupPeopleRepository,
        ICollectionGroupRepository collectionGroupRepository,
        ICollectionRepository collectionRepository,
        IStatisticRepository statisticRepository,
        ISubscribeRepository subscribeRepository,
        IEmailService emailService,
        IAuthorRepository authorRepository,
        ICommunityGroupRepository communityGroupRepository,
        IFileModifyAccessRepository fileModifyAccessRepository,
        IItemRepository itemRepository,
        IFileUploadRepository fileUploadRepository,
        IUserRepository userRepository
    )
    {
        _mapper = mapper;
        _response = new ResponseDTO();
        _metadataValueRepository = metadataValueRepository;
        _groupPeopleRepository = groupPeopleRepository;
        _collectionGroupRepository = collectionGroupRepository;
        _collectionRepository = collectionRepository;
        _statisticRepository = statisticRepository;
        _subscribeRepository = subscribeRepository;
        _emailService = emailService;
        _authorRepository = authorRepository;
        _communityGroupRepository = communityGroupRepository;
        _fileModifyAccessRepository = fileModifyAccessRepository;
        _authorRepository = authorRepository;
        _itemRepository = itemRepository;
        _fileUploadRepository = fileUploadRepository;
        _userRepository = userRepository;
    }

    private void SortBy(ref IQueryable<Item> queryItem, SortType sortType)
    {
        if (sortType == SortType.TitleAscending)
        {
            queryItem = queryItem.OrderBy(x => x.MetadataValue.FirstOrDefault(y => y.MetadataFieldId == 57).TextValue);
        }
        else if (sortType == SortType.TitleDescending)
        {
            queryItem = queryItem.OrderByDescending(x =>
                x.MetadataValue.FirstOrDefault(y => y.MetadataFieldId == 57).TextValue);
        }
        else if (sortType == SortType.DateIssueAscending)
        {
            queryItem = queryItem.Where(x => x.MetadataValue.FirstOrDefault(y => y.MetadataFieldId == 12) != null).OrderBy(x => x.MetadataValue.FirstOrDefault(y => y.MetadataFieldId == 12).TextValue);
        }
        else
        {
            queryItem = queryItem.Where(x => x.MetadataValue.FirstOrDefault(y => y.MetadataFieldId == 12) != null).OrderByDescending(x =>
                x.MetadataValue.FirstOrDefault(y => y.MetadataFieldId == 12).TextValue);
        }
    }


    private async Task<List<ItemDTOForSelectList>> GetAuthorItem(List<ItemDTOForSelectList> objmap)
    {
        try
        {
            foreach (var obj in objmap)
            {
                obj.AuthorItems = new List<AuthorDTOForSelectItem>();
                foreach (var authorId in obj.Authors)
                {
                    int authorid = int.Parse(authorId);
                    var author = await _authorRepository.GetAuthor(authorid);
                    var authorMap = _mapper.Map<AuthorDTOForSelectItem>(author);
                    obj.AuthorItems.Add(authorMap);
                }
            }

            return objmap;
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return null;
        }
    }

    private async Task<List<ItemDTOForSelectListForStaff>> GetAuthorItemForStaff(
        List<ItemDTOForSelectListForStaff> objmap)
    {
        try
        {
            foreach (var obj in objmap)
            {
                obj.AuthorItems = new List<AuthorDTOForSelectItem>();
                foreach (var authorId in obj.Authors)
                {
                    int authorid = int.Parse(authorId);
                    var author = await _authorRepository.GetAuthor(authorid);
                    var authorMap = _mapper.Map<AuthorDTOForSelectItem>(author);
                    obj.AuthorItems.Add(authorMap);
                }
            }

            return objmap;
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return null;
        }
    }

    private async Task<List<ItemDTOForSelectListForUser>> GetAuthorItemForUser(List<ItemDTOForSelectListForUser> objmap)
    {
        foreach (var obj in objmap)
        {
            obj.AuthorItems = new List<AuthorDTOForSelectItem>();
            foreach (var authorId in obj.Authors)
            {
                int authorid = int.Parse(authorId);
                var author = await _authorRepository.GetAuthor(authorid);
                var authorMap = _mapper.Map<AuthorDTOForSelectItem>(author);
                obj.AuthorItems.Add(authorMap);
            }
        }

        return objmap;
    }

    private void SearchItemByAuthor(ref IQueryable<Item> queryItem, List<string> listAuthor)
    {
        if (listAuthor.Count() > 0)
        {
            foreach (var author in listAuthor)
            {
                queryItem = queryItem.Where(x =>
                    x.MetadataValue.Where(x => x.MetadataFieldId == 1 || x.MetadataFieldId == 2)
                        .Select(x => x.TextValue).Contains(author));
            }
        }
    }

    private void SearchItemBySubjectKeyword(ref IQueryable<Item> queryItem, List<string> listSubjectKeyword)
    {
        if (listSubjectKeyword.Count() > 0)
        {
            foreach (var subjectKeyword in listSubjectKeyword)
            {
                queryItem = queryItem.Where(x =>
                    x.MetadataValue.Where(x => x.MetadataFieldId == 51).Select(x => x.TextValue)
                        .Any(y => y.Contains(subjectKeyword)));
            }
        }
    }

    private void SearchItemByTitle(ref IQueryable<Item> queryItem, string title)
    {
        queryItem = queryItem.Where(x =>
            x.MetadataValue.Where(x => x.MetadataFieldId == 57).Select(x => x.TextValue.Replace(" ", ""))
                .Any(y => y.Contains(title)));
    }

    private void SearchItemByYearOfDateIssue(ref IQueryable<Item> queryItem, string year)
    {
        queryItem = queryItem.Where(x =>
            x.MetadataValue.FirstOrDefault(x => x.MetadataFieldId == 12).TextValue.StartsWith(year));
    }

    private void SearchItemByMonthOfDateIssue(ref IQueryable<Item> queryItem, string month)
    {
        queryItem = queryItem.Where(x =>
            (x.MetadataValue.FirstOrDefault(x => x.MetadataFieldId == 12).TextValue.Contains("-" + month + "-") &&
             x.MetadataValue.FirstOrDefault(x => x.MetadataFieldId == 12).TextValue.Length == 10) || 
            (x.MetadataValue.FirstOrDefault(x => x.MetadataFieldId == 12).TextValue.Contains("-" + month) &&
             x.MetadataValue.FirstOrDefault(x => x.MetadataFieldId == 12).TextValue.Length == 7));
    }

    private void SearchItemByDayOfDateIssue(ref IQueryable<Item> queryItem, string day)
    {
        queryItem = queryItem.Where(x =>
            x.MetadataValue.FirstOrDefault(x => x.MetadataFieldId == 12).TextValue.EndsWith("-" + day) &&
            x.MetadataValue.FirstOrDefault(x => x.MetadataFieldId == 12).TextValue.Length == 10);
    }

    private void SearchItemByDiscoverable(ref IQueryable<Item> queryItem, bool discoverable)
    {
        queryItem = queryItem.Where(x => x.Discoverable);
    }

    private void SearchItemByCollectionId(ref IQueryable<Item> queryItem, int collectionId)
    {
        queryItem = queryItem.Where(x => x.CollectionId == collectionId);
    }

    private IQueryable<Item> GetQueryItemForUser()
    {
        IQueryable<Item> queryItem = _itemRepository.GetQueryAll()
            .Include(x => x.MetadataValue)
            .Include(x => x.Collection)
            .ThenInclude(x => x.Community)
            .Include(x => x.File)
            .ThenInclude(x => x.FileAccess)
            .Where(x => x.Discoverable);
        return queryItem;
    }

    private IQueryable<Item> GetQueryItemForAdmin()
    {
        IQueryable<Item> queryItem = _itemRepository.GetQueryAll()
            .Include(x => x.MetadataValue)
            .Include(x => x.Collection);
        return queryItem;
    }

    private async Task<IQueryable<Item>> GetQueryItemForStaff(int peopleId)
    {
        List<int> listCollectionId = await GetAllCollectForStaffId(peopleId);
        IQueryable<Item> queryItem = _itemRepository.GetQueryAll()
            .Where(x => listCollectionId.Any(c => c == x.CollectionId))
            .Include(x => x.MetadataValue)
            .Include(x => x.Collection);
        return queryItem;
    }

    private async Task<List<int>> GetAllCollectForStaffId(int peopleId)
    {
        List<int> listCollectionId = await _collectionGroupRepository.GetListCollectionIdByStaffId(peopleId);
        List<int> listCollectionInCommunityId = await _collectionGroupRepository.GetListCollectionIdByStaffId(peopleId);
        if (listCollectionInCommunityId.Count() > 0)
        {
            listCollectionId.AddRange(listCollectionInCommunityId);
        }

        return listCollectionId.Distinct().ToList();
    }

    private void PagingItem(ref IQueryable<Item> queryItem, int pageIndex, int pageSize)
    {
        queryItem = queryItem.Skip(pageSize * (pageIndex - 1)).Take(pageSize);
    }

    public async Task<ResponseDTO> GetAllItemInOneCollection(int collectionId, int pageIndex, int pageSize,
        SortType sortType)
    {
        try
        {
            var queryItem = GetQueryItemForAdmin();
            SearchItemByCollectionId(ref queryItem, collectionId);
            int totalAmount = queryItem.Count();
            SortBy(ref queryItem, sortType);
            PagingItem(ref queryItem, pageIndex, pageSize);
            var objList = await queryItem.ToListAsync();
            var objmap = _mapper.Map<List<ItemDTOForSelectList>>(objList);
            decimal totalPage = Math.Ceiling((decimal)totalAmount / pageSize);
            objmap = await GetAuthorItem(objmap);
            var result = new
            {
                totalAmount,
                pageIndex,
                pageSize,
                totalPage,
                objmap
            };
            _response.IsSuccess = true;
            _response.ObjectResponse = result;
        }
        catch (Exception ex)
        {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
        }

        return _response;
    }


    public async Task<ResponseDTO> GetAllItem(int pageIndex, int pageSize, SortType sortType)
    {
        try
        {
            IQueryable<Item> queryItem = GetQueryItemForAdmin();
            int totalAmount = queryItem.Count();
            SortBy(ref queryItem, sortType);
            PagingItem(ref queryItem, pageIndex, pageSize);

            var objList = await queryItem.ToListAsync();
            var objmap = _mapper.Map<List<ItemDTOForSelectList>>(objList);
            objmap = await GetAuthorItem(objmap);
            decimal totalPage = Math.Ceiling((decimal)totalAmount / pageSize);

            var result = new
            {
                totalAmount,
                pageIndex,
                pageSize,
                totalPage,
                objmap
            };
            _response.IsSuccess = true;
            _response.ObjectResponse = result;
        }
        catch (Exception ex)
        {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
        }

        return _response;
    }

    public async Task<ResponseDTO> DeleteItem(int itemId)
    {
        try
        {
            var item = await _itemRepository.GetQueryAll()
                .Include(x => x.File)
                .ThenInclude(x => x.FileAccess)
                .Include(x => x.MetadataValue)
                .SingleOrDefaultAsync(x => x.ItemId.Equals(itemId));
            if (item == null)
            {
                _response.IsSuccess = true;
                _response.Message = "Item does not exist";
            }
            else
            {
                await _metadataValueRepository.DeleteListAsync(item.MetadataValue);

                foreach (var fileUpload in item.File)
                {
                    await _fileModifyAccessRepository.DeleteListAsync(fileUpload.FileAccess);
                }

                await _fileUploadRepository.DeleteListAsync(item.File);
                await _itemRepository.DeleteAsync(item);
                _response.IsSuccess = true;
                _response.Message = "Delete Item success";
            }
        }
        catch (Exception ex)
        {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
        }

        return _response;
    }

    public async Task<UserCredential> GetUserCredential()
    {
        UserCredential credential = null;
        using (var stream =
               new FileStream(
                   "client_secret_336699035226-8c0s3uv2pee71adqarpuiicagplfpkvj.apps.googleusercontent.com.json",
                   FileMode.Open, FileAccess.ReadWrite))
        {
            credential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
                GoogleClientSecrets.FromStream(stream).Secrets,
                Scopes,
                "user",
                CancellationToken.None,
                new FileDataStore("DSpace v2.0", true));
        }

        return credential;
    }

    private async Task<bool> UploadFile(IList<IFormFile> multipleFile, Task<UserCredential> credential, Item item,
        string accessFile)
    {
        try
        {
            var collection = await _collectionRepository.GetCollection(item.CollectionId);
            if (collection == null)
            {
                return false;
            }

            List<FileUpload> fileUploads = new List<FileUpload>();
            var listEmail = await _userRepository.GetListEmailRegister();
            DriveService service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = await credential
            });
            foreach (IFormFile oneFileOnly in multipleFile)
            {
                var insertFile = service.Files.Create(
                    new Google.Apis.Drive.v3.Data.File
                    {
                        Name = oneFileOnly.FileName,
                        Parents = new List<string>()
                        {
                            collection.FolderId
                        }
                    },
                    oneFileOnly.OpenReadStream(), oneFileOnly.ContentType);
                insertFile.ChunkSize = Google.Apis.Drive.v3.FilesResource.CreateMediaUpload.MinimumChunkSize * 3;
                insertFile.Body.CopyRequiresWriterPermission = true;
                await insertFile.UploadAsync();

                foreach (var email in listEmail)
                {
                    var permission = new Google.Apis.Drive.v3.Data.Permission()
                    {
                        Type = "user",
                        Role = "reader",
                        EmailAddress = email
                    };
                    var requestPermission = service.Permissions.Create(permission, insertFile.ResponseBody.Id);
                    requestPermission.Fields = "id";
                    requestPermission.SendNotificationEmail = false;
                    await requestPermission.ExecuteAsync();
                }

                var fileUpload = new ModifyFileUploadDto()
                {
                    FileUrl = Path.Combine(Path.GetTempPath(), insertFile.ResponseBody.Name),
                    FileKeyId = insertFile.ResponseBody.Id,
                    MimeType = insertFile.ResponseBody.MimeType,
                    Kind = insertFile.ResponseBody.Kind,
                    FileName = insertFile.ResponseBody.Name,
                    CreationTime = DateTime.Now,
                    isActive = true,
                    ItemId = item.ItemId
                };
                try
                {
                    if (insertFile.ResponseBody.MimeType.Equals("application/pdf"))
                    {
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            await using (StreamWriter streamWriter = new StreamWriter(memoryStream, leaveOpen: true))
                            {
                                iText.IO.Util.ResourceUtil.AddToResourceSearch("itext.font_asian.dll");
                                using (PdfReader reader = new PdfReader(oneFileOnly.OpenReadStream()))
                                {
                                    PdfDocument srcDoc = new PdfDocument(reader);
                                    for (int i = 1; i <= srcDoc.GetNumberOfPages(); i++)
                                    {
                                        var page = srcDoc.GetPage(i);
                                        string pageText =
                                            PdfTextExtractor.GetTextFromPage(page, new SimpleTextExtractionStrategy());
                                        streamWriter.WriteLine(pageText);
                                        streamWriter.Flush();
                                    }
                                }
                            }

                            var requestTextGenerated = service.Files.Create(
                                new Google.Apis.Drive.v3.Data.File
                                {
                                    Name = "temp.txt",
                                    Parents = new List<string>()
                                    {
                                        "1a86_QB7vETCmIG-Qqidf4IPT7FPJ_car"
                                    },
                                    MimeType = "text/plain",
                                },
                                memoryStream, "text/plain");
                            requestTextGenerated.Fields = "id";
                            await requestTextGenerated.UploadAsync();
                            var uploadTextGenerated = requestTextGenerated.ResponseBody;
                            var updateMetadata = new Google.Apis.Drive.v3.Data.File()
                            {
                                Name = fileUpload.FileKeyId
                            };
                            var updateRequest = service.Files.Update(updateMetadata, uploadTextGenerated.Id);
                            updateRequest.Fields = "id, name";
                            await updateRequest.ExecuteAsync();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Unknown PdfException");
                }

                var file = _mapper.Map<FileUpload>(fileUpload);
                fileUploads.Add(file);
            }

            if (fileUploads.Count > 0)
            {
                await _fileUploadRepository.AddListAsync(fileUploads);
                foreach (var fileUpload in fileUploads)
                {
                    switch (accessFile)
                    {
                        case "ALL":
                            await _fileModifyAccessRepository.AddAsync(new FileModifyAccess()
                            {
                                AccessType = FileAccessType.View,
                                FileId = fileUpload.FileId,
                                Role = "ALL",
                            });
                            break;
                        case "LECTURER":
                            await _fileModifyAccessRepository.AddAsync(new FileModifyAccess()
                            {
                                AccessType = FileAccessType.View,
                                FileId = fileUpload.FileId,
                                Role = "LECTURER",
                            });
                            break;
                        case "STUDENT":
                            await _fileModifyAccessRepository.AddAsync(new FileModifyAccess()
                            {
                                AccessType = FileAccessType.View,
                                FileId = fileUpload.FileId,
                                Role = "STUDENT",
                            });
                            break;
                    }
                }

                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

    private Stream StringToStream(string text)
    {
        byte[] byteArray = Encoding.UTF8.GetBytes(text);
        MemoryStream stream = new MemoryStream(byteArray);
        return stream;
    }

    public async Task<ResponseDTO> CreateSimpleItem(ItemDTOForCreateSimple itemDTO, int submitterId)
    {
        try
        {
            Item itemNew = new Item();
            itemNew.LastModified = DateTime.Now;
            itemNew.Discoverable = itemDTO.Discoverable;
            itemNew.SubmitterId = submitterId;
            itemNew.CollectionId = itemDTO.CollectionId;

            await _itemRepository.AddAsync(itemNew);
            foreach (var metadata in itemDTO.metadataValueDTO)
            {
                metadata.ItemId = itemNew.ItemId;
                if (metadata.MetadataFieldId == 1 || metadata.MetadataFieldId == 2)
                {
                    if (!Int32.TryParse(metadata.TextValue, out _))
                    {
                        var valueAuthor = metadata.TextValue.Split("_");
                        var newAuthor = new Author()
                        {
                            FullName = valueAuthor[1],
                            JobTitle = valueAuthor[2],
                            DateAccessioned = DateTime.Now,
                            DateAvailable = DateTime.Now,
                            Type = ""
                        };
                        await _authorRepository.AddAsync(newAuthor);

                        newAuthor.Uri = valueAuthor[0] + "/Dspace/Author/AuthorDetail/" + newAuthor.AuthorId;
                        await _authorRepository.UpdateAsync(newAuthor);

                        metadata.TextValue = newAuthor.AuthorId.ToString();
                    }
                }
            }

            List<MetadataValue> metadataValues = new List<MetadataValue>();
            foreach (var metadata in itemDTO.metadataValueDTO)
            {
                var metadataValue = new MetadataValue();
                metadataValue.MetadataFieldId = metadata.MetadataFieldId;

                metadataValue.TextValue = metadata.TextValue;
                metadataValue.TextLang = metadata.TextLang;
                metadataValue.ItemId = itemNew.ItemId;
                metadataValues.Add(metadataValue);
            }

            await _metadataValueRepository.AddListAsync(metadataValues);
            bool result = await UploadFile(itemDTO.multipleFiles, GetUserCredential(), itemNew, itemDTO.RoleFile);
            if (result == true)
            {
            if (itemDTO.Discoverable)
            {
               var listEmailSubcribed = await _subscribeRepository.GetListUserEmailSubcribeCollection(itemDTO.CollectionId);
               var collection = await _collectionRepository.GetCollection(itemDTO.CollectionId);
               string emailBody = "The manager has just added a new item <b>" + itemDTO.metadataValueDTO
                         .SingleOrDefault(x => x.MetadataFieldId == 57).TextValue +
                         "</b> to the collection <b>" +
                         collection.CollectionName +
                         "</b>. To disable notifications for new item updates in this collection, " +
                         "go to detail collection of <b>" + collection.CollectionName + "</b> then click Unsubscribe. " +
                         "Thanks for reading.";
               if (listEmailSubcribed.Count() > 0)
               {
                  _emailService.SendEmailBcc(listEmailSubcribed.ToList(),
                      EmailConstants.EMAIL_CREATE_NEW_ITEM_SUBJECT,
                      new MimeKit.TextPart(MimeKit.Text.TextFormat.Html)
                      {
                         Text = emailBody

                      });
               }
            }
            _response.Message = "Add new item and upload successful";
                _response.IsSuccess = true;
                _response.ObjectResponse = itemNew;
            }
            else
            {
                _response.Message = "Encounter an error when add new item";
                _response.IsSuccess = false;
            }
        }
        catch (Exception ex)
        {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
        }

        return _response;
    }

    public async Task<ResponseDTO> GetItemSimpleById(int itemId)
    {
        try
        {
            var item = await _itemRepository.GetQueryAll()
                .Include(x => x.MetadataValue)
                .Include(x => x.Collection)
                .ThenInclude(x => x.Community)
                .Include(x => x.File)
                .Include(x => x.People)
                .ThenInclude(x => x.User)
                .SingleOrDefaultAsync(x => x.ItemId == itemId);
            if (item == null)
            {
                _response.IsSuccess = true;
                _response.Message = "Not found Item";
            }
            else
            {
                ItemDTOForSelectDetailSimple objmap = _mapper.Map<ItemDTOForSelectDetailSimple>(item);
                objmap.AuthorItems = new List<AuthorDTOForSelectItem>();
                objmap.CommunityId = (await _collectionRepository.GetCollection(item.CollectionId)).CommunityId;
                foreach (var authorId in objmap.Authors)
                {
                    int authorid = int.Parse(authorId);
                    var author = await _authorRepository.GetAuthor(authorid);
                    var authorMap = _mapper.Map<AuthorDTOForSelectItem>(author);
                    objmap.AuthorItems.Add(authorMap);
                }

                _response.IsSuccess = true;
                _response.ObjectResponse = objmap;
            }
        }
        catch (Exception ex)
        {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
        }

        return _response;
    }

    public async Task<ResponseDTO> ModifyItem(MetadataValueDTOForModified metadataValueDTOForModified, int submitterId,
        int itemId)
    {
        try
        {
            var item = await _itemRepository.GetQueryAll().SingleOrDefaultAsync(x => x.ItemId == itemId);
            if (item == null)
            {
                throw new Exception("Not found item");
            }

            item.SubmitterId = submitterId;
            item.LastModified = DateTime.Now;
            if (metadataValueDTOForModified.Add != null)
            {
                List<MetadataValue> listCreate = _mapper.Map<List<MetadataValue>>(metadataValueDTOForModified.Add);
                foreach (var metadata in listCreate)
                {
                    if (metadata.MetadataFieldId == 1 || metadata.MetadataFieldId == 2)
                    {
                        if (!Int32.TryParse(metadata.TextValue.Split("_")[1], out _))
                        {
                            var valueAuthor = metadata.TextValue.Split("_");
                            var newAuthor = new Author()
                            {
                                FullName = valueAuthor[1],
                                JobTitle = "",
                                DateAccessioned = DateTime.Now,
                                DateAvailable = DateTime.Now,
                                Type = "",
                            };
                            await _authorRepository.AddAsync(newAuthor);
                            newAuthor.Uri = valueAuthor[0] + "/Dspace/Author/AuthorDetail/" + newAuthor.AuthorId;
                            await _authorRepository.UpdateAsync(newAuthor);
                            metadata.TextValue = newAuthor.AuthorId.ToString();
                        }
                        else if (Int32.TryParse(metadata.TextValue.Split("_")[1], out _) &&
                                 _authorRepository.GetAuthor(Int32.Parse(metadata.TextValue.Split("_")[1])) == null)
                        {
                            throw new Exception("Not found this author, please try another ID");
                        }
                        else if (Int32.TryParse(metadata.TextValue.Split("_")[1], out _) &&
                                 _authorRepository.GetAuthor(Int32.Parse(metadata.TextValue.Split("_")[1])) != null)
                        {
                            metadata.TextValue = metadata.TextValue.Split("_")[1];
                        }
                    }
                }

                var responseAdd = await _metadataValueRepository.AddListAsync(listCreate);
                if (!responseAdd)
                {
                    throw new Exception("Add failed");
                }
            }

            if (metadataValueDTOForModified.Update != null && metadataValueDTOForModified.Update.Count > 0)
            {
                List<MetadataValue> list = _mapper.Map<List<MetadataValue>>(metadataValueDTOForModified.Update);

                foreach (var metadataValueUpdate in list)
                {
                    if (metadataValueUpdate.MetadataFieldId == 1 || metadataValueUpdate.MetadataFieldId == 2)
                    {
                        if (!Int32.TryParse(metadataValueUpdate.TextValue.Split("_")[1], out _))
                        {
                            var valueAuthor = metadataValueUpdate.TextValue.Split("_");
                            var newAuthor = new Author()
                            {
                                FullName = valueAuthor[1],
                                JobTitle = "",
                                DateAccessioned = DateTime.Now,
                                DateAvailable = DateTime.Now,
                                Type = "",
                            };
                            await _authorRepository.AddAsync(newAuthor);
                            newAuthor.Uri = valueAuthor[0] + "/Dspace/Author/AuthorDetail/" + newAuthor.AuthorId;
                            await _authorRepository.UpdateAsync(newAuthor);
                            metadataValueUpdate.TextValue = newAuthor.AuthorId.ToString();
                        }
                        else if (Int32.TryParse(metadataValueUpdate.TextValue.Split("_")[1], out _) &&
                                 _authorRepository.GetAuthor(
                                     Int32.Parse(metadataValueUpdate.TextValue.Split("_")[1])) == null)
                        {
                            throw new Exception("Not found this author, please try another ID");
                        }
                        else if (Int32.TryParse(metadataValueUpdate.TextValue.Split("_")[1], out _) &&
                                 _authorRepository.GetAuthor(
                                     Int32.Parse(metadataValueUpdate.TextValue.Split("_")[1])) != null)
                        {
                            metadataValueUpdate.TextValue = metadataValueUpdate.TextValue.Split("_")[1];
                        }
                    }

                    var metadataValue =
                        await _metadataValueRepository.GetMetadataValue(metadataValueUpdate.MetadataValueId);
                    if (metadataValue != null)
                    {
                        metadataValue.TextValue = metadataValueUpdate.TextValue;
                        metadataValue.TextLang = metadataValueUpdate.TextLang;
                        metadataValue.MetadataFieldRegistry = metadataValueUpdate.MetadataFieldRegistry;
                    }
                }

                var responseUpdate = await _metadataValueRepository.UpdateListAsync(list);
                if (!responseUpdate)
                {
                    throw new Exception("Update failed");
                }
            }

            if (metadataValueDTOForModified.Delete != null && metadataValueDTOForModified.Delete.Count > 0)
            {
                List<MetadataValue> list = _mapper.Map<List<MetadataValue>>(metadataValueDTOForModified.Delete);
                var responseDelete = await _metadataValueRepository.DeleteListAsync(list);
                if (!responseDelete)
                {
                    throw new Exception("Delete failed");
                }
            }

            _response.IsSuccess = true;
            _response.Message = "Update item successful";
        }
        catch (Exception ex)
        {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
        }

        return _response;
    }

    public async Task<ResponseDTO> GetItemFullById(int itemId)
    {
        try
        {
            var item = await _itemRepository.GetQueryAll()
                .Include(x => x.MetadataValue)
                .ThenInclude(x => x.MetadataFieldRegistry)
                .Include(c => c.Collection)
                .ThenInclude(c => c.Community)
                .Include(f => f.File)
                .ThenInclude(f => f.FileAccess)
                .FirstOrDefaultAsync(x => x.ItemId == itemId);
            if (item != null)
            {
                var objectResponse = _mapper.Map<ItemDTOForSelectDetailFull>(item);
                objectResponse.ListAuthorDtoForSelect = new List<AuthorDTOForSelectItem>();
                objectResponse.ListAdvisorDtoForSelect = new List<AuthorDTOForSelectItem>();

                foreach (var author in objectResponse.ListMetadataValueDTOForSelect
                             .Where(x => x.MetadataFieldId == 1)
                             .Select(y => y.TextValue))
                {
                    if (author != null)
                    {
                        var authorDto = await _authorRepository.GetQueryAll().Select(x => new AuthorDTOForSelectItem
                        {
                            AuthorId = x.AuthorId,
                            FullName = x.FullName
                        }).SingleOrDefaultAsync(x => x.AuthorId == int.Parse(author));
                        if (authorDto != null)
                        {
                            objectResponse.ListAuthorDtoForSelect.Add(authorDto);
                        }
                    }
                }

                foreach (var advisor in objectResponse.ListMetadataValueDTOForSelect
                             .Where(x => x.MetadataFieldId == 2)
                             .Select(y => y.TextValue))
                {
                    if (advisor != null)
                    {
                        var advisorDto = await _authorRepository.GetQueryAll().Select(x => new AuthorDTOForSelectItem
                        {
                            AuthorId = x.AuthorId,
                            FullName = x.FullName
                        }).SingleOrDefaultAsync(x => x.AuthorId == int.Parse(advisor));
                        if (advisorDto != null)
                        {
                            objectResponse.ListAdvisorDtoForSelect.Add(advisorDto);
                        }
                    }
                }

                _response.IsSuccess = true;
                _response.Message = "Find item Success";
                _response.ObjectResponse = objectResponse;
            }
            else
            {
                _response.IsSuccess = false;
                _response.Message = "Not found item";
            }
        }
        catch (Exception ex)
        {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
        }

        return _response;
    }

    public async Task<ResponseDTO> SearchItemSimple(ItemDTOForSearchSimple itemDTOForSearchSimple, int pageIndex,
        int pageSize)
    {
        try
        {
            IQueryable<Item> queryItem = GetQueryItemForAdmin();
            if (itemDTOForSearchSimple.CollectionId != null && itemDTOForSearchSimple.CollectionId > 0)
            {
                queryItem = queryItem.Where(x => x.CollectionId == itemDTOForSearchSimple.CollectionId);
            }

            if (itemDTOForSearchSimple.Discoverable != null)
            {
                queryItem = queryItem.Where(x => x.Discoverable == itemDTOForSearchSimple.Discoverable);
            }

            if (itemDTOForSearchSimple.Title != null && itemDTOForSearchSimple.Title != string.Empty)
            {
                queryItem = queryItem.Where(x =>
                    x.MetadataValue.SingleOrDefault(x => x.MetadataFieldId == 57).TextValue.Replace(" ", "")
                        .Contains(itemDTOForSearchSimple.Title));
            }

            if (itemDTOForSearchSimple.Authors != null)
            {
                if (itemDTOForSearchSimple.Authors.Count() > 0)
                {
                    foreach (var author in itemDTOForSearchSimple.Authors)
                    {
                        queryItem = queryItem.Where(x =>
                            x.MetadataValue.Where(x => x.MetadataFieldId == 1).Select(x => x.TextValue)
                                .Any(y => y.Contains(author)));
                    }
                }
            }

            if (itemDTOForSearchSimple.SubjectKeywords != null)
            {
                if (itemDTOForSearchSimple.SubjectKeywords.Count() > 0)
                {
                    foreach (var subjectKeyword in itemDTOForSearchSimple.SubjectKeywords)
                    {
                        queryItem = queryItem.Where(x =>
                            x.MetadataValue.Where(x => x.MetadataFieldId == 51).Select(x => x.TextValue)
                                .Any(y => y.Contains(subjectKeyword)));
                        ;
                    }
                }
            }

            if (itemDTOForSearchSimple.Year != null && itemDTOForSearchSimple.Year != string.Empty)
            {
                queryItem = queryItem.Where(x =>
                    x.MetadataValue.SingleOrDefault(x => x.MetadataFieldId == 12).TextValue
                        .StartsWith(itemDTOForSearchSimple.Year));
            }

            if (itemDTOForSearchSimple.Month != null && itemDTOForSearchSimple.Month != string.Empty)
            {
                queryItem = queryItem.Where(x =>
                    x.MetadataValue.SingleOrDefault(x => x.MetadataFieldId == 12).TextValue
                        .Contains("-" + itemDTOForSearchSimple.Month + "-") &&
                    x.MetadataValue.SingleOrDefault(x => x.MetadataFieldId == 12).TextValue.Length >= 7);
            }

            if (itemDTOForSearchSimple.Day != null && itemDTOForSearchSimple.Day != string.Empty)
            {
                queryItem = queryItem.Where(x =>
                    x.MetadataValue.SingleOrDefault(x => x.MetadataFieldId == 12).TextValue
                        .EndsWith("-" + itemDTOForSearchSimple.Day) &&
                    x.MetadataValue.SingleOrDefault(x => x.MetadataFieldId == 12).TextValue.Length == 10);
            }

            int totalAmount = queryItem.Count();
            SortBy(ref queryItem, itemDTOForSearchSimple.SortType);
            PagingItem(ref queryItem, pageIndex, pageSize);
            var objList = await queryItem.ToListAsync();
            var objmap = _mapper.Map<List<ItemDTOForSelectList>>(objList);
            objmap = await GetAuthorItem(objmap);
            decimal totalPage = Math.Ceiling((decimal)totalAmount / pageSize);

            var result = new
            {
                totalAmount,
                pageIndex,
                pageSize,
                totalPage,
                objmap
            };
            _response.IsSuccess = true;
            _response.ObjectResponse = result;
        }
        catch (Exception ex)
        {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
        }

        return _response;
    }

    public async Task<ResponseDTO> SearchItemByAll(string search, int pageIndex, int pageSize, SortType sortType)
    {
        try
        {
            IList<string> fileNameContainText = await ListItemWithFile(search);
            IQueryable<Item> queryItem = null;
            if (fileNameContainText.Count > 0)
            {
                queryItem = GetQueryItemForAdmin()
                    .Where(item => item.MetadataValue.Select(x => x.TextValue).Any(y => y.Contains(search))
                                   || item.File.Any(file => fileNameContainText.Contains(file.FileKeyId)));
            }
            else
            {
                queryItem = GetQueryItemForAdmin()
                    .Where(x => x.MetadataValue.Select(x => x.TextValue).Any(y => y.Contains(search)));
            }

            int totalAmount = queryItem.Count();
            SortBy(ref queryItem, sortType);
            PagingItem(ref queryItem, pageIndex, pageSize);
            var listItem = await queryItem.ToListAsync();
            decimal totalPage = Math.Ceiling((decimal)totalAmount / pageSize);
            var objmap = _mapper.Map<List<ItemDTOForSelectList>>(listItem);
            var mapAuthorItem = await GetAuthorItem(objmap);
            if (mapAuthorItem != null)
            {
                objmap = mapAuthorItem;
            }

            var result = new
            {
                totalAmount,
                pageIndex,
                pageSize,
                totalPage,
                objmap
            };
            _response.IsSuccess = true;
            _response.ObjectResponse = result;
        }
        catch (Exception ex)
        {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
        }

        return _response;
    }


    public async Task<ResponseDTO> GetAllItemByCollectionIdForUser(int collectionId, int pageIndex, int pageSize,
        SortType sortType)
    {
        try
        {
            var queryItem = GetQueryItemForAdmin()
                .Where(x => x.CollectionId == collectionId && x.Discoverable);
            int totalAmount = queryItem.Count();

            SortBy(ref queryItem, sortType);
            PagingItem(ref queryItem, pageIndex, pageSize);

            var objList = await queryItem.ToListAsync();
            var objmap = _mapper.Map<List<ItemDTOForSelectListForUser>>(objList);

            objmap = await GetAuthorItemForUser(objmap);
            decimal totalPage = Math.Ceiling((decimal)totalAmount / pageSize);

            var result = new
            {
                totalAmount,
                pageIndex,
                pageSize,
                totalPage,
                objmap
            };

            _response.IsSuccess = true;
            _response.ObjectResponse = result;
        }
        catch (Exception ex)
        {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
        }

        return _response;
    }

    public async Task<ResponseDTO> GetItemSimpleByIdForUser(int itemId)
    {
        try
        {
            var item = await GetQueryItemForUser()
                .SingleOrDefaultAsync(x => x.ItemId == itemId && x.Discoverable);
            if (item == null)
            {
                _response.IsSuccess = false;
                _response.Message = "Not found Item";
            }
            else
            {
                var objMap = _mapper.Map<ItemDTOForSelectDetailSimpleForUser>(item);
                objMap.AuthorItems = new List<AuthorDTOForSelectItem>();
                objMap.CommunityId = (await _collectionRepository.GetCollection(item.CollectionId)).CommunityId;
                foreach (var authorId in objMap.Authors)
                {
                    int authorid = int.Parse(authorId);
                    var author = await _authorRepository.GetAuthor(authorid);
                    var authorMap = _mapper.Map<AuthorDTOForSelectItem>(author);
                    objMap.AuthorItems.Add(authorMap);
                }

                _response.IsSuccess = true;
                _response.Message = "Founded";
                _response.ObjectResponse = objMap;
            }
        }
        catch (Exception ex)
        {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
        }

        return _response;
    }

    public async Task<ResponseDTO> GetItemFullByIdForUser(int itemId)
    {
        try
        {
            var item = await _itemRepository.GetQueryAll()
                .Include(x => x.Collection)
                .Include(x => x.MetadataValue)
                .ThenInclude(x => x.MetadataFieldRegistry)
                .SingleOrDefaultAsync(x => x.ItemId == itemId && x.Discoverable);
            if (item != null)
            {
                var objectResponse = _mapper.Map<ItemDTOForSelectDetailFullForUser>(item);
                objectResponse.ListMetadataValueDTOForSelect =
                    _mapper.Map<List<MetadataValueDTOForSelect>>(item.MetadataValue);
                objectResponse.ListAuthorDtoForSelect = new List<AuthorDTOForSelectItem>();
                objectResponse.ListAdvisorDtoForSelect = new List<AuthorDTOForSelectItem>();
               foreach (var m in objectResponse.ListMetadataValueDTOForSelect.Where(x => x.MetadataFieldId == 1 || x.MetadataFieldId == 2))
               {
                  m.TextValue = (await _authorRepository.GetAuthor(int.Parse(m.TextValue))).FullName;
               }

               foreach (var author in objectResponse.ListMetadataValueDTOForSelect.Where(x => x.MetadataFieldId == 1)
                             .Select(y => y.TextValue))
                {
                    if (author != null)
                    {
                        var authorDto = await _authorRepository.GetQueryAll().Select(x => new AuthorDTOForSelectItem
                        {
                            AuthorId = x.AuthorId,
                            FullName = x.FullName
                        }).SingleOrDefaultAsync(x => x.AuthorId == int.Parse(author));
                        if (authorDto != null)
                        {
                            objectResponse.ListAuthorDtoForSelect.Add(authorDto);
                        }
                    }
                }

                foreach (var advisor in objectResponse.ListMetadataValueDTOForSelect.Where(x => x.MetadataFieldId == 2)
                             .Select(y => y.TextValue))
                {
                    if (advisor != null)
                    {
                        var advisorDto = await _authorRepository.GetQueryAll().Select(x => new AuthorDTOForSelectItem
                        {
                            AuthorId = x.AuthorId,
                            FullName = x.FullName
                        }).SingleOrDefaultAsync(x => x.AuthorId == int.Parse(advisor));
                        if (advisorDto != null)
                        {
                            objectResponse.ListAdvisorDtoForSelect.Add(advisorDto);
                        }
                    }
                }

                _response.IsSuccess = true;
                _response.Message = "Find item Success";
                _response.ObjectResponse = objectResponse;
            }
            else
            {
                _response.IsSuccess = false;
                _response.Message = "Not found item";
            }
        }
        catch (Exception ex)
        {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
        }

        return _response;
    }

    public async Task<ResponseDTO> GetListItemRecentForUser(string userEmail)
    {
        try
        {
            List<Item> items = new List<Item>();
            var listCollectionId = await _subscribeRepository.GetAllSubscribedCollectionId(userEmail);
            IQueryable<Item> queryItem = GetQueryItemForUser()
                .Where(x => listCollectionId.Any(y => y == x.CollectionId) && x.Discoverable);
            SortBy(ref queryItem, SortType.DateIssueDescending);
            PagingItem(ref queryItem, 1, 5);
            var objmap = _mapper.Map<List<ItemDTOForSelectListForUser>>(await queryItem.ToListAsync());
            objmap = await GetAuthorItemForUser(objmap);
            _response.IsSuccess = true;
            _response.ObjectResponse = objmap;
        }
        catch (Exception ex)
        {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
        }

        return _response;
    }

    public async Task<ResponseDTO> SearchItemForUser(ItemDTOForSearchForUser itemDtoForSearch, int pageIndex,
        int pageSize)
    {
        try
        {
            IQueryable<Item> queryItem = _itemRepository.GetQueryAll()
                .Include(x => x.Collection)
                .Include(x => x.MetadataValue)
                .Where(x => x.Discoverable);
            
            if (itemDtoForSearch.CollectionId != 0)
            {
                queryItem = queryItem.Where(x => x.CollectionId == itemDtoForSearch.CollectionId);
            }

            if (itemDtoForSearch.Title != null && itemDtoForSearch.Title != string.Empty)
            {
                var listItem = queryItem.AsEnumerable();
                queryItem = listItem.Where(x =>
                    RemoveDiacritics(x.MetadataValue.FirstOrDefault(y => y.MetadataFieldId == 57).TextValue)
                        .Contains(RemoveDiacritics(itemDtoForSearch.Title))).AsAsyncQueryable();
            }

            if (itemDtoForSearch.Authors.Count() > 0)
            {
                foreach (string author in itemDtoForSearch.Authors)
                {
                    queryItem = queryItem.Where(x =>
                        x.MetadataValue.Where(y => y.MetadataFieldId == 1 || y.MetadataFieldId == 2)
                            .Select(z => z.TextValue).Contains(author));
                }
            }

            if (itemDtoForSearch.Keywords.Count() > 0)
            {
                foreach (string keyword in itemDtoForSearch.Keywords)
                {
                    queryItem = queryItem.Where(x =>
                        x.MetadataValue.Where(y => y.MetadataFieldId == 51).Select(z => z.TextValue)
                            .Any(a => a.Contains(keyword)));
                }
            }

            if (itemDtoForSearch.Publisher != null && itemDtoForSearch.Publisher != string.Empty)
            {
                queryItem = queryItem.Where(x => x.MetadataValue.FirstOrDefault(y => y.MetadataFieldId == 36) != null &&
                    x.MetadataValue.FirstOrDefault(y => y.MetadataFieldId == 36).TextValue.Replace(" ", "")
                        .Contains(itemDtoForSearch.Publisher));
            }

            if (itemDtoForSearch.Types.Count() > 0)
            {
                foreach (string type in itemDtoForSearch.Types)
                {
                    queryItem = queryItem.Where(x =>
                        x.MetadataValue.Where(y => y.MetadataFieldId == 59).Select(z => z.TextValue)
                            .Any(a => a.Contains(type)));
                }
            }

            if (itemDtoForSearch.StartDateIssue != null)
            {
                queryItem = queryItem.Where(x => x.MetadataValue.FirstOrDefault(x => x.MetadataFieldId == 12).TextValue
                    .CompareTo(itemDtoForSearch.StartDateIssue.Value.ToString()) >= 0);
            }

            if (itemDtoForSearch.EndDateIssue != null)
            {
                queryItem = queryItem.Where(x => x.MetadataValue.FirstOrDefault(x => x.MetadataFieldId == 12).TextValue
                    .CompareTo((itemDtoForSearch.EndDateIssue.Value + 1).ToString()) < 0);
            }

            var totalAmount = queryItem.Count();
            SortBy(ref queryItem, itemDtoForSearch.SortType);
            PagingItem(ref queryItem, pageIndex, pageSize);

            decimal totalPage = Math.Ceiling((decimal)totalAmount / pageSize);
            var objmap = _mapper.Map<List<ItemDTOForSelectListForUser>>(await queryItem.ToListAsync());
            objmap = await GetAuthorItemForUser(objmap);
            var result = new
            {
                totalAmount,
                pageIndex,
                pageSize,
                totalPage,
                objmap
            };
            _response.IsSuccess = true;
            _response.ObjectResponse = result;
        }
        catch (Exception ex)
        {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
        }

        return _response;
    }

    public async Task<ResponseDTO> SearchItemByAllForUser(string search, int pageIndex, int pageSize, SortType sortType)
    {
        try
        {
            IList<string> fileNameContainText = await ListItemWithFile(search);
            IQueryable<Item> queryItem = null;
            if (fileNameContainText.Count > 0)
            {
                queryItem = GetQueryItemForUser()
                    .Where(x => x.MetadataValue.Select(x => x.TextValue).Any(y => y.Contains(search))
                                || x.File.Any(file => fileNameContainText.Contains(file.FileKeyId)));
            }
            else
            {
                queryItem = GetQueryItemForUser()
                    .Where(x => x.MetadataValue.Select(x => x.TextValue).Any(y => y.Contains(search)));
            }

            var totalAmount = queryItem.Count();
            SortBy(ref queryItem, sortType);
            PagingItem(ref queryItem, pageIndex, pageSize);
            var listItem = await queryItem.ToListAsync();
            decimal totalPage = Math.Ceiling((decimal)totalAmount / pageSize);
            var objmap = _mapper.Map<List<ItemDTOForSelectListForUser>>(listItem);
            objmap = await GetAuthorItemForUser(objmap);

            var result = new
            {
                totalAmount,
                pageIndex,
                pageSize,
                totalPage,
                objmap
            };

            _response.IsSuccess = true;
            _response.ObjectResponse = result;
        }
        catch (Exception ex)
        {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
        }

        return _response;
    }

    public async Task<ResponseDTO> Get5ItemRecentlyInOneCollectionForUser(int collectionId)
    {
        try
        {
            IQueryable<Item> queryItem = GetQueryItemForUser()
                .Where(x => x.CollectionId == collectionId);
            SortBy(ref queryItem, SortType.DateIssueDescending);
            PagingItem(ref queryItem, 1, 5);
            var objList = await queryItem.ToListAsync();

            var objmap = _mapper.Map<List<ItemDTOForSelectListForUser>>(objList);
            objmap = await GetAuthorItemForUser(objmap);

            objmap = objmap.TakeLast(5).ToList();
            _response.IsSuccess = true;
            _response.ObjectResponse = objmap;
        }
        catch (Exception ex)
        {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
        }

        return _response;
    }

    public async Task<ResponseDTO> GetAllItemForUser(int pageIndex, int pageSize, SortType sortType)
    {
        try
        {
            var queryItem = GetQueryItemForUser();
            var totalAmount = queryItem.Count();
            SortBy(ref queryItem, sortType);
            PagingItem(ref queryItem, pageIndex, pageSize);
            var objList = await queryItem.ToListAsync();
            var objmap = _mapper.Map<List<ItemDTOForSelectListForUser>>(objList);
            objmap = await GetAuthorItemForUser(objmap);
            decimal totalPage = Math.Ceiling((decimal)totalAmount / pageSize);

            var result = new
            {
                totalAmount,
                pageIndex,
                pageSize,
                totalPage,
                objmap
            };

            _response.IsSuccess = true;
            _response.ObjectResponse = result;
        }
        catch (Exception ex)
        {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
        }

        return _response;
    }

    public async Task<ResponseDTO> SearchItemSimpleForUser(ItemDTOForSearchSimpleForUser itemDTOForSearchSimple,
        int pageIndex, int pageSize)
    {
        try
        {
            IQueryable<Item> queryItem = GetQueryItemForUser();
            if (itemDTOForSearchSimple.CollectionId != null && itemDTOForSearchSimple.CollectionId > 0)
            {
                queryItem = queryItem.Where(x => x.CollectionId == itemDTOForSearchSimple.CollectionId);
            }

            if (itemDTOForSearchSimple.Title != null && itemDTOForSearchSimple.Title != string.Empty)
            {
                queryItem = queryItem.Where(x =>
                    x.MetadataValue.SingleOrDefault(x => x.MetadataFieldId == 57).TextValue.Replace(" ", "")
                        .Contains(itemDTOForSearchSimple.Title));
            }

            if (itemDTOForSearchSimple.Authors != null)
            {
                if (itemDTOForSearchSimple.Authors.Count() > 0)
                {
                    foreach (var author in itemDTOForSearchSimple.Authors)
                    {
                        queryItem = queryItem.Where(x =>
                            x.MetadataValue.Where(x => x.MetadataFieldId == 1).Select(x => x.TextValue)
                                .Any(y => y.Contains(author)));
                    }
                }
            }

            if (itemDTOForSearchSimple.SubjectKeywords != null)
            {
                if (itemDTOForSearchSimple.SubjectKeywords.Count() > 0)
                {
                    foreach (var subjectKeyword in itemDTOForSearchSimple.SubjectKeywords)
                    {
                        queryItem = queryItem.Where(x =>
                            x.MetadataValue.Where(x => x.MetadataFieldId == 51).Select(x => x.TextValue)
                                .Any(y => y.Contains(subjectKeyword)));
                        ;
                    }
                }
            }

            if (itemDTOForSearchSimple.Year != null && itemDTOForSearchSimple.Year != string.Empty)
            {
                queryItem = queryItem.Where(x =>
                    x.MetadataValue.SingleOrDefault(x => x.MetadataFieldId == 12).TextValue
                        .StartsWith(itemDTOForSearchSimple.Year));
            }

            if (itemDTOForSearchSimple.Month != null && itemDTOForSearchSimple.Month != string.Empty)
            {
                queryItem = queryItem.Where(x =>
                    x.MetadataValue.SingleOrDefault(x => x.MetadataFieldId == 12).TextValue
                        .Contains("-" + itemDTOForSearchSimple.Month + "-"));
            }

            if (itemDTOForSearchSimple.Day != null && itemDTOForSearchSimple.Day != string.Empty)
            {
                queryItem = queryItem.Where(x =>
                    x.MetadataValue.SingleOrDefault(x => x.MetadataFieldId == 12).TextValue
                        .EndsWith("-" + itemDTOForSearchSimple.Day));
            }

            int totalAmount = queryItem.Count();
            SortBy(ref queryItem, itemDTOForSearchSimple.SortType);
            PagingItem(ref queryItem, pageIndex, pageSize);
            var objList = await queryItem.ToListAsync();

            var objmap = _mapper.Map<List<ItemDTOForSelectListForUser>>(objList);
            objmap = await GetAuthorItemForUser(objmap);
            decimal totalPage = Math.Ceiling((decimal)totalAmount / pageSize);

            var result = new
            {
                totalAmount,
                pageIndex,
                pageSize,
                totalPage,
                objmap
            };
            _response.IsSuccess = true;
            _response.ObjectResponse = result;
        }
        catch (Exception ex)
        {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
        }

        return _response;
    }

    public async Task<ResponseDTO> GetAllItemInOneCollectionForStaff(int collectionId, int pageIndex, int pageSize,
        int peopleId, SortType sortType)
    {
        try
        {
            var totalAmount = _itemRepository.GetQueryAll()
                .Where(x => x.CollectionId == collectionId)
                .Count();

            var queryItem = _itemRepository.GetQueryAll()
                .Include(i => i.Collection)
                .Include(x => x.MetadataValue)
                .Where(x => x.CollectionId == collectionId);
            SortBy(ref queryItem, sortType);
            PagingItem(ref queryItem, pageIndex, pageSize);
            var objList = await queryItem.ToListAsync();

            var objmap = _mapper.Map<List<ItemDTOForSelectListForStaff>>(objList);
            decimal totalPage = Math.Ceiling((decimal)totalAmount / pageSize);
            objmap = await GetAuthorItemForStaff(objmap);
            var listCollectionId = await _collectionGroupRepository.GetListCollectionIdByStaffId(peopleId);

            if (listCollectionId.Contains(collectionId))
            {
                foreach (var itemMap in objmap)
                {
                    itemMap.CanEdit = true;
                }
            }


            var result = new
            {
                totalAmount,
                pageIndex,
                pageSize,
                totalPage,
                objmap
            };
            _response.IsSuccess = true;
            _response.ObjectResponse = result;
        }
        catch (Exception ex)
        {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
        }

        return _response;
    }

    public async Task<ResponseDTO> GetAllItemForStaff(int peopleId, int pageIndex, int pageSize, SortType sortType)
    {
        try
        {
            List<int> listCollectionId = await _collectionGroupRepository.GetListCollectionIdByStaffId(peopleId);
            var queryItem = GetQueryItemForAdmin();
            var totalAmount = queryItem.Count();
            SortBy(ref queryItem, sortType);
            PagingItem(ref queryItem, pageIndex, pageSize);
            var objList = await queryItem.ToListAsync();
            var objmap = _mapper.Map<List<ItemDTOForSelectListForStaff>>(objList);
            decimal totalPage = Math.Ceiling((decimal)totalAmount / pageSize);

            objmap = await GetAuthorItemForStaff(objmap);

            foreach (var itemMap in objmap)
            {
                if (listCollectionId.Contains(itemMap.CollectionId))
                {
                    itemMap.CanEdit = true;
                }
            }

            var result = new
            {
                totalAmount,
                pageIndex,
                pageSize,
                totalPage,
                objmap
            };

            _response.IsSuccess = true;
            _response.ObjectResponse = result;
        }
        catch (Exception ex)
        {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
        }

        return _response;
    }

    public async Task<ResponseDTO> CreateSimpleItemForStaff(ItemDTOForCreateSimple itemDTO, int submmitterId)
    {
        try
        {
            var collectionGroup = await _collectionGroupRepository.GetQueryAll()
                .SingleOrDefaultAsync(x =>
                    x.CollectionId == itemDTO.CollectionId &&
                    x.Group.GroupPeoples.Select(x => x.PeopleId).Contains(submmitterId));
            if (collectionGroup == null)
            {
                _response.IsSuccess = false;
                _response.Message = "you not have permission in this collection";
            }
            else
            {
                _response = await CreateSimpleItem(itemDTO, submmitterId);
            }
        }
        catch (Exception ex)
        {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
        }

        return _response;
    }

    public async Task<ResponseDTO> GetItemSimpleByIdForStaff(int itemId, int peopleId)
    {
        try
        {
            var listCollectionId = await _collectionGroupRepository.GetListCollectionIdByStaffId(peopleId);

            var item = await _itemRepository.GetQueryAll()
                .Include(x => x.MetadataValue)
                .Include(x => x.Collection)
                .ThenInclude(x => x.Community)
                .Include(x => x.File)
                .Include(x => x.People)
                .ThenInclude(x => x.User)
                .SingleOrDefaultAsync(x => x.ItemId == itemId);
            if (item == null)
            {
                _response.IsSuccess = true;
                _response.Message = "Not found Item";
            }
            else
            {
                ItemDTOForSelectDetailSimpleForStaff objmap = _mapper.Map<ItemDTOForSelectDetailSimpleForStaff>(item);
                objmap.AuthorItems = new List<AuthorDTOForSelectItem>();
                objmap.CommunityId = (await _collectionRepository.GetCollection(item.CollectionId)).CommunityId;
                foreach (var authorId in objmap.Authors)
                {
                    int authorid = int.Parse(authorId);
                    var author = await _authorRepository.GetAuthor(authorid);
                    var authorMap = _mapper.Map<AuthorDTOForSelectItem>(author);
                    objmap.AuthorItems.Add(authorMap);
                }

                if (listCollectionId.Contains(objmap.CollectionId))
                {
                    objmap.CanEdit = true;
                }

                _response.IsSuccess = true;
                _response.ObjectResponse = objmap;
            }
        }
        catch (Exception ex)
        {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
        }

        return _response;
    }

    public async Task<ResponseDTO> GetItemFullByIdForStaff(int itemId, int peopleId)
    {
        try
        {
            var listCollectionId = await _collectionGroupRepository.GetListCollectionIdByStaffId(peopleId);
            var item = await _itemRepository.GetQueryAll()
                .Include(x => x.MetadataValue)
                .ThenInclude(x => x.MetadataFieldRegistry)
                .Include(x => x.MetadataValue)
                .Include(c => c.Collection)
                .ThenInclude(c => c.Community)
                .Include(f => f.File)
                .SingleOrDefaultAsync(x => x.ItemId == itemId);
            if (item != null)
            {
                var objectResponse = _mapper.Map<ItemDTOForSelectDetailFullForStaff>(item);
                objectResponse.ListAuthorDtoForSelect = new List<AuthorDTOForSelectItem>();
                objectResponse.ListAdvisorDtoForSelect = new List<AuthorDTOForSelectItem>();

                foreach (var author in objectResponse.ListMetadataValueDTOForSelect
                             .Where(x => x.MetadataFieldId == 1)
                             .Select(y => y.TextValue))
                {
                    if (author != null)
                    {
                        var authorDto = await _authorRepository.GetQueryAll().Select(x => new AuthorDTOForSelectItem
                        {
                            AuthorId = x.AuthorId,
                            FullName = x.FullName
                        }).SingleOrDefaultAsync(x => x.AuthorId == int.Parse(author));
                        if (authorDto != null)
                        {
                            objectResponse.ListAuthorDtoForSelect.Add(authorDto);
                        }
                    }
                }

                foreach (var advisor in objectResponse.ListMetadataValueDTOForSelect
                             .Where(x => x.MetadataFieldId == 2)
                             .Select(y => y.TextValue))
                {
                    if (advisor != null)
                    {
                        var advisorDto = await _authorRepository.GetQueryAll().Select(x => new AuthorDTOForSelectItem
                        {
                            AuthorId = x.AuthorId,
                            FullName = x.FullName
                        }).SingleOrDefaultAsync(x => x.AuthorId == int.Parse(advisor));
                        if (advisorDto != null)
                        {
                            objectResponse.ListAdvisorDtoForSelect.Add(advisorDto);
                        }
                    }
                }

                if (listCollectionId.Contains(objectResponse.CollectionId))
                {
                    objectResponse.CanEdit = true;
                }


                _response.IsSuccess = true;
                _response.ObjectResponse = objectResponse;
            }
            else
            {
                _response.IsSuccess = false;
                _response.Message = "Not found item";
            }
        }
        catch (Exception ex)
        {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
        }

        return _response;
    }

    public async Task<ResponseDTO> ModifyItemForStaff(MetadataValueDTOForModified metadataValueDTOForModified,
        int submmitterId, int itemId)
    {
        try
        {
            List<int> listCollectionId = await GetAllCollectForStaffId(submmitterId);
            var item = await _itemRepository.GetQueryAll()
                .Include(x => x.Collection)
                .SingleOrDefaultAsync(x => x.ItemId == itemId);
            if (item == null)
            {
                _response.IsSuccess = false;
                _response.Message = "you not have permission in this Item";
                return _response;
            }

            if (!listCollectionId.Any(x => x == item.CollectionId))
            {
                _response.IsSuccess = false;
                _response.Message = "you not have permission in this Item";
                return _response;
            }

            _response = await ModifyItem(metadataValueDTOForModified, submmitterId, itemId);
        }
        catch (Exception ex)
        {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
        }

        return _response;
    }

    private string RemoveDiacritics(string text)
    {
        var normalizedString = text.Normalize(NormalizationForm.FormD);
        var stringBuilder = new StringBuilder();

        foreach (char c in normalizedString)
        {
            var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
            if (unicodeCategory != UnicodeCategory.NonSpacingMark && unicodeCategory != UnicodeCategory.SpaceSeparator)
            {
                stringBuilder.Append(c);
            }
        }

        return stringBuilder.ToString().Normalize(NormalizationForm.FormC).ToLower();
    }


    public async Task<ResponseDTO> SearchItem(ItemDtoForSearch itemDtoForSearch, int pageIndex, int pageSize)
    {
        try
        {
            IQueryable<Item> queryItem = _itemRepository.GetQueryAll()
                .Include(x => x.Collection)
                .Include(x => x.MetadataValue);

            if (itemDtoForSearch.CollectionId != 0)
            {
                SearchItemByCollectionId(ref queryItem, itemDtoForSearch.CollectionId);
            }

            if (itemDtoForSearch.Discoverable != null)
            {
                queryItem = queryItem.Where(x => x.Discoverable == itemDtoForSearch.Discoverable);
            }

            if (itemDtoForSearch.Title != null && itemDtoForSearch.Title != string.Empty)
            {
                var listItem = queryItem.AsEnumerable();
                queryItem = listItem.Where(x =>
                    RemoveDiacritics(x.MetadataValue.FirstOrDefault(y => y.MetadataFieldId == 57).TextValue)
                        .Contains(RemoveDiacritics(itemDtoForSearch.Title))).AsAsyncQueryable();
            }

            if (itemDtoForSearch.Authors.Count() > 0)
            {
                foreach (string author in itemDtoForSearch.Authors)
                {
                    queryItem = queryItem.Where(x =>
                        x.MetadataValue.Where(y => y.MetadataFieldId == 1 || y.MetadataFieldId == 2)
                            .Select(z => z.TextValue).Contains(author));
                }
            }

            if (itemDtoForSearch.Keywords.Count() > 0)
            {
                foreach (string keyword in itemDtoForSearch.Keywords)
                {
                    queryItem = queryItem.Where(x =>
                        x.MetadataValue.Where(y => y.MetadataFieldId == 51).Select(z => z.TextValue)
                            .Any(a => a.Contains(keyword)));
                }
            }

            if (itemDtoForSearch.Publisher != null && itemDtoForSearch.Publisher != string.Empty)
            {
                queryItem = queryItem.Where(x => x.MetadataValue.FirstOrDefault(y => y.MetadataFieldId == 36) != null &&
                    x.MetadataValue.FirstOrDefault(y => y.MetadataFieldId == 36).TextValue.Replace(" ", "")
                        .Contains(itemDtoForSearch.Publisher));
            }

            if (itemDtoForSearch.Types.Count() > 0)
            {
                foreach (string type in itemDtoForSearch.Types)
                {
                    queryItem = queryItem.Where(x =>
                        x.MetadataValue.Where(y => y.MetadataFieldId == 59).Select(z => z.TextValue)
                            .Any(a => a.Contains(type)));
                }
            }

            if (itemDtoForSearch.StartDateIssue != null)
            {
                queryItem = queryItem.Where(x => x.MetadataValue.FirstOrDefault(x => x.MetadataFieldId == 12).TextValue
                    .CompareTo(itemDtoForSearch.StartDateIssue.Value.ToString()) >= 0);
            }

            if (itemDtoForSearch.EndDateIssue != null)
            {
                queryItem = queryItem.Where(x => x.MetadataValue.FirstOrDefault(x => x.MetadataFieldId == 12).TextValue
                    .CompareTo((itemDtoForSearch.EndDateIssue.Value + 1).ToString()) < 0);
            }


            int totalAmount = queryItem.Count();
            SortBy(ref queryItem, itemDtoForSearch.SortType);
            PagingItem(ref queryItem, pageIndex, pageSize);
            decimal totalPage = Math.Ceiling((decimal)totalAmount / pageSize);
            var objmap = _mapper.Map<List<ItemDTOForSelectList>>(await queryItem.ToListAsync());
            objmap = await GetAuthorItem(objmap);
            var result = new
            {
                totalAmount,
                pageIndex,
                pageSize,
                totalPage,
                objmap
            };
            _response.IsSuccess = true;
            _response.ObjectResponse = result;
        }
        catch (Exception ex)
        {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
        }

        return _response;
    }

    public async Task<ResponseDTO> Get5ItemRecentlyInOneCollection(int collectionId)
    {
        try
        {
            IQueryable<Item> queryItem = _itemRepository.GetQueryAll()
                .Include(i => i.Collection)
                .Include(x => x.MetadataValue)
                .Where(x => x.CollectionId == collectionId);
            SortBy(ref queryItem, SortType.DateIssueDescending);
            PagingItem(ref queryItem, 1, 5);
            var objList = await queryItem.ToListAsync();

            var objmap = _mapper.Map<List<ItemDTOForSelectList>>(objList);
            objmap = objmap.OrderBy(x => x.DateOfIssue).TakeLast(5).ToList();
            objmap = await GetAuthorItem(objmap);
            _response.IsSuccess = true;
            _response.ObjectResponse = objmap;
        }
        catch (Exception ex)
        {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
        }

        return _response;
    }

    public async Task<ResponseDTO> SearchItemForStaff(ItemDtoForSearch itemDtoForSearch, int peopleId, int pageIndex,
        int pageSize)
    {
        try
        {
            IQueryable<Item> queryItem = GetQueryItemForAdmin();
            if (itemDtoForSearch.Discoverable != null)
            {
                queryItem = queryItem.Where(x => x.Discoverable == itemDtoForSearch.Discoverable);
            }

            if (itemDtoForSearch.CollectionId != 0)
            {
                queryItem = queryItem.Where(x => x.CollectionId == itemDtoForSearch.CollectionId);
            }

            if (itemDtoForSearch.Title != null && itemDtoForSearch.Title != string.Empty)
            {
                var listItem = queryItem.AsEnumerable();
                queryItem = listItem.Where(x =>
                    RemoveDiacritics(x.MetadataValue.FirstOrDefault(y => y.MetadataFieldId == 57).TextValue)
                        .Contains(RemoveDiacritics(itemDtoForSearch.Title))).AsAsyncQueryable();
            }

            if (itemDtoForSearch.Authors.Count > 0)
            {
                if (itemDtoForSearch.Authors.Count() > 0)
                {
                    foreach (string author in itemDtoForSearch.Authors)
                    {
                        queryItem = queryItem.Where(x =>
                            x.MetadataValue.Where(y => y.MetadataFieldId == 1 || y.MetadataFieldId == 2)
                                .Select(z => z.TextValue).Contains(author));
                    }
                }
            }

            if (itemDtoForSearch.Keywords.Count() > 0)
            {
                foreach (string keyword in itemDtoForSearch.Keywords)
                {
                    queryItem = queryItem.Where(x =>
                        x.MetadataValue.Where(y => y.MetadataFieldId == 51).Select(z => z.TextValue)
                            .Any(a => a.Contains(keyword)));
                }
            }

            if (itemDtoForSearch.Publisher != null && itemDtoForSearch.Publisher != string.Empty)
            {
                queryItem = queryItem.Where(x => x.MetadataValue.FirstOrDefault(y => y.MetadataFieldId == 36) != null &&
                    x.MetadataValue.FirstOrDefault(y => y.MetadataFieldId == 36).TextValue.Replace(" ", "")
                        .Contains(itemDtoForSearch.Publisher));
            }

            if (itemDtoForSearch.Types.Count() > 0)
            {
                foreach (string type in itemDtoForSearch.Types)
                {
                    queryItem = queryItem.Where(x =>
                        x.MetadataValue.Where(y => y.MetadataFieldId == 59).Select(z => z.TextValue)
                            .Any(a => a.Contains(type)));
                }
            }

            if (itemDtoForSearch.StartDateIssue != null)
            {
                queryItem = queryItem.Where(x => x.MetadataValue.FirstOrDefault(x => x.MetadataFieldId == 12).TextValue
                    .CompareTo(itemDtoForSearch.StartDateIssue.Value.ToString()) >= 0);
            }

            if (itemDtoForSearch.EndDateIssue != null)
            {
                queryItem = queryItem.Where(x => x.MetadataValue.FirstOrDefault(x => x.MetadataFieldId == 12).TextValue
                    .CompareTo((itemDtoForSearch.EndDateIssue.Value + 1).ToString()) < 0);
            }

            var totalAmount = queryItem.Count();
            SortBy(ref queryItem, itemDtoForSearch.SortType);
            PagingItem(ref queryItem, pageIndex, pageSize);
            var objList = await queryItem.ToListAsync();
            var objmap = _mapper.Map<List<ItemDTOForSelectListForStaff>>(objList);

            decimal totalPage = Math.Ceiling((decimal)totalAmount / pageSize);
            objmap = await GetAuthorItemForStaff(objmap);
            var listCollectionId = await _collectionGroupRepository.GetListCollectionIdByStaffId(peopleId);

            foreach (var itemMap in objmap)
            {
                if (listCollectionId.Contains(itemMap.CollectionId))
                {
                    itemMap.CanEdit = true;
                }
            }


            var result = new
            {
                totalAmount,
                pageIndex,
                pageSize,
                totalPage,
                objmap
            };
            _response.IsSuccess = true;
            _response.ObjectResponse = result;
        }
        catch (Exception ex)
        {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
        }

        return _response;
    }

    public async Task<ResponseDTO> SearchItemByAllForStaff(string search, int peopleId, int pageIndex, int pageSize,
        SortType sortType)
    {
        try
        {
            IList<string> fileNameContainText = await ListItemWithFile(search);
            IQueryable<Item> queryItem = null;
            if (fileNameContainText.Count > 0)
            {
                queryItem = GetQueryItemForAdmin()
                    .Where(item => item.MetadataValue.Select(x => x.TextValue).Any(y => y.Contains(search))
                                   || item.File.Any(file => fileNameContainText.Contains(file.FileKeyId)));
            }
            else
            {
                queryItem = GetQueryItemForAdmin()
                    .Where(x => x.MetadataValue.Select(x => x.TextValue).Any(y => y.Contains(search)));
            }

            var totalAmount = queryItem.Count();
            decimal totalPage = Math.Ceiling((decimal)totalAmount / pageSize);
            SortBy(ref queryItem, sortType);
            PagingItem(ref queryItem, pageIndex, pageSize);
            var objList = await queryItem.ToListAsync();

            var objmap = _mapper.Map<List<ItemDTOForSelectListForStaff>>(objList);
            objmap = await GetAuthorItemForStaff(objmap);
            var listCollectionId = await _collectionGroupRepository.GetListCollectionIdByStaffId(peopleId);

            foreach (var itemMap in objmap)
            {
                if (listCollectionId.Contains(itemMap.CollectionId))
                {
                    itemMap.CanEdit = true;
                }
            }

            var result = new
            {
                totalAmount,
                pageIndex,
                pageSize,
                totalPage,
                objmap
            };
            _response.IsSuccess = true;
            _response.ObjectResponse = result;
        }
        catch (Exception ex)
        {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
        }

        return _response;
    }

    public async Task<ResponseDTO> SearchItemSimpleForStaff(ItemDTOForSearchSimple itemDTOForSearchSimple, int peopleId,
        int pageIndex, int pageSize)
    {
        try
        {
            int totalAmount = 0;

            var collectionCheck = await _collectionGroupRepository.GetListCollectionIdByStaffId(peopleId);
            IQueryable<Item> queryItem = GetQueryItemForAdmin();
            if (itemDTOForSearchSimple.Discoverable != null)
            {
                queryItem = queryItem.Where(x => x.Discoverable == itemDTOForSearchSimple.Discoverable);
            }

            if (collectionCheck == null || collectionCheck.Count() == 0)
            {
                _response.IsSuccess = false;
                _response.Message = "You can not access";
                return _response;
            }

            if (itemDTOForSearchSimple.CollectionId != null && itemDTOForSearchSimple.CollectionId > 0)
            {
                queryItem = queryItem.Where(x => x.CollectionId == itemDTOForSearchSimple.CollectionId);
            }

            if (itemDTOForSearchSimple.Title != null && itemDTOForSearchSimple.Title != string.Empty)
            {
                queryItem = queryItem.Where(x =>
                    x.MetadataValue.SingleOrDefault(x => x.MetadataFieldId == 57).TextValue.Replace(" ", "")
                        .Contains(itemDTOForSearchSimple.Title));
            }

            if (itemDTOForSearchSimple.Authors != null)
            {
                if (itemDTOForSearchSimple.Authors.Count() > 0)
                {
                    foreach (var author in itemDTOForSearchSimple.Authors)
                    {
                        queryItem = queryItem.Where(x =>
                            x.MetadataValue.Where(x => x.MetadataFieldId == 1).Select(x => x.TextValue)
                                .Any(y => y.Contains(author)));
                    }
                }
            }

            if (itemDTOForSearchSimple.SubjectKeywords != null)
            {
                if (itemDTOForSearchSimple.SubjectKeywords.Count() > 0)
                {
                    foreach (var subjectKeyword in itemDTOForSearchSimple.SubjectKeywords)
                    {
                        queryItem = queryItem.Where(x =>
                            x.MetadataValue.Where(x => x.MetadataFieldId == 51).Select(x => x.TextValue)
                                .Any(y => y.Contains(subjectKeyword)));
                        ;
                    }
                }
            }

            if (itemDTOForSearchSimple.Year != null && itemDTOForSearchSimple.Year != string.Empty)
            {
                queryItem = queryItem.Where(x =>
                    x.MetadataValue.SingleOrDefault(x => x.MetadataFieldId == 12).TextValue
                        .StartsWith(itemDTOForSearchSimple.Year));
            }

            if (itemDTOForSearchSimple.Month != null && itemDTOForSearchSimple.Month != string.Empty)
            {
                queryItem = queryItem.Where(x =>
                    x.MetadataValue.SingleOrDefault(x => x.MetadataFieldId == 12).TextValue
                        .Contains("-" + itemDTOForSearchSimple.Month + "-"));
            }

            if (itemDTOForSearchSimple.Day != null && itemDTOForSearchSimple.Day != string.Empty)
            {
                queryItem = queryItem.Where(x =>
                    x.MetadataValue.SingleOrDefault(x => x.MetadataFieldId == 12).TextValue
                        .EndsWith("-" + itemDTOForSearchSimple.Day));
            }

            totalAmount = queryItem.Count();
            SortBy(ref queryItem, itemDTOForSearchSimple.SortType);
            PagingItem(ref queryItem, pageIndex, pageSize);

            var objList = await queryItem.ToListAsync();

            var objmap = _mapper.Map<List<ItemDTOForSelectListForStaff>>(objList);

            decimal totalPage = Math.Ceiling((decimal)totalAmount / pageSize);
            objmap = await GetAuthorItemForStaff(objmap);
            var listCollectionId = await _collectionGroupRepository.GetListCollectionIdByStaffId(peopleId);

            foreach (var itemMap in objmap)
            {
                if (listCollectionId.Contains(itemMap.CollectionId))
                {
                    itemMap.CanEdit = true;
                }
            }

            var result = new
            {
                totalAmount,
                pageIndex,
                pageSize,
                totalPage,
                objmap
            };
            _response.IsSuccess = true;
            _response.ObjectResponse = result;
        }
        catch (Exception ex)
        {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
        }

        return _response;
    }

    private async Task<IList<string>> ListItemWithFile(string fileContent)
    {
        try
        {
            List<Task<string>> checkFileTasks = new List<Task<string>>();
            IList<SearchFileDto> listFile = await GetListFileNames(GetUserCredential(), fileContent);
            IList<string> fileNameContainText = new List<string>();
            foreach (var fileDto in listFile)
            {
                var task = CheckFileContainFolder(fileDto, "1a86_QB7vETCmIG-Qqidf4IPT7FPJ_car");
                checkFileTasks.Add(task);
            }

            var results = await Task.WhenAll(checkFileTasks);
            foreach (var fileCheck in results)
            {
                if (fileCheck != null)
                {
                    fileNameContainText.Add(fileCheck);
                }
            }

            var itemsFound = _itemRepository.GetQueryAll()
                .Where(item => item.File.Any(file => fileNameContainText.Contains(file.FileKeyId)))
                .ToList();
            return fileNameContainText;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public async Task<ResponseDTO> SearchFileContent(string fileContent, int pageIndex, int pageSize)
    {
        try
        {
            List<Task<string>> checkFileTasks = new List<Task<string>>();
            IList<SearchFileDto> listFile = await GetListFileNames(GetUserCredential(), fileContent);
            IList<string> fileNameContainText = new List<string>();
            foreach (var fileDto in listFile)
            {
                var task = CheckFileContainFolder(fileDto, "1a86_QB7vETCmIG-Qqidf4IPT7FPJ_car");
                checkFileTasks.Add(task);
            }

            var results = await Task.WhenAll(checkFileTasks);
            foreach (var fileCheck in results)
            {
                if (fileCheck != null)
                {
                    fileNameContainText.Add(fileCheck);
                }
            }

            var itemsFound = _itemRepository.GetQueryAll()
                .Where(item => item.File.Any(file => fileNameContainText.Contains(file.FileKeyId)))
                .ToList();

            _response.IsSuccess = true;
            _response.ObjectResponse = itemsFound;
        }
        catch (Exception ex)
        {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
        }

        return _response;
    }

    private async Task<string> CheckFileContainFolder(SearchFileDto fileDto, string folderId)
    {
        if (fileDto.FileParents != null)
        {
            foreach (var parentId in fileDto.FileParents)
            {
                if (parentId.Equals(folderId))
                {
                    return fileDto.FileName;
                }
            }
        }

        return null;
    }

    private async Task<IList<SearchFileDto>> GetListFileNames(Task<UserCredential> credential, string contentNeed)
    {
        IList<SearchFileDto> listFileIds = new List<SearchFileDto>();
        DriveService service = new DriveService(new BaseClientService.Initializer()
        {
            HttpClientInitializer = await credential
        });
        var requestWithQ = service.Files.List();
        requestWithQ.Fields = "files(id, name, parents)";
        requestWithQ.Q = $@"fullText contains '{contentNeed}' and mimeType='text/plain'";
        var result = await requestWithQ.ExecuteAsync();
        IList<File> files = result.Files;
        foreach (var file in files)
        {
            listFileIds.Add(new SearchFileDto()
            {
                FileId = file.Id,
                FileName = file.Name,
                FileParents = file.Parents
            });
        }

        return listFileIds;
    }

    public async Task<ResponseDTO> UpdateStatus(int itemId, bool discoverable, int peopleId)
    {
        try
        {
            var item = await _itemRepository.GetQueryAll().SingleOrDefaultAsync(x => x.ItemId == itemId);
            if (item == null)
            {
                _response.IsSuccess = false;
                _response.Message = "Not found item update";
            }
            else
            {
                item.Discoverable = discoverable;
                item.SubmitterId = peopleId;
                item.LastModified = DateTime.Now;
                await _itemRepository.UpdateAsync(item);

                _response.IsSuccess = true;
                _response.Message = "update status of item success";
            }
        }
        catch (Exception ex)
        {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
        }

        return _response;
    }

    public async Task<ResponseDTO> UpdateCollection(int itemId, int collectionId, int peopleId)
    {
        try
        {
            var item = await _itemRepository.GetQueryAll().SingleOrDefaultAsync(x => x.ItemId == itemId);
            if (item == null)
            {
                _response.IsSuccess = false;
                _response.Message = "Not found item update";
            }
            else
            {
                var collectionCheck = await _collectionRepository.GetCollection(collectionId);
                if (collectionCheck == null)
                {
                    _response.IsSuccess = false;
                    _response.Message = "Not found collection to update";
                }
                else
                {
                    item.CollectionId = collectionId;
                    item.SubmitterId = peopleId;
                    item.LastModified = DateTime.Now;
                    await _itemRepository.UpdateAsync(item);
                    _response.IsSuccess = true;
                    _response.Message = "update status of item success";
                }
            }
        }
        catch (Exception ex)
        {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
        }

        return _response;
    }

    public async Task<ResponseDTO> SearchItemByAuthor(List<string> listAuthor, int collectionId, bool? discoverable,
        SortType sortType, int pageIndex, int pageSize)
    {
        try
        {
            IQueryable<Item> queryItem = _itemRepository.GetQueryAll()
                .Include(x => x.Collection)
                .Include(x => x.MetadataValue);
            SearchItemByAuthor(ref queryItem, listAuthor);
            if (collectionId > 0)
            {
                SearchItemByCollectionId(ref queryItem, collectionId);
            }

            if (discoverable != null)
            {
                SearchItemByDiscoverable(ref queryItem, discoverable.Value);
            }

            int totalAmount = queryItem.Count();
            SortBy(ref queryItem, sortType);
            PagingItem(ref queryItem, pageIndex, pageSize);
            var objList = await queryItem.ToListAsync();
            var objmap = _mapper.Map<List<ItemDTOForSelectList>>(objList);
            objmap = await GetAuthorItem(objmap);
            decimal totalPage = Math.Ceiling((decimal)totalAmount / pageSize);

            var result = new
            {
                totalAmount,
                pageIndex,
                pageSize,
                totalPage,
                objmap
            };
            _response.IsSuccess = true;
            _response.ObjectResponse = result;
        }
        catch (Exception ex)
        {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
        }

        return _response;
    }

    public async Task<ResponseDTO> SearchItemByTitle(string title, int collectionId, bool? discoverable,
        SortType sortType, int pageIndex, int pageSize)
    {
        try
        {
            IQueryable<Item> queryItem = _itemRepository.GetQueryAll()
                .Include(x => x.Collection)
                .Include(x => x.MetadataValue);

            SearchItemByTitle(ref queryItem, title);
            if (discoverable != null)
            {
                SearchItemByDiscoverable(ref queryItem, discoverable.Value);
            }

            if (collectionId > 0)
            {
                SearchItemByCollectionId(ref queryItem, collectionId);
            }

            int totalAmount = queryItem.Count();
            SortBy(ref queryItem, sortType);
            PagingItem(ref queryItem, pageIndex, pageSize);
            var objList = await queryItem.ToListAsync();
            var objmap = _mapper.Map<List<ItemDTOForSelectList>>(objList);
            objmap = await GetAuthorItem(objmap);
            decimal totalPage = Math.Ceiling((decimal)totalAmount / pageSize);

            var result = new
            {
                totalAmount,
                pageIndex,
                pageSize,
                totalPage,
                objmap
            };
            _response.IsSuccess = true;
            _response.ObjectResponse = result;
        }
        catch (Exception ex)
        {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
        }

        return _response;
    }

    public async Task<ResponseDTO> SearchItemBySubjectKeyWords(List<string> listSubjectKeywords, int collectionId,
        bool? discoverable, SortType sortType, int pageIndex, int pageSize)
    {
        try
        {
            IQueryable<Item> queryItem = _itemRepository.GetQueryAll()
                .Include(x => x.Collection)
                .Include(x => x.MetadataValue);
            SearchItemBySubjectKeyword(ref queryItem, listSubjectKeywords);
            if (discoverable != null)
            {
                SearchItemByDiscoverable(ref queryItem, discoverable.Value);
            }

            if (collectionId > 0)
            {
                SearchItemByCollectionId(ref queryItem, collectionId);
            }

            int totalAmount = queryItem.Count();
            SortBy(ref queryItem, sortType);
            PagingItem(ref queryItem, pageIndex, pageSize);
            var objList = await queryItem.ToListAsync();
            var objmap = _mapper.Map<List<ItemDTOForSelectList>>(objList);
            objmap = await GetAuthorItem(objmap);
            decimal totalPage = Math.Ceiling((decimal)totalAmount / pageSize);

            var result = new
            {
                totalAmount,
                pageIndex,
                pageSize,
                totalPage,
                objmap
            };
            _response.IsSuccess = true;
            _response.ObjectResponse = result;
        }
        catch (Exception ex)
        {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
        }

        return _response;
    }

    public async Task<ResponseDTO> SearchItemByDate(string? year, string? month, string? date, int collectionId,
        bool? discoverable, SortType sortType, int pageIndex, int pageSize)
    {
        try
        {
            IQueryable<Item> queryItem = _itemRepository.GetQueryAll()
                .Include(x => x.Collection)
                .Include(x => x.MetadataValue);
            if (collectionId > 0)
            {
                SearchItemByCollectionId(ref queryItem, collectionId);
            }

            if (year != null && year != string.Empty)
            {
                SearchItemByYearOfDateIssue(ref queryItem, year);
            }

            if (month != null && month != string.Empty)
            {
                SearchItemByMonthOfDateIssue(ref queryItem, month);
            }

            if (date != null && date != string.Empty)
            {
                SearchItemByDayOfDateIssue(ref queryItem, date);
            }

            if (discoverable != null)
            {
                SearchItemByDiscoverable(ref queryItem, discoverable.Value);
            }

            int totalAmount = queryItem.Count();
            SortBy(ref queryItem, sortType);
            PagingItem(ref queryItem, pageIndex, pageSize);
            var objList = await queryItem.ToListAsync();
            var objmap = _mapper.Map<List<ItemDTOForSelectList>>(objList);
            objmap = await GetAuthorItem(objmap);
            decimal totalPage = Math.Ceiling((decimal)totalAmount / pageSize);

            var result = new
            {
                totalAmount,
                pageIndex,
                pageSize,
                totalPage,
                objmap
            };
            _response.IsSuccess = true;
            _response.ObjectResponse = result;
        }
        catch (Exception ex)
        {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
        }

        return _response;
    }

    public async Task<ResponseDTO> SearchItemByAuthorForUser(List<string> listAuthor, int collectionId,
        SortType sortType, int pageIndex, int pageSize)
    {
        try
        {
            IQueryable<Item> queryItem = GetQueryItemForUser();
            SearchItemByAuthor(ref queryItem, listAuthor);
            if (collectionId > 0)
            {
                SearchItemByCollectionId(ref queryItem, collectionId);
            }

            int totalAmount = queryItem.Count();
            SortBy(ref queryItem, sortType);
            PagingItem(ref queryItem, pageIndex, pageSize);
            var objList = await queryItem.ToListAsync();
            var objmap = _mapper.Map<List<ItemDTOForSelectList>>(objList);
            objmap = await GetAuthorItem(objmap);
            decimal totalPage = Math.Ceiling((decimal)totalAmount / pageSize);

            var result = new
            {
                totalAmount,
                pageIndex,
                pageSize,
                totalPage,
                objmap
            };
            _response.IsSuccess = true;
            _response.ObjectResponse = result;
        }
        catch (Exception ex)
        {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
        }

        return _response;
    }

    public async Task<ResponseDTO> SearchItemByTitleForUser(string title, int collectionId, SortType sortType,
        int pageIndex, int pageSize)
    {
        try
        {
            IQueryable<Item> queryItem = GetQueryItemForUser();
            SearchItemByTitle(ref queryItem, title);
            if (collectionId > 0)
            {
                SearchItemByCollectionId(ref queryItem, collectionId);
            }

            int totalAmount = queryItem.Count();
            SortBy(ref queryItem, sortType);
            PagingItem(ref queryItem, pageIndex, pageSize);
            var objList = await queryItem.ToListAsync();
            var objmap = _mapper.Map<List<ItemDTOForSelectList>>(objList);
            objmap = await GetAuthorItem(objmap);
            decimal totalPage = Math.Ceiling((decimal)totalAmount / pageSize);

            var result = new
            {
                totalAmount,
                pageIndex,
                pageSize,
                totalPage,
                objmap
            };
            _response.IsSuccess = true;
            _response.ObjectResponse = result;
        }
        catch (Exception ex)
        {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
        }

        return _response;
    }

    public async Task<ResponseDTO> SearchItemBySubjectKeyWordsForUser(List<string> listSubjectKeywords,
        int collectionId, SortType sortType, int pageIndex, int pageSize)
    {
        try
        {
            IQueryable<Item> queryItem = GetQueryItemForUser();
            SearchItemBySubjectKeyword(ref queryItem, listSubjectKeywords);
            if (collectionId > 0)
            {
                SearchItemByCollectionId(ref queryItem, collectionId);
            }

            int totalAmount = queryItem.Count();
            SortBy(ref queryItem, sortType);
            PagingItem(ref queryItem, pageIndex, pageSize);
            var objList = await queryItem.ToListAsync();
            var objmap = _mapper.Map<List<ItemDTOForSelectList>>(objList);
            objmap = await GetAuthorItem(objmap);
            decimal totalPage = Math.Ceiling((decimal)totalAmount / pageSize);

            var result = new
            {
                totalAmount,
                pageIndex,
                pageSize,
                totalPage,
                objmap
            };
            _response.IsSuccess = true;
            _response.ObjectResponse = result;
        }
        catch (Exception ex)
        {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
        }

        return _response;
    }

    public async Task<ResponseDTO> SearchItemByDateForUser(string? year, string? month, string? date, int collectionId,
        SortType sortType, int pageIndex, int pageSize)
    {
        try
        {
            IQueryable<Item> queryItem = GetQueryItemForUser();
            if (collectionId > 0)
            {
                SearchItemByCollectionId(ref queryItem, collectionId);
            }

            if (year != null && year != string.Empty)
            {
                SearchItemByYearOfDateIssue(ref queryItem, year);
            }

            if (month != null && month != string.Empty)
            {
                SearchItemByMonthOfDateIssue(ref queryItem, month);
            }

            if (date != null && date != string.Empty)
            {
                SearchItemByDayOfDateIssue(ref queryItem, date);
            }

            int totalAmount = queryItem.Count();
            SortBy(ref queryItem, sortType);
            PagingItem(ref queryItem, pageIndex, pageSize);
            var objList = await queryItem.ToListAsync();
            var objmap = _mapper.Map<List<ItemDTOForSelectList>>(objList);
            objmap = await GetAuthorItem(objmap);
            decimal totalPage = Math.Ceiling((decimal)totalAmount / pageSize);

            var result = new
            {
                totalAmount,
                pageIndex,
                pageSize,
                totalPage,
                objmap
            };
            _response.IsSuccess = true;
            _response.ObjectResponse = result;
        }
        catch (Exception ex)
        {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
        }

        return _response;
    }

    public async Task<ResponseDTO> SearchItemByAuthorForStaff(List<string> listAuthor, int collectionId,
        bool? discoverable, SortType sortType, int peopleId, int pageIndex, int pageSize)
    {
        try
        {
            IQueryable<Item> queryItem = GetQueryItemForAdmin();
            SearchItemByAuthor(ref queryItem, listAuthor);
            if (collectionId > 0)
            {
                SearchItemByCollectionId(ref queryItem, collectionId);
            }

            if (discoverable != null)
            {
                SearchItemByDiscoverable(ref queryItem, discoverable.Value);
            }

            int totalAmount = queryItem.Count();
            SortBy(ref queryItem, sortType);
            PagingItem(ref queryItem, pageIndex, pageSize);
            var objList = await queryItem.ToListAsync();
            var objmap = _mapper.Map<List<ItemDTOForSelectListForStaff>>(objList);

            decimal totalPage = Math.Ceiling((decimal)totalAmount / pageSize);
            objmap = await GetAuthorItemForStaff(objmap);
            var listCollectionId = await _collectionGroupRepository.GetListCollectionIdByStaffId(peopleId);

            foreach (var itemMap in objmap)
            {
                if (listCollectionId.Contains(itemMap.CollectionId))
                {
                    itemMap.CanEdit = true;
                }
            }

            var result = new
            {
                totalAmount,
                pageIndex,
                pageSize,
                totalPage,
                objmap
            };
            _response.IsSuccess = true;
            _response.ObjectResponse = result;
        }
        catch (Exception ex)
        {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
        }

        return _response;
    }

    public async Task<ResponseDTO> SearchItemByTitleForStaff(string title, int collectionId, bool? discoverable,
        SortType sortType, int peopleId, int pageIndex, int pageSize)
    {
        try
        {
            IQueryable<Item> queryItem = GetQueryItemForAdmin();

            SearchItemByTitle(ref queryItem, title);
            if (collectionId > 0)
            {
                SearchItemByCollectionId(ref queryItem, collectionId);
            }

            if (discoverable != null)
            {
                SearchItemByDiscoverable(ref queryItem, discoverable.Value);
            }

            int totalAmount = queryItem.Count();
            SortBy(ref queryItem, sortType);
            PagingItem(ref queryItem, pageIndex, pageSize);
            var objList = await queryItem.ToListAsync();
            var objmap = _mapper.Map<List<ItemDTOForSelectListForStaff>>(objList);

            decimal totalPage = Math.Ceiling((decimal)totalAmount / pageSize);
            objmap = await GetAuthorItemForStaff(objmap);
            var listCollectionId = await _collectionGroupRepository.GetListCollectionIdByStaffId(peopleId);

            foreach (var itemMap in objmap)
            {
                if (listCollectionId.Contains(itemMap.CollectionId))
                {
                    itemMap.CanEdit = true;
                }
            }

            var result = new
            {
                totalAmount,
                pageIndex,
                pageSize,
                totalPage,
                objmap
            };
            _response.IsSuccess = true;
            _response.ObjectResponse = result;
        }
        catch (Exception ex)
        {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
        }

        return _response;
    }

    public async Task<ResponseDTO> SearchItemBySubjectKeyWordsForStaff(List<string> listSubjectKeyword,
        int collectionId, bool? discoverable, SortType sortType, int peopleId, int pageIndex, int pageSize)
    {
        try
        {
            IQueryable<Item> queryItem = GetQueryItemForAdmin();

            SearchItemBySubjectKeyword(ref queryItem, listSubjectKeyword);
            if (collectionId > 0)
            {
                SearchItemByCollectionId(ref queryItem, collectionId);
            }

            if (discoverable != null)
            {
                SearchItemByDiscoverable(ref queryItem, discoverable.Value);
            }

            int totalAmount = queryItem.Count();
            SortBy(ref queryItem, sortType);
            PagingItem(ref queryItem, pageIndex, pageSize);
            var objList = await queryItem.ToListAsync();
            var objmap = _mapper.Map<List<ItemDTOForSelectListForStaff>>(objList);

            decimal totalPage = Math.Ceiling((decimal)totalAmount / pageSize);
            objmap = await GetAuthorItemForStaff(objmap);
            var listCollectionId = await _collectionGroupRepository.GetListCollectionIdByStaffId(peopleId);

            foreach (var itemMap in objmap)
            {
                if (listCollectionId.Contains(itemMap.CollectionId))
                {
                    itemMap.CanEdit = true;
                }
            }

            var result = new
            {
                totalAmount,
                pageIndex,
                pageSize,
                totalPage,
                objmap
            };
            _response.IsSuccess = true;
            _response.ObjectResponse = result;
        }
        catch (Exception ex)
        {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
        }

        return _response;
    }

    public async Task<ResponseDTO> SearchItemByDateForStaff(string? year, string? month, string? date, int collectionId,
        bool? discoverable, SortType sortType, int peopleId, int pageIndex, int pageSize)
    {
        try
        {
            IQueryable<Item> queryItem = GetQueryItemForAdmin();
            if (collectionId > 0)
            {
                SearchItemByCollectionId(ref queryItem, collectionId);
            }

            if (year != null && year != string.Empty)
            {
                SearchItemByYearOfDateIssue(ref queryItem, year);
            }

            if (month != null && month != string.Empty)
            {
                SearchItemByMonthOfDateIssue(ref queryItem, month);
            }

            if (date != null && date != string.Empty)
            {
                SearchItemByDayOfDateIssue(ref queryItem, date);
            }

            if (discoverable != null)
            {
                SearchItemByDiscoverable(ref queryItem, discoverable.Value);
            }

            int totalAmount = queryItem.Count();
            SortBy(ref queryItem, sortType);
            PagingItem(ref queryItem, pageIndex, pageSize);
            var objList = await queryItem.ToListAsync();

            var objmap = _mapper.Map<List<ItemDTOForSelectListForStaff>>(objList);

            decimal totalPage = Math.Ceiling((decimal)totalAmount / pageSize);
            objmap = await GetAuthorItemForStaff(objmap);
            var listCollectionId = await _collectionGroupRepository.GetListCollectionIdByStaffId(peopleId);

            foreach (var itemMap in objmap)
            {
                if (listCollectionId.Contains(itemMap.CollectionId))
                {
                    itemMap.CanEdit = true;
                }
            }

            var result = new
            {
                totalAmount,
                pageIndex,
                pageSize,
                totalPage,
                objmap
            };
            _response.IsSuccess = true;
            _response.ObjectResponse = result;
        }
        catch (Exception ex)
        {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
        }

        return _response;
    }
}