using Application.CollectionGroups;
using Application.Collections;
using Application.Responses;
using AutoMapper;
using Domain;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using Infrastructure.Repositories.CollectionGroupRepositories;
using Infrastructure.Repositories.CollectionRepositories;
using Infrastructure.Repositories.CommunityGroupRepositories;
using Infrastructure.Repositories.PeopleRepositories;
using Infrastructure.Repositories.StatisticRepositories;
using Infrastructure.Repositories.SubscribeRepositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

namespace Infrastructure.Services.Implements
{
    public class CollectionService : ICollectionService
    {
        public IMapper _mapper;
        private ICollectionRepository _collectionRepository;
        private static readonly string[] Scopes = new[] { DriveService.Scope.DriveFile };
        protected ResponseDTO _response;
        private ICollectionGroupRepository _collectionGroupRepository;
        private ISubscribeRepository _subscribeRepository;
        private IStatisticRepository _statisticRepository;
        private IPeopleRepository _peopleRepository;
        private ICommunityGroupRepository _communityGroupRepository;

        public CollectionService(IMapper mapper,
            ICollectionRepository collectionRepository,
            ICollectionGroupRepository collectionGroupRepository,
            ISubscribeRepository subscribeService,
            IStatisticRepository statisticRepository,
            IPeopleRepository peopleRepository,
            ICommunityGroupRepository communityGroupRepository
        )
        {
            _collectionGroupRepository = collectionGroupRepository;
            _communityGroupRepository = communityGroupRepository;
            _statisticRepository = statisticRepository;
            _collectionRepository = collectionRepository;
            _response = new ResponseDTO();
            _mapper = mapper;

            _subscribeRepository = subscribeService;
            _peopleRepository = peopleRepository;
        }

        public async Task<ResponseDTO> CreateCollection(CollectionDTOForCreateOrUpdate collectionDTO, int userCreateId)
        {
            try
            {
                Collection collection = new Collection();
                collection.CollectionName = collectionDTO.CollectionName;
                collection.LogoUrl = collectionDTO.LogoUrl;
                collection.CommunityId = collectionDTO.CommunityId;
                collection.ShortDescription = collectionDTO.ShortDescription;
                collection.CreateBy = userCreateId;
                collection.UpdateBy = userCreateId;
                collection.isActive = collectionDTO.isActive;
                collection.License = collectionDTO.License;
                collection.EntityTypeId = collectionDTO.EntityTypeId;
                collection.FolderId = await CreateFolder(GetUserCredential(), collectionDTO.CollectionName);
                collection.CreateTime = DateTime.Now;
                collection.UpdateTime = DateTime.Now;
                await _collectionRepository.AddAsync(collection);
                var listGroup = await _communityGroupRepository.GetQueryAll()
                    .Where(x => x.CommunityId == collectionDTO.CommunityId).Select(x => x.GroupId).ToListAsync();
                if (listGroup.Count() > 0)
                {
                    List<CollectionGroup> collectionGroups = new List<CollectionGroup>();

                    foreach (var groupId in listGroup)
                    {
                        var collectionGroup = new CollectionGroup();
                        collectionGroup.GroupId = groupId;
                        collectionGroup.CollectionId = collection.CollectionId;
                        collectionGroups.Add(collectionGroup);
                    }

                    await _collectionGroupRepository.AddListAsync(collectionGroups);
                }

                if (collectionDTO.File != null)
                {
                    await using (var stream = new FileStream(collectionDTO.LogoUrl, FileMode.Create))
                    {
                        await collectionDTO.File.CopyToAsync(stream);
                    }
                }

                _response.IsSuccess = true;
                _response.Message = "Add Collection success";
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }

            return _response;
        }

        public async Task<ResponseDTO> CreateCollectionForStaff(CollectionDTOForCreateOrUpdate collectionDTO,
            int userCreateId)
        {
            try
            {
                var listCommunityId = await _communityGroupRepository.GetListCommunityIdByStaff(userCreateId);
                if (listCommunityId.Count() == 0 || !listCommunityId.Contains(collectionDTO.CommunityId))
                {
                    _response.IsSuccess = false;
                    _response.Message = "Add colection fail";
                    return _response;
                }

                return await CreateCollection(collectionDTO, userCreateId);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }

            return _response;
        }

        public async Task<ResponseDTO> UpdateCollectionForStaff(int collectionId,
            CollectionDTOForCreateOrUpdate collectionDTO, int userCreateId)
        {
            try
            {
                var listCommunityId = await _communityGroupRepository.GetListCommunityIdByStaff(userCreateId);
                if (listCommunityId.Count() == 0 || !listCommunityId.Contains(collectionDTO.CommunityId))
                {
                    if(_collectionRepository.GetCollection(collectionId).Result.CommunityId == collectionDTO.CommunityId)
                    {
                        return await UpdateCollection(collectionId, collectionDTO, userCreateId);
                    }
                    _response.IsSuccess = false;
                    _response.Message = "update colection fail";
                    return _response;
                }

                return await UpdateCollection(collectionId, collectionDTO, userCreateId);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }

            return _response;
        }

        public async Task<ResponseDTO> DeleteCollectionForStaff(int collectionId, int userCreateId)
        {
            try
            {
                var listCommunityId = await _communityGroupRepository.GetListCommunityIdByStaff(userCreateId);
                var collection = await _collectionRepository.GetCollection(collectionId);
                if (collection == null)
                {
                    _response.IsSuccess = false;
                    _response.Message = "update colection fail";
                    return _response;
                }

                if (listCommunityId.Count() == 0 || !listCommunityId.Contains(collection.CommunityId))
                {
                    _response.IsSuccess = false;
                    _response.Message = "update colection fail";
                    return _response;
                }

                return await DeleteCollection(collectionId);
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }

            return _response;
        }

        public async Task<string> CreateFolder(Task<UserCredential> credential, string FolderName)
        {
            DriveService service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = await credential
            });

            Google.Apis.Drive.v3.Data.File FileMetaData = new Google.Apis.Drive.v3.Data.File()
            {
                Name = FolderName,
                MimeType = "application/vnd.google-apps.folder",
                Parents = new List<string>()
                {
                    "19db_KTeQlQcl4fre2BMyRc80-ydKd7gk"
                }
            };

            var request = service.Files.Create(FileMetaData);
            request.Fields = "id";
            var folder = await request.ExecuteAsync();
            return folder.Id;
        }

        public async Task<UserCredential> GetUserCredential()
        {
            UserCredential credential = null;
            using (var stream =
                   new FileStream(
                       "client_secret_336699035226-8c0s3uv2pee71adqarpuiicagplfpkvj.apps.googleusercontent.com.json",
                       FileMode.Open, FileAccess.ReadWrite))
            {
                credential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.FromStream(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore("DSpace v2.0", true));
            }

            return credential;
        }

        public async Task<ResponseDTO> DeleteCollection(int collectionId)
        {
            try
            {
                var check = await _collectionRepository.GetQueryAll().Include(x => x.CollectionGroups)
                    .Include(x => x.Items).SingleOrDefaultAsync(x => x.CollectionId == collectionId);
                var statsCollection = await _statisticRepository.GetQueryAll().Include(x => x.Collection)
                    .Where(x => x.CollectionId == collectionId).ToListAsync();

                if (check == null)
                {
                    _response.IsSuccess = true;
                    _response.Message = "Collection doesnot exist";
                }
                else
                {
                    if (check.Items.Count != 0)
                    {
                        _response.IsSuccess = false;
                        _response.Message =
                            "Collection have item, if you want delete collection please delete all item in the collection";
                    }
                    else
                    {
                        if (!statsCollection.IsNullOrEmpty())
                        {
                            await _statisticRepository.DeleteListAsync(statsCollection);
                        }

                        if (check.CollectionGroups.Count() > 0)
                        {
                            await _collectionGroupRepository.DeleteListAsync(check.CollectionGroups);
                        }

                        await _collectionRepository.DeleteAsync(check);

                        _response.IsSuccess = true;
                        _response.Message = "Delete Collection success";
                    }
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }

            return _response;
        }

        public async Task<ResponseDTO> GetCollectionByID(int id)
        {
            try
            {
                var collection = await _collectionRepository.GetQueryAll()
                    .Include(x => x.EntityType)
                    .Include(x => x.Community)
                    .Include(x => x.People)
                    .ThenInclude(x => x.User)
                    .Include(x => x.Items)
                    .Include(x => x.CollectionGroups)
                    .ThenInclude(x => x.Group)
                    .SingleOrDefaultAsync(x => x.CollectionId == id);
                if (collection == null)
                {
                    _response.IsSuccess = true;
                    _response.Message = "Collection with id " + id + " does not exist";
                }
                else
                {
                    var result = _mapper.Map<CollectionDTOForDetail>(collection);
                    result.CollectionGroupDTOForDetails =
                        _mapper.Map<List<CollectionGroupDTOForDetail>>(collection.CollectionGroups);
                    if (collection.UpdateBy != null)
                    {
                        var peopleUpdateBy = await _peopleRepository.GetQueryAll().Include(x => x.User)
                            .SingleOrDefaultAsync(x => x.PeopleId == collection.UpdateBy.Value);
                        result.UpdateBy = peopleUpdateBy.User.Email;
                    }

                    _response.IsSuccess = true;
                    _response.Message = "Get Collection success";
                    _response.ObjectResponse = result;
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }

            return _response;
        }

        public async Task<ResponseDTO> GetCollectionByName(string name)
        {
            try
            {
                IEnumerable<Collection> objList = await _collectionRepository.GetQueryAll()
                    .Where(x => x.CollectionName.Contains(name)).Include(x => x.Community).Include(x => x.Items)
                    .ToListAsync();
                _response.IsSuccess = true;
                _response.ObjectResponse = _mapper.Map<List<CollectionDTOForSelectList>>(objList);
            }
            catch (Exception ex)
            {
                _response.Message = ex.Message;
                _response.IsSuccess = false;
            }

            return _response;
        }

        public async Task<ResponseDTO> UpdateCollection(int id, CollectionDTOForCreateOrUpdate collectionDTO,
            int userUpdateId)
        {
            try
            {
                var check = await _collectionRepository.GetCollection(id);
                if (check == null)
                {
                    _response.IsSuccess = true;
                    _response.Message = "Collection with id " + id + " does not exist";
                }
                else
                {
                    check.CollectionName = collectionDTO.CollectionName;
                    check.LogoUrl = collectionDTO.LogoUrl;
                    check.UpdateTime = DateTime.Now;
                    check.ShortDescription = collectionDTO.ShortDescription;
                    check.UpdateBy = userUpdateId;
                    check.isActive = collectionDTO.isActive;
                    check.License = collectionDTO.License;
                    check.CommunityId = collectionDTO.CommunityId;
                    await _collectionRepository.UpdateAsync(check);

                    if (collectionDTO.File != null)
                    {
                        await using (var stream = new FileStream(collectionDTO.LogoUrl, FileMode.Create))
                        {
                            await collectionDTO.File.CopyToAsync(stream);
                        }
                    }

                    _response.IsSuccess = true;
                    _response.Message = "Update Collection success";
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }

            return _response;
        }

        public async Task<ResponseDTO> GetAllCollection()
        {
            try
            {
                IEnumerable<Collection> objList = await _collectionRepository.GetQueryAll().Include(x => x.Community)
                    .Include(x => x.People).ThenInclude(x => x.User).Include(x => x.Items).ToListAsync();

                _response.IsSuccess = true;
                _response.ObjectResponse = _mapper.Map<List<CollectionDTOForSelectList>>(objList);
            }
            catch (Exception ex)
            {
                _response.Message = ex.Message;
                _response.IsSuccess = false;
            }

            return _response;
        }

        public async Task<ResponseDTO> GetCollectionByCommunityId(int communityId)
        {
            try
            {
                IEnumerable<Collection> objList = await _collectionRepository.GetQueryAll()
                    .Where(x => x.CommunityId == communityId)
                    .Include(x => x.Items)
                    .Include(x => x.Community)
                    .Include(x => x.EntityType)
                    .Include(x => x.People)
                    .ThenInclude(x => x.User).ToListAsync();
                _response.IsSuccess = true;
                _response.ObjectResponse = _mapper.Map<List<CollectionDTOForSelectList>>(objList);
            }
            catch (Exception ex)
            {
                _response.Message = ex.Message;
                _response.IsSuccess = false;
            }

            return _response;
        }

        public async Task<ResponseDTO> GetCollectionByIDForUser(int id, string email)
        {
            try
            {
                var Collection = await _collectionRepository.GetQueryAll().Where(x => x.isActive)
               .Include(x=>x.Community)
                    .SingleOrDefaultAsync(x => x.CollectionId == id);
                if (Collection == null)
                {
                    _response.IsSuccess = true;
                    _response.Message = "Collection with id " + id + " does not exist";
                }
                else
                {
                    _response.IsSuccess = true;
                    _response.Message = "Get Collection success";
                    var objMap = _mapper.Map<CollectionDTOForSelectOfUser>(Collection);

                    var subcribeCheck = await _subscribeRepository.CheckSubscribe(id, email);
                    objMap.IsSubcribed = subcribeCheck;
                    _response.ObjectResponse = objMap;
                }
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }

            return _response;
        }

        public async Task<ResponseDTO> GetCollectionByNameForUser(string name, string email)
        {
            try
            {
                IEnumerable<Collection> objList = await _collectionRepository.GetQueryAll()
                    .Where(x => x.CollectionName.Contains(name) && x.isActive).Include(x => x.Community).ToListAsync();
                var objMap = _mapper.Map<List<CollectionDTOForSelectOfUser>>(objList);
                var listSubscribeCollectionId = await _subscribeRepository.GetAllSubscribedCollectionId(email);
                foreach (var collectionMap in objMap)
                {
                    collectionMap.IsSubcribed = listSubscribeCollectionId.Contains(collectionMap.CollectionId);
                }

                _response.IsSuccess = true;
                _response.ObjectResponse = objMap;
            }
            catch (Exception ex)
            {
                _response.Message = ex.Message;
                _response.IsSuccess = false;
            }

            return _response;
        }

        public async Task<ResponseDTO> GetAllCollectionForUser(string email)
        {
            try
            {
                IEnumerable<Collection> objList = await _collectionRepository.GetQueryAll().Where(x => x.isActive)
                    .Include(x => x.Community).ToListAsync();
                var objMap = _mapper.Map<List<CollectionDTOForSelectOfUser>>(objList);
                var listSubscribeCollectionId = await _subscribeRepository.GetAllSubscribedCollectionId(email);
                foreach (var collectionMap in objMap)
                {
                    collectionMap.IsSubcribed = listSubscribeCollectionId.Contains(collectionMap.CollectionId);
                }

                _response.IsSuccess = true;
                _response.ObjectResponse = objMap;
            }
            catch (Exception ex)
            {
                _response.Message = ex.Message;
                _response.IsSuccess = false;
            }

            return _response;
        }

        public async Task<ResponseDTO> GetAllCollectionByStaff(int peopleId)
        {
            try
            {
                List<int> listCollectionId = await _collectionGroupRepository.GetListCollectionIdByStaffId(peopleId);


                IEnumerable<Collection> listCollection = await _collectionRepository.GetQueryAll()
                    .Include(x => x.Community)
                    .Include(x => x.People)
                    .ThenInclude(x => x.User)
                    .Include(x => x.Items)
                    .ToListAsync();
                var listColectionMap = _mapper.Map<List<CollectionDTOForSelectListForStaff>>(listCollection);
                foreach (var collectionMap in listColectionMap)
                {
                    if (listCollectionId.Contains(collectionMap.CollectionId))
                    {
                        collectionMap.CanEdit = true;
                    }
                }

                _response.IsSuccess = true;
                _response.ObjectResponse = listColectionMap;
            }
            catch (Exception ex)
            {
                _response.Message = ex.Message;
                _response.IsSuccess = false;
            }

            return _response;
        }

        public async Task<ResponseDTO> GetCollectionByCollectionIdAndPeopleId(int collectionId, int peopleId)
        {
            try
            {
                List<int> listCollectionId = await _collectionGroupRepository.GetListCollectionIdByStaffId(peopleId);
                List<int> listCommunitiesId = await _communityGroupRepository.GetListCommunityIdByStaff(peopleId);

                var collection = await _collectionRepository.GetQueryAll()
                    .Include(x => x.EntityType)
                    .Include(x => x.Community)
                    .Include(x => x.People)
                    .ThenInclude(x => x.User)
                    .Include(x => x.Items)
                    .Include(x => x.CollectionGroups)
                    .ThenInclude(x => x.Group)
                    .SingleOrDefaultAsync(x => x.CollectionId == collectionId);
                if (collection == null)
                {
                    _response.IsSuccess = false;
                    _response.Message = "Not Found";
                    return _response;
                }

                var collectionMap = _mapper.Map<CollectionDTOForDetailForStaff>(collection);
                if (collection.UpdateBy != null)
                {
                    var peopleUpdateBy = await _peopleRepository.GetPeople(peopleId);
                    collectionMap.UpdateBy = peopleUpdateBy.User.Email;
                }

                if (listCollectionId.Contains(collectionMap.CollectionId))
                {
                    collectionMap.CanEdit = true;
                }
                if (listCommunitiesId.Contains(collectionMap.CommunityId))
                {
                    collectionMap.CommunityDTOForSelect.canEdit = true;
                }
                _response.IsSuccess = true;
                _response.ObjectResponse = collectionMap;
            }
            catch (Exception ex)
            {
                _response.Message = ex.Message;
                _response.IsSuccess = false;
            }

            return _response;
        }
    }
}