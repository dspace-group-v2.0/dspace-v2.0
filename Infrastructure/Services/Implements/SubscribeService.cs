﻿using Application.Collections;
using Application.CollectionSubcribes;
using Application.Responses;
using AutoMapper;
using Domain;
using Infrastructure.Repositories.CollectionRepositories;
using Infrastructure.Repositories.SubscribeRepositories;
using Infrastructure.Repositories.UserRepositories;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Services.Implements
{
   public class SubscribeService : ISubscribeService
   {

      private IMapper _mapper;
      private IUserRepository _userRepository;
      private ResponseDTO _response;
      private ISubscribeRepository _subscribeRepository;
      private ICollectionRepository _collectionRepository;

      public SubscribeService(IMapper mapper, IUserRepository userRepository, ISubscribeRepository subscribeRepository, ICollectionRepository collectionRepository)
      {
         _mapper = mapper;
         _userRepository = userRepository;
         _response = new ResponseDTO();
         _subscribeRepository = subscribeRepository;
         _collectionRepository = collectionRepository;
      }

      public async Task<ResponseDTO> UnSubcribeCollection(int collectionId, string userEmail)
      {
         try
         {
            var userCheck = await _userRepository.getUserByEmail(userEmail);
            var subcribeCheck = await _subscribeRepository.GetQueryAll().SingleOrDefaultAsync(x => x.CollectionId.Equals(collectionId) && x.UserId == userCheck.Id);
            if (subcribeCheck == null)
            {
               _response.IsSuccess = false;
               _response.Message = "You have already unsubscribed this collection";
            }
            else
            {
               await _subscribeRepository.DeleteAsync(subcribeCheck);

               _response.IsSuccess = true;
               _response.Message = "You have just unsubscribed this collection";
            }
         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }
         return _response;
      }

      public async Task<ResponseDTO> SubcribeCollection(int collectionId, string userEmail)
      {
         try
         {
            var collectionCheck = await _collectionRepository.GetCollection(collectionId);
            var userCheck = await _userRepository.getUserByEmail(userEmail);
            var subcribeCheck = await CheckSubcribe(collectionId, userEmail);
            if (collectionCheck == null || userCheck == null || subcribeCheck)
            {
               _response.IsSuccess = false;
               if (collectionCheck == null)
               {
                  _response.Message = "Collection does not exist";
               }
               if (userCheck == null)
               {
                  _response.Message = "User does not log in";
               }
               if (subcribeCheck)
               {
                  _response.Message = "You have already subscribed this collection";
               }
            }
            else
            {
               Subscribe subscribeNew = new Subscribe();
               subscribeNew.CollectionId = collectionId;
               subscribeNew.UserId = userCheck.Id;
               await _subscribeRepository.AddAsync(subscribeNew);
               _response.IsSuccess = true;
               _response.Message = "You have just subscribed this collection";
            }

         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }
         return _response;
      }

      public async Task<ResponseDTO> ViewListSubcribe()
      {
         try
         {
            var subcribes = await _collectionRepository.GetQueryAll().Include(x => x.Subscribes).Select(y => new SubcribeDTO
            {
               CollectionId = y.CollectionId,
               CollectionName = y.CollectionName,
               SubcribeAmount = y.Subscribes.Count()
            }).ToListAsync();
            _response.IsSuccess = true;
            _response.ObjectResponse = subcribes;
         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }
         return _response;
      }

      public async Task<List<int>> GetAllCollectionIdByUserEmail(string userEmail)
      {
         try
         {
            var userCheck = await _userRepository.getUserByEmail(userEmail);
            var subcribes = await _subscribeRepository.GetQueryAll().Where(x => x.UserId == userCheck.Id).Select(x => x.CollectionId).ToListAsync();
            return subcribes;
         }
         catch (Exception ex)
         {
            throw new Exception(ex.Message);
         }

      }

      public async Task<bool> CheckSubcribe(int collectionId, string email)
      {
         try
         {
            var userCheck = await _userRepository.getUserByEmail(email);
            var subcribes = await _subscribeRepository.GetQueryAll().SingleOrDefaultAsync(x => x.UserId == userCheck.Id && x.CollectionId == collectionId);
            if (subcribes == null)
            {
               return false;
            }
            return true;
         }
         catch (Exception ex)
         {
            throw new Exception(ex.Message);
         }

      }

      public async Task<ResponseDTO> ViewListSubcribeForUser(string email)
      {
         try
         {
            var userCheck = await _userRepository.getUserByEmail(email);
            var collections = await _subscribeRepository.GetQueryAll().Where(x => x.UserId == userCheck.Id && x.Collection.isActive).Select(x => x.Collection).ToListAsync();
            var objMap = _mapper.Map<List<CollectionDTOForGetListSubcribedForUser>>(collections);

            _response.IsSuccess = true;
            _response.ObjectResponse = objMap;


         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }
         return _response;
      }

      public async Task<IEnumerable<string>> GetListUserEmailSubcribeCollection(int collectionId)
      {
         try
         {
            IEnumerable<string> listEmailSubcribed = await _subscribeRepository.GetQueryAll().Include(x => x.User).Where(x => x.CollectionId == collectionId).Select(x => x.User.Email).ToListAsync();
            return listEmailSubcribed;

         }
         catch (Exception ex)
         {
            throw new Exception(ex.Message);
         }

      }
   }
}
