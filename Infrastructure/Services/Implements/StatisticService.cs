﻿using Application.Responses;
using Application.Statistics;
using AutoMapper;
using Domain;
using Infrastructure;
using Infrastructure.Repositories.ItemRepositories;
using Infrastructure.Repositories.StatisticRepositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

namespace Infrastructure.Services.Implements
{
   public class StatisticService : IStatisticService
   {
      private IStatisticRepository _statisticRepository;
      protected IMapper _mapper;
      protected ResponseDTO _response;
      private IItemRepository _itemRepository;


      public StatisticService(IStatisticRepository statisticRepository, IMapper mapper,IItemRepository itemRepository)
      {
         _statisticRepository = statisticRepository;
         _mapper = mapper;
         _itemRepository = itemRepository;
         _response = new ResponseDTO();
      }

      public async Task<ResponseDTO> GetAllStatistics()
      {
         try
         {
            var statistics = await _statisticRepository.GetAll();

            var listItem = statistics.GroupBy(x => x.ItemId).ToList();

            var listItemId = listItem.Select(x => x.Key).ToList();

            List<StatisticDTOForSelectList> result = new List<StatisticDTOForSelectList>();

            foreach (var itemId in listItemId)
            {
               var itemStatic = await GetAllStatisticByItemId2(itemId);
               result.Add(itemStatic);
            }
            _response.IsSuccess = true;
            _response.ObjectResponse = result;

         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }
         return _response;
      }

     
      public async Task<ResponseDTO> GetAllStatisticByItemId(int itemId)
      {
         try
         {
            StatisticDTOForSelectList result = new StatisticDTOForSelectList();
            var statistic = await _statisticRepository.GetQueryAll().Where(x => x.ItemId == itemId).ToListAsync();


            var item = await _itemRepository.GetQueryAll().Include(x => x.MetadataValue).FirstOrDefaultAsync(x => x.ItemId == itemId);

            result.ItemTitle = item.MetadataValue.FirstOrDefault(x => x.MetadataFieldId == 57) == null ?
               "" : item.MetadataValue.FirstOrDefault(x => x.MetadataFieldId == 57).TextValue;
            result.Statistics = _mapper.Map<List<StatisticDTOForSelect>>(statistic);
            result.TotalView = 0;
            foreach (var s in result.Statistics)
            {
               result.TotalView += s.ViewCount;
            }

            _response.IsSuccess = true;
            _response.ObjectResponse = result;
         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }
         return _response;

      }
      public async Task<StatisticDTOForSelectList> GetAllStatisticByItemId2(int? itemId)
      {
         try
         {
            StatisticDTOForSelectList result = new StatisticDTOForSelectList();
            var statistic = await _statisticRepository.GetQueryAll().Where(x => x.ItemId == itemId).ToListAsync();
            var item = await _itemRepository.GetQueryAll().Include(x => x.MetadataValue).FirstOrDefaultAsync(x => x.ItemId == itemId);
            result.ItemTitle = item.MetadataValue.FirstOrDefault(x => x.MetadataFieldId == 57) == null ?
               "" : item.MetadataValue.FirstOrDefault(x => x.MetadataFieldId == 57).TextValue;
            result.Statistics = _mapper.Map<List<StatisticDTOForSelect>>(statistic);
            result.TotalView = 0;
            foreach (var s in result.Statistics)
            {
               result.TotalView += s.ViewCount;
            }
            return result;
         }
         catch (Exception ex)
         {
            throw new Exception(ex.Message);
         }
      }

      public async Task<ResponseDTO> UploadStatistic(List<StatisticDTOForCreate> listCounterViewer)
      {
         try
         {
            var currentStatistic = await _statisticRepository.GetAll();
            if (currentStatistic.IsNullOrEmpty())
            {
               foreach (var counterADay in listCounterViewer)
               {
                  await _statisticRepository.AddAsync(_mapper.Map<Statistic>(counterADay));
               }
            }
            else
            {
               foreach (var counterADay in listCounterViewer)
               {
                  foreach (var specificStatistic in currentStatistic)
                  {
                     if (counterADay.ItemId == specificStatistic.ItemId)
                     {
                        specificStatistic.ViewOnDay = counterADay.ViewOnDay;
                     }
                     else if (counterADay.ItemId != specificStatistic.ItemId)
                     {
                        await _statisticRepository.AddAsync(_mapper.Map<Statistic>(counterADay));
                     }
                  }
               }
            }
            
            _response.IsSuccess = true;
            _response.ObjectResponse = listCounterViewer;
         }
         catch (Exception e)
         {
            _response.IsSuccess = false;
            _response.Message = e.Message;
         }
         return _response;
      }
      
      public async Task<ResponseDTO> UploadStatisticIndividual(StatisticDTOForCreateIndividual counterViewer, string userId)
      {
         try
         {
            var currentStatistic = await _statisticRepository.GetQueryAll().SingleOrDefaultAsync(s => 
               s.Year == counterViewer.Year
               && s.Month == counterViewer.Month
               && s.Day == counterViewer.Day 
               && s.CollectionId == counterViewer.CollectionId
               && s.CommunityId == counterViewer.CommunityId
               && s.ItemId == counterViewer.ItemId
               && s.UserId == userId);
            if (currentStatistic == null)
            {
               counterViewer.UserId = userId;
               await _statisticRepository.AddAsync(_mapper.Map<Statistic>(counterViewer));
            }
            else
            {
               currentStatistic.ViewOnDay += counterViewer.ViewOnDay;
               await _statisticRepository.UpdateAsync(currentStatistic);
            }
            
            _response.IsSuccess = true;
            _response.ObjectResponse = counterViewer;
         }
         catch (Exception e)
         {
            _response.IsSuccess = false;
            _response.Message = e.Message;
         }
         return _response;
      }

      public async Task<ResponseDTO> GetStatisticForItem(int itemId)
      {
         try
         {
            var statisticItemResult = await _statisticRepository.GetQueryAll()
               .Where(x => x.ItemId == itemId && x.CommunityId.HasValue && x.CollectionId.HasValue)
               .GroupBy(s => new { s.Month, s.Year, s.ItemId, s.CommunityId, s.CollectionId,})
               .Select(g => new StatisticDTO
               { 
                  CommunityId = g.Key.CommunityId,
                  CollectionId = g.Key.CollectionId,
                  ItemId = g.Key.ItemId,
                  Month = g.Key.Month,
                  Year = g.Key.Year,
                  TotalCount = g.Sum(s => s.ViewOnDay),
               })
               .OrderBy(x => x.Year)
               .ThenBy(x => x.Month)
               .ToListAsync();
            var statsForCollection = new StatisticDTOForItem()
            {
               Item = statisticItemResult
            };
            _response.IsSuccess = true;
            _response.ObjectResponse = statsForCollection;
         }
         catch (Exception e)
         {
            _response.IsSuccess = false;
            _response.Message = e.Message;
         }
         return _response;
      }

      public async Task<ResponseDTO> GetStatisticForCollection(int collectionId)
      {
         try
         {
            var statisticItemResult = await _statisticRepository.GetQueryAll()
               .Where(x => x.CollectionId == collectionId && x.CommunityId.HasValue &&  x.ItemId.HasValue)
               .GroupBy(s => new { s.Month, s.Year, s.CommunityId, s.CollectionId})
               .Select(g => new StatisticDTO
               {
                  CommunityId = g.Key.CommunityId,
                  CollectionId = g.Key.CollectionId,
                  Month = g.Key.Month,
                  Year = g.Key.Year,
                  TotalCount = g.Sum(s => s.ViewOnDay),
               })
               .OrderBy(x => x.Year)
               .ThenBy(x => x.Month)
               .ToListAsync();
            
            var statisticCollectionResult = await _statisticRepository.GetQueryAll()
               .Where(x => x.CollectionId == collectionId && x.CommunityId.HasValue && x.ItemId == null)
               .GroupBy(s => new { s.Month, s.Year, s.CommunityId, s.CollectionId })
               .Select(g => new StatisticDTO
               {
                  CommunityId = g.Key.CommunityId,
                  CollectionId = g.Key.CollectionId,
                  Month = g.Key.Month,
                  Year = g.Key.Year,
                  TotalCount = g.Sum(s => s.ViewOnDay),
               })
               .OrderBy(x => x.Year)
               .ThenBy(x => x.Month)
               .ToListAsync();
            var statsForCollection = new StatisticDTOForCollection()
            {
               Collection = statisticCollectionResult,
               Item = statisticItemResult
            };
            _response.IsSuccess = true;
            _response.ObjectResponse = statsForCollection;
         }
         catch (Exception e)
         {
            _response.IsSuccess = false;
            _response.Message = e.Message;
         }
         return _response;
      }

      public async Task<ResponseDTO> GetStatisticForCommunity(int communityId)
      {
         try
         {
            var statisticCommunityResult = await _statisticRepository.GetQueryAll()
               .Where(x => x.CommunityId == communityId && x.CollectionId == null && x.ItemId == null)
               .GroupBy(s => new { s.Month, s.Year, s.CommunityId })
               .Select(g => new StatisticDTO
               {
                  CommunityId = g.Key.CommunityId,
                  Month = g.Key.Month,
                  Year = g.Key.Year,
                  TotalCount = g.Sum(s => s.ViewOnDay),
               })
               .OrderBy(x => x.Year)
               .ThenBy(x => x.Month)
               .ToListAsync();
            var statisticCollectionResult = await _statisticRepository.GetQueryAll()
               .Where(x => x.CommunityId == communityId && x.CollectionId.HasValue && x.ItemId == null)
               .GroupBy(s => new { s.Month, s.Year, s.CommunityId })
               .Select(g => new StatisticDTO
               {
                  CommunityId = g.Key.CommunityId,
                  Month = g.Key.Month,
                  Year = g.Key.Year,
                  TotalCount = g.Sum(s => s.ViewOnDay),
               })
               .OrderBy(x => x.Year)
               .ThenBy(x => x.Month)
               .ToListAsync();
            var statisticItemResult = await _statisticRepository.GetQueryAll()
               .Where(x => x.CommunityId == communityId && x.CollectionId.HasValue &&  x.ItemId.HasValue)
               .GroupBy(s => new { s.Month, s.Year, s.CommunityId })
               .Select(g => new StatisticDTO
               {
                  CommunityId = g.Key.CommunityId,
                  Month = g.Key.Month,
                  Year = g.Key.Year,
                  TotalCount = g.Sum(s => s.ViewOnDay),
               })
               .OrderBy(x => x.Year)
               .ThenBy(x => x.Month)
               .ToListAsync();
            var statsForCommunity = new StatisticDTOForCommunity()
            {
               Community = statisticCommunityResult,
               Collection = statisticCollectionResult,
               Item = statisticItemResult
            };
            _response.IsSuccess = true;
            _response.ObjectResponse = statsForCommunity;
         }
         catch (Exception e)
         {
            _response.IsSuccess = false;
            _response.Message = e.Message;
         }
         return _response;
      }
   }
}
