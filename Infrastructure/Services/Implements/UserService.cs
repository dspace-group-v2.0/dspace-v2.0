﻿using Application.LoginRequest;
using Application.Responses;
using AutoMapper;
using Domain;
using Infrastructure;
using Infrastructure.Repositories.UserRepositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Shared.Constants;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Infrastructure.Services.Implements
{
   public class UserService : IUserService
   {
      private readonly IMapper _mapper;

      private UserManager<User> _userManager;
      private RoleManager<Role> _roleManager;
      private IUserRepository _userRepository;
      private readonly SignInManager<User> _signInManager;
      private ResponseDTO _response;
      public IConfiguration _configuration;
      public string _jwtKey;
      public string _jwtAudience;
      public string _jwtIssuer;
      public int _jwtExpires;

      public UserService(IMapper mapper, UserManager<User> userManager, IUserRepository userRepository, SignInManager<User> signInManager, RoleManager<Role> roleManager, IConfiguration configuration)
      {
         _mapper = mapper;
         _userManager = userManager;
         _userRepository = userRepository;
         _signInManager = signInManager;
         _roleManager = roleManager;
         _configuration = configuration;
         _response = new ResponseDTO();
         _jwtKey = _configuration["JWT:Secret"];
         _jwtAudience = _configuration["JWT:ValidAudience"];
         _jwtIssuer = _configuration["JWT:ValidIssuer"];
         _jwtExpires = Int32.Parse(_configuration["JWT:Expires"]);

      }

      public async Task<bool> AssignRole(User user, string role)
      {
         try
         {
            IList<string> roles = await _userManager.GetRolesAsync(user);
            if (roles != null)
            {
               var removeRoleResult = await _userManager.RemoveFromRolesAsync(user, roles);
               if (!removeRoleResult.Succeeded)
               {
                  throw new Exception();
               }
            }


            var result = await _userManager.AddToRoleAsync(user, role);
            return result.Succeeded;
         }
         catch (Exception ex)
         {

            return false;
         }
      }

      public User CreateInstanceUser()
      {
         try
         {
            return Activator.CreateInstance<User>();
         }
         catch
         {
            throw new InvalidOperationException($"Can't create an instance of '{nameof(User)}'. ");
         }
      }

      public async Task<string> GetRole(User user)
      {
         try
         {
            var roles = await _userManager.GetRolesAsync(user);
            var role = roles.ToArray();
            return role[0];
         }
         catch (Exception ex)
         {
            return null;
         }
      }
      public async Task<bool> CreateUser(User user)
      {
         try
         {
            var result = await _userManager.CreateAsync(user);
            return result.Succeeded;
         }
         catch (Exception ex)
         {
            return false;


         }
      }
      public async Task<bool> UpdateUser(User user)
      {
         try
         {
            var result = await _userManager.UpdateAsync(user);
            return result.Succeeded;
         }
         catch (Exception ex)
         {
            return false;

         }
      }
      public async Task<User> getUserByEmail(string email)
      {
         try
         {
            var result = await _userManager.Users.Include(x => x.People).FirstOrDefaultAsync(x => x.Email.Equals(email));
            return result;
         }
         catch (Exception ex)
         {
            return null;
         }
      }
      public async Task<User> getUserById(string id)
      {
         try
         {
            var result = await _userManager.FindByIdAsync(id);
            return result;
         }
         catch (Exception ex)
         {
            return null;
         }
      }

      public List<Claim> getUserClaim(User user, string role)
      {

         List<Claim> listClaims = new List<Claim>();
         listClaims.Add(new Claim(ClaimTypes.NameIdentifier, user.Id));
         listClaims.Add(new Claim(ClaimTypes.Email, user.Email));
         listClaims.Add(new Claim(ClaimTypes.Name, user.FirstName + user.LastName));
         listClaims.Add(new Claim(ClaimTypes.Role, role));
         if ((role.Equals("ADMIN") || role.Equals("STAFF")) && user.People != null)
         {
            listClaims.Add(new Claim(ClaimTypes.Authentication, user.People.PeopleId.ToString()));

         }

         //listClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
         return listClaims;

      }

      public string GenerateToken(List<Claim> claims)
      {
         var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtKey));
         var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
         var token = new JwtSecurityToken(_jwtIssuer,
           _jwtAudience,
           claims,
           expires: DateTime.Now.AddMinutes(_jwtExpires),
           signingCredentials: credentials);

         return new JwtSecurityTokenHandler().WriteToken(token);
      }

      public async Task<ResponseDTO> Login(LoginRequest request)
      {
         try
         {
            string email = request.Email;
            //if(email.Contains("@fpt.edu.vn"))
            //{
            //   return Unauthorized();
            //}
            var userCheck = await _userRepository.getUserByEmail(email);
            if (userCheck == null)
            {
               var userNew = _userRepository.CreateInstanceUser(); //CreateUser();
               userNew.Email = email;
               userNew.FirstName = request.Family_Name;
               userNew.LastName = request.Given_Name;

               userNew.isActive = true;
               userNew.UserName = email;

               var createUserResult = await _userRepository.CreateUser(userNew);
               if (createUserResult)
               {
                  var emailInfomation = email.Split('@');
                  var emailCheck = emailInfomation[0];
                  if (emailCheck.Substring(emailCheck.Length - 4).All(char.IsDigit))
                  {
                     await _userRepository.AssignRole(userNew, RolesConstants.STUDENT);
                  }
                  else
                  {
                     await _userRepository.AssignRole(userNew, RolesConstants.LECTURER);
                  }
               }
            }
            var user = await _userRepository.getUserByEmail(email);

            var userRole = await _userRepository.GetRole(user);

            var authClaims = _userRepository.getUserClaim(user, userRole);

            var token = GenerateToken(authClaims);
            _response.IsSuccess = true;
            _response.ObjectResponse = new LoginResponse
            {
               Token = token,
               Role = userRole,
               Name = request.Given_Name + " " + request.Family_Name
            };

            return _response; 
         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = "Login fail";
            return _response;
         }
      }
   }
}

