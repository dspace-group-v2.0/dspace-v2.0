﻿using Application.CollectionGroups;
using Application.Collections;
using Application.CommunityGroups;
using Application.GroupPeoples;
using Application.Responses;
using AutoMapper;
using Domain;
using Infrastructure;
using Infrastructure.Repositories.CollectionGroupRepositories;
using Infrastructure.Repositories.CollectionRepositories;
using Infrastructure.Repositories.GroupPeopleRepositories;
using Infrastructure.Repositories.GroupRepositories;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Services.Implements
{
   public class CollectionGroupService : ICollectionGroupService
   {
      private IMapper _mapper;
      private ICollectionGroupRepository _collectionGroupRepository;
      private IGroupRepository _groupRepository;
      private ICollectionRepository _collectionRepository;
      private IGroupPeopleRepository _groupPeopleRepository;
      protected ResponseDTO _response;
      public CollectionGroupService(IMapper mapper,
         ICollectionGroupRepository collectionGroupRepository,
         IGroupRepository groupRepository,
         ICollectionRepository collectionRepository,
         IGroupPeopleRepository groupPeopleRepository)
      {
         _groupRepository = groupRepository;
         _mapper = mapper;
         _collectionGroupRepository = collectionGroupRepository;
         _response = new ResponseDTO();
         _collectionRepository= collectionRepository;
         _groupPeopleRepository = groupPeopleRepository;

      }

      public async Task<ResponseDTO> AddCollectionManageGroup(List<CollectionGroupDTOForCreate> listCollectionGroupDTO)
      {
         try
         {
            foreach (var collectionGroupDTO in listCollectionGroupDTO)
            {
               var groupCheck = await _groupRepository.GetGroup(collectionGroupDTO.GroupId);
               var collectionCheck = await _collectionRepository.GetCollection(collectionGroupDTO.CollectionId);
               if (groupCheck == null || collectionCheck == null)
               {

                  if (groupCheck == null)
                  {
                     _response.IsSuccess = false;
                     _response.Message = "Group " + collectionGroupDTO.GroupId + " does not exist";
                     return _response;
                  }
                  if (collectionCheck == null)
                  {
                     _response.IsSuccess = false;
                     _response.Message = "Collection " + collectionGroupDTO.CollectionId + " does not exist";
                     return _response;
                  }
               }
               var collectionGroupCheck = await _collectionGroupRepository.GetQueryAll().SingleOrDefaultAsync(x => x.GroupId == collectionGroupDTO.GroupId && x.CollectionId == collectionGroupDTO.CollectionId);
               if (collectionGroupCheck != null)
               {
                  _response.IsSuccess = false;
                  _response.Message = "group " + groupCheck.Title + " already manage collection " + collectionCheck.CollectionName;
                  return _response;
               }

            }
            List<CollectionGroup> listCollectionGroup = _mapper.Map<List<CollectionGroup>>(listCollectionGroupDTO);
            await _collectionGroupRepository.AddListAsync(listCollectionGroup);

            _response.IsSuccess = true;
            _response.Message = "Add success";

         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }
         return _response;
      }
      public async Task<ResponseDTO> UpdateCollectionManageGroup(CollectionGroupDTOForUpdate collectionGroupDTO)
      {
         try
         {
            var groupCheck = await _groupRepository.GetGroup(collectionGroupDTO.GroupId);
            var collectionCheck = await _collectionRepository.GetCollection(collectionGroupDTO.CollectionId);
            var collectionGroupCheck = await _collectionGroupRepository.GetCollectionGroup(collectionGroupDTO.Id);
            if (groupCheck == null || collectionCheck == null || collectionGroupCheck == null)
            {
               _response.IsSuccess = false;
               if (groupCheck == null)
               {
                  _response.Message = "Group " + collectionGroupDTO.GroupId + " does not exist";
               }
               if (collectionCheck == null)
               {
                  _response.Message = "Collection " + collectionGroupDTO.CollectionId + " does not exist";
               }
               if (collectionGroupCheck == null)
               {
                  _response.Message = "Collection Group" + collectionGroupDTO.Id + " does not exist";

               }
            }
            else
            {
               await _collectionGroupRepository.UpdateAsync(collectionGroupCheck);
               _response.IsSuccess = true;
               _response.Message = "Update success";
            }

         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }
         return _response;
      }

      public async Task<ResponseDTO> DeleteCollectionManageInGroup(int id)
      {
         try
         {
            var CollectionGroupCheck = await _collectionGroupRepository.GetCollectionGroup(id);
            if (CollectionGroupCheck == null)
            {
               _response.IsSuccess = true;
               _response.Message = "Group does not manage this Collection";
            }
            else
            {
               await _collectionGroupRepository.DeleteAsync(CollectionGroupCheck);
               _response.IsSuccess = true;
               _response.Message = "Delete group manage Collection success";
            }
         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }
         return _response;
      }
      public async Task<ResponseDTO> GetListCollectionGroup()
      {
         try
         {
            IEnumerable<CollectionGroup> collectionGroups = await _collectionGroupRepository.GetQueryAll().Include(x => x.Group).Include(x => x.Collection).ToListAsync();

            var result = _mapper.Map<List<CollectionGroupDTOForDetail>>(collectionGroups);
            _response.IsSuccess = true;
            _response.ObjectResponse = result;

         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }
         return _response;
      }
      public async Task<List<Group>> GetListGroupByCollectionId(int CollectionId)
      {
         try
         {
            List<Group> groups = await _collectionGroupRepository.GetQueryAll().Where(x => x.CollectionId == CollectionId).Select(x => x.Group).ToListAsync();
            return groups;
         }
         catch (Exception ex)
         {
            throw new Exception(ex.Message);
         }
      }

      public async Task<List<Collection>> GetListCollectionByGroupId(int groupId)
      {
         try
         {
            List<Collection> collections = await _collectionGroupRepository.GetQueryAll().Where(x => x.GroupId == groupId).Select(x => x.Collection).ToListAsync();
            return collections;
         }
         catch (Exception ex)
         {
            throw new Exception(ex.Message);
         }
      }

      public async Task<CollectionGroup> GetCollectionGroupByGroupIdAndCollectionId(int collectionId, int groupId)
      {
         try
         {
            var collections = await _collectionGroupRepository.GetQueryAll().SingleOrDefaultAsync(x => x.GroupId == groupId && x.CollectionId == collectionId);
            return collections;
         }
         catch (Exception ex)
         {
            throw new Exception(ex.Message);
         }
      }

      public async Task<ResponseDTO> GetCollectionGroupDetailByStaff(int collectionId, int peopleId)
      {
         try
         {
            List<int> groups = await _groupPeopleRepository.GetListGroupByStaff(peopleId);
            List<CollectionGroup> collectionGroups = new List<CollectionGroup>();
            collectionGroups = await _collectionGroupRepository.GetQueryAll().ToListAsync();

            CollectionGroup collectionGroupCheck = null;
            foreach (var group in groups)
            {
               if (collectionGroups.SingleOrDefault(x => x.CollectionId == collectionId && x.GroupId == group) != null)
               {
                  collectionGroupCheck = collectionGroups.SingleOrDefault(x => x.CollectionId == collectionId && x.GroupId == group);
               }
            }

            if (collectionGroupCheck == null)
            {
               _response.IsSuccess = false;
               _response.Message = "you not have right in this collection";
            }
            else
            {
               _response.IsSuccess = true;
               _response.ObjectResponse = _mapper.Map<CollectionGroupDTOForDetail>(collectionGroupCheck);

            }
         }
         catch (Exception ex)
         {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
         }
         return _response;
      }
      public async Task<List<int>> GetListCollectionIdByStaffId(int peopleId)
      {
         List<int> listGroupId = await _groupPeopleRepository.GetListGroupByStaff(peopleId);
         List<int> listCollectionId = new List<int>();
         foreach (var groupId in listGroupId)
         {
            List<int> listCollectionGroup = await _collectionGroupRepository.GetQueryAll().Where(x => x.GroupId == groupId).Select(x => x.CollectionId).ToListAsync();
            listCollectionId.AddRange(listCollectionGroup);
         }

         return listCollectionId;
      }
   }
}
