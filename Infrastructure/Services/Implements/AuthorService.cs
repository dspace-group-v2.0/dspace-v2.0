﻿using Application.Authors;
using Application.Responses;
using AutoMapper;
using Domain;
using Infrastructure.Repositories.AuthorRepositories;
using Infrastructure.Repositories.MetadataValueRepositories;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Services.Implements
{
   public class AuthorService : IAuthorService
   {
      private IMapper _mapper;
      private IAuthorRepository _authorRepository;
      private IMetadataValueRepository _metadataValueRepository;

      protected ResponseDTO _response;

      public AuthorService(IMapper mapper, IAuthorRepository authorRepository, IMetadataValueRepository metadataValueRepository)
      {
         _metadataValueRepository= metadataValueRepository;
         _authorRepository = authorRepository;
         _response = new ResponseDTO();
         _mapper = mapper;
      }

      public async Task<ResponseDTO> CreateAuthor(AuthorDTOForCreateUpdate authorDTO)
      {
         try
         {
            Author author = new Author();
            author.FullName = authorDTO.FullName;
            author.JobTitle = authorDTO.JobTitle;
            author.DateAccessioned = authorDTO.DateAccessioned;
            author.DateAvailable = authorDTO.DateAvailable;

            author.Type = authorDTO.Type;
            await _authorRepository.AddAsync(author);
            author.Uri = authorDTO.Uri + author.AuthorId;
            await _authorRepository.UpdateAsync(author);
            _response.IsSuccess = true;
            _response.Message = "Author added successfully";
         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = "Failed to add a new author";

         }
         return _response;
      }

      public async Task<ResponseDTO> DeleteAuthor(int authorId)
      {
         try
         {
            var check = await _authorRepository.GetAuthor(authorId);
            if (check == null)
            {
               _response.IsSuccess = true;
               _response.Message = "Author does not exist";
            }
            else
            {
               if (await _metadataValueRepository.GetQueryAll()
                      .SingleOrDefaultAsync(x =>
                         (x.MetadataFieldId == 1 || x.MetadataFieldId == 2)
                         && x.TextValue.Equals(authorId.ToString())) != null)
               {
                  _response.IsSuccess = false;
                  _response.Message = "This author cannot be deleted because it is currently associated with an item";
               }
               else
               {
                  await _authorRepository.DeleteAsync(check);
                  _response.IsSuccess = true;
                  _response.Message = "Author deleted successfully";
               }
            }
         }
         catch (Exception ex)
         {

            _response.IsSuccess = false;
            _response.Message = ex.Message;

         }
         return _response;
      }

      public async Task<ResponseDTO> GetAuthorByID(int authorId)
      {
         try
         {
            var author = await _authorRepository.GetAuthor(authorId);
            if (author == null)
            {
               _response.IsSuccess = false;
               _response.Message = "This author does not exist";
            }
            else
            {
               _response.IsSuccess = true;
               _response.ObjectResponse = _mapper.Map<AuthorDTOForSelect>(author);
            }
         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }
         return _response;
      }

      public async Task<ResponseDTO> GetAuthorByName(string name)
      {
         try
         {
            var objList = await _authorRepository.GetQueryAll().Where(x => (x.FullName).Contains(name)).ToListAsync();

            if (objList != null)
            {
               _response.IsSuccess = true;
               _response.ObjectResponse = _mapper.Map<List<AuthorDTOForSelect>>(objList);
               _response.Message = "Found the Author";
            }
            else
            {
               _response.IsSuccess = false;
               _response.Message = "Not found the Author with that name";
            }
            
         }
         catch (Exception ex)
         {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
         }
         return _response;
      }

      public async Task<ResponseDTO> UpdateAuthor(int id, AuthorDTOForCreateUpdate authorDTO)
      {
         try
         {
            var author = await _authorRepository.GetAuthor(id);
            if (author == null)
            {
               _response.IsSuccess = true;
               _response.Message = "This author does not exist";
            }
            else
            {
               author.FullName = authorDTO.FullName;
               author.JobTitle = authorDTO.JobTitle;
               author.DateAccessioned = authorDTO.DateAccessioned;
               author.DateAvailable = authorDTO.DateAvailable;
               author.Uri = authorDTO.Uri;
               author.Type = authorDTO.Type;
               await _authorRepository.UpdateAsync(author);
               _response.IsSuccess = true;
               _response.Message = "Author updated successfully";
            }
         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }
         return _response;
      }

      public async Task<ResponseDTO> GetAllAuthor()
      {
         try
         {
            IEnumerable<Author> objList = await _authorRepository.GetAll();
            _response.IsSuccess = true;
            _response.ObjectResponse = _mapper.Map<List<AuthorDTOForSelect>>(objList);

         }
         catch (Exception ex)
         {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
         }
         return _response;
      }
   }
}

