using Application.Peoples;
using Application.Responses;
using AutoMapper;
using Domain;
using Infrastructure;
using Infrastructure.Repositories.PeopleRepositories;
using Infrastructure.Repositories.UserRepositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Services.Implements
{
   public class PeopleService : IPeopleService
   {
      private IMapper _mapper;
      private IPeopleRepository _peopleRepository;
      private UserManager<User> _userManager;
      private IUserRepository _userRepository;


      protected ResponseDTO _response;

      public PeopleService(IMapper mapper, IPeopleRepository peopleRepository, UserManager<User> userManager, IUserRepository userRepository)
      {
         _peopleRepository = peopleRepository;
         _response = new ResponseDTO();
         _mapper = mapper;
         _userManager = userManager;
         _userRepository = userRepository;
      }

      public async Task<ResponseDTO> CreatePeople(PeopleDTOForCreateUpdate peopleDTO, int userCreateId)
      {
         try
         {
            var userCheck = await _userManager.Users.Include(x => x.People).FirstOrDefaultAsync(x => x.Email.Equals(peopleDTO.Email));
            if (userCheck == null)
            {
               var userNew = _userRepository.CreateInstanceUser();
               userNew.Email = peopleDTO.Email;
               userNew.isActive = true;
               userNew.FirstName = peopleDTO.FirstName;
               userNew.LastName = peopleDTO.LastName;
               userNew.UserName = peopleDTO.Email.Split('@')[0];
               var result = await _userRepository.CreateUser(userNew);
               if (result == false)
               {
                  throw new Exception();
               }
               userCheck = userNew;
            }
            else
            {
               userCheck.Email = peopleDTO.Email;
               userCheck.isActive = true;
               userCheck.FirstName = peopleDTO.FirstName;
               userCheck.LastName = peopleDTO.LastName;
               userCheck.UserName = peopleDTO.Email.Split('@')[0];
               var result = await _userRepository.UpdateUser(userCheck);
               if (result == false)
               {
                  throw new Exception();
               }

            }
            var peopleCheck = userCheck.People;
            if (userCheck.People == null)
            {
               People peopleNew = new People();
               peopleNew.Address = peopleDTO.Address;
               peopleNew.PhoneNumber = peopleDTO.PhoneNumber;
               peopleNew.CreatedDate = DateTime.Now;
               peopleNew.LastModifiedDate = DateTime.Now;
               peopleNew.UserId = userCheck.Id;
               peopleNew.CreatedBy = userCreateId;
               await _peopleRepository.AddAsync(peopleNew);
               peopleCheck = peopleNew;
            }
            else
            {
               _response.Message = "Email already exist";
               _response.IsSuccess = false;
               return _response;
               //peopleCheck.Address = peopleDTO.Address;
               //peopleCheck.PhoneNumber = peopleDTO.PhoneNumber;
               //peopleCheck.CreatedDate = DateTime.Now;
               //peopleCheck.LastModifiedDate = DateTime.Now;
               //await _peopleRepository.UpdateAsync(peopleCheck);
            }
            await _userRepository.AssignRole(userCheck, peopleDTO.Role);

            _response.IsSuccess = true;
            _response.Message = "Add people success";
            var peopleDTOForSelect = _mapper.Map<PeopleDTOForSelect>(peopleCheck);
            //var peopleDTO = _mapper.Map<PeopleDTOForSelect>(people);
            var user = await _userRepository.getUserByEmail(peopleDTO.Email);
            peopleDTOForSelect.Role = await _userRepository.GetRole(user);
            _response.ObjectResponse = peopleDTO;
         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }
         return _response;
      }

      public async Task<ResponseDTO> DeletePeople(int id)
      {
         try
         {
            var peopleDelete = await _peopleRepository.GetQueryAll().Include(x => x.PeopleParent).Include(x => x.GroupPeoples).Include(x => x.Collection).Include(x => x.Community).Include(x => x.ListPeopleCreated).Include(x => x.User).SingleOrDefaultAsync(x => x.PeopleId == id);
            if (peopleDelete == null)
            {
               _response.IsSuccess = false;
               _response.Message = "People with " + id + "does not exist";
            }
            else
            {
               if (peopleDelete.GroupPeoples.Count != 0 || peopleDelete.Community.Count != 0 || peopleDelete.Collection.Count != 0 || peopleDelete.ListPeopleCreated.Count != 0)
               {
                  _response.IsSuccess = false;
                  _response.Message = "People with " + id + " is relative with collections or communities or list people created or people is exist in a group, please delete all it if you want delete this people  ";
               }
               else
               {
                  var user = peopleDelete.User;
                  var rolesForUser = await _userManager.GetRolesAsync(user);
                  if (rolesForUser.Count() > 0)
                  {
                     foreach (var item in rolesForUser.ToList())
                     {
                        // item should be the name of the role
                        var result = await _userManager.RemoveFromRoleAsync(user, item);
                     }
                  }
                  await _peopleRepository.DeleteAsync(peopleDelete);

                  await _userManager.DeleteAsync(user);

                  _response.IsSuccess = true;
                  _response.Message = "Delete people with " + id + "success";
               }
            }
         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }
         return _response;
      }
      public async Task<ResponseDTO> GetPeopleByID(int id)
      {
         try
         {
            var people = await _peopleRepository.GetQueryAll()
               .Include(x => x.PeopleParent.User)
               .Include(x => x.User)
               .ThenInclude(x => x.UserRoles)
               .ThenInclude(x => x.Role)

               .SingleOrDefaultAsync(x => x.PeopleId == id);
            var peopleDTO = _mapper.Map<PeopleDTOForSelect>(people);
            var user = await _userRepository.getUserByEmail(peopleDTO.Email);
            peopleDTO.Role = await _userRepository.GetRole(user);
            if (people == null)
            {
               _response.IsSuccess = true;
               _response.Message = "people with id " + id + " does not exist";
            }
            else
            {
               _response.IsSuccess = true;
               _response.ObjectResponse = peopleDTO;
            }
         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }

         return _response;
      }

      public async Task<ResponseDTO> GetPeopleByName(string name)
      {
         try
         {
            IEnumerable<People> objList = await _peopleRepository.GetQueryAll()
               .Include(p => p.User)
               .Where(x => (x.User.FirstName).Contains(name) || (x.User.LastName).Contains(name))
               .Include(x => x.User)
               .ThenInclude(x => x.UserRoles)
               .ThenInclude(x => x.Role)
               .ToListAsync();

            var peopleDTOMapper = _mapper.Map<List<PeopleDTOForSelect>>(objList);
            foreach (var people in peopleDTOMapper)
            {
               var user = await _userRepository.getUserByEmail(people.Email);
               people.Role = await _userRepository.GetRole(user);
            }
            _response.IsSuccess = true;
            _response.ObjectResponse = peopleDTOMapper;

         }
         catch (Exception ex)
         {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
         }
         return _response;
      }

      public async Task<ResponseDTO> UpdatePeople(int id, PeopleDTOForCreateUpdate peopleDTO, int userUpdateId)
      {
         try
         {
            var check = await _peopleRepository.GetPeople(id);
            if (check == null)
            {
               _response.IsSuccess = true;
               _response.Message = "people with id " + id + " does not exist";
            }

            else
            {
               var userCheck = await _userRepository.getUserById(check.UserId);
               var userEmailCheck = await _userRepository.getUserByEmail(peopleDTO.Email);
               if (userCheck.Email != peopleDTO.Email)
               {
                  if (userEmailCheck.People != null)
                  {
                     _response.Message = "Email already exist";
                     _response.IsSuccess = false;
                     return _response;
                  }
               }

               check.Address = peopleDTO.Address;
               check.PhoneNumber = peopleDTO.PhoneNumber;
               check.LastModifiedBy = userUpdateId;
               check.LastModifiedDate = DateTime.Now;
               //check.UserId = peopleDTO.UserId;
               userCheck.FirstName = peopleDTO.FirstName;
               userCheck.LastName = peopleDTO.LastName;
               userCheck.Email = peopleDTO.Email;
               userCheck.UserName = peopleDTO.Email.Split('@')[0];
               await _userRepository.AssignRole(userCheck, peopleDTO.Role);
               var updateResult = await _userRepository.UpdateUser(userCheck);
               if (!updateResult)
               {
                  throw new Exception();
               }


               await _peopleRepository.UpdateAsync(check);

               var peopleDTOForSelect = _mapper.Map<PeopleDTOForSelect>(check);
               //var peopleDTO = _mapper.Map<PeopleDTOForSelect>(people);
               var user = await _userRepository.getUserByEmail(peopleDTO.Email);
               peopleDTOForSelect.Role = await _userRepository.GetRole(user);
               _response.ObjectResponse = peopleDTO;
               _response.IsSuccess = true;
            }
         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }
         return _response;
      }
      public async Task<ResponseDTO> GetAllPeople()
      {
         try
         {
            IEnumerable<People> objList = await _peopleRepository.GetQueryAll().Include(x => x.User).ThenInclude(x => x.UserRoles).ThenInclude(x => x.Role).ToListAsync();

            var peopleDTOMapper = _mapper.Map<List<PeopleDTOForSelect>>(objList);
            //var peopleCheck = peopleDTOMapper.FirstOrDefault(x => x.PeopleId == id);
            //if (peopleCheck != null) {
            //   peopleDTOMapper.Remove(peopleCheck);
            //      }
            foreach (var people in peopleDTOMapper)
            {
               var user = await _userRepository.getUserByEmail(people.Email);
               people.Role = await _userRepository.GetRole(user);
            }
            _response.IsSuccess = true;
            _response.ObjectResponse = peopleDTOMapper;

         }
         catch (Exception ex)
         {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
         }
         return _response;
      }

      public async Task<ResponseDTO> GetPeopleByEmail(string email)
      {
         try
         {
            var peoples = await _peopleRepository.GetQueryAll().Where(x => x.User.Email.Contains(email)).Include(x => x.User).ThenInclude(x => x.UserRoles).ToListAsync();

            if (peoples == null)
            {
               _response.IsSuccess = true;
               _response.Message = "people with " + email + " does not exist";
               _response.ObjectResponse = null;
            }
            else
            {
               var peopleDTOMapper = _mapper.Map<List<PeopleDTOForSelect>>(peoples);
               foreach (var people in peopleDTOMapper)
               {
                  var user = await _userRepository.getUserByEmail(people.Email);
                  people.Role = await _userRepository.GetRole(user);
               }
               _response.IsSuccess = true;
               _response.Message = "success";
               _response.ObjectResponse = peopleDTOMapper;
            }
         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }
         return _response;
      }
   }
}