using Application.FileUploads;
using Application.Responses;
using AutoMapper;
using Domain;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Download;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using Infrastructure.Repositories.CollectionRepositories;
using Infrastructure.Repositories.FileModifyAccessRepositories;
using Infrastructure.Repositories.FileUploadRepositories;
using Infrastructure.Repositories.ItemRepositories;
using Infrastructure.Repositories.UserRepositories;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas.Parser;
using iText.Kernel.Pdf.Canvas.Parser.Listener;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Shared.Enums;

namespace Infrastructure.Services.Implements;

public class FileUploadService : IFileUploadService
{
    private IFileUploadRepository _fileUploadRepository;
    private IItemRepository _itemRepository;
    private ICollectionRepository _collectionRepository;
    private IFileModifyAccessRepository _fileModifyAccessRepository;
    private IUserRepository _userRepository;
    protected ResponseDTO _response;
    private readonly IMapper _mapper;
    private static readonly string[] Scopes = new[] { DriveService.Scope.DriveFile };

    public FileUploadService(IFileUploadRepository fileUploadRepository,
        IMapper mapper,
        IFileModifyAccessRepository fileModifyAccessRepository,
        IItemRepository itemRepository,
        ICollectionRepository collectionRepository,
        IUserRepository userRepository)
    {
        _itemRepository = itemRepository;
        _mapper = mapper;
        _fileModifyAccessRepository = fileModifyAccessRepository;
        _fileUploadRepository = fileUploadRepository;
        _response = new ResponseDTO();
        _collectionRepository = collectionRepository;
        _userRepository = userRepository;
    }

    public async Task<UserCredential> GetUserCredential()
    {
        UserCredential credential = null;
        using (var stream =
               new FileStream(
                   "client_secret_336699035226-8c0s3uv2pee71adqarpuiicagplfpkvj.apps.googleusercontent.com.json",
                   FileMode.Open, FileAccess.ReadWrite))
        {
            credential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
                GoogleClientSecrets.FromStream(stream).Secrets,
                Scopes,
                "user",
                CancellationToken.None,
                new FileDataStore("DSpace v2.0", true));
        }

        return credential;
    }

    public async Task<ResponseDTO> DownloadFileWithId(string fileId)
    {
        try
        {
            var data = await GetFileWithSpecificUrl(GetUserCredential(), fileId);
            if (data.Length > 0)
            {
                _response.Message = "Item download successful";
                _response.IsSuccess = true;
                _response.ObjectResponse = data;
            }
            else
            {
                _response.Message = "Encounter an error when download item";
                _response.IsSuccess = false;
            }
        }
        catch (Exception e)
        {
            _response.Message = e.Message;
            _response.IsSuccess = false;
        }

        return _response;
    }

    public async Task<ResponseDTO> DeleteFileWithFileId(int fileId)
    {
        try
        {
            var fileUpload = await _fileUploadRepository.GetFileUpload(fileId);
            var fileAccess = await _fileModifyAccessRepository.GetQueryAll().Where(x => x.FileId == fileId)
                .ToListAsync();
            if (fileUpload != null)
            {
                if (fileAccess.Count() > 0)
                {
                    await _fileModifyAccessRepository.DeleteListAsync(fileAccess);
                }

                await _fileUploadRepository.DeleteAsync(fileUpload);
                _response.Message = "File remove successful";
                _response.IsSuccess = true;
            }
            else
            {
                _response.Message = "Encounter an error when remove this file";
                _response.IsSuccess = false;
            }
        }
        catch (Exception e)
        {
            _response.Message = e.Message;
            _response.IsSuccess = false;
        }

        return _response;
    }

    public async Task<ResponseDTO> UndiscoverableFileWithFileId(int fileId)
    {
        try
        {
            var fileUpload = await _fileUploadRepository.GetFileUpload(fileId);

            if (fileUpload != null)
            {
                if (fileUpload.isActive == false)
                {
                    fileUpload.isActive = true;
                    _response.Message = "This file can be discovered now.";
                    _response.IsSuccess = true;
                }
                else if (fileUpload.isActive == true)
                {
                    fileUpload.isActive = false;
                    _response.Message = "This file can't be discovered now.";
                    _response.IsSuccess = true;
                }

                await _fileUploadRepository.UpdateAsync(fileUpload);
            }
            else
            {
                _response.Message = "Encounter an error when modify this file";
                _response.IsSuccess = false;
            }
        }
        catch (Exception e)
        {
            _response.Message = e.Message;
            _response.IsSuccess = false;
        }

        return _response;
    }

    public async Task<ResponseDTO> ModifyFileWithFileId(ModifyFileWithAccessDto modifyFileWithAccessDto)
    {
        try
        {
            var fileUpload = await _fileUploadRepository.GetQueryAll()
                .Include(c => c.FileAccess)
                .Include(c => c.Item)
                .SingleOrDefaultAsync(c => c.FileId == modifyFileWithAccessDto.FileId);
            if (fileUpload != null)
            {
                if (modifyFileWithAccessDto.File != null)
                {
                    IList<IFormFile> tempList = new List<IFormFile>();
                    tempList.Add(modifyFileWithAccessDto.File);
                    bool result = await UploadFile(tempList, GetUserCredential(), fileUpload.Item,
                        modifyFileWithAccessDto.Role, fileUpload, modifyFileWithAccessDto.AccessType);
                    if (result == true)
                    {
                        fileUpload.FileAccess[0].AccessType = modifyFileWithAccessDto.AccessType;
                        fileUpload.FileAccess[0].Role = modifyFileWithAccessDto.Role;
                        await _fileUploadRepository.UpdateAsync(fileUpload);
                        _response.Message = "Update file with contain file successful";
                        _response.IsSuccess = true;
                    }
                }
                else
                {
                    fileUpload.FileAccess[0].AccessType = modifyFileWithAccessDto.AccessType;
                    fileUpload.FileAccess[0].Role = modifyFileWithAccessDto.Role;
                    await _fileUploadRepository.UpdateAsync(fileUpload);
                    _response.Message = "Update file with no contain file successful";
                    _response.IsSuccess = true;
                }
            }
        }
        catch (Exception ex)
        {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
        }

        return _response;
    }

    public async Task<ResponseDTO> AddNewFileToItem(int itemId, IList<IFormFile> multipleFiles, string role)
    {
        try
        {
            var item = await _itemRepository.GetItem(itemId);
            if (item != null)
            {
                bool result = await UploadFile(multipleFiles, GetUserCredential(), item, role, null, null);
                if (result == true)
                {
                    _response.Message = "Add new file in item successful";
                    _response.IsSuccess = true;
                }
            }
        }
        catch (Exception e)
        {
            _response.IsSuccess = false;
            _response.Message = e.Message;
        }

        return _response;
    }


    private async Task<bool> UploadFile(IList<IFormFile> multipleFile, Task<UserCredential> credential, Item item,
        string accessFile, FileUpload? fileUploadUpdate, FileAccessType? accessType)
    {
        try
        {
            var collection = await _collectionRepository.GetCollection(item.CollectionId);
            if (collection == null)
            {
                return false;
            }

            List<FileUpload> fileUploads = new List<FileUpload>();
            var listEmail = await _userRepository.GetListEmailRegister();
            DriveService service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = await credential
            });
            foreach (IFormFile oneFileOnly in multipleFile)
            {
                var insertFile = service.Files.Create(
                    new Google.Apis.Drive.v3.Data.File
                    {
                        Name = oneFileOnly.FileName,
                        Parents = new List<string>()
                        {
                            collection.FolderId
                        }
                    },
                    oneFileOnly.OpenReadStream(), oneFileOnly.ContentType);
                insertFile.ChunkSize = Google.Apis.Drive.v3.FilesResource.CreateMediaUpload.MinimumChunkSize * 3;
                insertFile.Body.CopyRequiresWriterPermission = true;
                await insertFile.UploadAsync();
                foreach (var email in listEmail)
                {
                    var permission = new Google.Apis.Drive.v3.Data.Permission()
                    {
                        Type = "user",
                        Role = "reader",
                        EmailAddress = email
                    };
                    var requestPermission = service.Permissions.Create(permission, insertFile.ResponseBody.Id);
                    requestPermission.Fields = "id";
                    requestPermission.SendNotificationEmail = false;
                    await requestPermission.ExecuteAsync();
                }
                if (fileUploadUpdate == null)
                {
                    var fileUpload = new ModifyFileUploadDto()
                    {
                        FileUrl = Path.Combine(Path.GetTempPath(), insertFile.ResponseBody.Name),
                        FileKeyId = insertFile.ResponseBody.Id,
                        MimeType = insertFile.ResponseBody.MimeType,
                        Kind = insertFile.ResponseBody.Kind,
                        FileName = insertFile.ResponseBody.Name,
                        CreationTime = DateTime.Now,
                        isActive = true,
                        ItemId = item.ItemId
                    };
                    try
                    {
                        if (insertFile.ResponseBody.MimeType.Equals("application/pdf"))
                        {
                            using (MemoryStream memoryStream = new MemoryStream())
                            {
                                await using (StreamWriter streamWriter =
                                             new StreamWriter(memoryStream, leaveOpen: true))
                                {
                                    iText.IO.Util.ResourceUtil.AddToResourceSearch("itext.font_asian.dll");
                                    using (PdfReader reader = new PdfReader(oneFileOnly.OpenReadStream()))
                                    {
                                        PdfDocument srcDoc = new PdfDocument(reader);
                                        for (int i = 1; i <= srcDoc.GetNumberOfPages(); i++)
                                        {
                                            var page = srcDoc.GetPage(i);
                                            string pageText =
                                                PdfTextExtractor.GetTextFromPage(page,
                                                    new SimpleTextExtractionStrategy());
                                            streamWriter.WriteLine(pageText);
                                            streamWriter.Flush();
                                        }
                                    }
                                }

                                var requestTextGenerated = service.Files.Create(
                                    new Google.Apis.Drive.v3.Data.File
                                    {
                                        Name = "temp.txt",
                                        Parents = new List<string>()
                                        {
                                            "1a86_QB7vETCmIG-Qqidf4IPT7FPJ_car"
                                        },
                                        MimeType = "text/plain",
                                    },
                                    memoryStream, "text/plain");
                                requestTextGenerated.Fields = "id";
                                await requestTextGenerated.UploadAsync();
                                var uploadTextGenerated = requestTextGenerated.ResponseBody;
                                var updateMetadata = new Google.Apis.Drive.v3.Data.File()
                                {
                                    Name = fileUpload.FileKeyId
                                };
                                var updateRequest = service.Files.Update(updateMetadata, uploadTextGenerated.Id);
                                updateRequest.Fields = "id, name";
                                await updateRequest.ExecuteAsync();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Unknown PdfException");
                    }

                    var file = _mapper.Map<FileUpload>(fileUpload);
                    fileUploads.Add(file);
                }
                else
                {
                    fileUploadUpdate.FileUrl = Path.Combine(Path.GetTempPath(), insertFile.ResponseBody.Name);
                    fileUploadUpdate.FileKeyId = insertFile.ResponseBody.Id;
                    fileUploadUpdate.MimeType = insertFile.ResponseBody.MimeType;
                    fileUploadUpdate.Kind = insertFile.ResponseBody.Kind;
                    fileUploadUpdate.FileName = insertFile.ResponseBody.Name;
                    fileUploadUpdate.isActive = true;
                    fileUploadUpdate.ItemId = item.ItemId;
                    try
                    {
                        if (insertFile.ResponseBody.MimeType.Equals("application/pdf"))
                        {
                            using (MemoryStream memoryStream = new MemoryStream())
                            {
                                await using (StreamWriter streamWriter =
                                             new StreamWriter(memoryStream, leaveOpen: true))
                                {
                                    iText.IO.Util.ResourceUtil.AddToResourceSearch("itext.font_asian.dll");
                                    using (PdfReader reader = new PdfReader(oneFileOnly.OpenReadStream()))
                                    {
                                        PdfDocument srcDoc = new PdfDocument(reader);
                                        for (int i = 1; i <= srcDoc.GetNumberOfPages(); i++)
                                        {
                                            var page = srcDoc.GetPage(i);
                                            string pageText =
                                                PdfTextExtractor.GetTextFromPage(page,
                                                    new SimpleTextExtractionStrategy());
                                            streamWriter.WriteLine(pageText);
                                            streamWriter.Flush();
                                        }
                                    }
                                }

                                var requestTextGenerated = service.Files.Create(
                                    new Google.Apis.Drive.v3.Data.File
                                    {
                                        Name = "temp.txt",
                                        Parents = new List<string>()
                                        {
                                            "1a86_QB7vETCmIG-Qqidf4IPT7FPJ_car"
                                        },
                                        MimeType = "text/plain",
                                    },
                                    memoryStream, "text/plain");
                                requestTextGenerated.Fields = "id";
                                await requestTextGenerated.UploadAsync();
                                var uploadTextGenerated = requestTextGenerated.ResponseBody;
                                var updateMetadata = new Google.Apis.Drive.v3.Data.File()
                                {
                                    Name = fileUploadUpdate.FileKeyId
                                };
                                var updateRequest = service.Files.Update(updateMetadata, uploadTextGenerated.Id);
                                updateRequest.Fields = "id, name";
                                await updateRequest.ExecuteAsync();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Unknown PdfException");
                    }

                    fileUploads.Add(fileUploadUpdate);
                }
            }

            if (fileUploads.Count > 0)
            {
                if (fileUploadUpdate == null)
                {
                    await _fileUploadRepository.AddListAsync(fileUploads);
                }
                else
                {
                    await _fileUploadRepository.UpdateListAsync(fileUploads);
                }
                if (accessType != null)
                {
                    foreach (var fileUpload in fileUploads)
                    {
                        switch (accessFile)
                        {
                            case "ALL":
                                await _fileModifyAccessRepository.AddAsync(new FileModifyAccess()
                                {
                                    AccessType = (FileAccessType)accessType,
                                    FileId = fileUpload.FileId,
                                    Role = "ALL",
                                });
                                break;
                            case "LECTURER":
                                await _fileModifyAccessRepository.AddAsync(new FileModifyAccess()
                                {
                                    AccessType = (FileAccessType)accessType,
                                    FileId = fileUpload.FileId,
                                    Role = "LECTURER",
                                });
                                break;
                            case "STUDENT":
                                await _fileModifyAccessRepository.AddAsync(new FileModifyAccess()
                                {
                                    AccessType = (FileAccessType)accessType,
                                    FileId = fileUpload.FileId,
                                    Role = "STUDENT",
                                });
                                break;
                        }
                    }
                }
                else
                {
                    foreach (var fileUpload in fileUploads)
                    {
                        switch (accessFile)
                        {
                            case "ALL":
                                await _fileModifyAccessRepository.AddAsync(new FileModifyAccess()
                                {
                                    AccessType = FileAccessType.View,
                                    FileId = fileUpload.FileId,
                                    Role = "ALL",
                                });
                                break;
                            case "LECTURER":
                                await _fileModifyAccessRepository.AddAsync(new FileModifyAccess()
                                {
                                    AccessType = FileAccessType.View,
                                    FileId = fileUpload.FileId,
                                    Role = "LECTURER",
                                });
                                break;
                            case "STUDENT":
                                await _fileModifyAccessRepository.AddAsync(new FileModifyAccess()
                                {
                                    AccessType = FileAccessType.View,
                                    FileId = fileUpload.FileId,
                                    Role = "STUDENT",
                                });
                                break;
                        }
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

    public async Task<byte[]> GetFileWithSpecificUrl(Task<UserCredential> credential, string fileId)
    {
        try
        {
            DriveService service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = await credential
            });
            var request = service.Files.Get(fileId);
            var memoryStream = new MemoryStream();
            request.MediaDownloader.ProgressChanged +=
                progress =>
                {
                    switch (progress.Status)
                    {
                        case DownloadStatus.Downloading:
                        {
                            Console.WriteLine(progress.BytesDownloaded);
                            break;
                        }
                        case DownloadStatus.Completed:
                        {
                            Console.WriteLine("Download complete.");
                            break;
                        }
                        case DownloadStatus.Failed:
                        {
                            Console.WriteLine("Download failed.");
                            break;
                        }
                    }
                };
            request.Download(memoryStream);
            return memoryStream.ToArray();
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
}