﻿using AutoMapper;
using Domain;
using Application.GroupPeoples;
using Application.Responses;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Repositories.GroupRepositories;
using Infrastructure.Repositories.PeopleRepositories;
using Infrastructure.Repositories.GroupPeopleRepositories;
using Org.BouncyCastle.Math.EC.Rfc7748;

namespace Infrastructure.Services.Implements
{
   public class GroupPeopleService : IGroupPeopleService
   {
      public IMapper _mapper;
      private IGroupRepository _groupRepository;
      private IPeopleRepository _peopleRepository;
      private IGroupPeopleRepository _groupPeopleRepository;


      protected ResponseDTO _response;

      public GroupPeopleService(IMapper mapper, IGroupRepository groupRepository, IPeopleRepository peopleRepository,
          IGroupPeopleRepository groupPeopleRepository)
      {
         _groupPeopleRepository = groupPeopleRepository;
         _groupRepository = groupRepository;
         _peopleRepository = peopleRepository;
         _response = new ResponseDTO();
         _mapper = mapper;
      }

      public async Task<ResponseDTO> AddPeopleInGroup(GroupPeopleDTO groupPeopleDTO)
      {
         try
         {
            var groupCheck = await _groupRepository.GetGroup(groupPeopleDTO.GroupId);
            var peopleCheck = await _peopleRepository.GetPeople(groupPeopleDTO.PeopleId);
            var groupPeopleCheck = await _groupPeopleRepository.GetGroupPeopleByGroupIdAndPeopleId(groupPeopleDTO.GroupId, groupPeopleDTO.PeopleId);

            if (groupCheck == null || peopleCheck == null)
            {
               _response.IsSuccess = false;
               if (groupCheck == null)
               {
                  _response.Message = "Group " + groupPeopleDTO.GroupId + " does not exist";
                  return _response;
               }

               if (peopleCheck == null)
               {
                  _response.Message = "Group " + groupPeopleDTO.PeopleId + " does not exist";
                  return _response;
               }
            }
            if (groupPeopleCheck != null)
            {
               _response.IsSuccess = false;
               _response.Message = "Group is exist People";
               return _response;

            }

            GroupPeople groupPeople = _mapper.Map<GroupPeople>(groupPeopleDTO);
            await _groupPeopleRepository.AddAsync(groupPeople);
            _response.IsSuccess = true;
            _response.Message = "Add people into group success";

         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }

         return _response;
      }

      public async Task<ResponseDTO> DeletePeopleInGroup(GroupPeopleDTO groupPeopleDTO)
      {
         try
         {
            var groupPeopleCheck =
                await _groupPeopleRepository.GetGroupPeopleByGroupIdAndPeopleId(groupPeopleDTO.GroupId,
                    groupPeopleDTO.PeopleId);
            if (groupPeopleCheck == null)
            {
               _response.IsSuccess = false;
               _response.Message = "People does not exist in group";
            }
            else
            {
               await _groupPeopleRepository.DeleteAsync(groupPeopleCheck);
               _response.IsSuccess = true;
               _response.Message = "Group is exist People";
            }
         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }

         return _response;
      }

      public async Task<ResponseDTO> AddListPeopleInGroup(GroupPeopleListDTOForCreateUpdate groupPeopleListDTO)
      {
         try
         {
            var groupCheck = await _groupRepository.GetGroup(groupPeopleListDTO.GroupId);

            if (groupCheck == null)
            {
               throw new Exception("Group does not exist");
            }

            List<GroupPeople> listGroupPeople = new List<GroupPeople>();
            foreach (int peopleId in groupPeopleListDTO.ListPeopleId)
            {
               var peopleCheck = await _peopleRepository.GetPeople(peopleId);
               var groupPeopleCheck = await _groupPeopleRepository.GetGroupPeopleByGroupIdAndPeopleId(groupPeopleListDTO.GroupId, peopleId);
               if (peopleCheck == null)
               {
                  _response.IsSuccess = false;
                  _response.Message = "people does not exist";
                  return _response;
               }
               if (groupPeopleCheck != null)
               {
                  _response.IsSuccess = false;
                  _response.Message = "people does not exist";
                  return _response;
               }


               listGroupPeople.Add(new GroupPeople
               {
                  GroupId = groupPeopleListDTO.GroupId,
                  PeopleId = peopleId,
               });
            }

            await _groupPeopleRepository.AddListAsync(listGroupPeople);
            _response.IsSuccess = true;
            _response.Message = "Add list people in Group success";
         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }

         return _response;
      }

      //public async Task<ResponseDTO> UpdateListPeopleInGroup(GroupPeopleListDTOForCreateUpdate groupPeopleListDTO)
      //{
      //   try
      //   {
      //      var groupCheck = await _groupRepository.GetGroup(groupPeopleListDTO.GroupId);

      //      if (groupCheck == null)
      //      {
      //         throw new Exception("Group does not exist");
      //      }

      //      List<GroupPeople> listGroupPeople = new List<GroupPeople>();
      //      await 

      //      await _context.GroupPeoples.AddRangeAsync(listGroupPeople);
      //      await _context.SaveChangesAsync();
      //      _response.IsSuccess = true;
      //      _response.Message = "Add list people in Group success";
      //   }
      //   catch (Exception ex)
      //   {
      //      _response.IsSuccess = false;
      //      _response.Message = ex.Message;
      //   }
      //   return _response;
      //}

      public async Task<ResponseDTO> DeleteListPeopleInGroup(int groupId)
      {
         try
         {
            List<GroupPeople> groupPeopleCheck = await _groupPeopleRepository.GetQueryAll()
                .Where(x => x.GroupId.Equals(groupId)).ToListAsync();
            if (groupPeopleCheck.Count() == 0)
            {
               _response.IsSuccess = true;
               _response.Message = "group does not exist any people";
            }
            else
            {
               await _groupPeopleRepository.DeleteListAsync(groupPeopleCheck);
               _response.IsSuccess = true;
               _response.Message = "Delete people in group success";
            }
         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }

         return _response;
      }

      //public async Task<List<Group>> GetListGroupByStaff(int peopleId)
      //{
      //   try
      //   {
      //      List<Group> groups = await _groupPeopleRepository.GetQueryAll().Where(x => x.PeopleId == peopleId)
      //          .Select(x => x.Group).ToListAsync();
      //      return groups;
      //   }
      //   catch (Exception ex)
      //   {
      //      throw new Exception(ex.Message);
      //   }
      //}

      //public async Task<List<People>> GetListPeopleByGroupId(int groupId)
      //{
      //   try
      //   {
      //      List<People> peoples = await _groupPeopleRepository.GetQueryAll().Where(x => x.GroupId == groupId)
      //          .Select(x => x.People).ToListAsync();
      //      return peoples;
      //   }
      //   catch (Exception ex)
      //   {
      //      throw new Exception(ex.Message);
      //   }
      //}
   }
}