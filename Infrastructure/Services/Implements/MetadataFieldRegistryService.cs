﻿using Application.Metadatas;
using AutoMapper;
using Domain;
using Infrastructure;
using Infrastructure.Repositories.MetadataFieldRepositories;

namespace Infrastructure.Services.Implements
{
   public class MetadataFieldRegistryService : IMetadataFieldRegistryService
   {
      private IMapper _mapper;

      private IMetadataFieldRegistryRepository _metadataFieldRegistryRepository;
      public MetadataFieldRegistryService(IMapper mapper, IMetadataFieldRegistryRepository metadataFieldRegistryRepository)
      {
         _mapper = mapper;
         _metadataFieldRegistryRepository = metadataFieldRegistryRepository;
      }

      public List<MetadateFieldDTO> GetMetadataFieldRegistries()
      {
         return _mapper.Map<List<MetadateFieldDTO>>(_metadataFieldRegistryRepository.GetAll());
      }
   }
}
