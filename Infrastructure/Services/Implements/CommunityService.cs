using Application.Communities;
using Application.Responses;
using Domain;
using AutoMapper;
using Infrastructure;
using System.Runtime.InteropServices;
using Microsoft.EntityFrameworkCore;
using Application.CommunityGroups;
using static iText.StyledXmlParser.Jsoup.Select.Evaluator;
using Microsoft.IdentityModel.Tokens;
using System.Linq;
using iText.Commons.Datastructures;
using Infrastructure.Repositories.CommunityGroupRepositories;
using Infrastructure.Repositories.CollectionGroupRepositories;
using Infrastructure.Repositories.SubscribeRepositories;
using Infrastructure.Repositories.CommunityRepositories;
using Infrastructure.Repositories.StatisticRepositories;
using Infrastructure.Repositories.PeopleRepositories;

namespace Infrastructure.Services.Implements
{
   public class CommunityService : ICommunityService
   {
      protected IMapper _mapper;
      private ICommunityGroupRepository _communityGroupRepository;
      private ICollectionGroupRepository _collectionGroupRepository;
      private ISubscribeRepository _subscribeRepository;
      private ICommunityRepository _communityRepository;
      private IStatisticRepository _statisticRepository;
      private IPeopleRepository _peopleRepository;


      protected ResponseDTO _response;

      public CommunityService(IMapper mapper,
          ICommunityGroupRepository communityGroupRepository,
          ICollectionGroupRepository collectionGroupRepository,
          ISubscribeRepository subscribeRepository,
          ICommunityRepository communityRepository,
          IStatisticRepository statisticRepository,
          IPeopleRepository peopleRepository
      )
      {
         _response = new ResponseDTO();
         _mapper = mapper;
         _collectionGroupRepository = collectionGroupRepository;
         _subscribeRepository = subscribeRepository;
         _communityRepository = communityRepository;
         _communityGroupRepository = communityGroupRepository;
         _statisticRepository = statisticRepository;
         _peopleRepository = peopleRepository;
      }

      public async Task<ResponseDTO> CreateCommunityForStaff(CommunityDTOForCreateOrUpdate communityDTO,
          int userCreateId)
      {
         try
         {
            var listCommunityId = await _communityGroupRepository.GetListCommunityIdByStaff(userCreateId);
            if (listCommunityId.Count() == 0 || communityDTO.ParentCommunityId == null ||
                !listCommunityId.Contains(communityDTO.ParentCommunityId.Value))
            {
               _response.IsSuccess = false;
               _response.Message = "Add community fail";
               return _response;
            }

            var communityResult = await CreateCommunity(communityDTO, userCreateId);
            if (!communityResult.IsSuccess)
            {
               return communityResult;
            }

            if (communityDTO.ParentCommunityId.HasValue)
            {
               var listGroupId =
                   await _communityGroupRepository.GetListGroupIdByCommunityId(
                       communityDTO.ParentCommunityId.Value);
               var listCommunityGroup = new List<CommunityGroup>();
               if (listGroupId.Count() > 0)
               {
                  foreach (var group in listGroupId)
                  {
                     var newCommunityGroup = new CommunityGroup
                     {
                        GroupId = group,
                        CommunityId = ((Community)communityResult.ObjectResponse).CommunityId
                     };
                     listCommunityGroup.Add(newCommunityGroup);
                  }

                  await _communityGroupRepository.AddListAsync(listCommunityGroup);
               }
            }
         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }

         return _response;
      }

      public async Task<ResponseDTO> UpdateCommunityForStaff(int communityId,
          CommunityDTOForCreateOrUpdate communityDTO, int userCreateId)
      {
         try
         {
            var listCommunityId = await _communityGroupRepository.GetListCommunityIdByStaff(userCreateId);
            if (listCommunityId.Count() == 0 || communityDTO.ParentCommunityId == null ||
                !listCommunityId.Contains(communityDTO.ParentCommunityId.Value))
            {
               _response.IsSuccess = false;
               _response.Message = "Add community fail";
               return _response;
            }

            return await UpdateCommunity(communityId, communityDTO, userCreateId);
         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }

         return _response;
      }

      public async Task<ResponseDTO> DeleteCommunityForStaff(int communityId, int userCreateId)
      {
         try
         {
            var listCommunityId = await _communityGroupRepository.GetListCommunityIdByStaff(userCreateId);
            if (listCommunityId.Count() == 0 || !listCommunityId.Contains(communityId))
            {
               _response.IsSuccess = false;
               _response.Message = "Add community fail";
               return _response;
            }

            return await DeleteCommunity(communityId);
         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }

         return _response;
      }


      public async Task<ResponseDTO> CreateCommunity(CommunityDTOForCreateOrUpdate communityDTO, int userCreateId)
      {
         try
         {
            Community community = new Community();
            community.CommunityName = communityDTO.CommunityName;
            community.LogoUrl = communityDTO.LogoUrl;
            community.ShortDescription = communityDTO.ShortDescription;
            community.CreateBy = userCreateId;
            community.isActive = communityDTO.isActive;
            community.ParentCommunityId = communityDTO.ParentCommunityId;
            community.CreateTime = DateTime.Now;
            community.UpdateTime = DateTime.Now;
            await _communityRepository.AddAsync(community);
            if (communityDTO.File != null)
            {
               await using (var stream = new FileStream(communityDTO.LogoUrl, FileMode.Create))
               {
                  await communityDTO.File.CopyToAsync(stream);
               }
            }

            _response.IsSuccess = true;
            _response.Message = "Add Community success";
            _response.ObjectResponse = community;
         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }

         return _response;
      }

      public async Task<ResponseDTO> DeleteCommunity(int communityId)
      {
         try
         {
            var check = await _communityRepository.GetQueryAll().Include(x => x.CommunityGroups)
                .Include(x => x.Collections).Include(x => x.SubCommunities)
                .SingleOrDefaultAsync(x => x.CommunityId == communityId);
            var statsCommunity = await _statisticRepository.GetQueryAll().Include(x => x.Community)
                .Where(x => x.CommunityId == communityId).ToListAsync();
            if (check == null)
            {
               _response.IsSuccess = true;
               _response.Message = "Community doesnot exist";
            }
            else
            {
               if (check.Collections.Count != 0 || check.SubCommunities.Count != 0)
               {
                  _response.IsSuccess = false;
                  _response.Message =
                      "Community have collection or sub community, if you want delete this community please delete all collection or subcommunity in the community";
               }
               else
               {
                  if (!statsCommunity.IsNullOrEmpty())
                  {
                     await _statisticRepository.DeleteListAsync(statsCommunity);
                  }

                  if (check.CommunityGroups.Count() > 0)
                  {
                     await _communityGroupRepository.DeleteListAsync(check.CommunityGroups);
                  }

                  await _communityRepository.DeleteAsync(check);
                  _response.IsSuccess = true;
                  _response.Message = "Delete Community success";
               }
            }
         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }

         return _response;
      }

      public async Task<ResponseDTO> GetCommunityByID(int id)
      {
         try
         {
            var community = await _communityRepository.GetQueryAll()
           .Include(x => x.ParentCommunity)
           .Include(x => x.CommunityGroups)
                .ThenInclude(x => x.Group)
                .Include(x => x.People)
                .ThenInclude(x => x.User)
                .Include(x => x.SubCommunities)
                .Include(x => x.Collections)
                .ThenInclude(x => x.Items)
                .SingleOrDefaultAsync(x => x.CommunityId == id);
            if (community == null)
            {
               _response.IsSuccess = true;
               _response.Message = "Community does not exist";
            }
            else
            {
               var result = _mapper.Map<CommunityDTOForDetail>(community);
               result.communityGroupDTOForDetails =
                   _mapper.Map<List<CommunityGroupDTOForDetail>>(community.CommunityGroups);
               if (community.UpdateBy != null)
               {
                  result.UpdateBy = await _peopleRepository.GetPeopleEmail(community.UpdateBy.Value);
               }

               _response.IsSuccess = true;
               _response.Message = "Get Community success";
               _response.ObjectResponse = result;
            }
         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }

         return _response;
      }

      public async Task<ResponseDTO> GetCommunityByName(string name)
      {
         try
         {
            IEnumerable<Community> objList = await _communityRepository.GetQueryAll()
                .Where(x => x.CommunityName.Contains(name)).ToListAsync();
            _response.IsSuccess = true;
            _response.ObjectResponse = _mapper.Map<List<CommunityDTOForSelectList>>(objList);
         }
         catch (Exception ex)
         {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
         }

         return _response;
      }

      public async Task<ResponseDTO> UpdateCommunity(int id, CommunityDTOForCreateOrUpdate communityDTO,
          int userUpdateId)
      {
         try
         {
            var check = await _communityRepository.GetCommunity(id);
            if (check == null)
            {
               _response.IsSuccess = true;
               _response.Message = "Community with id " + id + " does not exist";
            }
            else
            {
               check.CommunityName = communityDTO.CommunityName;
               check.LogoUrl = communityDTO.LogoUrl;
               check.UpdateTime = DateTime.Now;
               check.ShortDescription = communityDTO.ShortDescription;
               check.UpdateBy = userUpdateId;
               check.isActive = communityDTO.isActive;
               check.ParentCommunityId = communityDTO.ParentCommunityId;
               await _communityRepository.UpdateAsync(check);
               if (communityDTO.File != null)
               {
                  await using (var stream = new FileStream(communityDTO.LogoUrl, FileMode.Create))
                  {
                     await communityDTO.File.CopyToAsync(stream);
                  }
               }

               _response.IsSuccess = true;
               _response.Message = "Update community success";
            }
         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }

         return _response;
      }

      public async Task<ResponseDTO> GetAllCommunityTree()
      {
         try
         {
            IEnumerable<Community> objList = await _communityRepository.GetQueryAll()
                .Include(x => x.ParentCommunity)
                .Include(x => x.Collections)
                .ThenInclude(x => x.Items)
                .ToListAsync();


            var objMap = _mapper.Map<List<CommunityDTO>>(objList);
            objMap = objMap.Where(x => x.ParentCommunityId == null).ToList();
            //var re = objMap.Where(x => x.ParentCommunityId==1).Select(x=>x.CommunityId).ToList();


            _response.IsSuccess = true;
            _response.ObjectResponse = objMap;
         }
         catch (Exception ex)
         {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
         }

         return _response;
      }

      public async Task<ResponseDTO> GetAllCommunityTreeForUser()
      {
         try
         {
            IEnumerable<Community> objList = await _communityRepository.GetQueryAll()
                .Include(x => x.ParentCommunity)
                .Include(x => x.Collections.Where(x => x.isActive))
                .ThenInclude(x => x.Items.Where(x => x.Discoverable))
                .Where(x => x.isActive)
                .Where(x => x.ParentCommunity.isActive || x.ParentCommunityId == null)
                .ToListAsync();


            var objMap = _mapper.Map<List<CommunityDTO>>(objList);
            objMap = objMap.Where(x => x.ParentCommunityId == null).ToList();
            //var re = objMap.Where(x => x.ParentCommunityId==1).Select(x=>x.CommunityId).ToList();


            _response.IsSuccess = true;
            _response.ObjectResponse = objMap;
         }
         catch (Exception ex)
         {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
         }

         return _response;
      }

      public async Task<ResponseDTO> GetAllCommunity()
      {
         try
         {
            IEnumerable<Community> objlist =
                await _communityRepository.GetQueryAll().Include(x => x.People).ToListAsync();
            var objmap = _mapper.Map<List<CommunityDTOForSelectList>>(objlist);

            _response.IsSuccess = true;
            _response.ObjectResponse = objmap;
         }
         catch (Exception ex)
         {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
         }

         return _response;
      }

      public async Task<ResponseDTO> GetAllCommunityParent()
      {
         try
         {
            IEnumerable<Community> objList = await _communityRepository.GetQueryAll()
                .Where(x => x.ParentCommunityId == null)
                .ToListAsync();


            _response.IsSuccess = true;
            _response.ObjectResponse = _mapper.Map<List<CommunityDTOForDetailForUser>>(objList);
         }
         catch (Exception ex)
         {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
         }

         return _response;
      }

      public async Task<ResponseDTO> GetCommunityByParentId(int communityParentId)
      {
         try
         {
            IEnumerable<Community> objList = await _communityRepository.GetQueryAll()
                .Where(x => x.ParentCommunityId == communityParentId).Include(x => x.People)
                .ThenInclude(x => x.User).ToListAsync();
            _response.IsSuccess = true;
            _response.ObjectResponse = _mapper.Map<List<CommunityDTOForSelectList>>(objList);
         }
         catch (Exception ex)
         {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
         }

         return _response;
      }

      public async Task<ResponseDTO> GetAllCommunityForUser()
      {
         try
         {
            IEnumerable<Community> objList =
                await _communityRepository.GetQueryAll().Where(x => x.isActive).ToListAsync();

            _response.IsSuccess = true;
            _response.ObjectResponse = _mapper.Map<List<CommunityDTOForDetailForUser>>(objList);
         }
         catch (Exception ex)
         {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
         }

         return _response;
      }


      public async Task<ResponseDTO> GetCommunityByIDForUser(int id, string email)
      {
         try
         {
            var community = await _communityRepository.GetQueryAll().Where(x => x.isActive)
           .Include(x => x.ParentCommunity)

                .Include(x => x.SubCommunities.Where(y => y.isActive))
                .Include(x => x.Collections.Where(y => y.isActive))
                .ThenInclude(x => x.Items)
                .Include(x => x.Collections.Where(y => y.isActive))
                .ThenInclude(x => x.Subscribes)
                .SingleOrDefaultAsync(x => x.CommunityId == id);
            if (community == null)
            {
               _response.IsSuccess = true;
               _response.Message = "Community with id " + id + " does not exist";
            }
            else
            {
               var communityMap = _mapper.Map<CommunityDTOForDetailForUser>(community);
               foreach (var collectionMap in communityMap.Collections)
               {
                  collectionMap.IsSubcribe =
                      await _subscribeRepository.CheckSubscribe(collectionMap.CollectionId, email);
               }

               _response.IsSuccess = true;
               _response.ObjectResponse = communityMap;
            }
         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }

         return _response;
      }

      public async Task<ResponseDTO> GetCommunityByNameForUser(string name)
      {
         try
         {
            IEnumerable<Community> objList = await _communityRepository.GetQueryAll()
                .Where(x => x.CommunityName.Contains(name) && x.isActive).ToListAsync();
            _response.IsSuccess = true;
            _response.ObjectResponse = _mapper.Map<List<CommunityDTOForDetailForUser>>(objList);
         }
         catch (Exception ex)
         {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
         }

         return _response;
      }

      public async Task<ResponseDTO> GetCommunityByStaff(int peopleId)
      {
         try
         {
            var listCommunity = await _communityGroupRepository.GetListCommunityByStaff(peopleId);


            if (listCommunity.Count() == 0)
            {
               _response.IsSuccess = true;
               _response.Message = "You are not assigned to any communities";
               return _response;
            }

            var listCommunityMap = _mapper.Map<List<CommunityDTOForSelectListForStaff>>(listCommunity);
            _response.IsSuccess = true;

            _response.ObjectResponse = listCommunityMap;
         }
         catch (Exception ex)
         {
            _response.Message = ex.Message;
            _response.IsSuccess = false;
         }

         return _response;
      }

      public async Task<ResponseDTO> GetCommunityByIDForStaff(int communityId, int peopleId)
      {
         try
         {
            var listCommunities = await _communityGroupRepository.GetListCommunityIdByStaff(peopleId);
            var communityCheck = listCommunities.Distinct().Contains(communityId);
            var listCollections = await _collectionGroupRepository.GetListCollectionIdByStaffId(peopleId);
            var community = await _communityRepository.GetQueryAll()
                .Include(x => x.Collections)
                .ThenInclude(x => x.Items)
                .Include(x => x.SubCommunities)
                .Include(x => x.CommunityGroups)
                .ThenInclude(x => x.Group)
                .Include(x => x.People)
                .ThenInclude(x => x.User)
                .Include(x => x.ParentCommunity)
                .SingleOrDefaultAsync(x => x.CommunityId == communityId);
            if (community == null)
            {
               _response.IsSuccess = true;
               _response.Message = "Community with id " + communityId + " does not exist";
            }
            else
            {
               var result = _mapper.Map<CommunityDTOForDetailForStaff>(community);
               if (community.UpdateBy != null)
               {
                  result.UpdateBy = await _peopleRepository.GetPeopleEmail(community.UpdateBy.Value);
               }

               if (listCommunities.Contains(result.CommunityId))
               {
                  result.CanEdit = true;
               }

               foreach (var communitySub in result.SubCommunities)
               {
                  if (listCommunities.Contains(communitySub.CommunityId))
                  {
                     communitySub.CanEdit = true;
                  }
               }

               foreach (var collection in result.Collections)
               {
                  if (listCollections.Contains(collection.CollectionId))
                  {
                     collection.CanEdit = true;
                  }
               }

               if (community.ParentCommunityId != null)
               {
                  if (listCommunities.Contains((int)community.ParentCommunityId))
                  {
                     result.communityDTOParentStaff.canEdit = true;
                  }
               }

               _response.IsSuccess = true;
               _response.Message = "Get Community success";
               _response.ObjectResponse = result;
            }
         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }

         return _response;
      }
   }
}