﻿using Application.CommunityGroups;
using Application.GroupPeoples;
using Application.Responses;
using AutoMapper;
using Domain;
using Infrastructure;
using Infrastructure.Repositories.CollectionGroupRepositories;
using Infrastructure.Repositories.CommunityGroupRepositories;
using Infrastructure.Repositories.CommunityRepositories;
using Infrastructure.Repositories.GroupPeopleRepositories;
using Infrastructure.Repositories.GroupRepositories;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Services.Implements
{
   public class CommunityGroupService : ICommunityGroupService
   {
      public IMapper _mapper;
      private ICommunityGroupRepository _communityGroupRepository;
      private IGroupRepository _groupRepository;
      private ICommunityRepository _communityRepository;
      private ICollectionGroupRepository _collectionGroupRepository;
      private IGroupPeopleRepository _groupPeopleRepository;


      protected ResponseDTO _response;

      public CommunityGroupService(IMapper mapper, 
         ICommunityGroupRepository communityGroupRepository,
         IGroupRepository groupRepository,
         ICommunityRepository communityRepository,
         ICollectionGroupRepository collectionGroupRepository,
         IGroupPeopleRepository groupPeopleRepository
         )
      {
         _mapper = mapper;
         _response = new ResponseDTO();
         _communityGroupRepository = communityGroupRepository;
         _groupRepository = groupRepository;
         _collectionGroupRepository = collectionGroupRepository;
         _communityRepository= communityRepository;
         _groupPeopleRepository= groupPeopleRepository;
      }

      public async Task<ResponseDTO> AddCommunityManageGroup(List<CommunityGroupDTOForCreate> listCommunityGroupDTO)
      {
         try
         {
            foreach (var communityGroupDTO in listCommunityGroupDTO)
            {
               var groupCheck = await _groupRepository.GetGroup(communityGroupDTO.GroupId);
               var communityCheck = await _communityRepository.GetCommunity(communityGroupDTO.CommunityId);
               if (groupCheck == null || communityCheck == null)
               {
                  if (groupCheck == null)
                  {
                     _response.Message = "Group " + communityGroupDTO.GroupId + " does not exist";
                     _response.IsSuccess = false;
                     return _response;
                  }
                  if (communityCheck == null)
                  {
                     _response.Message = "Community " + communityGroupDTO.CommunityId + " does not exist";
                     _response.IsSuccess = false;
                     return _response;
                  }
               }
               var communityGroupCheck = await _communityGroupRepository.GetCommunityGroupByCommunityIdAndGroupId(communityGroupDTO.CommunityId,communityGroupDTO.GroupId);
               if (communityGroupCheck != null)
               {
                  _response.Message = "Group " + communityGroupDTO.GroupId + " does not exist";
                  _response.IsSuccess = false;
                  return _response;
               }

               CommunityGroup communityGroup = _mapper.Map<CommunityGroup>(communityGroupDTO);
               await _communityGroupRepository.AddAsync(communityGroup);
               List<int> listCollectionIdInCommunity = await _communityRepository.GetQueryAll().Where(x => x.CommunityId == communityGroupDTO.CommunityId).Include(x => x.SubCommunities).ThenInclude(x => x.Collections).Include(x => x.SubCommunities).ThenInclude(x => x.Collections).Include(x => x.Collections).SelectMany(x => x.Collections).Select(x => x.CollectionId).ToListAsync();
               List<int> listCollectionIdInCollectionGroup = await _collectionGroupRepository.GetQueryAll().Include(x => x.Collection).Where(x => x.GroupId == communityGroupDTO.GroupId && x.Collection.CommunityId == communityGroupDTO.CommunityId).Select(x => x.CollectionId).ToListAsync();
               foreach (int collectionId in listCollectionIdInCollectionGroup)
               {
                  if (listCollectionIdInCommunity.Any(x => x == collectionId))
                  {
                     listCollectionIdInCommunity.Remove(collectionId);
                  }
               }
               List<CollectionGroup> listCollectionGroup = new List<CollectionGroup>();
               foreach (int collectionId in listCollectionIdInCommunity)
               {
                  listCollectionGroup.Add(new CollectionGroup
                  {
                     CollectionId = collectionId,
                     GroupId = communityGroupDTO.GroupId
                  });
               }
               await _collectionGroupRepository.AddListAsync(listCollectionGroup);
            }
            
            _response.IsSuccess = true;
            _response.Message = "Add success";


         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }
         return _response;
      }
      public async Task<ResponseDTO> UpdateCommunityManageGroup(CommunityGroupDTOForUpdate communityGroupDTO)
      {
         try
         {
            var group = await _groupRepository.GetGroup(communityGroupDTO.GroupId);
            var community = await _communityRepository.GetCommunity(communityGroupDTO.CommunityId); 
            var communityGroup = await _communityGroupRepository.GetCommunityGroup(communityGroupDTO.Id);
            if (group == null || community == null || communityGroup == null)
            {
               _response.IsSuccess = false;
               if (group == null)
               {
                  _response.Message = "Group " + communityGroupDTO.GroupId + " does not exist";
                  return _response;
               }
               if (community == null)
               {
                  _response.Message = "Community " + communityGroupDTO.CommunityId + " does not exist";
                  return _response;

               }
               if (communityGroup == null)
               {
                  _response.Message = "Community Group" + communityGroupDTO.Id + " does not exist";
                  return _response;
               }

            }
            else
            {
               await _communityGroupRepository.UpdateAsync(communityGroup);
               _response.IsSuccess = true;
               _response.Message = "Update success";
            }

         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }
         return _response;
      }

      public async Task<ResponseDTO> DeleteCommunityManageInGroup(int id)
      {
         try
         {
            var communityGroupCheck = await _communityGroupRepository.GetCommunityGroup(id);
            if (communityGroupCheck == null)
            {
               _response.IsSuccess = true;
               _response.Message = "Group does not manage this community";
            }
            else
            {
              await _communityGroupRepository.DeleteAsync(communityGroupCheck);

               List<int> listCollectionInCommunity = await _communityRepository.GetQueryAll().Include(x => x.Collections).Where(x => x.CommunityId == communityGroupCheck.CommunityId).SelectMany(x => x.Collections).Select(x => x.CollectionId).ToListAsync();
               List<CollectionGroup> listCollectionGroup = await _collectionGroupRepository.GetQueryAll().Where(x => x.GroupId == communityGroupCheck.GroupId && listCollectionInCommunity.Any(y => y == x.CollectionId)).ToListAsync();
               await _collectionGroupRepository.DeleteListAsync(listCollectionGroup);
               _response.IsSuccess = true;
               _response.Message = "Delete group manage community success";
            }
         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }
         return _response;
      }

      public async Task<ResponseDTO> GetListCommunityGroup()
      {
         try
         {
            IEnumerable<CommunityGroup> communityGroups = await _communityGroupRepository.GetQueryAll()
               .Include(x => x.Group)
               .Include(x => x.Community)
               .ToListAsync();
            var result = _mapper.Map<List<CommunityGroupDTOForDetail>>(communityGroups);
            _response.IsSuccess = true;
            _response.ObjectResponse = result;

         }
         catch (Exception ex)
         {
            _response.IsSuccess = false;
            _response.Message = ex.Message;
         }
         return _response;
      }

      public async Task<List<Group>> GetListGroupByCommunityId(int communityId)
      {
         try
         {
            List<Group> groups = await _communityGroupRepository.GetQueryAll()
               .Where(x => x.CommunityId == communityId)
               .Select(x => x.Group)
               .ToListAsync();
            return groups;
         }
         catch (Exception ex)
         {
            throw new Exception(ex.Message);
         }
      }

      public async Task<List<Community>> GetListCommunityByGroupId(int groupId)
      {

         try
         {
            List<Community> communities = await _communityGroupRepository.GetQueryAll()
               .Where(x => x.GroupId == groupId)
               .Select(x => x.Community)
               .ToListAsync();
            return communities;
         }
         catch (Exception ex)
         {
            throw new Exception(ex.Message);
         }
      }

      public async Task<List<int>> GetListCommunityIdByStaff(int peopleId)
      {
         List<int> listGroupId = await _groupPeopleRepository.GetQueryAll().Where(x => x.PeopleId == peopleId).Select(x => x.GroupId).ToListAsync();
         List<int> listCommunityId = new List<int>();
         foreach (var groupId in listGroupId)
         {
            List<int> listCollectionGroup = await _communityGroupRepository.GetQueryAll().Include(x => x.Community).ThenInclude(x => x.SubCommunities).Where(x => x.GroupId == groupId).Select(x => x.CommunityId).ToListAsync();
            listCommunityId.AddRange(listCollectionGroup.ToList());
         }
         return listCommunityId;
      }

      public async Task<List<int>> GetListCollectionByStaff(int peopleId)
      {
         List<int> listCommunityId = await GetListCommunityIdByStaff(peopleId);
         List<int> listCollectionId = new List<int>();
         foreach (int communityId in listCommunityId)
         {
            var listTempCollectionId = await _communityRepository.GetQueryAll().Include(x => x.SubCommunities).ThenInclude(x => x.SubCommunities).Where(x => listCommunityId.Any(y => y == x.CommunityId)).SelectMany(x => x.Collections).Select(x => x.CollectionId).ToListAsync();
            if (listTempCollectionId.Count() > 0 || listTempCollectionId != null)
            {
               listCollectionId.AddRange(listTempCollectionId);
            }
         }
         return listCollectionId;
      }
   }
}
