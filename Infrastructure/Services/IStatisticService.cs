﻿using Application.Responses;
using Application.Statistics;

namespace Infrastructure.Services
{
   public interface IStatisticService
   {
      //public Task<bool> IncreaseView(int itemId,DateTime dateAccess);
      public Task<ResponseDTO> GetAllStatistics();
      public Task<ResponseDTO> GetAllStatisticByItemId(int itemId);
      public Task<StatisticDTOForSelectList> GetAllStatisticByItemId2(int? itemId);
      public Task<ResponseDTO> UploadStatistic(List<StatisticDTOForCreate> listCounterViewer);
      public Task<ResponseDTO> UploadStatisticIndividual(StatisticDTOForCreateIndividual counterViewer, string userId);
      public Task<ResponseDTO> GetStatisticForItem(int itemId);
      public Task<ResponseDTO> GetStatisticForCollection(int collectionId);
      public Task<ResponseDTO> GetStatisticForCommunity(int communityId);
   }
}
