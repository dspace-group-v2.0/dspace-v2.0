﻿using Application.GroupPeoples;
using Application.Responses;
using Domain;

namespace Infrastructure.Services
{
   public interface IGroupPeopleService
   {
      public Task<ResponseDTO> AddPeopleInGroup(GroupPeopleDTO groupPeopleDTO);
      public Task<ResponseDTO> DeletePeopleInGroup(GroupPeopleDTO groupPeopleDTO);

      public Task<ResponseDTO> AddListPeopleInGroup(GroupPeopleListDTOForCreateUpdate groupPeopleListDTO);

      //public Task<ResponseDTO> UpdateListPeopleInGroup(GroupPeopleListDTOForCreateUpdate groupPeopleListDTO);

      public Task<ResponseDTO> DeleteListPeopleInGroup(int groupId);

      //public Task<List<Group>> GetListGroupByStaff(int peopleId);

      //public Task<List<People>> GetListPeopleByGroupId(int groupId);







   }
}
