﻿using Application.Metadatas;
using Domain;
using Infrastructure.Repositories.Interfaces;

namespace Infrastructure.Services
{
   public interface IMetadataFieldRegistryService
   {
      public List<MetadateFieldDTO> GetMetadataFieldRegistries();
   }
}
