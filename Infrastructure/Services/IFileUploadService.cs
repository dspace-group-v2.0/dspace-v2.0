using Application.FileUploads;
using Application.Responses;
using Microsoft.AspNetCore.Http;

namespace Infrastructure.Services;

public interface IFileUploadService
{
    public Task<ResponseDTO> DownloadFileWithId(string fileKeyId);
    public Task<ResponseDTO> DeleteFileWithFileId(int fileId);
    public Task<ResponseDTO> UndiscoverableFileWithFileId(int fileId);
    public Task<ResponseDTO> ModifyFileWithFileId(ModifyFileWithAccessDto modifyFileWithAccessDto);
    public Task<ResponseDTO> AddNewFileToItem(int itemId, IList<IFormFile> multipleFiles, string role);
}