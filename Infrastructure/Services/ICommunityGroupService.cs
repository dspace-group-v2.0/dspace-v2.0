﻿using Application.CommunityGroups;
using Application.Responses;
using Domain;

namespace Infrastructure.Services
{
   public interface ICommunityGroupService
   {
      public Task<ResponseDTO> AddCommunityManageGroup(List<CommunityGroupDTOForCreate> communityGroupDTO);
      public Task<ResponseDTO> UpdateCommunityManageGroup(CommunityGroupDTOForUpdate communityGroupDTO);

      public Task<ResponseDTO> DeleteCommunityManageInGroup(int id);


      public Task<List<Group>> GetListGroupByCommunityId(int communityId);

      public Task<List<Community>> GetListCommunityByGroupId(int groupId);

      public Task<List<int>> GetListCommunityIdByStaff(int peopleId);

      public Task<List<int>> GetListCollectionByStaff(int peopleId);



      public Task<ResponseDTO> GetListCommunityGroup();
   }
}
