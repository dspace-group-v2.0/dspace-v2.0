﻿using Application.Emails;
using MimeKit;

namespace Infrastructure.Services
{
   public interface IEmailService
   {
      public void SendEmail(EmailDTO emailDTO);

      public void SendEmailBcc(List<string> emailBcc, string subject, TextPart body);
   }
}
