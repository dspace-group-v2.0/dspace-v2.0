﻿using Application.CollectionGroups;
using Application.Responses;
using Domain;

namespace Infrastructure.Services
{
   public interface ICollectionGroupService
   {
      public Task<ResponseDTO> AddCollectionManageGroup(List<CollectionGroupDTOForCreate> listCollectionGroupDTO);

      public Task<ResponseDTO> UpdateCollectionManageGroup(CollectionGroupDTOForUpdate collectionGroupDTO);

      public Task<ResponseDTO> DeleteCollectionManageInGroup(int id);

      public Task<List<Group>> GetListGroupByCollectionId(int collectionId);

      public Task<List<Collection>> GetListCollectionByGroupId(int groupId);

      public Task<ResponseDTO> GetListCollectionGroup();

      public Task<CollectionGroup> GetCollectionGroupByGroupIdAndCollectionId(int collectionId, int groupId);

      public Task<ResponseDTO> GetCollectionGroupDetailByStaff(int collectionId,int peopleId);

      public Task<List<int>> GetListCollectionIdByStaffId(int peopleId);

    

   }
}
