﻿using Domain;
using Infrastructure.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.AuthorRepositories
{
   public interface IAuthorRepository:IRepositoryBase<Author>
   {
      public Task<Author?> GetAuthor(int id);
      
   }
}
