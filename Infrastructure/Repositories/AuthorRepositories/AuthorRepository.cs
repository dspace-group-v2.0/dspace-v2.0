﻿using Domain;
using Infrastructure.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.AuthorRepositories
{
   public class AuthorRepository : RepositoryBase<Author> , IAuthorRepository
   {
      
      public AuthorRepository(DSpaceDbContext context,IUnitOfWork unitOfWork):base(context,unitOfWork) { 
        
      }
      public async Task<Author?> GetAuthor(int id)
      {
         return await GetQueryAll().SingleOrDefaultAsync(x => x.AuthorId.Equals(id));
      }

      
   }
}
