﻿using Domain;
using Infrastructure.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.MetadataValueRepositories
{
   public interface IMetadataValueRepository:IRepositoryBase<MetadataValue>
   {
      public Task<MetadataValue?> GetMetadataValue(int id);
   }
}
