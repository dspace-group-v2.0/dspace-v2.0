﻿using Domain;
using Infrastructure.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Org.BouncyCastle.Math.EC.Rfc7748;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.MetadataValueRepositories
{
   public class MetadataValueRepository : RepositoryBase<MetadataValue>, IMetadataValueRepository
   {
      public MetadataValueRepository(DSpaceDbContext context, IUnitOfWork unitOfWork) : base(context, unitOfWork)
      {
      }

      public async Task<MetadataValue?> GetMetadataValue(int id)
      {
         return await GetQueryAll().AsNoTracking().SingleOrDefaultAsync(x => x.MetadataValueId.Equals(id));
      }
   }
}
