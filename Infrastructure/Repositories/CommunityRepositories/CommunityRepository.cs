﻿using Domain;
using Infrastructure.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.CommunityRepositories
{
   public class CommunityRepository : RepositoryBase<Community>, ICommunityRepository
   {
      public CommunityRepository(DSpaceDbContext context, IUnitOfWork unitOfWork) : base(context, unitOfWork)
      {
      }
      public async Task<Community?> GetCommunity(int id)
      {
         return await GetQueryAll().SingleOrDefaultAsync(x => x.CommunityId.Equals(id));
      }
   }
}
