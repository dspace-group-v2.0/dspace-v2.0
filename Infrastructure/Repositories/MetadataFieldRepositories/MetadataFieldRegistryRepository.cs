﻿using Domain;
using Infrastructure.Repositories.Interfaces;

namespace Infrastructure.Repositories.MetadataFieldRepositories
{
   public class MetadataFieldRegistryRepository : RepositoryBase<MetadataFieldRegistry>, IMetadataFieldRegistryRepository
   {
      public MetadataFieldRegistryRepository(DSpaceDbContext context, IUnitOfWork unitOfWork) : base(context, unitOfWork)
      {
      }
   }
}
