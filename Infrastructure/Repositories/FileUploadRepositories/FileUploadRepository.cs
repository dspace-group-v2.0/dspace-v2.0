﻿using Domain;
using Infrastructure.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Org.BouncyCastle.Math.EC.Rfc7748;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.FileUploadRepositories
{
   public class FileUploadRepository : RepositoryBase<FileUpload>, IFileUploadRepository
   {
      public FileUploadRepository(DSpaceDbContext context, IUnitOfWork unitOfWork) : base(context, unitOfWork)
      {
      }
      public async Task<FileUpload?> GetFileUpload(int id)
      {
         return await GetQueryAll().SingleOrDefaultAsync(x => x.FileId.Equals(id));
      }

   }
}
