﻿using Domain;
using Infrastructure.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.FileUploadRepositories
{
   public interface IFileUploadRepository : IRepositoryBase<FileUpload>
   {
      public Task<FileUpload?> GetFileUpload(int id);
   }
}
