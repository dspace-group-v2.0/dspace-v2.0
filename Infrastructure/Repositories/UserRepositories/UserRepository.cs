﻿using Domain;
using Infrastructure.Repositories.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.UserRepositories
{
   public class UserRepository : RepositoryBase<User>, IUserRepository
   {
      private UserManager<User> _userManager;
      private RoleManager<Role> _roleManager;
      public UserRepository(DSpaceDbContext context, IUnitOfWork unitOfWork,UserManager<User> userManager,RoleManager<Role> roleManager) : base(context, unitOfWork)
      {
         _userManager = userManager;
         _roleManager = roleManager;
      }
      public async Task<bool> AssignRole(User user, string role)
      {
         try
         {
            IList<string> roles = await _userManager.GetRolesAsync(user);
            if (roles != null)
            {
               var removeRoleResult = await _userManager.RemoveFromRolesAsync(user, roles);
               if (!removeRoleResult.Succeeded)
               {
                  throw new Exception();
               }
            }


            var result = await _userManager.AddToRoleAsync(user, role);
            return result.Succeeded;
         }
         catch (Exception ex)
         {

            return false;
         }
      }

      public User CreateInstanceUser()
      {
         try
         {
            return Activator.CreateInstance<User>();
         }
         catch
         {
            throw new InvalidOperationException($"Can't create an instance of '{nameof(User)}'. ");
         }
      }

      public async Task<string> GetRole(User user)
      {
         try
         {
            var roles = await _userManager.GetRolesAsync(user);
            var role = roles.ToArray();
            return role[0];
         }
         catch (Exception ex)
         {
            return null;
         }
      }
      public async Task<bool> CreateUser(User user)
      {
         try
         {
            var result = await _userManager.CreateAsync(user);
            return result.Succeeded;
         }
         catch (Exception ex)
         {
            return false;


         }
      }
      public async Task<bool> UpdateUser(User user)
      {
         try
         {
            var result = await _userManager.UpdateAsync(user);
            return result.Succeeded;
         }
         catch (Exception ex)
         {
            return false;

         }
      }
      public async Task<User> getUserByEmail(string email)
      {
         try
         {
            var result = await _userManager.Users.Include(x => x.People).FirstOrDefaultAsync(x => x.Email.Equals(email));
            return result;
         }
         catch (Exception ex)
         {
            return null;
         }
      }
      public async Task<User> getUserById(string id)
      {
         try
         {
            var result = await _userManager.FindByIdAsync(id);
            return result;
         }
         catch (Exception ex)
         {
            return null;
         }
      }

      public List<Claim> getUserClaim(User user, string role)
      {
         List<Claim> listClaims = new List<Claim>();
         listClaims.Add(new Claim(ClaimTypes.NameIdentifier, user.Id));
         listClaims.Add(new Claim(ClaimTypes.Email, user.Email));
         listClaims.Add(new Claim(ClaimTypes.Name, user.FirstName + user.LastName));
         listClaims.Add(new Claim(ClaimTypes.Role, role));
         if ((role.Equals("ADMIN") || role.Equals("STAFF")) && user.People != null)
         {
            listClaims.Add(new Claim(ClaimTypes.Authentication, user.People.PeopleId.ToString()));
         }
         return listClaims;
      }
      public async Task<List<String>> GetListEmailRegister()
      {
         try
         {
            var result = await _userManager.Users.Select(x => x.Email).ToListAsync();
            return result;
         }
         catch (Exception ex)
         {
            return null;
         }
      }
   }
}
