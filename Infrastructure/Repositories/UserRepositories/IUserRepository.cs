﻿using Domain;
using Infrastructure.Repositories.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.UserRepositories
{
   public interface IUserRepository:IRepositoryBase<User>
   {
      public User CreateInstanceUser();
      public Task<string> GetRole(User user);
      public Task<bool> AssignRole(User user, string role);

      public Task<bool> CreateUser(User user);

      public Task<User> getUserByEmail(string email);

      public List<Claim> getUserClaim(User user, string roles);


      public Task<User> getUserById(string id);

      public Task<bool> UpdateUser(User user);
      public Task<List<string>> GetListEmailRegister();
   }
}
