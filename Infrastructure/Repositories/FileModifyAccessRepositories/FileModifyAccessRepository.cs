﻿using Domain;
using Infrastructure.Repositories.Interfaces;

namespace Infrastructure.Repositories.FileModifyAccessRepositories
{
   public class FileModifyAccessRepository : RepositoryBase<FileModifyAccess>, IFileModifyAccessRepository
   {
      public FileModifyAccessRepository(DSpaceDbContext context, IUnitOfWork unitOfWork) : base(context, unitOfWork)
      {
      }
   }
}
