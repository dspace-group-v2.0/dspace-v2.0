﻿using Domain;
using Infrastructure.Repositories.GroupPeopleRepositories;
using Infrastructure.Repositories.Interfaces;
using Infrastructure.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static iText.StyledXmlParser.Jsoup.Select.Evaluator;

namespace Infrastructure.Repositories.CommunityGroupRepositories
{
   public class CommunityGroupRepository : RepositoryBase<CommunityGroup>, ICommunityGroupRepository
   {
      private IGroupPeopleRepository _groupPeopleRepository;
      public CommunityGroupRepository(DSpaceDbContext context, IUnitOfWork unitOfWork,IGroupPeopleRepository groupPeopleRepository) : base(context, unitOfWork)
      {
         _groupPeopleRepository = groupPeopleRepository;
      }

      public async Task<CommunityGroup?> GetCommunityGroup(int id)
      {
         return await GetQueryAll().SingleOrDefaultAsync(x => x.Id.Equals(id));
      }

      public async Task<CommunityGroup?> GetCommunityGroupByCommunityIdAndGroupId(int communityId, int groupId)
      {
         return await GetQueryAll().SingleOrDefaultAsync(x => x.CommunityId.Equals(communityId)&&x.GroupId.Equals(groupId));

      }

      public async Task<List<int>> GetListCommunityIdByStaff(int peopleId)
      {
         var listGroup =await _groupPeopleRepository.GetListGroupByStaff(peopleId);

         return await GetQueryAll().Where(x => listGroup.Any(y=>y==x.GroupId)).Select(x=>x.CommunityId).Distinct().ToListAsync();

      }
      public async Task<List<Community>> GetListCommunityByStaff(int peopleId)
      {
         var listGroup = await _groupPeopleRepository.GetListGroupByStaff(peopleId);

         return await GetQueryAll().Include(x=>x.Community).Where(x => listGroup.Any(y => y == x.GroupId)).Select(x => x.Community).Distinct().ToListAsync();

      }
      public async Task<List<int>> GetListGroupIdByCommunityId(int communityId) { 
         return await GetQueryAll().Where(x => x.CommunityId ==communityId).Select(x=>x.GroupId).ToListAsync();

      }

   }
}
