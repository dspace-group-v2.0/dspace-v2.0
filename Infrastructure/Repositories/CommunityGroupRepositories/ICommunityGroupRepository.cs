﻿using Application.CommunityGroups;
using Application.Responses;
using Domain;
using Infrastructure.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.CommunityGroupRepositories
{
   public interface ICommunityGroupRepository:IRepositoryBase<CommunityGroup>
   {

      public Task<CommunityGroup?> GetCommunityGroup(int id);

      public Task<List<int>> GetListCommunityIdByStaff(int peopleId);

      public Task<List<Community>> GetListCommunityByStaff(int peopleId);


      public Task<List<int>> GetListGroupIdByCommunityId(int groupId);

      public Task<CommunityGroup?> GetCommunityGroupByCommunityIdAndGroupId(int communityId, int groupId);


   }
}
