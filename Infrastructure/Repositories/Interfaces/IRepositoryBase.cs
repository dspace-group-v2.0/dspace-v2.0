﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.Interfaces
{
   public interface IRepositoryBase<M> where M : class
   {
      public Task<bool> AddListAsync(List<M> list);
      public Task<bool> UpdateListAsync(List<M> list);

      public Task<bool> DeleteListAsync(List<M> list);

      public Task<M> AddAsync(M entity);
      public Task<M> UpdateAsync(M entity);
      public Task DeleteAsync(M entity);
      public IQueryable<M> GetQueryAll();
      public Task<IEnumerable<M>> GetAll();

   }
}
