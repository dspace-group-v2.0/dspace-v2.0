﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.Interfaces
{
   public class UnitOfWork : IUnitOfWork
   {
      private readonly DSpaceDbContext _context;
      public UnitOfWork(DSpaceDbContext context)
      {
         _context = context;
      }

      public async Task<int> CommitAsync()
      {
         return await _context.SaveChangesAsync();
      }

      public void Dispose()
      {
         if (_context == null) return;
         _context.Dispose();
      }
      public async Task<bool> Save() { 
         bool isSuccess = await _context.SaveChangesAsync()>0;
         return isSuccess;

      }

   }
}
