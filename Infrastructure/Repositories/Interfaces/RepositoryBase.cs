﻿using iText.Commons.Actions.Contexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.Interfaces
{
   public class RepositoryBase<M> : IRepositoryBase<M> where M : class
   {
      private readonly DSpaceDbContext _context;
      private readonly IUnitOfWork _unitOfWork;
      public RepositoryBase(DSpaceDbContext context,IUnitOfWork unitOfWork)
      {
         _unitOfWork = unitOfWork ?? throw new ArgumentOutOfRangeException(nameof(unitOfWork));
         _context = context ?? throw new ArgumentOutOfRangeException(nameof(context));
      }
      public async Task<M> AddAsync(M entity)
      {
         await _context.Set<M>().AddAsync(entity);
         await _unitOfWork.CommitAsync();
         return entity;
      }

      public async Task<bool> AddListAsync(List<M> list)
      {
         await _context.Set<M>().AddRangeAsync(list);
         return (await _unitOfWork.CommitAsync())>0;
         
      }

      public async Task DeleteAsync(M entity)
      {
         _context.Set<M>().Remove(entity);
         await _unitOfWork.CommitAsync();
      }

      public async Task<bool> DeleteListAsync(List<M> list)
      {
         _context.Set<M>().RemoveRange(list);
         return (await _unitOfWork.CommitAsync()) > 0;

      }

      public async Task<IEnumerable<M>> GetAll()
      {
         return await _context.Set<M>().ToListAsync();
      }

      

      public IQueryable<M> GetQueryAll()
      {
         return _context.Set<M>();
      }

      public async Task<M> UpdateAsync(M entity)
      {
         _context.Set<M>().Update(entity);
         await _unitOfWork.CommitAsync();
         return entity;
      }

      public async Task<bool> UpdateListAsync(List<M> list)
      {
         _context.Set<M>().UpdateRange(list);
         return (await _unitOfWork.CommitAsync()) > 0;
      }
   }
}
