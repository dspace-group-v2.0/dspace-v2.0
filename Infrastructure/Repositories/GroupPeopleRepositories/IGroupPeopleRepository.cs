﻿using Application.GroupPeoples;
using Application.Responses;
using Domain;
using Infrastructure.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.GroupPeopleRepositories
{
   public interface IGroupPeopleRepository: IRepositoryBase<GroupPeople>
   {
      public Task<GroupPeople?> GetGroupPeopleByGroupIdAndPeopleId(int groupId, int peopleId);
      public Task<List<int>> GetListGroupByStaff(int peopleId);
   }
}
