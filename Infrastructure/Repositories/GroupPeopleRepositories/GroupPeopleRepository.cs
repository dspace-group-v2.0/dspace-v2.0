﻿using Application.GroupPeoples;
using Application.Responses;
using Domain;
using Infrastructure.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Org.BouncyCastle.Math.EC.Rfc7748;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.GroupPeopleRepositories
{
   public class GroupPeopleRepository : RepositoryBase<GroupPeople>, IGroupPeopleRepository
   {
      public GroupPeopleRepository(DSpaceDbContext context, IUnitOfWork unitOfWork) : base(context, unitOfWork)
      {
      }
      public async Task<GroupPeople?> GetGroupPeopleByGroupIdAndPeopleId(int groupId, int peopleId)
      {
         return await GetQueryAll().Include(x => x.Group).SingleOrDefaultAsync(x => x.Group.isActive && x.GroupId.Equals(groupId) && x.PeopleId.Equals(peopleId));
      }
      public async Task<List<int>> GetListGroupByStaff(int peopleId)
      {
         return await GetQueryAll().Include(x => x.Group).Where(x => x.Group.isActive && x.PeopleId.Equals(peopleId)).Select(x=>x.GroupId).ToListAsync();
      }
   }
}
