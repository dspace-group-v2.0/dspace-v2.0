﻿using Domain;
using Infrastructure.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.ItemRepositories
{
   public class ItemRepository : RepositoryBase<Item>, IItemRepository
   {
      public ItemRepository(DSpaceDbContext context, IUnitOfWork unitOfWork) : base(context, unitOfWork)
      {
      }
      public async Task<Item?> GetItem(int id)
      {
         return await GetQueryAll().SingleOrDefaultAsync(x => x.ItemId.Equals(id));
      }
   }
}
