﻿using Domain;
using Infrastructure.Repositories.GroupPeopleRepositories;
using Infrastructure.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories.CollectionGroupRepositories
{
   public class CollectionGroupRepository : RepositoryBase<CollectionGroup>, ICollectionGroupRepository
   {
      private IGroupPeopleRepository _groupPeopleRepository;
      public CollectionGroupRepository(DSpaceDbContext context, IUnitOfWork unitOfWork, IGroupPeopleRepository groupPeopleRepository) : base(context, unitOfWork)
      {
         _groupPeopleRepository = groupPeopleRepository;
      }

      public async Task<CollectionGroup?> GetCollectionGroup(int id)
      {
         return await GetQueryAll().SingleOrDefaultAsync(x => x.Id.Equals(id));
      }

      public async Task<List<int>> GetListCollectionIdByStaffId(int peopleId)
      {
         var listGroup = await _groupPeopleRepository.GetListGroupByStaff(peopleId);

         return await GetQueryAll().Where(x => listGroup.Any(y => y == x.GroupId)).Select(x => x.CollectionId).Distinct().ToListAsync();
      }
   }
}
