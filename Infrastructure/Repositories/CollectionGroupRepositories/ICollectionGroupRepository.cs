﻿using Domain;
using Infrastructure.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.CollectionGroupRepositories
{
   public interface ICollectionGroupRepository:IRepositoryBase<CollectionGroup>
   {
      public Task<CollectionGroup?> GetCollectionGroup(int id);
      public IQueryable<CollectionGroup> GetQueryAll();
      public Task<List<int>> GetListCollectionIdByStaffId(int peopleId);
   }
}
