﻿using Domain;
using Infrastructure.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.GroupRepositories
{
   public interface IGroupRepository : IRepositoryBase<Group>
   {
      public Task<Group?> GetGroup(int id);
   }
      
}
