﻿using Domain;
using Infrastructure.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.GroupRepositories
{
   public class GroupRepository : RepositoryBase<Group>, IGroupRepository
   {
      public GroupRepository(DSpaceDbContext context, IUnitOfWork unitOfWork) : base(context, unitOfWork)
      {
      }
      public async Task<Group?> GetGroup(int id)
      {
         return await GetQueryAll().SingleOrDefaultAsync(x => x.GroupId.Equals(id));
      }

   }
}
