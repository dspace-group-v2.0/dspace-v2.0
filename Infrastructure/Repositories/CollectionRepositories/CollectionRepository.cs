﻿using Domain;
using Infrastructure.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.CollectionRepositories
{
   public class CollectionRepository : RepositoryBase<Collection>, ICollectionRepository
   {
      public CollectionRepository(DSpaceDbContext context,IUnitOfWork unitOfWork):base(context,unitOfWork) { }
    
      public async Task<Collection?> GetCollection(int id)
      {
         return await GetQueryAll().SingleOrDefaultAsync(x => x.CollectionId.Equals(id));
      }
   }
}
