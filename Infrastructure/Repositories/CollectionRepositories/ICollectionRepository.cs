﻿using Domain;
using Infrastructure.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.CollectionRepositories
{
   public interface ICollectionRepository:IRepositoryBase<Collection>
   {
      public Task<Collection?> GetCollection(int id);
     
   }
}
