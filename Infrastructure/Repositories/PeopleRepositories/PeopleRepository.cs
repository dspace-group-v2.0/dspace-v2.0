﻿using Domain;
using Infrastructure.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.PeopleRepositories
{
   public class PeopleRepository : RepositoryBase<People>, IPeopleRepository
   {
      public PeopleRepository(DSpaceDbContext context, IUnitOfWork unitOfWork) : base(context, unitOfWork)
      {
      }

      public async Task<People?> GetPeople(int peopleId)
      {
         var people = await GetQueryAll().Include(x=>x.User).SingleOrDefaultAsync(x=>x.PeopleId.Equals(peopleId));
         return people;
      }

      public async Task<string> GetPeopleEmail(int peopleId)
      {
         var people = await GetQueryAll().Include(x => x.User).SingleOrDefaultAsync(x => x.PeopleId.Equals(peopleId));
         return people.User.Email;
      }
   }
}
