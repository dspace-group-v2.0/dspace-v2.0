﻿using Domain;
using Infrastructure.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.PeopleRepositories
{
   public interface IPeopleRepository:IRepositoryBase<People>
   {
      public Task<People?> GetPeople(int peopleId);

      public Task<string> GetPeopleEmail(int peopleId);
   }
}
