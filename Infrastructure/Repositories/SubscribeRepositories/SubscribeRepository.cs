﻿using Domain;
using Infrastructure.Repositories.Interfaces;
using Infrastructure.Repositories.UserRepositories;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories.SubscribeRepositories
{
   public class SubscribeRepository : RepositoryBase<Subscribe>, ISubscribeRepository
   {
      private IUserRepository _userRepository;
      public SubscribeRepository(DSpaceDbContext context, IUnitOfWork unitOfWork, IUserRepository userRepository) : base(context, unitOfWork)
      {
         _userRepository = userRepository;
      }

      public async Task<bool> CheckSubscribe(int collectionId, string email)
      {
         var userCheck = await _userRepository.getUserByEmail(email);
         var subcribes = GetQueryAll().SingleOrDefault(x => x.UserId == userCheck.Id && x.CollectionId == collectionId);
         if (subcribes == null)
         {
            return false;
         }
         return true;

      }
      public async Task<List<string>> GetListUserEmailSubcribeCollection(int collectionId)
      {
         return await GetQueryAll().Where(x => x.CollectionId.Equals(collectionId)).Include(x => x.User).Select(x => x.User.Email).ToListAsync();
      }
      public async Task<List<int>> GetAllSubscribedCollectionId(string email)
      {
         var user = await _userRepository.getUserByEmail(email);
         List<int> listCollectionId = await GetQueryAll().Where(x => x.UserId == user.Id).Select(x => x.CollectionId).ToListAsync();
         return listCollectionId;
      }

   }
}
