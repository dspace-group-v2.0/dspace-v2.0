﻿using Application.Items;
using Domain;
using Infrastructure.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.SubscribeRepositories
{
   public interface ISubscribeRepository:IRepositoryBase<Subscribe>
   {
      public Task<bool> CheckSubscribe(int collectionId,string email);

      public Task<List<string>> GetListUserEmailSubcribeCollection(int collectionId);

      public Task<List<int>> GetAllSubscribedCollectionId(string email);
   }
}
