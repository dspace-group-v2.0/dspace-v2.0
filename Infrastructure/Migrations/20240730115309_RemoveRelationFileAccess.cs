﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class RemoveRelationFileAccess : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FileModifyAccesses_Roles_RoleId",
                table: "FileModifyAccesses");

            migrationBuilder.DropIndex(
                name: "IX_FileModifyAccesses_RoleId",
                table: "FileModifyAccesses");

            migrationBuilder.DropColumn(
                name: "RoleId",
                table: "FileModifyAccesses");

            migrationBuilder.AddColumn<string>(
                name: "Role",
                table: "FileModifyAccesses",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Role",
                table: "FileModifyAccesses");

            migrationBuilder.AddColumn<string>(
                name: "RoleId",
                table: "FileModifyAccesses",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_FileModifyAccesses_RoleId",
                table: "FileModifyAccesses",
                column: "RoleId");

            migrationBuilder.AddForeignKey(
                name: "FK_FileModifyAccesses_Roles_RoleId",
                table: "FileModifyAccesses",
                column: "RoleId",
                principalTable: "Roles",
                principalColumn: "Id");
        }
    }
}
