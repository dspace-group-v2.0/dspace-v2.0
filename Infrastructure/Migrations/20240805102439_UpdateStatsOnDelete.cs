﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class UpdateStatsOnDelete : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Statistics_Collections_CollectionId",
                table: "Statistics");

            migrationBuilder.DropForeignKey(
                name: "FK_Statistics_Communities_CommunityId",
                table: "Statistics");

            migrationBuilder.DropForeignKey(
                name: "FK_Statistics_Items_ItemId",
                table: "Statistics");

            migrationBuilder.DropForeignKey(
                name: "FK_Statistics_Users_UserId",
                table: "Statistics");

            migrationBuilder.AddForeignKey(
                name: "FK_Statistics_Collections_CollectionId",
                table: "Statistics",
                column: "CollectionId",
                principalTable: "Collections",
                principalColumn: "CollectionId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Statistics_Communities_CommunityId",
                table: "Statistics",
                column: "CommunityId",
                principalTable: "Communities",
                principalColumn: "CommunityId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Statistics_Items_ItemId",
                table: "Statistics",
                column: "ItemId",
                principalTable: "Items",
                principalColumn: "ItemId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Statistics_Users_UserId",
                table: "Statistics",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Statistics_Collections_CollectionId",
                table: "Statistics");

            migrationBuilder.DropForeignKey(
                name: "FK_Statistics_Communities_CommunityId",
                table: "Statistics");

            migrationBuilder.DropForeignKey(
                name: "FK_Statistics_Items_ItemId",
                table: "Statistics");

            migrationBuilder.DropForeignKey(
                name: "FK_Statistics_Users_UserId",
                table: "Statistics");

            migrationBuilder.AddForeignKey(
                name: "FK_Statistics_Collections_CollectionId",
                table: "Statistics",
                column: "CollectionId",
                principalTable: "Collections",
                principalColumn: "CollectionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Statistics_Communities_CommunityId",
                table: "Statistics",
                column: "CommunityId",
                principalTable: "Communities",
                principalColumn: "CommunityId");

            migrationBuilder.AddForeignKey(
                name: "FK_Statistics_Items_ItemId",
                table: "Statistics",
                column: "ItemId",
                principalTable: "Items",
                principalColumn: "ItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_Statistics_Users_UserId",
                table: "Statistics",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id");
        }
    }
}
