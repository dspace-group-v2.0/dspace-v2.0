﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class UpdateEntityStats : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Statistics_Users_StudentId",
                table: "Statistics");

            migrationBuilder.DropColumn(
                name: "LastCheck",
                table: "Statistics");

            migrationBuilder.RenameColumn(
                name: "ViewCount",
                table: "Statistics",
                newName: "Day");

            migrationBuilder.RenameColumn(
                name: "StudentId",
                table: "Statistics",
                newName: "UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Statistics_StudentId",
                table: "Statistics",
                newName: "IX_Statistics_UserId");

            migrationBuilder.AlterColumn<int>(
                name: "ItemId",
                table: "Statistics",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "CollectionId",
                table: "Statistics",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CommunityId",
                table: "Statistics",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "GroupId",
                table: "CollectionGroups",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CollectionId",
                table: "CollectionGroups",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Statistics_CollectionId",
                table: "Statistics",
                column: "CollectionId");

            migrationBuilder.CreateIndex(
                name: "IX_Statistics_CommunityId",
                table: "Statistics",
                column: "CommunityId");

            migrationBuilder.AddForeignKey(
                name: "FK_Statistics_Collections_CollectionId",
                table: "Statistics",
                column: "CollectionId",
                principalTable: "Collections",
                principalColumn: "CollectionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Statistics_Communities_CommunityId",
                table: "Statistics",
                column: "CommunityId",
                principalTable: "Communities",
                principalColumn: "CommunityId");

            migrationBuilder.AddForeignKey(
                name: "FK_Statistics_Users_UserId",
                table: "Statistics",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Statistics_Collections_CollectionId",
                table: "Statistics");

            migrationBuilder.DropForeignKey(
                name: "FK_Statistics_Communities_CommunityId",
                table: "Statistics");

            migrationBuilder.DropForeignKey(
                name: "FK_Statistics_Users_UserId",
                table: "Statistics");

            migrationBuilder.DropIndex(
                name: "IX_Statistics_CollectionId",
                table: "Statistics");

            migrationBuilder.DropIndex(
                name: "IX_Statistics_CommunityId",
                table: "Statistics");

            migrationBuilder.DropColumn(
                name: "CollectionId",
                table: "Statistics");

            migrationBuilder.DropColumn(
                name: "CommunityId",
                table: "Statistics");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "Statistics",
                newName: "StudentId");

            migrationBuilder.RenameColumn(
                name: "Day",
                table: "Statistics",
                newName: "ViewCount");

            migrationBuilder.RenameIndex(
                name: "IX_Statistics_UserId",
                table: "Statistics",
                newName: "IX_Statistics_StudentId");

            migrationBuilder.AlterColumn<int>(
                name: "ItemId",
                table: "Statistics",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastCheck",
                table: "Statistics",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AlterColumn<int>(
                name: "GroupId",
                table: "CollectionGroups",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "CollectionId",
                table: "CollectionGroups",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Statistics_Users_StudentId",
                table: "Statistics",
                column: "StudentId",
                principalTable: "Users",
                principalColumn: "Id");
        }
    }
}
