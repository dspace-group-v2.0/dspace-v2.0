﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class DeleteMetadataValueInAuthor : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MetadataValues_Authors_AuthorId",
                table: "MetadataValues");

            migrationBuilder.DropIndex(
                name: "IX_MetadataValues_AuthorId",
                table: "MetadataValues");

            migrationBuilder.DropColumn(
                name: "AuthorId",
                table: "MetadataValues");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AuthorId",
                table: "MetadataValues",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_MetadataValues_AuthorId",
                table: "MetadataValues",
                column: "AuthorId");

            migrationBuilder.AddForeignKey(
                name: "FK_MetadataValues_Authors_AuthorId",
                table: "MetadataValues",
                column: "AuthorId",
                principalTable: "Authors",
                principalColumn: "AuthorId");
        }
    }
}
