﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class UpdateStatisticAndItemAuthor : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AuthorItems");

            migrationBuilder.DropColumn(
                name: "LanguageId",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "MetadataId",
                table: "Items");

            migrationBuilder.AddColumn<DateTime>(
                name: "LastCheck",
                table: "Statistics",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "StudentId",
                table: "Statistics",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AuthorId",
                table: "MetadataValues",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Statistics_StudentId",
                table: "Statistics",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_MetadataValues_AuthorId",
                table: "MetadataValues",
                column: "AuthorId");

            migrationBuilder.AddForeignKey(
                name: "FK_MetadataValues_Authors_AuthorId",
                table: "MetadataValues",
                column: "AuthorId",
                principalTable: "Authors",
                principalColumn: "AuthorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Statistics_Users_StudentId",
                table: "Statistics",
                column: "StudentId",
                principalTable: "Users",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MetadataValues_Authors_AuthorId",
                table: "MetadataValues");

            migrationBuilder.DropForeignKey(
                name: "FK_Statistics_Users_StudentId",
                table: "Statistics");

            migrationBuilder.DropIndex(
                name: "IX_Statistics_StudentId",
                table: "Statistics");

            migrationBuilder.DropIndex(
                name: "IX_MetadataValues_AuthorId",
                table: "MetadataValues");

            migrationBuilder.DropColumn(
                name: "LastCheck",
                table: "Statistics");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Statistics");

            migrationBuilder.DropColumn(
                name: "AuthorId",
                table: "MetadataValues");

            migrationBuilder.AddColumn<int>(
                name: "LanguageId",
                table: "Items",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MetadataId",
                table: "Items",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "AuthorItems",
                columns: table => new
                {
                    ItemId = table.Column<int>(type: "int", nullable: false),
                    AuthorId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthorItems", x => new { x.ItemId, x.AuthorId });
                    table.ForeignKey(
                        name: "FK_AuthorItems_Authors_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Authors",
                        principalColumn: "AuthorId");
                    table.ForeignKey(
                        name: "FK_AuthorItems_Items_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Items",
                        principalColumn: "ItemId");
                });

            migrationBuilder.CreateIndex(
                name: "IX_AuthorItems_AuthorId",
                table: "AuthorItems",
                column: "AuthorId");
        }
    }
}
