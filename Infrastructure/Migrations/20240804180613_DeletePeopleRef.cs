﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class DeletePeopleRef : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Collections_Peoples_CreateBy",
                table: "Collections");

            migrationBuilder.DropForeignKey(
                name: "FK_Communities_Peoples_CreateBy",
                table: "Communities");

            migrationBuilder.DropForeignKey(
                name: "FK_Items_Peoples_SubmitterId",
                table: "Items");

            migrationBuilder.DropForeignKey(
                name: "FK_Peoples_Users_UserId",
                table: "Peoples");

            migrationBuilder.AddForeignKey(
                name: "FK_Collections_Peoples_CreateBy",
                table: "Collections",
                column: "CreateBy",
                principalTable: "Peoples",
                principalColumn: "PeopleId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Communities_Peoples_CreateBy",
                table: "Communities",
                column: "CreateBy",
                principalTable: "Peoples",
                principalColumn: "PeopleId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Items_Peoples_SubmitterId",
                table: "Items",
                column: "SubmitterId",
                principalTable: "Peoples",
                principalColumn: "PeopleId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Peoples_Users_UserId",
                table: "Peoples",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Collections_Peoples_CreateBy",
                table: "Collections");

            migrationBuilder.DropForeignKey(
                name: "FK_Communities_Peoples_CreateBy",
                table: "Communities");

            migrationBuilder.DropForeignKey(
                name: "FK_Items_Peoples_SubmitterId",
                table: "Items");

            migrationBuilder.DropForeignKey(
                name: "FK_Peoples_Users_UserId",
                table: "Peoples");

            migrationBuilder.AddForeignKey(
                name: "FK_Collections_Peoples_CreateBy",
                table: "Collections",
                column: "CreateBy",
                principalTable: "Peoples",
                principalColumn: "PeopleId");

            migrationBuilder.AddForeignKey(
                name: "FK_Communities_Peoples_CreateBy",
                table: "Communities",
                column: "CreateBy",
                principalTable: "Peoples",
                principalColumn: "PeopleId");

            migrationBuilder.AddForeignKey(
                name: "FK_Items_Peoples_SubmitterId",
                table: "Items",
                column: "SubmitterId",
                principalTable: "Peoples",
                principalColumn: "PeopleId");

            migrationBuilder.AddForeignKey(
                name: "FK_Peoples_Users_UserId",
                table: "Peoples",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id");
        }
    }
}
