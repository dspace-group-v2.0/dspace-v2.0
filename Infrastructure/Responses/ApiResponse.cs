﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Responses
{
   public class ApiResponse
   {
      public bool IsSuccess;
      public string Message;
      public object ObjectResponse;
      public ApiResponse() { }
      public ApiResponse(object value)
      {
         ObjectResponse = value;
      }
      public ApiResponse(bool isSuccess, object value)
      {
         IsSuccess = isSuccess;
         ObjectResponse = value;
      }
      public ApiResponse(bool isSuccess, string message)
      {
         IsSuccess= isSuccess;
         Message = message;
      }
      public ApiResponse(bool isSuccess, string message, object value)
      {
         ObjectResponse = value;
         IsSuccess = isSuccess;
         Message = message;
      }
   }
}
