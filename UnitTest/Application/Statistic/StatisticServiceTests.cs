﻿using Xunit;
using Moq;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Application.Responses;
using Application.Statistics;
using Domain;
using Infrastructure.Repositories.ItemRepositories;
using Infrastructure.Repositories.StatisticRepositories;
using Infrastructure.Services.Implements;
using UnitTest.Application;

namespace UnitTest.Application.Statistic
{
    public class StatisticServiceTests
    {
        private readonly Mock<IStatisticRepository> _statisticRepositoryMock;
        private readonly Mock<IMapper> _mapperMock;
        private readonly Mock<IItemRepository> _itemRepositoryMock;
        private readonly StatisticService _statisticService;

        public StatisticServiceTests()
        {
            _statisticRepositoryMock = new Mock<IStatisticRepository>();
            _mapperMock = new Mock<IMapper>();
            _itemRepositoryMock = new Mock<IItemRepository>();

            _statisticService = new StatisticService(
                _statisticRepositoryMock.Object,
                _mapperMock.Object,
                _itemRepositoryMock.Object
            );
        }
        
        [Fact]
        public async Task UploadStatistic_AddsOrUpdatesStatistics()
        {
            // Arrange
            var statisticDtos = new List<StatisticDTOForCreate>
            {
                new StatisticDTOForCreate { ItemId = 1, ViewOnDay = 5, Month = 8, Year = 2024 },
                new StatisticDTOForCreate { ItemId = 2, ViewOnDay = 10, Month = 8, Year = 2024 }
            };

            _statisticRepositoryMock.Setup(r => r.GetAll())
                .ReturnsAsync(new List<Domain.Statistic>());

            _mapperMock.Setup(m => m.Map<Domain.Statistic>(It.IsAny<StatisticDTOForCreate>()))
                .Returns(new Domain.Statistic());

            // Act
            var result = await _statisticService.UploadStatistic(statisticDtos);

            // Xunit.Assert
            Xunit.Assert.True(result.IsSuccess);
            _statisticRepositoryMock.Verify(r => r.AddAsync(It.IsAny<Domain.Statistic>()), Times.Exactly(2));
            _statisticRepositoryMock.Verify(r => r.UpdateAsync(It.IsAny<Domain.Statistic>()), Times.Never);
        }

        public async Task GetStatisticForItem_ReturnsStatisticForSpecificItemWithNoViewCount()
        {
            // Arrange
            int itemId = 1;

            var statistics = new List<Domain.Statistic>
            {
                //new Statistic { ItemId = itemId, ViewOnDay = 10, Month = 1, Year = 2024 },
                //new Statistic { ItemId = itemId, ViewOnDay = 5, Month = 2, Year = 2024 }
            };

            _statisticRepositoryMock.Setup(r => r.GetQueryAll())
                .Returns(statistics.AsAsyncQueryable());

            _mapperMock.Setup(m => m.Map<List<StatisticDTO>>(statistics))
                .Returns(new List<StatisticDTO>());

            // Act
            var result = await _statisticService.GetStatisticForItem(itemId);

            // Xunit.Assert
            Xunit.Assert.True(result.IsSuccess);
            var stats = Xunit.Assert.IsType<StatisticDTOForItem>(result.ObjectResponse);

            Xunit.Assert.Equal(0, stats.Item.First().TotalCount);

        }

        [Fact]
        public async Task GetStatisticForCollection_ReturnsStatisticForSpecificCollection()
        {
            // Arrange
            int collectionId = 1;

            var statistics = new List<Domain.Statistic>
            {
                new Domain.Statistic { CollectionId = collectionId, ViewOnDay = 10, Month = 1, Year = 2024 },
                new Domain.Statistic { CollectionId = collectionId, ViewOnDay = 5, Month = 2, Year = 2024 }
            };

            _statisticRepositoryMock.Setup(r => r.GetQueryAll())
                .Returns(statistics.AsAsyncQueryable());

            _mapperMock.Setup(m => m.Map<List<StatisticDTO>>(statistics))
                .Returns(new List<StatisticDTO>());

            // Act
            var result = await _statisticService.GetStatisticForCollection(collectionId);

            // Xunit.Assert
            Xunit.Assert.True(result.IsSuccess);

        }

        [Fact]
        public async Task GetStatisticForCommunity_ReturnsStatisticForSpecificCommunity()
        {
            // Arrange
            int communityId = 1;

            var statistics = new List<Domain.Statistic>
            {
                new Domain.Statistic { CommunityId = communityId, ViewOnDay = 10, Month = 1, Year = 2024 },
                new Domain.Statistic { CommunityId = communityId, ViewOnDay = 5, Month = 2, Year = 2024 }
            };

            _statisticRepositoryMock.Setup(r => r.GetQueryAll())
                .Returns(statistics.AsAsyncQueryable());

            _mapperMock.Setup(m => m.Map<List<StatisticDTO>>(statistics))
                .Returns(new List<StatisticDTO>());

            // Act
            var result = await _statisticService.GetStatisticForCommunity(communityId);

            // Xunit.Assert
            Xunit.Assert.True(result.IsSuccess);

        }

        [Fact]
        public async Task GetStatisticForCommunity_ReturnsStatisticForSpecificCommunityEqual0()
        {
            // Arrange
            int communityId = 1;

            var statistics = new List<Domain.Statistic>
            {
                //new Statistic { CommunityId = communityId, ViewOnDay = 10, Month = 1, Year = 2024 },
                //new Statistic { CommunityId = communityId, ViewOnDay = 5, Month = 2, Year = 2024 }
            };

            _statisticRepositoryMock.Setup(r => r.GetQueryAll())
                .Returns(statistics.AsAsyncQueryable());

            _mapperMock.Setup(m => m.Map<List<StatisticDTO>>(statistics))
                .Returns(new List<StatisticDTO>());

            // Act
            var result = await _statisticService.GetStatisticForCommunity(communityId);

            // Xunit.Assert
            Xunit.Assert.True(result.IsSuccess);
            var stats = Xunit.Assert.IsType<StatisticDTOForCommunity>(result.ObjectResponse);
            //Xunit.Assert.Equal(2, stats.Community.Count);
            Xunit.Assert.Equal(0, stats.Community.Count());
        }
    }
}