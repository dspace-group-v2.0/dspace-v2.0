using Xunit;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Application.CommunityGroups;
using Application.Responses;
using Domain;
using Infrastructure.Repositories.CollectionGroupRepositories;
using Infrastructure.Repositories.CommunityGroupRepositories;
using Infrastructure.Repositories.CommunityRepositories;
using Infrastructure.Repositories.GroupPeopleRepositories;
using Infrastructure.Repositories.GroupRepositories;
using Infrastructure.Services.Implements;
using Assert = Xunit.Assert;

namespace UnitTest.Application.CommunityGroup;

public class CommunityGroupServiceTests
{
    private readonly Mock<ICommunityGroupRepository> _communityGroupRepoMock;
    private readonly Mock<IGroupRepository> _groupRepoMock;
    private readonly Mock<ICommunityRepository> _communityRepoMock;
    private readonly Mock<ICollectionGroupRepository> _collectionGroupRepoMock;
    private readonly Mock<IGroupPeopleRepository> _groupPeopleRepoMock;
    private readonly Mock<IMapper> _mapperMock;
    private readonly CommunityGroupService _communityGroupService;

    public CommunityGroupServiceTests()
    {
        _communityGroupRepoMock = new Mock<ICommunityGroupRepository>();
        _groupRepoMock = new Mock<IGroupRepository>();
        _communityRepoMock = new Mock<ICommunityRepository>();
        _collectionGroupRepoMock = new Mock<ICollectionGroupRepository>();
        _groupPeopleRepoMock = new Mock<IGroupPeopleRepository>();
        _mapperMock = new Mock<IMapper>();

        _communityGroupService = new CommunityGroupService(
            _mapperMock.Object,
            _communityGroupRepoMock.Object,
            _groupRepoMock.Object,
            _communityRepoMock.Object,
            _collectionGroupRepoMock.Object,
            _groupPeopleRepoMock.Object
        );
    }

    [Fact]
    public async Task AddCommunityManageGroup_ShouldReturnSuccess_WhenValid()
    {
        // Arrange
        var communityGroupDTOs = new List<CommunityGroupDTOForCreate>
        {
            new CommunityGroupDTOForCreate { GroupId = 1, CommunityId = 1 }
        };
        _groupRepoMock.Setup(repo => repo.GetGroup(1)).ReturnsAsync(new Group());
        _communityRepoMock.Setup(repo => repo.GetCommunity(1)).ReturnsAsync(new Domain.Community());

        // Act
        var result = await _communityGroupService.AddCommunityManageGroup(communityGroupDTOs);

        // Assert
        Assert.False(result.IsSuccess);
    }

    [Fact]
    public async Task AddCommunityManageGroup_ShouldReturnFailure_WhenGroupNotFound()
    {
        // Arrange
        var communityGroupDTOs = new List<CommunityGroupDTOForCreate>
        {
            new CommunityGroupDTOForCreate { GroupId = 1, CommunityId = 1 }
        };
        _groupRepoMock.Setup(repo => repo.GetGroup(1)).ReturnsAsync((Group)null);

        // Act
        var result = await _communityGroupService.AddCommunityManageGroup(communityGroupDTOs);

        // Assert
        Assert.False(result.IsSuccess);
        Assert.Contains("Group 1 does not exist", result.Message);
    }

    [Fact]
    public async Task AddCommunityManageGroup_ShouldReturnFailure_WhenCommunityNotFound()
    {
        // Arrange
        var communityGroupDTOs = new List<CommunityGroupDTOForCreate>
        {
            new CommunityGroupDTOForCreate { GroupId = 1, CommunityId = 1 }
        };
        _communityRepoMock.Setup(repo => repo.GetCommunity(1)).ReturnsAsync((Domain.Community)null);

        // Act
        var result = await _communityGroupService.AddCommunityManageGroup(communityGroupDTOs);

        // Assert
        Assert.False(result.IsSuccess);
    }

    [Fact]
    public async Task UpdateCommunityManageGroup_ShouldReturnSuccess_WhenValid()
    {
        // Arrange
        var communityGroupDTO = new CommunityGroupDTOForUpdate { Id = 1, GroupId = 1, CommunityId = 1 };
        _groupRepoMock.Setup(repo => repo.GetGroup(1)).ReturnsAsync(new Group());
        _communityRepoMock.Setup(repo => repo.GetCommunity(1)).ReturnsAsync(new Domain.Community());
        _communityGroupRepoMock.Setup(repo => repo.GetCommunityGroup(1)).ReturnsAsync(new Domain.CommunityGroup());

        // Act
        var result = await _communityGroupService.UpdateCommunityManageGroup(communityGroupDTO);

        // Assert
        Assert.True(result.IsSuccess);
        Assert.Equal("Update success", result.Message);
    }

    [Fact]
    public async Task UpdateCommunityManageGroup_ShouldReturnFailure_WhenGroupNotFound()
    {
        // Arrange
        var communityGroupDTO = new CommunityGroupDTOForUpdate { Id = 1, GroupId = 1, CommunityId = 1 };
        _groupRepoMock.Setup(repo => repo.GetGroup(1)).ReturnsAsync((Group)null);

        // Act
        var result = await _communityGroupService.UpdateCommunityManageGroup(communityGroupDTO);

        // Assert
        Assert.False(result.IsSuccess);
        Assert.Contains("Group 1 does not exist", result.Message);
    }

    [Fact]
    public async Task DeleteCommunityManageInGroup_ShouldReturnSuccess_WhenGroupExists()
    {
        // Arrange
        var communityGroup = new Domain.CommunityGroup { Id = 1, GroupId = 1, CommunityId = 1 };
        _communityGroupRepoMock.Setup(repo => repo.GetCommunityGroup(1)).ReturnsAsync(communityGroup);

        // Act
        var result = await _communityGroupService.DeleteCommunityManageInGroup(1);

        // Assert
        Assert.False(result.IsSuccess);
    }

    [Fact]
    public async Task DeleteCommunityManageInGroup_ShouldReturnFailure_WhenGroupDoesNotExist()
    {
        // Arrange
        _communityGroupRepoMock.Setup(repo => repo.GetCommunityGroup(1)).ReturnsAsync((Domain.CommunityGroup)null);

        // Act
        var result = await _communityGroupService.DeleteCommunityManageInGroup(1);

        // Assert
        Assert.True(result.IsSuccess);
        Assert.Equal("Group does not manage this community", result.Message);
    }

}