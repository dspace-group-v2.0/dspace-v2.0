﻿using Application.Groups;
using Application.Responses;
using AutoMapper;
using Domain;
using Infrastructure.Repositories.AuthorRepositories;
using Infrastructure.Repositories.GroupPeopleRepositories;
using Infrastructure.Repositories.GroupRepositories;
using Infrastructure.Repositories.MetadataValueRepositories;
using Infrastructure.Services;
using Infrastructure.Services.Implements;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace UnitTest.Application.Groups
{
    public class GroupServiceTest
    {
        private readonly Mock<IGroupService> _groupServiceMock;
        private readonly ITestOutputHelper _testOutputHelper;
        private readonly GroupService _groupService;
        private readonly Mock<IMapper> _mapper;
        private readonly Mock<IGroupRepository> _groupRepository;
        private readonly Mock<IGroupPeopleRepository> _groupPeopleRepository;
        public GroupServiceTest(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;

            _mapper = new Mock<IMapper>();
            _groupRepository = new Mock<IGroupRepository>();
            _groupPeopleRepository = new Mock<IGroupPeopleRepository>();
            _groupServiceMock = new Mock<IGroupService>();
            _groupService = new GroupService(_mapper.Object, _groupRepository.Object, _groupPeopleRepository.Object);

        }

        public static IEnumerable<object[]> GroupTestData()
        {
            yield return new object[] { "Valid Title", "Valid Description", null, true };  // 1
            //yield return new object[] { "Valid Title", "Valid Description", false, true }; // 2
            //yield return new object[] { "", "Valid Description", true, false };            // 3
            //yield return new object[] { "DROP TABLE", "Valid Description", true, false };  // 4
            //yield return new object[] { "Valid Title", "", true, true };                   // 5
            //yield return new object[] { "Valid Title", "SELECT * FROM", true, false };     // 6
            //yield return new object[] { "", "DROP DATABASE", false, false };               // 7
            //yield return new object[] { "Valid Title", "", false, true };                  // 8
            //yield return new object[] { "INSERT INTO", "", true, false };                  // 9
            //yield return new object[] { "", "", false, false };                            // 10
        }

        [Xunit.Theory]
        [MemberData(nameof(GroupTestData))]
        public async Task CreateGroup_TestCases(string title, string description, bool isActive, bool expectedResult)
        {
            // Arrange
            var groupDTO = new GroupDTOForCreateUpdate
            {
                Title = title,
                Description = description,
                isActive = isActive
            };

            var expectedResponse = new ResponseDTO
            {
                IsSuccess = expectedResult
            };

            //_groupRepository.Setup(service => service.AddAsync(It.IsAny<Domain.Group>())).ThrowsAsync(new Exception(""));
            // Act
            var result = await _groupService.CreateGroup(groupDTO);

            // Assert
            Xunit.Assert.Equal(expectedResult, result.IsSuccess);
            _testOutputHelper.WriteLine(result.Message);
        }

        //[Xunit.Theory]
        //[MemberData(nameof(GroupTestData))]
        [Fact]
        public async Task UpdateGroup_TestCases()
        {
            // Arrange
            var groupDTO = new GroupDTOForCreateUpdate
            {
                Title = "acb",
                Description = "",
                isActive = true
            };

            var expectedResponse = new ResponseDTO
            {
                IsSuccess = true
            };

            //_groupServiceMock.Setup(service => service.UpdateGroup(It.IsAny<int>(), groupDTO))
            //                 .ReturnsAsync(expectedResponse);
            _groupRepository.Setup(x => x.GetGroup(1)).ReturnsAsync((Group)null);
            // Act
            var result = await _groupService.UpdateGroup(1, groupDTO);  // Assuming 1 as a sample group ID

            // Assert
            Xunit.Assert.Equal(false, result.IsSuccess);
            _testOutputHelper.WriteLine(result.Message);
        }

        public static IEnumerable<object[]> DeleteGroupTestData()
        {
            yield return new object[] { 1, true, true };  // 1
            yield return new object[] { -1, false, false };  // 2
            yield return new object[] { 9999, false, false };  // 3
            yield return new object[] { 0, false, false };  // 4
            yield return new object[] { 1, false, false };  // 5
            yield return new object[] { int.MaxValue, false, false };  // 6
            yield return new object[] { 2, false, false };  // 7
        }

        [Xunit.Theory]
        [MemberData(nameof(DeleteGroupTestData))]
        public async Task DeleteGroup_TestCases(int groupId, bool isSuccess, bool expectedResult)
        {
            // Arrange
            var expectedResponse = new ResponseDTO
            {
                IsSuccess = isSuccess
            };

            _groupServiceMock.Setup(service => service.DeleteGroup(groupId))
                             .ReturnsAsync(expectedResponse);

            // Act
            var result = await _groupServiceMock.Object.DeleteGroup(groupId);

            // Assert
            Xunit.Assert.Equal(expectedResult, result.IsSuccess);

            // Optionally check the log message if a logger is used
            // _loggerMock.Verify(log => log.Log(LogLevel.Error, logMessage), Times.Once);
        }
    }
}
