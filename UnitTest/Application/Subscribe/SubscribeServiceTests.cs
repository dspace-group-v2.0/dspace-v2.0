using Application.CollectionSubcribes;
using Application.Responses;
using AutoMapper;
using Domain;
using Infrastructure.Repositories.CollectionRepositories;
using Infrastructure.Repositories.SubscribeRepositories;
using Infrastructure.Repositories.UserRepositories;
using Infrastructure.Services.Implements;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Assert = Xunit.Assert;

namespace UnitTest.Application.Subscribe;

public class SubscribeServiceTests
    {
        private readonly Mock<IMapper> _mapperMock;
        private readonly Mock<IUserRepository> _userRepositoryMock;
        private readonly Mock<ISubscribeRepository> _subscribeRepositoryMock;
        private readonly Mock<ICollectionRepository> _collectionRepositoryMock;
        private readonly SubscribeService _subscribeService;

        public SubscribeServiceTests()
        {
            _mapperMock = new Mock<IMapper>();
            _userRepositoryMock = new Mock<IUserRepository>();
            _subscribeRepositoryMock = new Mock<ISubscribeRepository>();
            _collectionRepositoryMock = new Mock<ICollectionRepository>();
            _subscribeService = new SubscribeService(
                _mapperMock.Object,
                _userRepositoryMock.Object,
                _subscribeRepositoryMock.Object,
                _collectionRepositoryMock.Object
            );
        }

        [Fact]
        public async Task UnSubcribeCollection_ShouldReturnError_WhenUserNotFound()
        {
            // Arrange
            _userRepositoryMock.Setup(repo => repo.getUserByEmail(It.IsAny<string>()))
                .ReturnsAsync((User)null);

            // Act
            var result = await _subscribeService.UnSubcribeCollection(1, "test@test.com");

            // Assert
            Assert.False(result.IsSuccess);
        }

        [Fact]
        public async Task UnSubcribeCollection_ShouldReturnError_WhenSubscriptionNotFound()
        {
            // Arrange
            _userRepositoryMock.Setup(repo => repo.getUserByEmail(It.IsAny<string>()))
                .ReturnsAsync(new User { Id = "1" });
            _subscribeRepositoryMock.Setup(repo => repo.GetQueryAll())
                .Returns((new List<Domain.Subscribe>()).AsAsyncQueryable());

            // Act
            var result = await _subscribeService.UnSubcribeCollection(1, "test@test.com");

            // Assert
            Assert.False(result.IsSuccess);
        }

      
        [Fact]
        public async Task SubcribeCollection_ShouldReturnError_WhenCollectionNotFound()
        {
            // Arrange
            _collectionRepositoryMock.Setup(repo => repo.GetCollection(It.IsAny<int>()))
                .ReturnsAsync((Domain.Collection)null);

            // Act
            var result = await _subscribeService.SubcribeCollection(1, "test@test.com");

            // Assert
            Assert.False(result.IsSuccess);
        }
        
    }