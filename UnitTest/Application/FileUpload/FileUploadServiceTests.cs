using Xunit;
using Moq;
using System.Threading.Tasks;
using Application.FileUploads;
using Application.Responses;
using Infrastructure.Services.Implements;
using Infrastructure.Repositories.FileUploadRepositories;
using Infrastructure.Repositories.ItemRepositories;
using Infrastructure.Repositories.CollectionRepositories;
using Infrastructure.Repositories.FileModifyAccessRepositories;
using AutoMapper;
using Domain;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Google.Apis.Auth.OAuth2;
using System.IO;
using Infrastructure.Repositories.UserRepositories;
using Microsoft.EntityFrameworkCore;
using Shared.Enums;
using Assert = Xunit.Assert;

namespace UnitTest.Application.FileUpload;

public class FileUploadServiceTests
{
    private readonly Mock<IFileUploadRepository> _mockFileUploadRepo;
    private readonly Mock<IItemRepository> _mockItemRepo;
    private readonly Mock<ICollectionRepository> _mockCollectionRepo;
    private readonly Mock<IFileModifyAccessRepository> _mockFileAccessRepo;
    private readonly Mock<IMapper> _mockMapper;
    private readonly FileUploadService _service;
    private readonly Mock<IUserRepository> _mockUserRepo;
    public FileUploadServiceTests()
    {
        _mockFileUploadRepo = new Mock<IFileUploadRepository>();
        _mockItemRepo = new Mock<IItemRepository>();
        _mockCollectionRepo = new Mock<ICollectionRepository>();
        _mockFileAccessRepo = new Mock<IFileModifyAccessRepository>();
        _mockUserRepo = new Mock<IUserRepository>();
        _mockMapper = new Mock<IMapper>();
        _service = new FileUploadService(
            _mockFileUploadRepo.Object,
            _mockMapper.Object,
            _mockFileAccessRepo.Object,
            _mockItemRepo.Object,
            _mockCollectionRepo.Object,
            _mockUserRepo.Object);
    }
    [Fact]
    public async Task DeleteFileWithFileId_FileExists_ReturnsSuccessResponse()
    {
        // Arrange
        var fileId = 1;
        var fileUpload = new Domain.FileUpload();
        _mockFileUploadRepo.Setup(x => x.GetFileUpload(fileId)).ReturnsAsync(fileUpload);
        // Act
        var result = await _service.DeleteFileWithFileId(fileId);

        // Assert
        Assert.False(result.IsSuccess);
    }

    [Fact]
    public async Task DeleteFileWithFileId_FileNotFound_ReturnsFailureResponse()
    {
        // Arrange
        var fileId = 1;
        _mockFileUploadRepo.Setup(x => x.GetFileUpload(fileId)).ReturnsAsync((Domain.FileUpload)null);

        // Act
        var result = await _service.DeleteFileWithFileId(fileId);

        // Assert
        Assert.False(result.IsSuccess);
    }

    [Fact]
    public async Task UndiscoverableFileWithFileId_FileExistsAndActive_ReturnsSuccessResponse()
    {
        // Arrange
        var fileId = 1;
        var fileUpload = new Domain.FileUpload() { isActive = true };
        _mockFileUploadRepo.Setup(x => x.GetFileUpload(fileId)).ReturnsAsync(fileUpload);

        // Act
        var result = await _service.UndiscoverableFileWithFileId(fileId);

        // Assert
        Assert.True(result.IsSuccess);
        Assert.Equal("This file can't be discovered now.", result.Message);
    }

    [Fact]
    public async Task UndiscoverableFileWithFileId_FileExistsAndInactive_ReturnsSuccessResponse()
    {
        // Arrange
        var fileId = 1;
        var fileUpload = new Domain.FileUpload() { isActive = false };
        _mockFileUploadRepo.Setup(x => x.GetFileUpload(fileId)).ReturnsAsync(fileUpload);

        // Act
        var result = await _service.UndiscoverableFileWithFileId(fileId);

        // Assert
        Assert.True(result.IsSuccess);
        Assert.Equal("This file can be discovered now.", result.Message);
    }

    [Fact]
    public async Task UndiscoverableFileWithFileId_FileNotFound_ReturnsFailureResponse()
    {
        // Arrange
        var fileId = 1;
        _mockFileUploadRepo.Setup(x => x.GetFileUpload(fileId)).ReturnsAsync((Domain.FileUpload)null);

        // Act
        var result = await _service.UndiscoverableFileWithFileId(fileId);

        // Assert
        Assert.False(result.IsSuccess);
        Assert.Equal("Encounter an error when modify this file", result.Message);
    }




    [Fact]
    public async Task AddNewFileToItem_ItemNotFound_ReturnsFailureResponse()
    {
        // Arrange
        var itemId = 1;
        _mockItemRepo.Setup(x => x.GetItem(itemId)).ReturnsAsync((Domain.Item)null);

        // Act
        var result = await _service.AddNewFileToItem(itemId, new List<IFormFile>(), "ALL");

        // Assert
        Assert.False(result.IsSuccess);
    }
}    