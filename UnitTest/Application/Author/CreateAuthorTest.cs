using Application.Authors;
using AutoMapper;
using Infrastructure.Repositories.AuthorRepositories;
using Infrastructure.Repositories.MetadataValueRepositories;
using Infrastructure.Services;
using Infrastructure.Services.Implements;
using Moq;
using Xunit;
using Assert = Xunit.Assert;

namespace UnitTest.Application.Author;

public class CreateAuthorTest
{
    private readonly Mock<IAuthorRepository> _authorRepository;
    private readonly Mock<IMetadataValueRepository> _metadataValueRepository;
    private readonly Mock<IMapper> _mapper;
    private readonly AuthorService _authorService;

    public CreateAuthorTest()
    {
        _authorRepository = new Mock<IAuthorRepository>();
        _metadataValueRepository = new Mock<IMetadataValueRepository>();
        _mapper = new Mock<IMapper>();
        _authorService = new AuthorService(_mapper.Object, _authorRepository.Object, _metadataValueRepository.Object);
    }

    [Fact]
    public async Task IfCreateSuccessful_CreateAuthorAsync()
    {
        //Arrange
        var newAuthor = new AuthorDTOForCreateUpdate()
        {
            FullName = "Le Trong Huu",
            DateAccessioned = DateTime.Now,
            DateAvailable = DateTime.Now,
            JobTitle = "",
            Uri = "",
            Type = "",
        };
        //Act
        var response = await _authorService.CreateAuthor(newAuthor);
        
        //Assert
        Assert.True(response.IsSuccess);
    }
}