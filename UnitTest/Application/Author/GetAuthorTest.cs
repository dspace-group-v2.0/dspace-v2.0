using Application.Authors;
using Application.Responses;
using AutoMapper;
using Infrastructure.Repositories.AuthorRepositories;
using Infrastructure.Repositories.MetadataValueRepositories;
using Infrastructure.Services.Implements;
using Moq;
using Xunit;
using Xunit.Abstractions;
using Assert = NUnit.Framework.Assert;

namespace UnitTest.Application.Author;

public class GetAuthorTest
{
    private readonly Mock<IMapper> _mockMapper;
    private readonly Mock<IAuthorRepository> _mockAuthorRepository;
    private readonly Mock<IMetadataValueRepository> _mockMetadataValueRepository;
    private readonly AuthorService _authorService;
    private readonly ITestOutputHelper _testOutputHelper;

    public GetAuthorTest(ITestOutputHelper testOutputHelper)
    {
        _mockMapper = new Mock<IMapper>();
        _mockAuthorRepository = new Mock<IAuthorRepository>();
        _mockMetadataValueRepository = new Mock<IMetadataValueRepository>();
        _authorService = new AuthorService(_mockMapper.Object, _mockAuthorRepository.Object, _mockMetadataValueRepository.Object);
        _testOutputHelper = testOutputHelper;
    }
    [Fact]
    public async Task GetAuthorById_Success()
    {
        // Arrange
        int authorId = 1;
        var author = new Domain.Author()
        {
            AuthorId = authorId,
            FullName = "John Doe",
            JobTitle = "Writer",
            DateAccessioned = DateTime.Now,
            DateAvailable = DateTime.Now.AddDays(1),
            Type = "Fiction",
            Uri = "http://example.com/authors/1"
        };
        _mockAuthorRepository.Setup(repo => repo.GetAuthor(authorId))
            .ReturnsAsync(author);

        // Act
        var response = await _authorService.GetAuthorByID(1);
        // Assert
        Assert.True(response.IsSuccess);
    }

    [Fact]
    public async Task GetAuthorByName_Failure()
    {
        // Arrange
        string name = "John";
        
        _mockAuthorRepository.Setup(repo => repo.GetQueryAll())
            .Throws(new Exception("Database error"));

        // Act
        var response = await _authorService.GetAuthorByName(name);

        // Assert
        Assert.AreEqual("Database error", response.Message);
    }
    [Fact]
    public async Task GetAuthorByName_Success()
    {
        // Arrange
        string name = "cccc";
        var authors = new List<Domain.Author>
        {
            new Domain.Author { AuthorId = 1, FullName = "John Doe" },
            new Domain.Author { AuthorId = 2, FullName = "Johnny Depp" }
        }.AsAsyncQueryable();
        _mockAuthorRepository.Setup(repo => repo.GetQueryAll())
            .Returns(authors);
        // Act
        var response = await _authorService.GetAuthorByName(name);
        // Assert
        Assert.True(response.IsSuccess);
    }
}