using Xunit;
using Moq;
using Application.Authors;
using AutoMapper;
using Infrastructure.Repositories.AuthorRepositories;
using Infrastructure.Repositories.MetadataValueRepositories;
using Infrastructure.Services.Implements;
using Assert = Xunit.Assert;

namespace UnitTest.Application.Author
{
    public class AuthorServiceTests
{
    private readonly Mock<IAuthorRepository> _authorRepositoryMock;
    private readonly Mock<IMetadataValueRepository> _metadataValueRepositoryMock;
    private readonly Mock<IMapper> _mapperMock;
    private readonly AuthorService _authorService;

    public AuthorServiceTests()
    {
        _authorRepositoryMock = new Mock<IAuthorRepository>();
        _metadataValueRepositoryMock = new Mock<IMetadataValueRepository>();
        _mapperMock = new Mock<IMapper>();
        _authorService = new AuthorService(
            _mapperMock.Object,
            _authorRepositoryMock.Object,
            _metadataValueRepositoryMock.Object
        );
    }

    [Fact]
    public async Task CreateAuthor_ShouldReturnSuccessResponse_WhenAuthorIsCreated()
    {
        // Arrange
        var authorDTO = new AuthorDTOForCreateUpdate
        {
            FullName = "John Doe",
            JobTitle = "Writer",
            DateAccessioned = System.DateTime.Now,
            DateAvailable = System.DateTime.Now,
            Type = "Fiction",
            Uri = "http://example.com/"
        };
        _mapperMock.Setup(m => m.Map<AuthorDTOForSelect>(It.IsAny<Domain.Author>())).Returns(new AuthorDTOForSelect());
        // Act
        var result = await _authorService.CreateAuthor(authorDTO);

        // Assert
        Assert.True(result.IsSuccess);
        Assert.Equal("Author added successfully", result.Message);
    }

    [Fact]
    public async Task DeleteAuthor_ShouldReturnNotFoundResponse_WhenAuthorDoesNotExist()
    {
        // Arrange
        int authorId = 1;

        _authorRepositoryMock.Setup(repo => repo.GetAuthor(authorId)).ReturnsAsync((Domain.Author)null);

        // Act
        var result = await _authorService.DeleteAuthor(authorId);

        // Assert
        Assert.True(result.IsSuccess);
        Assert.Equal("Author does not exist", result.Message);
    }

    [Fact]
    public async Task GetAuthorByID_ShouldReturnSuccessResponse_WhenAuthorExists()
    {
        // Arrange
        int authorId = 1;
        var author = new Domain.Author() { AuthorId = authorId, FullName = "John Doe" };

        _authorRepositoryMock.Setup(repo => repo.GetAuthor(authorId)).ReturnsAsync(author);
        _mapperMock.Setup(m => m.Map<AuthorDTOForSelect>(author)).Returns(new AuthorDTOForSelect());

        // Act
        var result = await _authorService.GetAuthorByID(authorId);

        // Assert
        Assert.True(result.IsSuccess);
    }

    [Fact]
    public async Task GetAuthorByName_ShouldReturnSuccessResponse_WhenAuthorsExist()
    {
        // Arrange
        string name = "John";
        var authors = new List<Domain.Author> { new Domain.Author() { FullName = "John Doe" } }.AsAsyncQueryable();
        
        _authorRepositoryMock.Setup(repo => repo.GetQueryAll()).Returns(authors);
        _mapperMock.Setup(m => m.Map<List<AuthorDTOForSelect>>(It.IsAny<List<Domain.Author>>())).Returns(new List<AuthorDTOForSelect>());

        // Act
        var result = await _authorService.GetAuthorByName(name);

        // Assert
        Assert.True(result.IsSuccess);
        Assert.Equal("Found the Author", result.Message);
    }

    [Fact]
    public async Task UpdateAuthor_ShouldReturnSuccessResponse_WhenAuthorIsUpdated()
    {
        // Arrange
        int authorId = 1;
        var authorDTO = new AuthorDTOForCreateUpdate { FullName = "John Doe" };
        var author = new Domain.Author() { AuthorId = authorId, FullName = "Jane Doe" };

        _authorRepositoryMock.Setup(repo => repo.GetAuthor(authorId)).ReturnsAsync(author);

        // Act
        var result = await _authorService.UpdateAuthor(authorId, authorDTO);

        // Assert
        Assert.True(result.IsSuccess);
        Assert.Equal("Author updated successfully", result.Message);
    }

    [Fact]
    public async Task GetAllAuthor_ShouldReturnSuccessResponse_WhenAuthorsExist()
    {
        // Arrange
        var authors = new List<Domain.Author> { new Domain.Author() { FullName = "John Doe" } };

        _authorRepositoryMock.Setup(repo => repo.GetAll()).ReturnsAsync(authors);
        _mapperMock.Setup(m => m.Map<List<AuthorDTOForSelect>>(authors)).Returns(new List<AuthorDTOForSelect>());

        // Act
        var result = await _authorService.GetAllAuthor();

        // Assert
        Assert.True(result.IsSuccess);
    }
}
}
