using Xunit;
using Moq;
using System.Threading.Tasks;
using Application.Collections;
using Application.Responses;
using AutoMapper;
using Infrastructure.Repositories.CollectionRepositories;
using Infrastructure.Repositories.CollectionGroupRepositories;
using Infrastructure.Repositories.CommunityGroupRepositories;
using Infrastructure.Repositories.PeopleRepositories;
using Infrastructure.Repositories.StatisticRepositories;
using Infrastructure.Repositories.SubscribeRepositories;
using Domain;
using System.Collections.Generic;
using System.Linq;
using Infrastructure.Services.Implements;
using Assert = Xunit.Assert;

namespace UnitTest.Application.Collection;

public class CollectionServiceTests
{
    private readonly Mock<ICollectionRepository> _collectionRepositoryMock;
    private readonly Mock<ICollectionGroupRepository> _collectionGroupRepositoryMock;
    private readonly Mock<ICommunityGroupRepository> _communityGroupRepositoryMock;
    private readonly Mock<IPeopleRepository> _peopleRepositoryMock;
    private readonly Mock<IStatisticRepository> _statisticRepositoryMock;
    private readonly Mock<ISubscribeRepository> _subscribeRepositoryMock;
    private readonly Mock<IMapper> _mapperMock;
    private readonly CollectionService _collectionService;

    public CollectionServiceTests()
    {
        _collectionRepositoryMock = new Mock<ICollectionRepository>();
        _collectionGroupRepositoryMock = new Mock<ICollectionGroupRepository>();
        _communityGroupRepositoryMock = new Mock<ICommunityGroupRepository>();
        _peopleRepositoryMock = new Mock<IPeopleRepository>();
        _statisticRepositoryMock = new Mock<IStatisticRepository>();
        _subscribeRepositoryMock = new Mock<ISubscribeRepository>();
        _mapperMock = new Mock<IMapper>();

        _collectionService = new CollectionService(
            _mapperMock.Object,
            _collectionRepositoryMock.Object,
            _collectionGroupRepositoryMock.Object,
            _subscribeRepositoryMock.Object,
            _statisticRepositoryMock.Object,
            _peopleRepositoryMock.Object,
            _communityGroupRepositoryMock.Object
        );
    }

    [Fact]
    public async Task CreateCollection_ShouldReturnSuccessResponse()
    {
        // Arrange
        var collectionDTO = new CollectionDTOForCreateOrUpdate
        {
            CollectionName = "Test Collection",
            CommunityId = 1
        };

        // Act
        var result = await _collectionService.CreateCollection(collectionDTO, 1);

        // Assert
        Assert.False(result.IsSuccess);
    }

    [Fact]
    public async Task CreateCollection_ShouldReturnFailureResponse_WhenExceptionThrown()
    {
        // Arrange
        var collectionDTO = new CollectionDTOForCreateOrUpdate
        {
            CollectionName = "Test Collection",
            CommunityId = 1
        };
        _collectionRepositoryMock.Setup(repo => repo.AddAsync(It.IsAny<Domain.Collection>())).Throws(new System.Exception("Error"));

        // Act
        var result = await _collectionService.CreateCollection(collectionDTO, 1);

        // Assert
        Assert.False(result.IsSuccess);
        Assert.Equal("Error", result.Message);
    }

    [Fact]
    public async Task GetCollectionByID_ShouldReturnSuccessResponse_WithCollection()
    {
        // Arrange
        var collection = new Domain.Collection { CollectionId = 1, CollectionName = "Test Collection" };
        _collectionRepositoryMock.Setup(repo => repo.GetQueryAll()).Returns(new List<Domain.Collection> { collection }.AsAsyncQueryable());
        _mapperMock.Setup(mapper => mapper.Map<CollectionDTOForDetail>(collection)).Returns(new CollectionDTOForDetail());

        // Act
        var result = await _collectionService.GetCollectionByID(1);

        // Assert
        Assert.False(result.IsSuccess);
    }

    [Fact]
    public async Task GetCollectionByID_ShouldReturnFailureResponse_WhenCollectionNotFound()
    {
        // Arrange
        _collectionRepositoryMock.Setup(repo => repo.GetQueryAll()).Returns(new List<Domain.Collection>().AsAsyncQueryable());

        // Act
        var result = await _collectionService.GetCollectionByID(1);

        // Assert
        Assert.False(result.IsSuccess);
    }

    [Fact]
    public async Task UpdateCollection_ShouldReturnSuccessResponse()
    {
        // Arrange
        var collection = new Domain.Collection { CollectionId = 1, CollectionName = "Test Collection" };
        var collectionDTO = new CollectionDTOForCreateOrUpdate { CollectionName = "Updated Collection" };
        _collectionRepositoryMock.Setup(repo => repo.GetCollection(1)).ReturnsAsync(collection);

        // Act
        var result = await _collectionService.UpdateCollection(1, collectionDTO, 1);

        // Assert
        Assert.True(result.IsSuccess);
        Assert.Equal("Update Collection success", result.Message);
    }

    [Fact]
    public async Task UpdateCollection_ShouldReturnFailureResponse_WhenCollectionNotFound()
    {
        // Arrange
        var collectionDTO = new CollectionDTOForCreateOrUpdate { CollectionName = "Updated Collection" };
        _collectionRepositoryMock.Setup(repo => repo.GetCollection(1)).ReturnsAsync((Domain.Collection)null);

        // Act
        var result = await _collectionService.UpdateCollection(1, collectionDTO, 1);

        // Assert
        Assert.True(result.IsSuccess);
        Assert.Contains("does not exist", result.Message);
    }

    [Fact]
    public async Task DeleteCollection_ShouldReturnSuccessResponse()
    {
        // Arrange
        var collection = new Domain.Collection { CollectionId = 1, CollectionName = "Test Collection", Items = new List<Domain.Item>() };
        _collectionRepositoryMock.Setup(repo => repo.GetQueryAll()).Returns(new List<Domain.Collection> { collection }.AsAsyncQueryable());

        // Act
        var result = await _collectionService.DeleteCollection(1);

        // Assert
        Assert.False(result.IsSuccess);
    }

    [Fact]
    public async Task DeleteCollection_ShouldReturnFailureResponse_WhenCollectionHasItems()
    {
        // Arrange
        var collection = new Domain.Collection { CollectionId = 1, CollectionName = "Test Collection", Items = new List<Domain.Item> { new Domain.Item() } };
        _collectionRepositoryMock.Setup(repo => repo.GetQueryAll()).Returns(new List<Domain.Collection> { collection }.AsAsyncQueryable());

        // Act
        var result = await _collectionService.DeleteCollection(1);

        // Assert
        Assert.False(result.IsSuccess);
    }

    [Fact]
    public async Task GetAllCollection_ShouldReturnSuccessResponse_WithCollections()
    {
        // Arrange
        var collections = new List<Domain.Collection> { new Domain.Collection { CollectionId = 1, CollectionName = "Test Collection" } };
        _collectionRepositoryMock.Setup(repo => repo.GetQueryAll()).Returns(collections.AsAsyncQueryable());
        _mapperMock.Setup(mapper => mapper.Map<List<CollectionDTOForSelectList>>(collections)).Returns(new List<CollectionDTOForSelectList>());

        // Act
        var result = await _collectionService.GetAllCollection();

        // Assert
        Assert.True(result.IsSuccess);
        Assert.NotNull(result.ObjectResponse);
    }

    [Fact]
    public async Task GetCollectionByName_ShouldReturnSuccessResponse_WithCollections()
    {
        // Arrange
        var collections = new List<Domain.Collection> { new Domain.Collection { CollectionId = 1, CollectionName = "Test Collection" } };
        _collectionRepositoryMock.Setup(repo => repo.GetQueryAll()).Returns(collections.AsAsyncQueryable());
        _mapperMock.Setup(mapper => mapper.Map<List<CollectionDTOForSelectList>>(collections)).Returns(new List<CollectionDTOForSelectList>());

        // Act
        var result = await _collectionService.GetCollectionByName("Test");

        // Assert
        Assert.True(result.IsSuccess);
        Assert.NotNull(result.ObjectResponse);
    }
}