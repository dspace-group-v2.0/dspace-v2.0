using Application.Communities;
using Application.Responses;
using Domain;
using AutoMapper;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using Application.CommunityGroups;
using Infrastructure.Repositories.CommunityGroupRepositories;
using Infrastructure.Repositories.CollectionGroupRepositories;
using Infrastructure.Repositories.SubscribeRepositories;
using Infrastructure.Repositories.CommunityRepositories;
using Infrastructure.Repositories.StatisticRepositories;
using Infrastructure.Repositories.PeopleRepositories;
using Moq;
using Xunit;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;
using Infrastructure.Services.Implements;
using Assert = Xunit.Assert;

namespace UnitTest.Application.Community;

public class CommunityServiceTests
    {
        private readonly Mock<IMapper> _mockMapper;
        private readonly Mock<ICommunityGroupRepository> _mockCommunityGroupRepository;
        private readonly Mock<ICollectionGroupRepository> _mockCollectionGroupRepository;
        private readonly Mock<ISubscribeRepository> _mockSubscribeRepository;
        private readonly Mock<ICommunityRepository> _mockCommunityRepository;
        private readonly Mock<IStatisticRepository> _mockStatisticRepository;
        private readonly Mock<IPeopleRepository> _mockPeopleRepository;
        private readonly CommunityService _service;

        public CommunityServiceTests()
        {
            _mockMapper = new Mock<IMapper>();
            _mockCommunityGroupRepository = new Mock<ICommunityGroupRepository>();
            _mockCollectionGroupRepository = new Mock<ICollectionGroupRepository>();
            _mockSubscribeRepository = new Mock<ISubscribeRepository>();
            _mockCommunityRepository = new Mock<ICommunityRepository>();
            _mockStatisticRepository = new Mock<IStatisticRepository>();
            _mockPeopleRepository = new Mock<IPeopleRepository>();
            _service = new CommunityService(
                _mockMapper.Object,
                _mockCommunityGroupRepository.Object,
                _mockCollectionGroupRepository.Object,
                _mockSubscribeRepository.Object,
                _mockCommunityRepository.Object,
                _mockStatisticRepository.Object,
                _mockPeopleRepository.Object
            );
        }

        [Fact]
        public async Task CreateCommunityForStaff_ShouldReturnFail_WhenUserHasNoCommunities()
        {
            // Arrange
            var communityDTO = new CommunityDTOForCreateOrUpdate();
            int userCreateId = 1;
            _mockCommunityGroupRepository.Setup(repo => repo.GetListCommunityIdByStaff(userCreateId))
                .ReturnsAsync(new List<int>());

            // Act
            var result = await _service.CreateCommunityForStaff(communityDTO, userCreateId);

            // Assert
            Assert.False(result.IsSuccess);
            Assert.Equal("Add community fail", result.Message);
        }

        [Fact]
        public async Task UpdateCommunityForStaff_ShouldReturnFail_WhenCommunityNotInUserList()
        {
            // Arrange
            var communityDTO = new CommunityDTOForCreateOrUpdate { ParentCommunityId = 2 };
            int communityId = 1;
            int userCreateId = 1;
            _mockCommunityGroupRepository.Setup(repo => repo.GetListCommunityIdByStaff(userCreateId))
                .ReturnsAsync(new List<int> { 3 });

            // Act
            var result = await _service.UpdateCommunityForStaff(communityId, communityDTO, userCreateId);

            // Assert
            Assert.False(result.IsSuccess);
            Assert.Equal("Add community fail", result.Message);
        }

        [Fact]
        public async Task DeleteCommunityForStaff_ShouldReturnFail_WhenCommunityNotInUserList()
        {
            // Arrange
            int communityId = 1;
            int userCreateId = 1;
            _mockCommunityGroupRepository.Setup(repo => repo.GetListCommunityIdByStaff(userCreateId))
                .ReturnsAsync(new List<int> { 2 });

            // Act
            var result = await _service.DeleteCommunityForStaff(communityId, userCreateId);

            // Assert
            Assert.False(result.IsSuccess);
            Assert.Equal("Add community fail", result.Message);
        }
        

        [Fact]
        public async Task DeleteCommunity_ShouldReturnFail_WhenCommunityDoesNotExist()
        {
            // Arrange
            int communityId = 1;
            _mockCommunityRepository.Setup(repo => repo.GetQueryAll())
                .Returns(new List<Domain.Community>().AsAsyncQueryable());

            // Act
            var result = await _service.DeleteCommunity(communityId);

            // Assert
            Assert.False(result.IsSuccess);
        }

        [Fact]
        public async Task GetCommunityByID_ShouldReturnCommunityDetails_WhenCommunityExists()
        {
            // Arrange
            int communityId = 1;
            var community = new Domain.Community { CommunityId = communityId };
            _mockCommunityRepository.Setup(repo => repo.GetQueryAll())
                .Returns(new List<Domain.Community> { community }.AsAsyncQueryable());
            _mockMapper.Setup(m => m.Map<CommunityDTOForDetail>(It.IsAny<Domain.Community>()))
                .Returns(new CommunityDTOForDetail { CommunityId = communityId });

            // Act
            var result = await _service.GetCommunityByID(communityId);

            // Assert
            Assert.False(result.IsSuccess);
        }

        [Fact]
        public async Task GetCommunityByName_ShouldReturnCommunities_WhenNameMatches()
        {
            // Arrange
            string name = "Test";
            var communities = new List<Domain.Community> { new Domain.Community { CommunityName = "Test Community" } };
            _mockCommunityRepository.Setup(repo => repo.GetQueryAll())
                .Returns(communities.AsAsyncQueryable());

            // Act
            var result = await _service.GetCommunityByName(name);

            // Assert
            Assert.True(result.IsSuccess);
        }
        

        [Fact]
        public async Task GetAllCommunityTree_ShouldReturnCommunityTree()
        {
            // Arrange
            var communities = new List<Domain.Community> { new Domain.Community { CommunityId = 1, ParentCommunityId = null } };
            _mockCommunityRepository.Setup(repo => repo.GetQueryAll()).Returns(communities.AsAsyncQueryable());
            _mockMapper.Setup(m => m.Map<List<CommunityDTO>>(It.IsAny<List<Domain.Community>>()))
                .Returns(new List<CommunityDTO> { new CommunityDTO { CommunityId = 1 } });

            // Act
            var result = await _service.GetAllCommunityTree();

            // Assert
            Assert.True(result.IsSuccess);
            Assert.NotNull(result.ObjectResponse);
        }

        [Fact]
        public async Task GetCommunityByStaff_ShouldReturnCommunitiesAssignedToStaff()
        {
            // Arrange
            int peopleId = 1;
            var communities = new List<Domain.Community> { new Domain.Community { CommunityId = 1 } };
            _mockCommunityGroupRepository.Setup(repo => repo.GetListCommunityByStaff(peopleId))
                .ReturnsAsync(communities);

            // Act
            var result = await _service.GetCommunityByStaff(peopleId);

            // Assert
            Assert.True(result.IsSuccess);
        }
    }