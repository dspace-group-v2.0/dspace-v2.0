﻿using Application.GroupPeoples;
using AutoMapper;
using Domain;
using Infrastructure.Repositories.GroupPeopleRepositories;
using Infrastructure.Repositories.GroupRepositories;
using Infrastructure.Repositories.PeopleRepositories;
using Infrastructure.Services.Implements;
using Moq;
using UnitTest.Application;
using Xunit;

namespace UnitTest.Application.GroupPeople
{
   public class GroupPeopleServiceTests
   {
      private readonly Mock<IGroupRepository> _mockGroupRepository;
      private readonly Mock<IPeopleRepository> _mockPeopleRepository;
      private readonly Mock<IGroupPeopleRepository> _mockGroupPeopleRepository;
      private readonly Mock<IMapper> _mockMapper;
      private readonly GroupPeopleService _service;

      public GroupPeopleServiceTests()
      {
         _mockGroupRepository = new Mock<IGroupRepository>();
         _mockPeopleRepository = new Mock<IPeopleRepository>();
         _mockGroupPeopleRepository = new Mock<IGroupPeopleRepository>();
         _mockMapper = new Mock<IMapper>();

         _service = new GroupPeopleService(
             _mockMapper.Object,
             _mockGroupRepository.Object,
             _mockPeopleRepository.Object,
             _mockGroupPeopleRepository.Object
         );
      }

      [Fact]
      public async Task AddPeopleInGroup_PeopleDoesNotExist_ReturnsErrorResponse()
      {
         // Arrange
         var groupPeopleDTO = new GroupPeopleDTO { GroupId = 1, PeopleId = 1 };
         Group group = new Group
         {
            GroupId= 1
         };

         _mockGroupRepository.Setup(x => x.GetGroup(1)).ReturnsAsync(group);
         _mockPeopleRepository.Setup(x => x.GetPeople(It.IsAny<int>())).ReturnsAsync((Domain.People)null);

         // Act
         var response = await _service.AddPeopleInGroup(groupPeopleDTO);

         //Xunit.Xunit. Assert
         Xunit.Assert.True(!response.IsSuccess);
         Xunit.Assert.Contains("does not exist", response.Message);
      }
      [Fact]
      public async Task AddPeopleInGroup_GroupPeopleExist_ReturnsErrorResponse()
      {
         // Arrange
         var groupPeopleDTO = new GroupPeopleDTO { GroupId = 1, PeopleId = 1 };
         Group group = new Group
         {
            GroupId = 1
         };
         Domain.People people = new Domain.People
         {
            PeopleId = 1
         };
         Domain.GroupPeople groupPeople = new Domain.GroupPeople
         {
            GroupId = 1,
            PeopleId = 1
         };

         _mockGroupRepository.Setup(x => x.GetGroup(1)).ReturnsAsync(group);
         _mockPeopleRepository.Setup(x => x.GetPeople(1)).ReturnsAsync(people);
         _mockGroupPeopleRepository.Setup(x => x.GetGroupPeopleByGroupIdAndPeopleId(1, 1)).ReturnsAsync(groupPeople);

         // Act
         var response = await _service.AddPeopleInGroup(groupPeopleDTO);

         //Xunit.Xunit. Assert
         Xunit.Assert.True(!response.IsSuccess);
         
      }
      [Fact]
      public async Task AddPeopleInGroup_GroupDoesNotExist_ReturnsErrorResponse()
      {
         // Arrange
         var groupPeopleDTO = new GroupPeopleDTO { GroupId = 1, PeopleId = 1 };
         Domain.People people= new Domain.People
         {
            PeopleId = 1
         };

         _mockGroupRepository.Setup(x => x.GetGroup(It.IsAny<int>())).ReturnsAsync((Domain.Group)null);
         _mockPeopleRepository.Setup(x => x.GetPeople(1)).ReturnsAsync(people);

         // Act
         var response = await _service.AddPeopleInGroup(groupPeopleDTO);

         //Xunit.Xunit. Assert
         Xunit.Assert.True(!response.IsSuccess);
         Xunit.Assert.Contains("does not exist", response.Message);

      }

      [Fact]
      public async Task AddPeopleInGroup_ValidGroupAndPeople_AddsPeopleToGroup()
      {
         // Arrange
         var groupPeopleDTO = new GroupPeopleDTO { GroupId = 1, PeopleId = 1 };
         var group = new Group { GroupId = 1, Title = "Test Group" };
         var people = new Domain.People { PeopleId = 1 };

         _mockGroupRepository.Setup(x => x.GetGroup(groupPeopleDTO.GroupId)).ReturnsAsync(group);
         _mockPeopleRepository.Setup(x => x.GetPeople(groupPeopleDTO.PeopleId)).ReturnsAsync(people);
         _mockMapper.Setup(m => m.Map<Domain.GroupPeople>(It.IsAny<GroupPeopleDTO>())).Returns(new Domain.GroupPeople { GroupId = 1, PeopleId = 1 });

         // Act
         var response = await _service.AddPeopleInGroup(groupPeopleDTO);

         //Xunit.Xunit. Assert
         _mockGroupPeopleRepository.Verify(x => x.AddAsync(It.IsAny<Domain.GroupPeople>()), Times.Once);
         Xunit.Assert.True(response.IsSuccess);
         Xunit.Assert.Equal("Add people into group success", response.Message);
      }

      [Fact]
      public async Task DeletePeopleInGroup_PeopleDoesNotExistInGroup_ReturnsErrorResponse()
      {
         // Arrange
         var groupPeopleDTO = new GroupPeopleDTO { GroupId = 1, PeopleId = 1 };

         _mockGroupPeopleRepository.Setup(x => x.GetGroupPeopleByGroupIdAndPeopleId(It.IsAny<int>(), It.IsAny<int>()))
             .ReturnsAsync((Domain.GroupPeople)null);

         // Act
         var response = await _service.DeletePeopleInGroup(groupPeopleDTO);

         //Xunit.Xunit. Assert
         Xunit.Assert.False(response.IsSuccess);
         Xunit.Assert.Equal("People does not exist in group", response.Message);
      }

      [Fact]
      public async Task DeletePeopleInGroup_ValidGroupAndPeople_DeletesPeopleFromGroup()
      {
         // Arrange
         var groupPeopleDTO = new GroupPeopleDTO { GroupId = 1, PeopleId = 1 };
         var groupPeople = new Domain.GroupPeople { GroupId = 1, PeopleId = 1 };

         _mockGroupPeopleRepository.Setup(x => x.GetGroupPeopleByGroupIdAndPeopleId(groupPeopleDTO.GroupId, groupPeopleDTO.PeopleId))
             .ReturnsAsync(groupPeople);

         // Act
         var response = await _service.DeletePeopleInGroup(groupPeopleDTO);

         //Xunit.Xunit. Assert
         _mockGroupPeopleRepository.Verify(x => x.DeleteAsync(groupPeople), Times.Once);
         Xunit.Assert.True(response.IsSuccess);
         Xunit.Assert.Equal("Group is exist People", response.Message);
      }
      [Fact]
      public async Task DeletePeopleInGroup_InValidGroupAndPeople_DeletesPeopleFromGroup()
      {
         // Arrange
         var groupPeopleDTO = new GroupPeopleDTO { GroupId = 1, PeopleId = 1 };
         var groupPeople = new Domain.GroupPeople { GroupId = 1, PeopleId = 1 };

         _mockGroupPeopleRepository.Setup(x => x.GetGroupPeopleByGroupIdAndPeopleId(groupPeopleDTO.GroupId, groupPeopleDTO.PeopleId))
             .ReturnsAsync((Domain.GroupPeople)null);

         // Act
         var response = await _service.DeletePeopleInGroup(groupPeopleDTO);

         //Xunit.Xunit. Assert
         //_mockGroupPeopleRepository.Verify(x => x.DeleteAsync(groupPeople), Times.Once);
         Xunit.Assert.True(!response.IsSuccess);
         
      }



      [Fact]
      public async Task AddListPeopleInGroup_ValidRequest_AddsPeopleSuccessfully()
      {
         // Arrange
         var groupPeopleListDTO = new GroupPeopleListDTOForCreateUpdate { GroupId = 1, ListPeopleId = new List<int> { 1, 2, 3 } };
         var group = new Group { GroupId = 1 };
         var peopleList = groupPeopleListDTO.ListPeopleId.Select(id => new Domain.People { PeopleId = id }).ToList();

         _mockGroupRepository.Setup(r => r.GetGroup(It.IsAny<int>())).ReturnsAsync(group);
         _mockPeopleRepository.Setup(r => r.GetPeople(It.IsAny<int>())).ReturnsAsync(new Domain.People { PeopleId = 1 });
         _mockPeopleRepository.Setup(r => r.GetPeople(It.IsAny<int>())).ReturnsAsync(new Domain.People { PeopleId = 2 });
         _mockPeopleRepository.Setup(r => r.GetPeople(It.IsAny<int>())).ReturnsAsync(new Domain.People { PeopleId = 3 });

         // Act
         var result = await _service.AddListPeopleInGroup(groupPeopleListDTO);

         //Xunit. Assert
         Xunit.Assert.True(result.IsSuccess);
         Xunit.Assert.Equal("Add list people in Group success", result.Message);
         _mockGroupPeopleRepository.Verify(r => r.AddListAsync(It.IsAny<List<Domain.GroupPeople>>()), Times.Once);
      }
      [Fact]
      public async Task AddListPeopleInGroup_InValidRequest_ReturnError()
      {
         // Arrange
         var groupPeopleListDTO = new GroupPeopleListDTOForCreateUpdate { GroupId = 1, ListPeopleId = new List<int> { 1, 2, 3 } };
         var group = new Group { GroupId = 1 };
         var peopleList = groupPeopleListDTO.ListPeopleId.Select(id => new Domain.People { PeopleId = id }).ToList();

         _mockGroupRepository.Setup(r => r.GetGroup(It.IsAny<int>())).ReturnsAsync(group);
         _mockPeopleRepository.Setup(r => r.GetPeople(1)).ReturnsAsync(new Domain.People { PeopleId = 1 });
         _mockPeopleRepository.Setup(r => r.GetPeople(2)).ReturnsAsync(( Domain.People)null);
         _mockPeopleRepository.Setup(r => r.GetPeople(3)).ReturnsAsync(new Domain.People { PeopleId = 3 });

         // Act
         var result = await _service.AddListPeopleInGroup(groupPeopleListDTO);

         //Xunit. Assert
         Xunit.Assert.True(!result.IsSuccess);
      }


      [Fact]
      public async Task DeleteListPeopleInGroup_GroupHasNoPeople_ReturnsSuccessResponse()
      {
         // Arrange
         int groupId = 1;
         _mockGroupPeopleRepository
             .Setup(r => r.GetQueryAll())
             .Returns(new List<Domain.GroupPeople>().AsAsyncQueryable());

         // Act
         var result = await _service.DeleteListPeopleInGroup(groupId);

         //Xunit. Assert
         Xunit.Assert.True(result.IsSuccess);
         Xunit.Assert.Equal("group does not exist any people", result.Message);
      }

      [Fact]
      public async Task DeleteListPeopleInGroup_ValidRequest_DeletesPeopleSuccessfully()
      {
         // Arrange
         int groupId = 1;
         var groupPeopleList = new List<Domain.GroupPeople>
        {
            new Domain.GroupPeople { GroupId = groupId, PeopleId = 1 },
            new Domain.GroupPeople { GroupId = groupId, PeopleId = 2 }
        };

         _mockGroupPeopleRepository
             .Setup(r => r.GetQueryAll())
             .Returns(groupPeopleList.AsAsyncQueryable());

         // Act
         var result = await _service.DeleteListPeopleInGroup(groupId);

         //Xunit. Assert
         Xunit.Assert.True(result.IsSuccess);
         Xunit.Assert.Equal("Delete people in group success", result.Message);
         _mockGroupPeopleRepository.Verify(r => r.DeleteListAsync(It.IsAny<List<Domain.GroupPeople>>()), Times.Once);
      }

   }
   
}
