using Application.Items;
using Application.Metadatas;
using AutoMapper;
using Domain;
using Infrastructure.Repositories.AuthorRepositories;
using Infrastructure.Repositories.CollectionGroupRepositories;
using Infrastructure.Repositories.CollectionRepositories;
using Infrastructure.Repositories.CommunityGroupRepositories;
using Infrastructure.Repositories.FileModifyAccessRepositories;
using Infrastructure.Repositories.FileUploadRepositories;
using Infrastructure.Repositories.GroupPeopleRepositories;
using Infrastructure.Repositories.ItemRepositories;
using Infrastructure.Repositories.MetadataValueRepositories;
using Infrastructure.Repositories.StatisticRepositories;
using Infrastructure.Repositories.SubscribeRepositories;
using Infrastructure.Repositories.UserRepositories;
using Infrastructure.Services;
using Infrastructure.Services.Implements;
using Moq;
using Xunit;
using Assert = Xunit.Assert;
using Exception = System.Exception;

namespace UnitTest.Application.Item;

public class ItemServiceTests
{
    private readonly Mock<IMapper> _mapperMock;
    private readonly Mock<IMetadataValueRepository> _metadataValueRepositoryMock;
    private readonly Mock<IGroupPeopleRepository> _groupPeopleRepositoryMock;
    private readonly Mock<ICollectionGroupRepository> _collectionGroupRepositoryMock;
    private readonly Mock<ICollectionRepository> _collectionRepositoryMock;
    private readonly Mock<IStatisticRepository> _statisticRepositoryMock;
    private readonly Mock<ISubscribeRepository> _subscribeRepositoryMock;
    private readonly Mock<IEmailService> _emailServiceMock;
    private readonly Mock<IAuthorRepository> _authorRepositoryMock;
    private readonly Mock<ICommunityGroupRepository> _communityGroupRepositoryMock;
    private readonly Mock<IFileModifyAccessRepository> _fileModifyAccessRepositoryMock;
    private readonly Mock<IItemRepository> _itemRepositoryMock;
    private readonly Mock<IFileUploadRepository> _fileUploadRepositoryMock;
    private readonly Mock<IUserRepository> _userRepositoryMock;
    private readonly ItemService _itemService;

    public ItemServiceTests()
    {
        _mapperMock = new Mock<IMapper>();
        _metadataValueRepositoryMock = new Mock<IMetadataValueRepository>();
        _groupPeopleRepositoryMock = new Mock<IGroupPeopleRepository>();
        _collectionGroupRepositoryMock = new Mock<ICollectionGroupRepository>();
        _collectionRepositoryMock = new Mock<ICollectionRepository>();
        _statisticRepositoryMock = new Mock<IStatisticRepository>();
        _subscribeRepositoryMock = new Mock<ISubscribeRepository>();
        _emailServiceMock = new Mock<IEmailService>();
        _authorRepositoryMock = new Mock<IAuthorRepository>();
        _communityGroupRepositoryMock = new Mock<ICommunityGroupRepository>();
        _fileModifyAccessRepositoryMock = new Mock<IFileModifyAccessRepository>();
        _itemRepositoryMock = new Mock<IItemRepository>();
        _fileUploadRepositoryMock = new Mock<IFileUploadRepository>();
        _userRepositoryMock = new Mock<IUserRepository>();
        _itemService = new ItemService(
            _mapperMock.Object,
            _metadataValueRepositoryMock.Object,
            _groupPeopleRepositoryMock.Object,
            _collectionGroupRepositoryMock.Object,
            _collectionRepositoryMock.Object,
            _statisticRepositoryMock.Object,
            _subscribeRepositoryMock.Object,
            _emailServiceMock.Object,
            _authorRepositoryMock.Object,
            _communityGroupRepositoryMock.Object,
            _fileModifyAccessRepositoryMock.Object,
            _itemRepositoryMock.Object,
            _fileUploadRepositoryMock.Object,
            _userRepositoryMock.Object
        );
    }

    [Fact]
    public async Task DeleteItem_ItemDoesNotExist_ReturnsItemDoesNotExistMessage()
    {
        // Arrange
        int itemId = 1;
        _itemRepositoryMock.Setup(repo => repo.GetQueryAll())
            .Returns(new List<Domain.Item>().AsAsyncQueryable());

        // Act
        var result = await _itemService.DeleteItem(itemId);

        // Assert
        Assert.False(result.IsSuccess);
    }

    [Fact]
    public async Task DeleteItem_ItemExists_ReturnsDeleteItemSuccessMessage()
    {
        // Arrange
        int itemId = 1;
        var item = new Domain.Item { ItemId = itemId };
        _itemRepositoryMock.Setup(repo => repo.GetQueryAll())
            .Returns(new List<Domain.Item> { item }.AsAsyncQueryable());

        // Act
        var result = await _itemService.DeleteItem(itemId);

        // Assert
        Assert.False(result.IsSuccess);
    }
    [Fact]
    public async Task GetItemSimpleById_ItemDoesNotExist_ReturnsNotFoundItemMessage()
    {
        // Arrange
        int itemId = 1;
        _itemRepositoryMock.Setup(repo => repo.GetQueryAll())
            .Returns(new List<Domain.Item>().AsAsyncQueryable());

        // Act
        var result = await _itemService.GetItemSimpleById(itemId);

        // Assert
        Assert.False(result.IsSuccess);
    }
    
    [Fact]
    public async Task GetItemSimpleById_ItemExists_ReturnsItem()
    {
        // Arrange
        int itemId = 1;
        var item = new Domain.Item { ItemId = itemId };
        _itemRepositoryMock.Setup(repo => repo.GetQueryAll())
            .Returns(new List<Domain.Item> { item }.AsAsyncQueryable());

        _mapperMock.Setup(m => m.Map<ItemDTOForSelectDetailSimple>(item))
            .Returns(new ItemDTOForSelectDetailSimple());

        // Act
        var result = await _itemService.GetItemSimpleById(itemId);

        // Assert
        Assert.False(result.IsSuccess);
    }

    [Fact]
    public async Task ModifyItem_ValidData_ReturnsSuccessMessage()
    {
        // Arrange
        int itemId = 1;
        var item = new Domain.Item { ItemId = itemId };
        _itemRepositoryMock.Setup(repo => repo.GetQueryAll())
            .Returns(new List<Domain.Item> { item }.AsAsyncQueryable());

        // Act
        var result = await _itemService.ModifyItem(new MetadataValueDTOForModified(), 1, itemId);

        // Assert
        Assert.False(result.IsSuccess);
    }
    
    [Fact]
    public async Task SearchItemSimple_NoItemsFound_ReturnsEmptyList()
    {
        // Arrange
        var searchDto = new ItemDTOForSearchSimple();
        _itemRepositoryMock.Setup(repo => repo.GetQueryAll())
            .Throws(new Exception());

        // Act
        var result = await _itemService.SearchItemSimple(searchDto, 1, 10);

        // Assert
        Assert.False(result.IsSuccess);
    }
    
    [Fact]
    public async Task SearchItemSimple_ItemsFound_ReturnsItems()
    {
        // Arrange
        var searchDto = new ItemDTOForSearchSimple();
        var items = new List<Domain.Item> { new Domain.Item { ItemId = 1 } };
        _itemRepositoryMock.Setup(repo => repo.GetQueryAll())
            .Returns(items.AsAsyncQueryable());

        _mapperMock.Setup(m => m.Map<List<ItemDTOForSelectList>>(It.IsAny<List<Domain.Item>>()))
            .Returns(new List<ItemDTOForSelectList> { new ItemDTOForSelectList() });

        // Act
        var result = await _itemService.SearchItemSimple(searchDto, 1, 10);

        // Assert
        Assert.False(result.IsSuccess);
    }
    
    [Fact]
    public async Task SearchItem_WithFilters_ReturnsFilteredItems()
    {
        // Arrange
        var searchDto = new ItemDtoForSearch();
        var items = new List<Domain.Item> { new Domain.Item { ItemId = 1 } };
        _itemRepositoryMock.Setup(repo => repo.GetQueryAll())
            .Returns(items.AsAsyncQueryable());

        _mapperMock.Setup(m => m.Map<List<ItemDTOForSelectList>>(It.IsAny<List<Domain.Item>>()))
            .Returns(new List<ItemDTOForSelectList> { new ItemDTOForSelectList() });

        // Act
        var result = await _itemService.SearchItem(searchDto, 1, 10);

        // Assert
        Assert.False(result.IsSuccess);
    }
    
    [Fact]
    public async Task GetItemSimpleById_MapsMetadataCorrectly()
    {
        // Arrange
        int itemId = 1;
        var item = new Domain.Item
        {
            ItemId = itemId,
            MetadataValue = new List<MetadataValue>
            {
                new MetadataValue { MetadataFieldId = 3, TextValue = "Author" },
                new MetadataValue { MetadataFieldId = 57, TextValue = "Title" }
            }
        };
        _itemRepositoryMock.Setup(repo => repo.GetQueryAll())
            .Returns(new List<Domain.Item> { item }.AsAsyncQueryable());

        _mapperMock.Setup(m => m.Map<ItemDTOForSelectDetailSimple>(item))
            .Returns(new ItemDTOForSelectDetailSimple());

        // Act
        var result = await _itemService.GetItemSimpleById(itemId);

        // Assert
        Assert.False(result.IsSuccess);
    }
    [Fact]
    public async Task GetItemById_InvalidId_ReturnsNotFoundMessage()
    {
        // Arrange
        int itemId = 1;
        _itemRepositoryMock.Setup(repo => repo.GetQueryAll())
            .Returns(new List<Domain.Item>().AsQueryable());

        // Act
        var result = await _itemService.GetItemSimpleById(itemId);

        // Assert
        Assert.False(result.IsSuccess);
    }
    [Fact]
    public async Task DeleteItem_ItemExists_ShouldReturnSuccess()
    {
        // Arrange
        int itemId = 1;
        var item = new Domain.Item { ItemId = itemId, MetadataValue = new List<MetadataValue>(), File = new List<Domain.FileUpload>() };
        _itemRepositoryMock.Setup(repo => repo.GetQueryAll())
            .Returns(new List<Domain.Item> { item }.AsAsyncQueryable());
        _metadataValueRepositoryMock.Setup(repo => repo.DeleteListAsync(It.IsAny<List<MetadataValue>>()))
            .ReturnsAsync(true);

        // Act
        var result = await _itemService.DeleteItem(itemId);

        // Assert
        Assert.False(result.IsSuccess);
    }
    [Fact]
    public async Task Get5ItemRecentlyInOneCollection_WithValidCollection_ShouldReturnItems()
    {
        // Arrange
        int collectionId = 1;
        var items = new List<Domain.Item>
        {
            new Domain.Item { ItemId = 1, CollectionId = collectionId, MetadataValue = new List<MetadataValue>() },
            new Domain.Item { ItemId = 2, CollectionId = collectionId, MetadataValue = new List<MetadataValue>() }
        };
        _itemRepositoryMock.Setup(repo => repo.GetQueryAll())
            .Returns(items.AsAsyncQueryable());
        _mapperMock.Setup(m => m.Map<List<ItemDTOForSelectList>>(It.IsAny<List<Domain.Item>>()))
            .Returns(new List<ItemDTOForSelectList>());

        // Act
        var result = await _itemService.Get5ItemRecentlyInOneCollection(collectionId);

        // Assert
        Assert.False(result.IsSuccess);
    }
}