using Xunit;
using Moq;
using Application.CollectionGroups;
using Application.Collections;
using Application.CommunityGroups;
using Application.GroupPeoples;
using Application.Responses;
using AutoMapper;
using Infrastructure.Repositories.CollectionGroupRepositories;
using Infrastructure.Repositories.CollectionRepositories;
using Infrastructure.Repositories.GroupPeopleRepositories;
using Infrastructure.Repositories.GroupRepositories;
using Infrastructure.Services.Implements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Domain;
using Assert = Xunit.Assert;

namespace UnitTest.Application.CollectionGroup;

public class CollectionGroupServiceTests
{
    private readonly Mock<IMapper> _mapperMock;
    private readonly Mock<ICollectionGroupRepository> _collectionGroupRepositoryMock;
    private readonly Mock<IGroupRepository> _groupRepositoryMock;
    private readonly Mock<ICollectionRepository> _collectionRepositoryMock;
    private readonly Mock<IGroupPeopleRepository> _groupPeopleRepositoryMock;
    private readonly CollectionGroupService _service;

    public CollectionGroupServiceTests()
    {
        _mapperMock = new Mock<IMapper>();
        _collectionGroupRepositoryMock = new Mock<ICollectionGroupRepository>();
        _groupRepositoryMock = new Mock<IGroupRepository>();
        _collectionRepositoryMock = new Mock<ICollectionRepository>();
        _groupPeopleRepositoryMock = new Mock<IGroupPeopleRepository>();
        _service = new CollectionGroupService(
            _mapperMock.Object,
            _collectionGroupRepositoryMock.Object,
            _groupRepositoryMock.Object,
            _collectionRepositoryMock.Object,
            _groupPeopleRepositoryMock.Object
        );
    }

    [Fact]
    public async Task AddCollectionManageGroup_GroupDoesNotExist_ReturnsErrorResponse()
    {
        // Arrange
        var collectionGroupDTOs = new List<CollectionGroupDTOForCreate>
        {
            new CollectionGroupDTOForCreate { GroupId = 1, CollectionId = 1 }
        };
        _groupRepositoryMock.Setup(x => x.GetGroup(It.IsAny<int>())).ReturnsAsync((Group)null);

        // Act
        var result = await _service.AddCollectionManageGroup(collectionGroupDTOs);

        // Assert
        Assert.False(result.IsSuccess);
        Assert.Equal("Group 1 does not exist", result.Message);
    }

    [Fact]
    public async Task AddCollectionManageGroup_CollectionDoesNotExist_ReturnsErrorResponse()
    {
        // Arrange
        var collectionGroupDTOs = new List<CollectionGroupDTOForCreate>
        {
            new CollectionGroupDTOForCreate { GroupId = 1, CollectionId = 1 }
        };
        _groupRepositoryMock.Setup(x => x.GetGroup(It.IsAny<int>())).ReturnsAsync(new Group());
        _collectionRepositoryMock.Setup(x => x.GetCollection(It.IsAny<int>())).ReturnsAsync((Domain.Collection)null);

        // Act
        var result = await _service.AddCollectionManageGroup(collectionGroupDTOs);

        // Assert
        Assert.False(result.IsSuccess);
        Assert.Equal("Collection 1 does not exist", result.Message);
    }

    [Fact]
    public async Task AddCollectionManageGroup_GroupAlreadyManagesCollection_ReturnsErrorResponse()
    {
        // Arrange
        var collectionGroupDTOs = new List<CollectionGroupDTOForCreate>
        {
            new CollectionGroupDTOForCreate { GroupId = 1, CollectionId = 1 }
        };
        _groupRepositoryMock.Setup(x => x.GetGroup(It.IsAny<int>())).ReturnsAsync(new Group { Title = "Group 1" });
        _collectionRepositoryMock.Setup(x => x.GetCollection(It.IsAny<int>())).ReturnsAsync(new Domain.Collection { CollectionName = "Collection 1" });
        _collectionGroupRepositoryMock.Setup(x => x.GetQueryAll()).Returns(new List<Domain.CollectionGroup> { new Domain.CollectionGroup { GroupId = 1, CollectionId = 1 } }.AsAsyncQueryable());

        // Act
        var result = await _service.AddCollectionManageGroup(collectionGroupDTOs);

        // Assert
        Assert.False(result.IsSuccess);
    }
    

    [Fact]
    public async Task UpdateCollectionManageGroup_GroupDoesNotExist_ReturnsErrorResponse()
    {
        // Arrange
        var collectionGroupDTO = new CollectionGroupDTOForUpdate { GroupId = 1, CollectionId = 1, Id = 1 };
        _groupRepositoryMock.Setup(x => x.GetGroup(It.IsAny<int>())).ReturnsAsync((Group)null);

        // Act
        var result = await _service.UpdateCollectionManageGroup(collectionGroupDTO);

        // Assert
        Assert.False(result.IsSuccess);
        Assert.Equal("Collection Group1 does not exist", result.Message);
    }

    [Fact]
    public async Task DeleteCollectionManageInGroup_GroupDoesNotManageCollection_ReturnsSuccessResponse()
    {
        // Arrange
        _collectionGroupRepositoryMock.Setup(x => x.GetCollectionGroup(It.IsAny<int>())).ReturnsAsync((Domain.CollectionGroup)null);

        // Act
        var result = await _service.DeleteCollectionManageInGroup(1);

        // Assert
        Assert.True(result.IsSuccess);
        Assert.Equal("Group does not manage this Collection", result.Message);
    }

    [Fact]
    public async Task GetListCollectionGroup_ReturnsListOfCollectionGroupDTOs()
    {
        // Arrange
        var collectionGroups = new List<Domain.CollectionGroup> { new Domain.CollectionGroup { Group = new Group(), Collection = new Domain.Collection() } };
        _collectionGroupRepositoryMock.Setup(x => x.GetQueryAll()).Returns(collectionGroups.AsAsyncQueryable());
        _mapperMock.Setup(x => x.Map<List<CollectionGroupDTOForDetail>>(It.IsAny<IEnumerable<Domain.CollectionGroup>>())).Returns(new List<CollectionGroupDTOForDetail>());

        // Act
        var result = await _service.GetListCollectionGroup();

        // Assert
        Assert.True(result.IsSuccess);
        Assert.NotNull(result.ObjectResponse);
    }
    

    [Fact]
    public async Task GetCollectionGroupDetailByStaff_NoMatchingGroup_ReturnsErrorResponse()
    {
        // Arrange
        var peopleId = 1;
        var collectionId = 1;
        _groupPeopleRepositoryMock.Setup(x => x.GetListGroupByStaff(It.IsAny<int>())).ReturnsAsync(new List<int> { 1 });
        _collectionGroupRepositoryMock.Setup(x => x.GetQueryAll()).Returns(new List<Domain.CollectionGroup> { new Domain.CollectionGroup { GroupId = 2, CollectionId = 2 } }.AsAsyncQueryable());

        // Act
        var result = await _service.GetCollectionGroupDetailByStaff(collectionId, peopleId);

        // Assert
        Assert.False(result.IsSuccess);
        Assert.Equal("you not have right in this collection", result.Message);
    }
}