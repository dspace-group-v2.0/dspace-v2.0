﻿using Application.Peoples;
using Application.Responses;
using Infrastructure.Services;
using Moq;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace UnitTest.Application.People
{
    public class PeopleServiceTest
    {
        private readonly Mock<IPeopleService> _peopleServiceMock;
        private readonly ITestOutputHelper _testOutputHelper;
        public PeopleServiceTest(ITestOutputHelper testOutputHelper)
        {
            _peopleServiceMock = new Mock<IPeopleService>();
            _testOutputHelper = testOutputHelper;
        }

        [Fact]
        public async Task CreatePeople_ShouldReturnSuccess_WhenPersonIsCreated()
        {
            // Arrange
            var peopleDTO = new PeopleDTOForCreateUpdate
            {
                FirstName = "John",
                LastName = "Doe",
                Address = "123 Main St",
                PhoneNumber = "0123456789",
                Email = "john.doe@example.com",
                Role = "ADMIN"
            };
            int userCreateId = 1;

            var expectedResponse = new ResponseDTO
            {
                IsSuccess = true
            };

            _peopleServiceMock.Setup(service => service.CreatePeople(peopleDTO, userCreateId))
                              .ReturnsAsync(expectedResponse);

            // Act
            var result = await _peopleServiceMock.Object.CreatePeople(peopleDTO, userCreateId);

            // Xunit.Assert
            Xunit.Assert.True(result.IsSuccess);
        }

        [Fact]
        public async Task UpdatePeople_ShouldReturnSuccess_WhenPersonIsUpdated()
        {
            // Arrange
            int personId = 1;
            var peopleDTO = new PeopleDTOForCreateUpdate
            {
                FirstName = "Jane",
                LastName = "Doe",
                Address = "456 Elm St",
                PhoneNumber = "9876543210",
                Email = "jane.doe@example.com",
                Role = "USER"
            };
            int userUpdateId = 1;

            var expectedResponse = new ResponseDTO
            {
                IsSuccess = true
            };

            _peopleServiceMock.Setup(service => service.UpdatePeople(personId, peopleDTO, userUpdateId))
                              .ReturnsAsync(expectedResponse);

            // Act
            var result = await _peopleServiceMock.Object.UpdatePeople(personId, peopleDTO, userUpdateId);

            // Xunit.Assert
            Xunit.Assert.True(result.IsSuccess);
        }

        [Fact]
        public async Task GetPeopleByID_ShouldReturnPerson_WhenPersonExists()
        {
            // Arrange
            int personId = 1;
            var expectedResponse = new ResponseDTO
            {
                IsSuccess = true,
                ObjectResponse = new PeopleDTOForCreateUpdate
                {
                    FirstName = "John",
                    LastName = "Doe",
                    Address = "123 Main St",
                    PhoneNumber = "0123456789",
                    Email = "john.doe@example.com",
                    Role = "ADMIN"
                }
            };

            _peopleServiceMock.Setup(service => service.GetPeopleByID(personId))
                              .ReturnsAsync(expectedResponse);

            // Act
            var result = await _peopleServiceMock.Object.GetPeopleByID(personId);

            // Xunit.Assert
            Xunit.Assert.True(result.IsSuccess);
            Xunit.Assert.Equal(expectedResponse.ObjectResponse, result.ObjectResponse);
        }

        [Fact]
        public async Task GetPeopleByEmail_ShouldReturnPerson_WhenPersonExists()
        {
            // Arrange
            string email = "john.doe@example.com";
            var expectedResponse = new ResponseDTO
            {
                IsSuccess = true,
                ObjectResponse = new PeopleDTOForCreateUpdate
                {
                    FirstName = "John",
                    LastName = "Doe",
                    Address = "123 Main St",
                    PhoneNumber = "0123456789",
                    Email = email,
                    Role = "ADMIN"
                }
            };

            _peopleServiceMock.Setup(service => service.GetPeopleByEmail(email))
                              .ReturnsAsync(expectedResponse);

            // Act
            var result = await _peopleServiceMock.Object.GetPeopleByEmail(email);

            // Xunit.Assert
            Xunit.Assert.True(result.IsSuccess);
            Xunit.Assert.Equal(expectedResponse.ObjectResponse, result.ObjectResponse);
        }

        [Fact]
        public async Task DeletePeople_ShouldReturnSuccess_WhenPersonIsDeleted()
        {
            // Arrange
            int personId = 1;

            var expectedResponse = new ResponseDTO
            {
                IsSuccess = true
            };

            _peopleServiceMock.Setup(service => service.DeletePeople(personId))
                              .ReturnsAsync(expectedResponse);

            // Act
            var result = await _peopleServiceMock.Object.DeletePeople(personId);

            // Xunit.Assert
            Xunit.Assert.True(result.IsSuccess);
        }

        // Test cases for failure scenarios (IsSuccess = false)

        [Fact]
        public async Task CreatePeople_ShouldReturnFalse_WhenPersonCreationFails()
        {
            // Arrange
            var peopleDTO = new PeopleDTOForCreateUpdate
            {
                FirstName = "Invalid",
                LastName = "Person",
                Address = "Invalid Address",
                PhoneNumber = "InvalidPhone",
                Email = "invalid.email.com",
                Role = "INVALIDROLE"
            };
            int userCreateId = 1;

            var expectedResponse = new ResponseDTO
            {
                IsSuccess = false,
                Message = "Person creation failed due to invalid data"
            };

            _peopleServiceMock.Setup(service => service.CreatePeople(peopleDTO, userCreateId))
                              .ReturnsAsync(expectedResponse);

            // Act
            var result = await _peopleServiceMock.Object.CreatePeople(peopleDTO, userCreateId);

            // Xunit.Assert
            Xunit.Assert.False(result.IsSuccess);
            Xunit.Assert.Equal(expectedResponse.Message, result.Message);
        }

        [Fact]
        public async Task UpdatePeople_ShouldReturnFalse_WhenPersonUpdateFails()
        {
            // Arrange
            int personId = -1;
            var peopleDTO = new PeopleDTOForCreateUpdate
            {
                FirstName = "Invalid",
                LastName = "Person",
                Address = "Invalid Address",
                PhoneNumber = "InvalidPhone",
                Email = "invalid.email.com",
                Role = "INVALIDROLE"
            };
            int userUpdateId = 1;

            var expectedResponse = new ResponseDTO
            {
                IsSuccess = false,
                Message = "Person update failed due to invalid data"
            };

            _peopleServiceMock.Setup(service => service.UpdatePeople(personId, peopleDTO, userUpdateId))
                              .ReturnsAsync(expectedResponse);

            // Act
            var result = await _peopleServiceMock.Object.UpdatePeople(personId, peopleDTO, userUpdateId);

            // Xunit.Assert
            Xunit.Assert.False(result.IsSuccess);
            Xunit.Assert.Equal(expectedResponse.Message, result.Message);
        }

        [Fact]
        public async Task GetPeopleByID_ShouldReturnFalse_WhenPersonDoesNotExist()
        {
            // Arrange
            int personId = -1;

            var expectedResponse = new ResponseDTO
            {
                IsSuccess = false,
                Message = "Person not found"
            };

            _peopleServiceMock.Setup(service => service.GetPeopleByID(personId))
                              .ReturnsAsync(expectedResponse);

            // Act
            var result = await _peopleServiceMock.Object.GetPeopleByID(personId);

            // Xunit.Assert
            Xunit.Assert.False(result.IsSuccess);
            Xunit.Assert.Equal(expectedResponse.Message, result.Message);
        }

        [Fact]
        public async Task GetPeopleByEmail_ShouldReturnFalse_WhenPersonDoesNotExist()
        {
            // Arrange
            string email = "nonexistent@example.com";

            var expectedResponse = new ResponseDTO
            {
                IsSuccess = false,
                Message = "Person not found"
            };

            _peopleServiceMock.Setup(service => service.GetPeopleByEmail(email))
                              .ReturnsAsync(expectedResponse);

            // Act
            var result = await _peopleServiceMock.Object.GetPeopleByEmail(email);

            // Xunit.Assert
            Xunit.Assert.False(result.IsSuccess);
            Xunit.Assert.Equal(expectedResponse.Message, result.Message);
        }

        [Fact]
        public async Task DeletePeople_ShouldReturnFalse_WhenPersonDeletionFails()
        {
            // Arrange
            int personId = -1;

            var expectedResponse = new ResponseDTO
            {
                IsSuccess = false,
                Message = "Person deletion failed"
            };

            _peopleServiceMock.Setup(service => service.DeletePeople(personId))
                              .ReturnsAsync(expectedResponse);

            // Act
            var result = await _peopleServiceMock.Object.DeletePeople(personId);

            // Xunit.Assert
            Xunit.Assert.False(result.IsSuccess);
            Xunit.Assert.Equal(expectedResponse.Message, result.Message);
        }
    }
}
