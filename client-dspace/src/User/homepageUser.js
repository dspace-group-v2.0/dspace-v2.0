import { Link, Navigate, useNavigate } from 'react-router-dom';
import './homepageUser.css';
import Header from '../components/Header/Header';
import React, { useEffect, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Helmet } from 'react-helmet';
import { jwtDecode } from 'jwt-decode';

const HomepageUser = () => {
  const token = localStorage.getItem('Token');
  const role = token ? jwtDecode(token)["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"] : null;
  const navigate = useNavigate();

  useEffect(() => {
    if (role !== null) {
      if (role === "STUDENT" || role === "LECTURER") {
        navigate('/Homepage');
      } else if (role === "STAFF") {
        navigate('/HomepageStaff');
      } else if (role === "ADMIN") {
        navigate('/HomepageAdmin');
      }
    } else {
      navigate('/Login');
    }
  }, []);

  return (
    <div>
      <Helmet>
        <meta charSet="utf-8" />
        <title>Redirect...</title>
      </Helmet>
      <Header />
      <div className="container my-5">
        <div>Redirect...</div>
      </div>
      <footer className="footer" style={{ backgroundColor: 'rgba(255, 255, 255, 0.6)', margin: '0' }}>
        <div className="container">
          <div className="row">
            <div className="col-12 text-center">
              <span>&copy; 2023 - FPT Education | <Link to="/about-us">About us</Link></span>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
};

export default HomepageUser;