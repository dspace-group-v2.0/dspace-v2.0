import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useParams, useNavigate, Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from '../components/Header/Header-admin';
import './homepageStaff.css'
import { Helmet } from 'react-helmet';

const GroupDetails = () => {
    const { groupId } = useParams();
    const navigate = useNavigate();

    const [error, setError] = useState(null);
    const [successMessage, setSuccessMessage] = useState('');
    const [failureMessage, setFailureMessage] = useState('');
    const [group, setGroup] = useState(null);
    const token = localStorage.getItem('Token');
    const header = `Bearer ${token}`;

    useEffect(() => {
        const fetchGroupDetails = async () => {
            try {
                const response = await axios.get(`${process.env.REACT_APP_BASE_URL}/api/GroupStaff/getGroup/${groupId}`, {
                    headers: {
                        Authorization: header
                    }
                });
                setGroup(response.data);
            } catch (error) {
                setError(error.message);
                console.error('Error fetching group details:', error);
            }
        };

        fetchGroupDetails();
    }, [groupId]);

    //     const [review, setReview] = useState(group?.canReview);
    //     const [submit, setSubmit] = useState(group?.canSubmit);
    //     const [edit, setEdit] = useState(group?.canEdit);

    // console.log(group.listCollectionGroup.canEdit)
    const handleDeleteMember = async (userId, groupId) => {
        if (window.confirm('Are you sure you want to delete this member?')) {
            try {
                const response = await axios.put(`${process.env.REACT_APP_BASE_URL}/api/GroupPeople/DeletePeopleInGroup`, {
                    "groupId": groupId + '',
                    "peopleId": userId + '',
                });
                if (response.status === 200) {
                    setSuccessMessage('Member deleted successfully');
                    setFailureMessage('');
                    setGroup({
                        ...group,
                        listPeopleInGroup: group.listPeopleInGroup.filter(member => member.peopleId !== userId)
                    });
                } else {
                    setFailureMessage('Failed to delete member');
                    setSuccessMessage('');
                }
            } catch (error) {
                console.error('Error deleting member:', error);
                setFailureMessage('Failed to delete member');
                setSuccessMessage('');
            }
        }
    };


    const [collections, setCollections] = useState([]);
    const [searchQuery, setSearchQuery] = useState('');
    const [searchType, setSearchType] = useState('collectionName'); // State to track the current search type
    const [currentPage, setCurrentPage] = useState(1);
    const itemsPerPage = 5;

    const handleSearchChange = (e) => {
        setSearchQuery(e.target.value);
        setCurrentPage(1); // Reset to first page on new search
    };

    const handleSearchTypeChange = (e) => {
        setSearchType(e.target.value);
        setSearchQuery(''); // Clear search query when changing search type
    };
    if (!group) {
        return <div>Loading...</div>;
    }

    const filteredCollections = group.listCollectionGroup ? group.listCollectionGroup.filter(collection => {
        if (!searchQuery.trim()) return true;
        const normalizedSearchQuery = searchQuery.replace(/\s+/g, '').toLowerCase(); // Normalize search query

        switch (searchType) {
            case 'collectionName':
                return collection.collectionName.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery);
            case 'parentCommunity':
                return collection.communityDTOForSelect.communityName.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery);
            case 'description':
                return collection.shortDescription.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery);
            case 'all':
                return Object.values(collection).some(value =>
                    typeof value === 'string' && value.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery)
                );
            default:
                return false;
        }
    }) : [];
    const filteredCommunities = group.listCommunityGroup ? group.listCommunityGroup.filter(community => {
        if (!searchQuery.trim()) return true;
        const normalizedSearchQuery = searchQuery.replace(/\s+/g, '').toLowerCase(); // Normalize search query

        switch (searchType) {
            case 'collectionName':
                return community.communityName.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery);
            // case 'parentCommunity':
            //     return collection.communityDTOForSelect.communityName.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery);
            // case 'description':
            //     return collection.shortDescription.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery);
            // case 'all':
            //     return Object.values(collection).some(value =>
            //         typeof value === 'string' && value.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery)
            //     );
            default:
                return false;
        }
    }) : [];


    const indexOfLastItem = currentPage * itemsPerPage;
    const indexOfFirstItem = indexOfLastItem - itemsPerPage;
    const currentCollections = filteredCollections.slice(indexOfFirstItem, indexOfLastItem);
    const currentCommunities = filteredCommunities.slice(indexOfFirstItem, indexOfLastItem);

    const totalPages = Math.ceil(filteredCollections.length / itemsPerPage);

    const handlePageChange = (pageNumber) => {
        setCurrentPage(pageNumber);
    };

    const handleNextPage = () => {
        if (currentPage < totalPages) {
            setCurrentPage(prevPage => prevPage + 1);
        }
    };

    const handlePreviousPage = () => {
        if (currentPage > 1) {
            setCurrentPage(prevPage => prevPage - 1);
        }
    };

    return (
        <div>
            <Header />
            <Helmet>
                <meta charSet="utf-8" />
                <title>Group - {`${group?.title}`} - dSPACE</title>
            </Helmet>
            <div className="container mt-5">
                <h1 className="mb-4">Group Details: {group.title}</h1>
                {error && <div className="alert alert-danger">Error: {error}</div>}
                {successMessage && <div className="alert alert-success">{successMessage}</div>}
                {failureMessage && <div className="alert alert-danger">{failureMessage}</div>}
                <div className="card" style={{ maxWidth: '50%' }}>
                    <div className="card-body">
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-3 text-end">Title:</div>
                            <div className="col-sm-6">{group.title}</div>
                        </div>
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-3 text-end">Description:</div>
                            <div className="col-sm-6">{group.description}</div>
                        </div>
                        <div className="row pb-2 my-3">
                            <div className="col-sm-3 text-end">Active:</div>
                            <div className="col-sm-6"><input type="checkbox" checked={group.isActive} readOnly /></div>
                        </div>
                    </div>
                </div>
                <div className="mt-4 mb-5">
                    <h4>Group Members</h4>
                    <table className="table table-striped table-bordered">
                        <thead className="thead-dark">
                            <tr>
                                <th>Full Name</th>
                                <th>Address</th>
                                <th>Phone Number</th>
                                <th>Email</th>
                                {/* <th>Actions</th> */}
                            </tr>
                        </thead>
                        <tbody>
                            {group.listPeopleInGroup.map(member => (
                                <tr key={member.peopleId}>
                                    <td>{`${member.firstName || ''} ${member.lastName || ''}`.trim() || 'N/A'}</td>
                                    <td>{member.address || 'N/A'}</td>
                                    <td>{member.phoneNumber || 'N/A'}</td>
                                    <td>{member.email || 'N/A'}</td>
                                    {/* <td>
                                        <button
                                            className="btn btn-danger btn-sm"
                                            onClick={() => handleDeleteMember(member.peopleId, groupId)}
                                        >
                                            Delete
                                        </button>
                                    </td> */}
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>


                {/* ======================================== */}

                <div className="row mb-4">

                    <div className="col-md-5">
                        <label htmlFor="searchInput" className="form-label fw-bold">Search</label>
                        <input
                            id="searchInput"
                            type="text"
                            className="form-control"
                            placeholder="Search by name..."
                            value={searchQuery}
                            onChange={handleSearchChange}
                        />
                    </div>
                    <h3 className='mb-3 mt-4'>Communities assigned for your group</h3>
                    <table className="table table-striped table-bordered">
                    <thead className="thead-dark">
                        <tr>
                            <th className="short-column">No</th>
                            {/* <th>Logo</th> */}
                            <th>Name</th>
                            {/* <th>Short Description</th>
                            <th>Active</th> */}
                        </tr>
                    </thead>
                    <tbody>
                        {currentCommunities.map((community, index) => (
                            <tr key={community.communityId}>
                                <td className="short-column">{index + 1 + (currentPage - 1) * itemsPerPage}</td>
                                {/* <td>{community.logoUrl ? (
                                    <img src={`/${getRelativePath(community.logoUrl)}`} alt="logo" className='img-thumbnail' style={{ width: '200px'}} />
                                ) : (
                                    <div>No Image</div>
                                )}</td> */}
                                <td>
                                    <Link to={`/Dspace/Community/CommunityDetail/${community.communityId}`}>
                                        {community.communityName}
                                    </Link>
                                </td>
                                {/* <td>{community.shortDescription}</td>
                                <td>
                                    <input type="checkbox" checked={community.isActive} readOnly />
                                </td> */}
                            </tr>
                        ))}
                    </tbody>
                </table>
                    <h3 className='mb-3 mt-4'>Collection assigned for your group</h3>
                    <div className="col-md-4">
                        <label hidden htmlFor="searchTypeSelect" className="form-label fw-bold">Search By</label>
                        <select
                            hidden
                            id="searchTypeSelect"
                            className="form-select"
                            value={searchType}
                            onChange={handleSearchTypeChange}
                        >
                            {/* <option value="all">Select...</option>  */}
                            <option value="collectionName">Collection Name</option>
                            {/* <option value="parentCommunity">Parent Community</option> */}
                            {/* <option value="description">Description</option>  */}
                        </select>
                    </div>
                    {/* <div className="col-md-3 d-flex align-items-end justify-content-end">
                        <Link to="/Dspace/Collection/CreateCollection" className="btn btn-primary">Create new collection</Link>
                    </div> */}
                </div>
                {error && <div className="alert alert-danger">{error}</div>}
                <table className="table table-striped table-bordered">
                    <thead className="thead-dark">
                        <tr>
                            <th className="short-column">No</th>
                            {/* <th>Logo</th> */}
                            <th>Name</th>
                            {/* <th>Short Description</th> */}
                            {/* <th className="short-column">Can Review</th>
                            <th className="short-column">Can Submit</th> */}
                            {/* <th className="short-column">Can Edit</th> */}
                        </tr>
                    </thead>
                    <tbody>
                        {currentCollections.map((collection, index) => (
                            <tr key={collection.collectionId}>
                                <td className="short-column">{index + 1 + (currentPage - 1) * itemsPerPage}</td>
                                {/* <td><img src={collection.logoUrl} alt="logo" className='img-thumbnail' style={{ width: '200px'}} /></td> */}
                                <td>
                                    <Link to={`/Dspace/Collection/CollectionDetail/${collection.collectionId}`}>
                                        {collection.collectionName}
                                    </Link>
                                </td>
                                {/* <td>{collection.shortDescription}</td> */}
                                {/* <td className="short-column">
                                    <input type="checkbox" checked={collection.canReview} readOnly />
                                </td>
                                <td className="short-column"> 
                                    <input type="checkbox" checked={collection.canSubmit} readOnly />
                                </td> */}
                                {/* <td className="short-column">
                                    <input type="checkbox" checked={collection.canEdit} readOnly />
                                </td> */}
                            </tr>
                        ))}
                        {currentCollections.length === 0 && (
                            <tr>
                                <td colSpan="7" className="text-center">No collections found</td>
                            </tr>
                        )}
                    </tbody>
                </table>
                <div className="mt-4">
                    {/* <button className="btn btn-success me-2" onClick={() => navigate(``)}>Add people to group</button>
                    <button className="btn btn-primary me-2" onClick={() => navigate(``)}>Edit</button>
                    <button className="btn btn-danger me-2" onClick={() => navigate(``)}>Delete</button> */}
                    <button className="btn btn-secondary" onClick={() => navigate('/Dspace/Group/GroupStaffList')}>Back to List</button>
                </div>
                <div className="d-flex justify-content-center">
                    <nav>
                        <ul className="pagination">
                            <li className={`page-item ${currentPage === 1 ? 'disabled' : ''}`}>
                                <button className="page-link" onClick={handlePreviousPage}>Previous</button>
                            </li>
                            {[...Array(totalPages)].map((_, i) => (
                                <li key={i} className={`page-item ${currentPage === i + 1 ? 'active' : ''}`}>
                                    <button className="page-link" onClick={() => handlePageChange(i + 1)}>
                                        {i + 1}
                                    </button>
                                </li>
                            ))}
                            <li className={`page-item ${currentPage === totalPages ? 'disabled' : ''}`}>
                                <button className="page-link" onClick={handleNextPage}>Next</button>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    );
};

export default GroupDetails;
