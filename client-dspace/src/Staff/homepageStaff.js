import React, { useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import '../Admin/homepage.css';
import HeaderAdmin from '../components/Header/Header-admin';
import { Search } from '@material-ui/icons'; // Assuming you're using Material-UI icons
import { Helmet } from 'react-helmet';
import Noti from "../components/Statistics/noti.js";
import { StyledEngineProvider } from '@mui/material/styles';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';
const HomepageAdmin = () => {
    const [communities, setCommunities] = useState([]);
    const [searchQuery, setSearchQuery] = useState('');
    const navigate = useNavigate();
    useEffect(() => {
        const fetchCommunities = async () => {
            try {
                const responseCommunities = await fetch(`${process.env.REACT_APP_BASE_URL}/api/Community/GetCommunityParent`);
                const dataCommunities = await responseCommunities.json();
                setCommunities(dataCommunities);
            } catch (error) {
                console.error('Error fetching communities:', error);
            }
        };

        fetchCommunities();
        // fetchCollections();
    }, []);

    const handleSearchChange = (event) => {
        setSearchQuery(event.target.value);
    };

    const handleSearchSubmit = () => {
        navigate(`/Dspace/Collection/ItemStaffList/0?search=${searchQuery}`);
    };

    return (
        <div className="">
            <Helmet>
                <meta charSet="utf-8" />
                <title>Homepage for Staff - dSPACE</title>
            </Helmet>
            <HeaderAdmin />
            <img
                src="FPT.jpg"
                alt="Background"
                className="background-image"
            />
            <div className="content-container">
                <div className="container">
                    <div className="row">
                        <div className="col-12 mt-5">
                            <p style={{ margin: '0', fontWeight: 'bold' }}>Search items by title</p>
                            <div className="search-container d-flex align-items-center mb-4">
                                <input
                                    type="text"
                                    className="form-control w-50"
                                    placeholder="Search by repository"
                                    style={{ borderRadius: '0', flex: '0 1 auto' }}
                                    value={searchQuery}
                                    onChange={handleSearchChange}
                                />
                                <button
                                    style={{ backgroundColor: '#43515F', color: 'white', borderRadius: '0 6px 6px 0' }}
                                    className="btn"
                                    onClick={handleSearchSubmit}
                                >
                                    <Search />
                                </button>
                            </div>

                            <h1>Communities in DSpace</h1>
                            {/* <StyledEngineProvider injectFirst> */}
                                {/* <Noti /> */}
                                {/* <ToastContainer position="top-right" newestOnTop /> */}
                            {/* </StyledEngineProvider> */}
                            <h4>Select a community to browse its collections</h4>
                            <ul>
                                {communities.map((community, index) => (
                                    <li key={index}>
                                        <Link to={`/Dspace/Community/CommunityDetail/${community.communityId}`} className="community-link">
                                            {community.communityName}
                                        </Link>
                                    </li>
                                ))}
                            </ul>
                        </div>
                    </div>

                    <hr className="separator-line" />
                </div>
            </div>

            <div className="ft">
                <div className="container">
                    <div className="row">
                        <div className="col-12 text-center">
                            <span>&copy; 2023 - FPT Education | <Link to="/about-us">About us</Link></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default HomepageAdmin;
