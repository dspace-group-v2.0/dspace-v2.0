import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/DeleteOutlined';
import SaveIcon from '@mui/icons-material/Save';
import CancelIcon from '@mui/icons-material/Close';
import {
    GridRowModes,
    DataGrid,
    GridToolbarContainer,
    GridActionsCellItem,
    GridRowEditStopReasons,
} from '@mui/x-data-grid';
import {
    randomId,
} from '@mui/x-data-grid-generator';
import Autocomplete from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';
import { useNavigate, useParams } from 'react-router-dom';
import axios from 'axios';
import MetadataField from '../../components/metadataFields'
import Header from '../../components/Header/Header';
import { Card, CardContent, Chip, Container, Typography } from '@mui/material';
import { Tabs, Tab, Table } from 'react-bootstrap';
import Save from '@mui/icons-material/Save';
import Close from '@mui/icons-material/Close';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import '../../Admin/Items/ModifyItem.css'
import { Helmet } from 'react-helmet';
function getMetadataValue(key) {
    const field = MetadataField.find(item => item.metadataFieldName === key);
    return field ? field.metadataFieldId : null;
}

function MetadataSelect(props) {
    const { id, field, value, api, colDef, row } = props;
    const fieldForId = 'metadataFieldId'
    console.log(props)
    const handleChange = (event, newValue) => {
        if (newValue !== null) {
            api.setEditCellValue({ id, field, value: newValue.metadataFieldName }, event);
        }
        console.log(newValue)
    };
    return (
        <Autocomplete
            sx={{
                width: '100%'
            }}
            options={MetadataField}
            getOptionLabel={(option) => `${option.metadataFieldName}`}
            id="metadata-customized-option-demo"
            onChange={handleChange}
            renderInput={(params) => (
                <TextField {...params} label="Choose a metadata" variant="standard" />
            )}
        />
    );
}


function EditToolbar(props) {
    const { setModifyFields, setRowModesModel, setModifyStatus } = props;

    const handleClick = () => {
        const metadataValueId = randomId();
        setModifyFields((oldRows) => [...oldRows, { metadataValueId, metadataFieldName: '', textValue: '', textLang: '' }]);
        setRowModesModel((oldModel) => ({
            ...oldModel,
            [metadataValueId]: { mode: GridRowModes.Edit, fieldToFocus: 'metadataFieldName' },
        }));
        setModifyStatus('ADD');
    };

    return (
        <GridToolbarContainer>
            <Button color="primary" startIcon={<AddIcon />} onClick={handleClick}>
                Add record
            </Button>
        </GridToolbarContainer>
    );
}

const PageLink = ({ href, text }) => {
    return (
        <a href={href}>
            {text}
        </a>
    );
};

function ConvertDateTime(datetime) {
    const date = new Date(datetime);

    // Get the components
    const day = date.toLocaleString('en-US', { weekday: 'short' });
    const month = date.toLocaleString('en-US', { month: 'short' });
    const dayOfMonth = date.getDate();
    const year = date.getFullYear();
    const hours = date.getHours().toString().padStart(2, '0');
    const minutes = date.getMinutes().toString().padStart(2, '0');
    const seconds = date.getSeconds().toString().padStart(2, '0');
    const timezoneOffset = -date.getTimezoneOffset() / 60;
    const timezone = `GMT${timezoneOffset >= 0 ? '+' : ''}${timezoneOffset.toString().padStart(2, '0')}00 (Indochina Time)`;
    return `${day} ${month} ${dayOfMonth} ${year} ${hours}:${minutes}:${seconds} ${timezone}`;
}

export default function ModifyItem() {
    const { itemId } = useParams();
    const [addFields, setAddFields] = React.useState([]);
    const [updateFields, setUpdateFields] = React.useState([]);
    const [deleteFields, setDeleteFields] = React.useState([]);
    const [items, setItems] = React.useState([]);
    const [collections, setCollections] = React.useState([]);
    const [error, setError] = React.useState(null);
    const [loading, setLoading] = React.useState(true);
    const token = localStorage.getItem('Token');
    const header = `Bearer ${token}`;
    const [modifyMetadata, setModifyFields] = React.useState([]);
    const [modifyStatus, setModifyStatus] = React.useState('');

    const navigate = useNavigate();
    const handleAddChange = (textValue, textLang, metadataFieldId) => {
        const newField = { textValue, textLang, metadataFieldId, itemId };
        addFields.push(newField);
        const requestBody = {
            add: addFields,
            update: updateFields,
            delete: deleteFields,
        };
        console.log(requestBody);
    };

    const handleUpdateChange = (metadataValueId, textValue, textLang, metadataFieldId) => {
        const newField = { metadataValueId, textValue, textLang, metadataFieldId };
        updateFields.push(newField);
        const requestBody = {
            add: addFields,
            update: updateFields,
            delete: deleteFields,
        };
        console.log(requestBody);
    };

    const handleDeleteChange = (metadataValueId) => {
        const newField = { metadataValueId }
        deleteFields.push(newField);
        const requestBody = {
            add: addFields,
            update: updateFields,
            delete: deleteFields,
        };
        console.log(requestBody);
    };

    React.useEffect(() => {
        if (itemId != 0) {
            axios.get(`${process.env.REACT_APP_BASE_URL}/api/Item/GetItemFullById/${itemId}`)
                .then(({ data }) => {
                    setItems(data);
                    setModifyFields(data.listMetadataValueDTOForSelect)
                    setLoading(false);
                })
                .catch(error => {
                    setError(error.message);
                    setLoading(false);
                });
            axios.get(`${process.env.REACT_APP_BASE_URL}/api/Collection/getListOfCollections`)
                .then(({ data }) => {
                    setCollections(data)
                    setLoading(false);
                })
                .catch(error => {
                    setError(error.message);
                    setLoading(false);
                });
        }
    }, [itemId]);

    const [rowModesModel, setRowModesModel] = React.useState({});

    const handleRowEditStop = (params, event) => {
        if (params.reason === GridRowEditStopReasons.rowFocusOut) {
            event.defaultMuiPrevented = true;
        }
    };

    const handleEditClick = (metadataValueId) => () => {
        setRowModesModel({ ...rowModesModel, [metadataValueId]: { mode: GridRowModes.Edit } });
        setModifyStatus('EDIT');
    };

    const handleSaveClick = (metadataValueId) => () => {
        setRowModesModel({ ...rowModesModel, [metadataValueId]: { mode: GridRowModes.View } });
    };

    const handleDeleteClick = (metadataValueId) => () => {
        handleDeleteChange(metadataValueId)
        setModifyFields(modifyMetadata.filter((row) => row.metadataValueId !== metadataValueId));
    };

    const handleCancelClick = (metadataValueId) => () => {
        setRowModesModel({
            ...rowModesModel,
            [metadataValueId]: { mode: GridRowModes.View, ignoreModifications: true },
        });

        if (modifyStatus.localeCompare('ADD') === 0) {
            setModifyFields(modifyMetadata.filter((row) => row.metadataValueId !== metadataValueId));
        }
    };

    const processRowUpdate = (newRow) => {
        const updatedRow = { ...newRow };
        const fieldMetadataIdGet = getMetadataValue(newRow.metadataFieldName);
        updatedRow.metadataFieldId = fieldMetadataIdGet;
        console.log(updatedRow)
        if (modifyStatus.localeCompare('ADD') === 0) {
            if (modifyMetadata.find((row) => row.metadataFieldName === updatedRow.metadataFieldName
                && row.textValue === updatedRow.textValue
                && row.textLang === updatedRow.textLang) === undefined) {
                const fieldMetadataIdGet = getMetadataValue(updatedRow.metadataFieldName)
                console.log(fieldMetadataIdGet);
                handleAddChange(updatedRow.textValue, updatedRow.textLang, fieldMetadataIdGet)
                setModifyFields(modifyMetadata.map((row) => (row.metadataValueId === updatedRow.metadataValueId ? updatedRow : row)));
            } else {
                console.log("Exist row, no add");
                setModifyFields(modifyMetadata.filter((row) => row.metadataValueId !== updatedRow.metadataValueId));
                return { ...updatedRow, _action: 'delete' };
            }
        } else if (modifyStatus.localeCompare('EDIT') === 0) {
            if (modifyMetadata.find((row) => row.metadataFieldId === updatedRow.metadataFieldId) !== null) {
                setModifyFields(modifyMetadata.filter((row) => row.metadataValueId !== updatedRow.metadataFieldId));
                console.log("Exist row, delete row and update again");
            }
            handleUpdateChange(updatedRow.metadataValueId, updatedRow.textValue, updatedRow.textLang, updatedRow.metadataFieldId)
            setModifyFields(modifyMetadata.map((row) => (row.metadataValueId === updatedRow.metadataValueId ? updatedRow : row)));
        }
        return updatedRow;
    };

    const handleRowModesModelChange = (newRowModesModel) => {
        setModifyStatus('EDIT');
        setRowModesModel(newRowModesModel);
    };

    const handleNonDiscoverableClick = () => {
        const userConfirmed = window.confirm("Do you want to make this item become non-discoverable");
        if (userConfirmed) {
            axios.put(`${process.env.REACT_APP_BASE_URL}/api/Item/ChangeDiscoverableOfItem`, null, {
                params: {
                    itemId: itemId,
                    discoverable: 'false'
                },
                headers: {
                    Authorization: header
                }
            })
                .then(response => {
                    console.log('This item is now setting to non-discoverable');
                })
                .catch(error => {
                    console.error('Error:', error);
                });
            window.location.reload();
        } else {
            console.log('Request canceled by user');
        }
    };

    const handleDeleteItem = () => {
        const confirmDelete = window.confirm("Are you sure you want to delete this item?");
        if (confirmDelete) {
            fetch(`${process.env.REACT_APP_BASE_URL}/api/Item/deleteItem/${itemId}`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                .then(response => {
                    if (!response.ok) {
                        throw new Error('Failed to delete item.');
                    }
                    window.alert('Item deleted successfully!');
                    navigate('/Dspace/Collection/ItemList/0');
                })
                .catch(error => {
                    console.error('Error deleting item:', error);
                });
        }
    };


    const handleDiscoverableClick = () => {
        const userConfirmed = window.confirm("Do you want to make this item become discoverable");
        if (userConfirmed) {
            axios.put(`${process.env.REACT_APP_BASE_URL}/api/Item/ChangeDiscoverableOfItem`, null, {
                params: {
                    itemId: itemId,
                    discoverable: 'true'
                },
                headers: {
                    Authorization: header
                }
            })
                .then(response => {
                    console.log('This item is now setting to discoverable');
                })
                .catch(error => {
                    console.error('Error:', error);
                });
            window.location.reload();
        } else {
            console.log('Request canceled by user');
        }
    };

    const handleRedirectToDetail = () => {
        navigate(`/DSpace/ItemDetail/` + itemId);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const requestBody = {
            add: addFields,
            update: updateFields,
            delete: deleteFields,
        };

        try {
            const response = await fetch(`${process.env.REACT_APP_BASE_URL}/api/Item/ModifyItem/${itemId}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: header
                },
                body: JSON.stringify(requestBody),
            });

            const data = await response.json();
            console.log(data);
        } catch (error) {
            console.error('Error:', error);
        }
    };

    const columns = [
        {
            field: 'metadataFieldId',
            headerName: 'Field ID',
            width: 180,
            hideable: true

        },
        {
            field: 'metadataFieldName',
            headerName: 'Field',
            width: 240,
            editable: true,
            renderEditCell: (params) => (
                <MetadataSelect {...params} />
            )
        },
        {
            field: 'textValue',
            headerName: 'Value',
            width: 420,
            editable: true
        },
        {
            field: 'textLang',
            headerName: 'Lang',
            width: 120,
            editable: true
        },
        // {
        //     field: 'role',
        //     headerName: 'Department',
        //     width: 220,
        //     editable: true,
        //     type: 'singleSelect',
        //     valueOptions: ['Market', 'Finance', 'Development'],
        // },
        {
            field: 'actions',
            type: 'actions',
            headerName: 'Actions',
            width: 100,
            cellClassName: 'actions',
            getActions: ({ id }) => {
                const isInEditMode = rowModesModel[id]?.mode === GridRowModes.Edit;

                if (isInEditMode) {
                    return [
                        <GridActionsCellItem
                            icon={<SaveIcon />}
                            label="Save"
                            sx={{
                                color: 'primary.main',
                            }}
                            onClick={handleSaveClick(id)}
                        />,
                        <GridActionsCellItem
                            icon={<CancelIcon />}
                            label="Cancel"
                            className="textPrimary"
                            onClick={handleCancelClick(id)}
                            color="inherit"
                        />,
                    ];
                }

                return [
                    <GridActionsCellItem
                        icon={<EditIcon />}
                        label="Edit"
                        className="textPrimary"
                        onClick={handleEditClick(id)}
                        color="inherit"
                    />,
                    <GridActionsCellItem
                        icon={<DeleteIcon />}
                        label="Delete"
                        onClick={handleDeleteClick(id)}
                        color="inherit"
                    />,
                ];
            },
        },
    ];

    const [selectedValue, setSelectedValue] = React.useState('');

    const handleSaveCollection = () => {
        if (selectedValue) {
            axios.put(`${process.env.REACT_APP_BASE_URL}/api/Item/ChangeCollectionOfItem`, null, {
                params: {
                    itemId: itemId,
                    collectionId: selectedValue.collectionId,
                },
                headers: {
                    Authorization: header
                }
            })
                .then(response => {
                    console.log('Success:', response.data);
                })
                .catch(error => {
                    console.error('Error:', error);
                });
            window.location.reload();
        } else {
            console.error('No collection selected');
        }
    };

    const [popup, setPop] = React.useState(false)
    const handleClickOpen = () => {
        setPop(!popup)
    }
    const closePopup = () => {
        setPop(false)
    }


    const collection = items && items.collection ? items.collection : '';
    const community = items && items.collection ? items.collection.communityDTOForSelectOfUser : '';
    const metadataTitle = items && items.listMetadataValueDTOForSelect ? items.listMetadataValueDTOForSelect.find((x) => x.metadataFieldId === 57).textValue : '';
    return (
        <div>
            <Header />
            <Helmet>
                <meta charSet="utf-8" />
                <title>Modify Item - {`${metadataTitle}`} - dSPACE</title>
            </Helmet>
            <div>
                {
                    popup ?
                        <div className="main">
                            <div className="popup">
                                <div className="popup-header mt-2 ms-2">
                                    <Typography
                                        variant="h5"
                                        component="h5"
                                        sx={{ textAlign: 'left', mt: 2, mb: 2 }}>
                                        Choose collection...
                                    </Typography>
                                    <Button className='ms-10' onClick={closePopup} startIcon={<Close />} >
                                    </Button>
                                </div>
                                <div className='mt-2 ms-4 me-4'>Select the collection you want to save the item to.</div>
                                <div className='d-flex justify-content-between mt-4 ms-4 me-4'>
                                    <div className='w-75'>
                                        <Autocomplete
                                            freeSolo
                                            id="free-solo-2-demo"
                                            disableClearable
                                            options={collections.map((option) => option.collectionName)}
                                            onInputChange={(event, newValue) => {
                                                const selected = collections.find(collect => collect.collectionName === newValue);
                                                setSelectedValue(selected);
                                            }}
                                            renderInput={(params) => (
                                                <TextField
                                                    {...params}
                                                    label="Collection..."
                                                    InputProps={{
                                                        ...params.InputProps,
                                                        type: 'search',
                                                    }}
                                                />
                                            )}
                                        />
                                    </div>
                                    <Button variant="outlined" className='ms-10' onClick={handleSaveCollection} startIcon={<Save />} >
                                        Save
                                    </Button>
                                </div>
                            </div>
                        </div> : ""
                }
            </div>
            <Container>
                <Typography
                    variant="h4"
                    component="h4"
                    sx={{ textAlign: 'left', mt: 3 }}>
                    Modify Item: {metadataTitle}
                </Typography>
                <hr
                    style={{
                        color: 'black',
                        backgroundColor: 'black',
                        height: 1
                    }}
                />
                <Tabs defaultActiveKey="status">
                    <Tab eventKey="status" title="Status">
                        <div className='mt-1'>Welcome to the item management page. From here you can move or delete the item. You may also update or add new metadata on the other tabs.</div>
                        <Table className='mt-3'>
                            <tbody>
                                <tr>
                                    <td style={{ border: "none" }}>Item Internal ID</td>
                                    <td style={{ border: "none" }}>{items.itemId}</td>
                                </tr>
                                <tr>
                                    <td style={{ border: "none" }}>Last Modified:</td>
                                    <td style={{ border: "none" }}>{ConvertDateTime(items.lastModified)}</td>
                                </tr>
                                <tr>
                                    <td style={{ border: "none" }}>Item Page:</td>
                                    <td style={{ border: "none" }}><PageLink href={window.location.origin + `/DSpace/ItemDetail/` + items.itemId} text={`/DSpace/ItemDetail/` + items.itemId} /></td>
                                </tr>
                                <tr><td style={{ border: "none" }}></td></tr>
                                <tr>
                                    <td style={{ border: "none" }}>Make item non-discoverable</td>

                                    {popup ?
                                        <div style={{ padding: '14px' }}>Hidden button</div> : (items.discoverable ? (
                                            <td style={{ border: "none" }}><Button variant="outlined" onClick={handleNonDiscoverableClick}>Make it non-discoverable...</Button></td>
                                        ) : (
                                            <td style={{ border: "none" }}><Button variant="outlined" onClick={handleDiscoverableClick}>Make it discoverable...</Button></td>
                                        ))
                                    }
                                </tr>
                                <tr>
                                    <td style={{ border: "none" }}>Move item to another collection</td>
                                    <td style={{ border: "none" }}>
                                        {popup ?
                                            <div style={{ padding: '6.5px' }}>Hidden button</div>
                                            : <Button variant="outlined" onClick={handleClickOpen}>Move this Item to a different Collection</Button>}
                                    </td>
                                </tr>
                                <tr>
                                    <td style={{ border: "none" }}>Completely expunge item</td>
                                    <td style={{ border: "none" }}>
                                        {popup ?
                                            <div style={{ padding: '6.5px' }}>Hidden button</div>
                                            : <Button variant="outlined" onClick={handleDeleteItem}>Pernamently delete</Button>}
                                    </td>
                                </tr>
                            </tbody>
                        </Table>

                        <Button variant="outlined" className="text-black mt-2 border-secondary mb-5" onClick={handleRedirectToDetail} startIcon={<ArrowBackIcon />} >
                            Back
                        </Button>
                    </Tab>
                    <Tab eventKey="metadata" title="Metadata">
                        <Box
                            sx={{
                                height: '100%',
                                width: '100%',
                                marginTop: 2,
                                '& .actions': {
                                    color: 'text.secondary',
                                },
                                '& .textPrimary': {
                                    color: 'text.primary',
                                },
                            }}
                        >
                            <DataGrid
                                rows={modifyMetadata}
                                columns={columns}
                                editMode="row"
                                rowModesModel={rowModesModel}
                                onRowModesModelChange={handleRowModesModelChange}
                                onRowEditStop={handleRowEditStop}
                                processRowUpdate={processRowUpdate}
                                getRowId={(row) => row.metadataValueId}
                                slots={{
                                    toolbar: EditToolbar,
                                }}
                                slotProps={{
                                    toolbar: { setModifyFields, setRowModesModel, setModifyStatus },
                                }}
                                columnVisibilityModel={{
                                    metadataFieldId: false
                                }}
                                initialState={{
                                    pagination: {
                                        paginationModel: { pageSize: 10, page: 0 },
                                    },
                                }}
                                pageSizeOptions={[10, 15, 25]}
                            />
                        </Box>
                        <div className='d-flex flex-column align-items-end'>
                            <div>
                                <Button variant="secondary" startIcon={<Save />} className="ms-1 mt-2 bg-secondary text-white" onClick={handleSubmit}>
                                    Save
                                </Button>
                                <Button className="ms-2 mt-2 bg-danger bg-gradient text-white" startIcon={<Close />} >
                                    Discard
                                </Button>
                            </div>
                            <Button variant="outlined" className="text-black mt-2 border-secondary mb-5" onClick={handleRedirectToDetail} startIcon={<ArrowBackIcon />} >
                                Back
                            </Button>
                        </div>

                    </Tab>
                    <Tab eventKey="relationships" title="Relationships">
                        <div>
                            <Typography
                                variant="h5"
                                component="h5"
                                sx={{ textAlign: 'left', mt: 3, mb: 2 }}>
                                Authors (persons)
                            </Typography>
                            {items !== null &&
                                items.listAuthorDtoForSelect !== null ? (
                                <p>No relationships</p>
                            ) : (
                                <ul>
                                    {items.listAuthorDtoForSelect.map((author, index) => (
                                        <li key={index}>{author.name}</li>
                                    ))}
                                </ul>
                            )
                            }
                        </div>
                        <div>
                            <Typography
                                variant="h5"
                                component="h5"
                                sx={{ textAlign: 'left', mt: 3, mb: 2 }}>
                                Related Collection
                            </Typography>
                            <Card sx={{ display: 'flex', alignItems: 'center', padding: 2, width: 400 }}>
                                <img src="https://cdn1.polaris.com/globalassets/pga/accessories/my20-orv-images/no_image_available6.jpg" alt="Description of image" style={{ width: '180px', height: 'auto' }} />
                                <CardContent sx={{ display: 'flex', flexDirection: 'column' }}>
                                    <Chip label="Collection" color="primary" sx={{ marginBottom: 1 }} />
                                    <Typography variant="h6" color="primary" style={{ wordWrap: "break-word" }}>
                                        {collection.collectionName}
                                    </Typography>
                                    <Typography variant="body2" color="textSecondary">
                                        {collection.shortDescription}
                                    </Typography>
                                </CardContent>
                            </Card>
                        </div>
                        <div>
                            <Typography
                                variant="h5"
                                component="h5"
                                sx={{ textAlign: 'left', mt: 3, mb: 2 }}>
                                Related Community
                            </Typography>
                            <Card sx={{ display: 'flex', alignItems: 'center', padding: 2, width: 400 }}>
                                <img src="https://cdn1.polaris.com/globalassets/pga/accessories/my20-orv-images/no_image_available6.jpg" alt="Description of image" style={{ width: '180px', height: 'auto' }} />
                                <CardContent sx={{ display: 'flex', flexDirection: 'column' }}>
                                    <Chip label="Community" color="primary" sx={{ marginBottom: 1 }} />
                                    <Typography variant="h6" color="primary" style={{ wordWrap: "break-word" }}>
                                        {community.communityName}
                                    </Typography>
                                    <Typography variant="body2" color="textSecondary">
                                        {community.shortDescription}
                                    </Typography>
                                </CardContent>
                            </Card>
                        </div>


                    </Tab>
                </Tabs>
            </Container>
        </div>

    );
}