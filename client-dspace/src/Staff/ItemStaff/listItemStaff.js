import React, { useEffect, useState } from 'react';
import { Link, useNavigate, useParams, useLocation } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from '../../components/Header/Header-admin';
import axios from 'axios';
import '../../Admin/Items/CollectionList.css'; // Import file CSS
import ManagementMenu from '../../components/Nav-bar/nav-bar-items';
import Select, { components } from 'react-select';
import { styled } from '@material-ui/core';
import { FormatListBulleted, GridOn, Search } from '@material-ui/icons';
import TextField from '@mui/material/TextField';
import InputAdornment from '@mui/material/InputAdornment';
import Typography from '@mui/material/Typography';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';
import {
    ExpandLess, ExpandMore, FindInPage
} from '@material-ui/icons';
import { Helmet } from 'react-helmet';
import DangerousIcon from '@mui/icons-material/Dangerous';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import Tooltip from '@mui/material/Tooltip';
import IconButton from '@mui/material/IconButton';
import { jwtDecode } from 'jwt-decode';

const CollectionList = () => {
    const location = useLocation();
    const searchParams = new URLSearchParams(location.search);
    const search = searchParams.get('search');
    const [page, setPage] = React.useState(1);
    const { collectionId } = useParams();
    const [selectedValue, setSelectedValue] = useState(collectionId);
    const [items, setItems] = useState([]);
    const [objItemsPerPage, setobjItemsPerPage] = useState([]);
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(false);
    const [searchQuery, setSearchQuery] = useState('');
    const [searchQuery2, setSearchQuery2] = useState('');
    const [discoverable, setDiscoverable] = useState(null);

    const handleChangePage = (event, value) => {
        setPage(value);
    };
    useEffect(() => {
        if (search) {
            setSearchQuery(search);
        }
    }, [search]);
    const navigate = useNavigate();

    const [startDate, setStartDate] = useState(null);
    const [endDate, setEndDate] = useState(null);
    const [numPerPage, setNumPerPage] = useState(5);
    const [sortState, setSortState] = useState(1);
    const [selectedAuthors, setSelectedAuthors] = useState([]);
    const [selectedKeywords, setSelectedKeywords] = useState([]);
    const [selectedTypes, setSelectedTypes] = useState([]);
    const [activeButton, setActiveButton] = useState('list'); // Default active button is 'list'

    const handleButtonClick = (buttonType) => {
        setActiveButton(buttonType);
    };
    const [collections, setCollections] = useState([]);
    const token = localStorage.getItem('Token');
    const userRole = token ? jwtDecode(token)["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"] : null;
    const header = `Bearer ${token}`;
    const [searchByTitleCheck, setSearchByTitleCheck] = useState(false);

    const [expandedAbstracts, setExpandedAbstracts] = useState({});
    const toggleAbstract = (itemId) => {
        setExpandedAbstracts(prevState => ({
            ...prevState,
            [itemId]: !prevState[itemId]
        }));
    };
    useEffect(() => {
        setLoading(true)
        axios.get(`${process.env.REACT_APP_BASE_URL}/api/Collection/getListOfCollections`, {
            headers: {
                Authorization: header
            }
        })
            .then(({ data }) => {
                setCollections(data);
            })
            .catch(error => {
                setError("You are not assigned any collection");
            }).finally(() => {
                setLoading(false);
            });
    }, []);

    const normalizeText = (text) => {
        // Chuyển đổi tất cả các ký tự thành chữ thường
        text = text.toLowerCase();
        text = text.replace(/\s/g, '');
        // Loại bỏ dấu câu và dấu cách
        // text = text.replace(/[\s\p{P}]/gu, '');

        // // Loại bỏ dấu tiếng Việt
        // text = text.replace(/[áàảãạăắằẳẵặâấầẩẫậ]/g, 'a');
        // text = text.replace(/[đ]/g, 'd');
        // text = text.replace(/[éèẻẽẹêếềểễệ]/g, 'e');
        // text = text.replace(/[íìỉĩị]/g, 'i');
        // text = text.replace(/[óòỏõọôốồổỗộơớờởỡợ]/g, 'o');
        // text = text.replace(/[úùủũụưứừửữự]/g, 'u');
        // text = text.replace(/[ýỳỷỹỵ]/g, 'y');

        // // Loại bỏ các dấu thanh và âm cuối
        // text = text.normalize('NFD').replace(/[\u0300-\u036f]/g, '');

        return text;
    };
    const handleSearchSubmit = (searchTitle) => {
        setLoading(true)
        let payload = null;
        if (searchTitle !== null) {
            if (searchQuery === '') {
                payload = {
                    collectionId: collectionId,
                    title: normalizeText(searchTitle.trim()),
                    startDateIssue: startDate ? startDate : null,
                    endDateIssue: endDate ? endDate : null,
                    publisher: normalizeText(searchQuery2.trim()), // Lấy từ trạng thái hoặc đầu vào người dùng
                    authors: selectedAuthors, // Mảng các tác giả đã chọn
                    keywords: selectedKeywords, // Mảng các từ khóa đã chọn
                    types: selectedTypes, // Mảng các loại đã chọn, nếu có,
                    sortType: sortState - 1,
                    discoverable: discoverable
                }
            }
            else {
                payload = {
                    collectionId: collectionId,
                    title: normalizeText(searchQuery.trim()),
                    startDateIssue: startDate ? startDate : null,
                    endDateIssue: endDate ? endDate : null,
                    publisher: normalizeText(searchQuery2.trim()), // Lấy từ trạng thái hoặc đầu vào người dùng
                    authors: selectedAuthors, // Mảng các tác giả đã chọn
                    keywords: selectedKeywords, // Mảng các từ khóa đã chọn
                    types: selectedTypes, // Mảng các loại đã chọn, nếu có,
                    sortType: sortState - 1,
                    discoverable: discoverable
                }
            }
        } else {
            payload = {
                collectionId: collectionId,
                title: normalizeText(searchQuery.trim()),
                startDateIssue: startDate ? startDate : null,
                endDateIssue: endDate ? endDate : null,
                publisher: normalizeText(searchQuery2.trim()), // Lấy từ trạng thái hoặc đầu vào người dùng
                authors: selectedAuthors, // Mảng các tác giả đã chọn
                keywords: selectedKeywords, // Mảng các từ khóa đã chọn
                types: selectedTypes, // Mảng các loại đã chọn, nếu có,
                sortType: sortState - 1,
                discoverable: discoverable
            };
        }
        console.log(payload);

        axios.post(`${process.env.REACT_APP_BASE_URL}/api/ItemStaff/SearchItem/${page}/${numPerPage}`, payload, {
            headers: {
                Authorization: header
            }
        })
            .then(({ data }) => {
                setItems(data);
                setobjItemsPerPage(data);
            })
            .catch(error => {
                setError(error.message);
            }).finally(() => {
                setLoading(false);
            });
    };
    useEffect(() => {
        if (collectionId < 0) { setobjItemsPerPage([]) }
        else {
            if (search) {
                if (searchByTitleCheck === false) {
                    setSearchByTitleCheck(true)
                    handleSearchSubmit(search)
                } else {
                    handleSearchSubmit('')
                }
            } else {
                handleSearchSubmit('')
            }
        }
    }, [collectionId, numPerPage, page, sortState, search]);

    function LoadingOverlay() {
        return (
            <div className="loading-overlay">
                <div className="spinner-border text-light" role="status">
                    <span className="sr-only"></span>
                </div>
            </div>
        );
    }

    const handleSearchChange = (e) => {
        setSearchQuery(e.target.value);
    };
    const handleRowClick = (itemId) => {
        navigate(`/DSpace/ItemDetail/${itemId}`);
    };
    const { ValueContainer, Placeholder } = components;

    const CustomValueContainer = ({ children, ...props }) => {
        return (
            <ValueContainer {...props}>
                <Placeholder {...props} isFocused={props.isFocused}>
                    {props.selectProps.placeholder}
                </Placeholder>
                {React.Children.map(children, child =>
                    child && child.key !== 'placeholder' ? child : null,
                )}
            </ValueContainer>
        );
    };
    const CollectionSelect = ({ collections, selectedValue, handleChange }) => {
        const [focused, setFocused] = useState(false);

        const options = [
            { value: '0', label: 'All items' },
            ...collections.map(collection => ({
                value: collection.collectionId.toString(),
                label: collection.collectionName
            }))
        ];

        return (
            <Select
                options={options}
                value={options.find(option => option.value === selectedValue)}
                onChange={selectedOption => handleChange(selectedOption ? selectedOption.value : '-1')}
                placeholder="Choose collections"
                isClearable
                components={{
                    ValueContainer: CustomValueContainer
                }}
                maxMenuHeight={300}
                onFocus={() => setFocused(true)}
                onBlur={() => setFocused(false)}
                isFocused={focused}
                theme={(theme) => ({
                    ...theme,
                    spacing: {
                        ...theme.spacing,
                        baseUnit: 2,
                        controlHeight: 56,
                        menuGutter: 8,
                    },
                })}
                styles={{
                    container: (provided) => ({
                        ...provided,
                        width: '160px',
                        // marginTop: 5
                    }),
                    control: (base) => ({
                        ...base,
                        border: '1px solid black',
                        borderRadius: '4px 0 0 4px',
                    }),
                    valueContainer: (provided) => ({
                        ...provided,
                        overflow: "visible",

                    }),
                    placeholder: (base, state) => ({
                        ...base,
                        position: 'absolute',
                        top: (state.hasValue || state.selectProps.inputValue || state.selectProps.isFocused) ? '-120%' : '0%',
                        transition: 'top 0.2s, font-size 0.2s',
                        fontSize: (state.hasValue || state.selectProps.inputValue || state.selectProps.isFocused) && 14,

                    }),
                    indicatorSeparator: () => ({
                        display: 'none',
                    }),
                    dropdownIndicator: (base) => ({
                        ...base,
                        display: 'none',
                    }),
                }}
            />
        );
    };

    const handleChange = (e) => {
        setSelectedValue(e)
        navigate(`/Dspace/Collection/ItemStaffList/${e}`);
    };

    const formatDate = (dateString) => {
        if (!dateString) return '';

        const parts = dateString.split('-');
        let formattedDate = '';

        if (parts.length === 3) {
            // Case: full date (YYYY-MM-DD)
            const [year, month, day] = parts;
            formattedDate = `${day.padStart(2, '0')}-${month.padStart(2, '0')}-${year}`;
        } else if (parts.length === 2) {
            // Case: year and month (YYYY-MM)
            const [year, month] = parts;
            formattedDate = `${month.padStart(2, '0')}-${year}`;
        } else if (parts.length === 1) {
            // Case: only year (YYYY)
            const [year] = parts;
            formattedDate = year;
        }

        return formattedDate;
    };

    // if (loading) {
    //     return (
    //         <div>
    //             <Header />
    //             <div className="container mt-5">
    //                 <h1 className="mb-4">Loading...</h1>
    //             </div>
    //         </div>
    //     );
    // }

    function Status({ status }) {
        const getStatusText = (status) => {
            if (status == true) {
                return "Published";
            } else {
                return "Undiscoverable";
            }
        };

        const getStatusColor = (status) => {
            if (status == true) {
                return "#28A745";
            } else {
                return "#DC3545";
            }
        };
        const getBoxShadow = (status) => {
            const color = getStatusColor(status);
            return `0 0 10px 2px ${color}`;
        };
        return (
            <p style={{ margin: '0' }}>
                <span style={{
                    margin: '0',
                    backgroundColor: getStatusColor(status),
                    borderRadius: '3px',
                    color: 'white',
                    padding: '2px',
                    fontSize: 'small',
                    boxShadow: getBoxShadow(status)

                }}>
                    {getStatusText(status)}
                </span>
            </p>
        );
    }
    return (
        <div>
            <Header />
            <Helmet>
                <meta charSet="utf-8" />
                <title>Item Management - dSPACE</title>
            </Helmet>
            <div className="main-content">
                <div className="row">
                    <div className="col-md-3 mt-5">

                        <div className="d-flex mb-4" style={{ marginLeft: '35%' }}>

                            <div
                                className="btn"
                                style={{
                                    backgroundColor: activeButton === 'list' ? '#9e9e9e' : '#757575',
                                    borderRadius: '4px 0 0 4px',
                                }}
                                onClick={() => handleButtonClick('list')}
                            >
                                <FormatListBulleted style={{ color: '#FFFFFF' }} />
                            </div>
                            <div
                                className="btn"
                                style={{
                                    backgroundColor: activeButton === 'grid' ? '#9e9e9e' : '#757575',
                                    borderRadius: '0 4px 4px 0',
                                }}
                                onClick={() => handleButtonClick('grid')}
                            >
                                <GridOn style={{ color: '#FFFFFF' }} />
                            </div>
                        </div>
                        {/* <h2 style={{ marginLeft: '35%', color: '#757575', marginBottom: '' }}>Filters</h2> */}
                        <div style={{ marginLeft: '30%' }}>
                            <ManagementMenu
                                startDate={startDate}
                                endDate={endDate}
                                setStartDate={setStartDate}
                                setEndDate={setEndDate}
                                numPerPage={numPerPage}
                                setNumPerPage={setNumPerPage}
                                sortState={sortState}
                                setSortState={setSortState}
                                selectedAuthors={selectedAuthors}
                                setSelectedAuthors={setSelectedAuthors}
                                selectedKeywords={selectedKeywords}
                                setSelectedKeywords={setSelectedKeywords}
                                selectedTypes={selectedTypes}
                                setSelectedTypes={setSelectedTypes}
                                searchQuery={searchQuery2}
                                setSearchQuery={setSearchQuery2}
                                filterSubmit={() => handleSearchSubmit('')}
                                discoverable={discoverable}
                                setDiscoverable={setDiscoverable}
                            />
                        </div>


                    </div>
                    <div className="container mt-5 col-md-9">
                        <h1 className='mb-4' style={{}}>List items</h1>
                        <div className="d-flex justify-content-between mb-4">

                            <div className="d-flex ">

                                <div className="me-2 d-flex">
                                    <div>
                                        <CollectionSelect
                                            collections={collections}
                                            selectedValue={selectedValue}
                                            handleChange={handleChange}
                                        />
                                    </div>
                                    <div>
                                        <TextField
                                            // className='mh-50'
                                            id="outlined-basic"
                                            label="Search By Title"
                                            variant="outlined"
                                            value={searchQuery}
                                            onChange={handleSearchChange}
                                            sx={{ "& .MuiOutlinedInput-root": { width: '35ch' }, "& fieldset": { borderRadius: 0, width: '35ch', } }}

                                        /></div>

                                    <button onClick={() => handleSearchSubmit('')} style={{ backgroundColor: '#43515F', color: 'white', borderRadius: '0 6px 6px 0' }} className="btn">
                                        <Search />
                                    </button>
                                </div>
                            </div>



                            <button onClick={() => navigate(`/Dspace/AdvanceSearch`)} style={{ backgroundColor: '#43515F', color: 'white', borderRadius: '6px' }} className="btn">
                                <FindInPage />
                                Advanced search
                            </button>
                            <div style={{ marginRight: '4%' }}>
                                <Link to={`/DSpace/CreateItem/${collectionId}`} className="btn btn-primary" style={{ backgroundColor: '#207698' }}>
                                    Create new item
                                </Link>
                            </div>

                        </div>

                        {error && <div className="alert alert-danger">{error}</div>}
                        <h2>Search result</h2>

                        {activeButton === 'list' && (
                            (objItemsPerPage.objmap?.length > 0) ? (
                                <div className='col-md-11'>
                                    <h6>Now showing {(page - 1) * numPerPage + 1} - {objItemsPerPage.objmap?.length + (page - 1) * numPerPage} of  {objItemsPerPage.totalAmount}</h6>
                                    {objItemsPerPage.objmap
                                        .map((item, index) => (
                                            <div key={index} style={{ display: 'flex', alignItems: 'flex-start', marginBottom: '20px' }}>
                                                <img
                                                    src={item.imageURL || '/The_logo_of_Little_Black_Book.png'} // Sử dụng URL ảnh từ item hoặc ảnh placeholder
                                                    alt={item.title}
                                                    style={{ width: '150px', height: '200px', objectFit: 'cover', marginRight: '20px' }}
                                                />
                                                <div>
                                                    <div className='d-flex' style={{ alignItems: 'center' }}>
                                                        {(userRole === "STAFF" || userRole === "ADMIN") && (
                                                            <Status status={item.discoverable} style={{ display: 'flex', alignItems: 'center' }} />
                                                        )}
                                                        {(userRole === "STAFF" && item.canEdit) && (
                                                            <Tooltip color='success' placement="top" title="You are assigned to this item">
                                                                <IconButton style={{ display: 'flex', alignItems: 'center' }}>
                                                                    <CheckCircleIcon />
                                                                </IconButton>
                                                            </Tooltip>
                                                        )}
                                                        {(userRole === "STAFF" && !item.canEdit) && (
                                                            <Tooltip placement="top" title="You are not assigned to this item">
                                                                <IconButton color='error' style={{ display: 'flex', alignItems: 'center' }}>
                                                                    <DangerousIcon />
                                                                </IconButton>
                                                            </Tooltip>
                                                        )}
                                                    </div>


                                                    <h3 style={{ margin: '0' }}>
                                                        <span className="author-item" onClick={() => handleRowClick(item.itemId)}>{item.title}</span>
                                                    </h3>
                                                    <p style={{ margin: '0' }}>{item.dateOfIssue && (<>({formatDate(item.dateOfIssue)})</>)} {item.publisher && (<> - {item.publisher}</>)}</p>
                                                    {/* <p className='d-flex' style={{margin: '0'}}><p style={{margin: '0', color: '#4b9cdb', fontWeight: 'bolder', marginRight: '10px'}}>Publisher: </p>{item.publisher}</p> */}
                                                    <p className='d-flex' style={{ margin: '0' }}><p style={{ margin: '0', color: '#207f9e', fontWeight: 'bolder', marginRight: '10px' }}>Contributors: </p>{item.authorItems
                                                        .filter((value, index, self) =>
                                                            index === self.findIndex((t) => t.authorId === value.authorId)
                                                        )
                                                        .map(authorItem => authorItem.fullName)
                                                        .join(', ')
                                                    }</p>
                                                    {/* <p className='d-flex' style={{margin: '0'}}><p style={{margin: '0', color: '#4b9cdb', fontWeight: 'bolder', marginRight: '10px'}}>Abstract: </p></p> */}
                                                    <div>
                                                        {expandedAbstracts[item.itemId] ? (
                                                            <div>
                                                                <div>{item.abstract}</div>
                                                                <button onClick={() => toggleAbstract(item.itemId)} style={{ background: 'none', color: '#207f9e', border: 'none', padding: '0', cursor: 'pointer', fontWeight: 'bolder' }}>
                                                                    <ExpandLess /> Collapse
                                                                </button>
                                                            </div>
                                                        ) : (
                                                            <div>
                                                                <div style={{ overflow: 'hidden', textOverflow: 'ellipsis', display: '-webkit-box', WebkitLineClamp: 3, WebkitBoxOrient: 'vertical' }}>
                                                                    {item.abstract}
                                                                </div>
                                                                {item.abstract.split(' ').length > 20 && (
                                                                    <button onClick={() => toggleAbstract(item.itemId)} style={{ background: 'none', color: '#207f9e', border: 'none', padding: '0', cursor: 'pointer', fontWeight: 'bolder' }}>
                                                                        <ExpandMore />  Show more
                                                                    </button>
                                                                )}
                                                            </div>
                                                        )}
                                                    </div>
                                                </div>
                                            </div>
                                        ))}
                                </div>
                            ) : <h6>Not found</h6>)}

                        {activeButton === 'grid' && ((objItemsPerPage.objmap?.length > 0) ? (
                            <div className='col-md-11'>
                                <div className='row'>
                                    <h6>Now showing {(page - 1) * numPerPage + 1} - {objItemsPerPage.objmap?.length + (page - 1) * numPerPage} of  {objItemsPerPage.totalAmount}</h6>
                                    {objItemsPerPage.objmap
                                        .map((item, index) => (
                                            <div key={index} className="col-md-4 grid-item" style={{ marginBottom: '20px', transition: 'transform 0.2s', }}>
                                                <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                                    <img
                                                        src={item.imageURL || '/The_logo_of_Little_Black_Book.png'}
                                                        alt={item.title}
                                                        style={{ width: '100%', height: '200px', objectFit: 'cover', marginBottom: '10px' }}
                                                    />
                                                    <Status status={item.discoverable} />
                                                    <h3 style={{ textAlign: 'center' }} className="author-item" onClick={() => handleRowClick(item.itemId)}>{item.title}</h3>
                                                    <p style={{ textAlign: 'center' }}>{item.dateOfIssue && (<>({formatDate(item.dateOfIssue)})</>)} {item.publisher && (<> - {item.publisher}</>)}</p>
                                                    <p style={{ textAlign: 'center' }}>
                                                        <span style={{ color: '#207f9e', fontWeight: 'bolder' }}>Contributors: </span>
                                                        {item.authorItems
                                                            .filter((value, index, self) =>
                                                                index === self.findIndex((t) => t.authorId === value.authorId)
                                                            )
                                                            .map(authorItem => authorItem.fullName)
                                                            .join(', ')
                                                        }
                                                    </p>
                                                    <div style={{}}>
                                                        {expandedAbstracts[item.itemId] ? (
                                                            <div>
                                                                <div>{item.abstract}</div>
                                                                <div>
                                                                    <button onClick={() => toggleAbstract(item.itemId)} style={{ background: 'none', color: '#207f9e', border: 'none', padding: '0', cursor: 'pointer', fontWeight: 'bolder' }}>
                                                                        <ExpandLess /> Collapse
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        ) : (
                                                            <div>
                                                                <div style={{ overflow: 'hidden', textOverflow: 'ellipsis', display: '-webkit-box', WebkitLineClamp: 3, WebkitBoxOrient: 'vertical' }}>
                                                                    {item.abstract}
                                                                </div>
                                                                {item.abstract.split(' ').length > 20 && (
                                                                    <div>
                                                                        <button onClick={() => toggleAbstract(item.itemId)} style={{ background: 'none', color: '#207f9e', border: 'none', padding: '0', cursor: 'pointer', fontWeight: 'bolder' }}>
                                                                            <ExpandMore /> Show more
                                                                        </button>
                                                                    </div>
                                                                )}
                                                            </div>
                                                        )}
                                                    </div>
                                                </div>
                                            </div>
                                        ))}
                                </div>
                            </div>
                        ) : <h6>Not found</h6>)}

                        <Stack spacing={2} alignItems="center">
                            {/* <Typography>Page: {page}</Typography> */}
                            <Pagination count={objItemsPerPage.totalPage} page={page} onChange={handleChangePage} size="large" />
                        </Stack>
                    </div>
                </div>
            </div>
            {loading && <LoadingOverlay />}
        </div>
    );
};
export default CollectionList;
