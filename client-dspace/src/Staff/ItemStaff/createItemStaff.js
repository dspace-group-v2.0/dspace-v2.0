import React, { useEffect, useState } from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link, useParams, useNavigate } from 'react-router-dom';
import Header from '../../components/Header/Header';
import Select from 'react-select';
import { TagsInput } from "react-tag-input-component";
import '../../Admin/Items/CreateItem.css';
import { TextareaAutosize } from '@mui/base/TextareaAutosize';
import MetadataField from '../../components/metadataFields'
import { Modal } from 'react-bootstrap';
import { components } from 'react-select';

import { AddRounded, DeleteRounded } from '@material-ui/icons';

import {
    Autocomplete,
    TextField,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogContentText,
    DialogActions,
    Button,
} from '@mui/material';
import { createFilterOptions } from '@mui/material/Autocomplete';
import { Helmet } from 'react-helmet';

const CreateCommunity = () => {
    const [identifierFields, setIdentifierFields] = useState([{ type: "", value: "" }]);
    const { collectionId } = useParams();
    const [selectedValue, setSelectedValue] = useState(collectionId);
    const navigate = useNavigate();
    const token = localStorage.getItem('Token');
    const header = `Bearer ${token}`;
    const [type, setType] = useState([]);
    const [selectedValueType, setSelectedValueType] = useState('');
    const [authors, setAuthors] = useState([]);
    const [selectedAuthors, setSelectedAuthors] = useState([]);
    const metadataValueDTO = [];
    const [showModal, setShowModal] = useState(false);
    const [createdItemId, setCreatedItemId] = useState(null);
    const [loading, setLoading] = useState(true);
    function LoadingOverlay() {
        return (
            <div className="loading-overlay">
                <div className="spinner-border text-light" role="status">
                    <span className="sr-only"></span>
                </div>
            </div>
        );
    }
    const [collections, setCollections] = useState([]);

    useEffect(() => {
        // Lấy dữ liệu từ API
        axios.get(`${process.env.REACT_APP_BASE_URL}/api/CollectionStaff/GetAllCollections`, {
            headers: {
                Authorization: header
            }
        })
            .then(({ data }) => {
                setCollections(data);
                setLoading(false);
            })
            .catch(error => {
                setLoading(false);
            });
    }, []);
    const createSimpleItem = async ({ data, multipleFileUploads }) => {
        setLoading(true); // Bắt đầu loading

        const metadataValueDTO = [];

        // Extract data fields
        const { authors, advisors1, title, otherTitle, dateOfIssue, publisher, citation, seriesNo, identifierFields, selectedTypes, subjectKeywords, abstract, sponsors, description, selectedLanguage, advisorsMore, authorsMore, additionalFields } = data;

        // Add authors
        authors.forEach(author => {
            metadataValueDTO.push({
                textValue: author.toString(),
                textLang: '',
                metadataFieldId: 1, // author
            });
        });
        authorsMore.forEach(author => {
            metadataValueDTO.push({
                textValue: window.location.origin + "_" + author.fullName + "_" + author.jobTitle,
                textLang: '',
                metadataFieldId: 1, // author
            });
        });

        // Add advisors
        advisors1.forEach(advisor => {
            metadataValueDTO.push({
                textValue: advisor.toString(),
                textLang: '',
                metadataFieldId: 2, // advisor
            });
        });
        advisorsMore.forEach(advisor => {
            metadataValueDTO.push({
                textValue: window.location.origin + "_" + advisor.fullName + "_" + advisor.jobTitle,
                textLang: '',
                metadataFieldId: 2, // author
            });
        });

        // Add title
        metadataValueDTO.push({
            textValue: title,
            textLang: '',
            metadataFieldId: 57, // title
        });

        // Add other title
        otherTitle.forEach(otherTitle => {
            metadataValueDTO.push({
                textValue: otherTitle,
                textLang: '',
                metadataFieldId: 58, // author
            });
        });

        if (dateOfIssue != "") {
            metadataValueDTO.push({
                textValue: dateOfIssue,
                textLang: '',
                metadataFieldId: 12, // date of issue
            });
        }

        if (publisher.trim() != "") {
            metadataValueDTO.push({
                textValue: publisher,
                textLang: '',
                metadataFieldId: 36, // date of issue
            });
        }
        if (citation.trim() != "") {
            metadataValueDTO.push({
                textValue: citation,
                textLang: '',
                metadataFieldId: 26, // date of issue
            });
        }

        seriesNo.forEach(seriesNo => {
            metadataValueDTO.push({
                textValue: seriesNo,
                textLang: '',
                metadataFieldId: 39, // author
            });
        });

        identifierFields.forEach(identify => {
            if (identify.value.trim() != "" && identify.type != "") {
                metadataValueDTO.push({
                    textValue: identify.value,
                    textLang: '',
                    metadataFieldId: parseInt(identify.type, 10), // author
                });
            }
        });

        selectedTypes.forEach(type => {
            metadataValueDTO.push({
                textValue: type,
                textLang: '',
                metadataFieldId: 59, // author
            });
        });

        subjectKeywords.forEach(keyword => {
            metadataValueDTO.push({
                textValue: keyword,
                textLang: '',
                metadataFieldId: 51, // author
            });
        });

        if (abstract.trim() != "") {
            metadataValueDTO.push({
                textValue: abstract,
                textLang: '',
                metadataFieldId: 15, // date of issue
            });
        }

        if (sponsors.trim() != "") {
            metadataValueDTO.push({
                textValue: sponsors,
                textLang: '',
                metadataFieldId: 17, // date of issue
            });
        }
        if (description.trim() != "") {
            metadataValueDTO.push({
                textValue: description,
                textLang: '',
                metadataFieldId: 14, // date of issue
            });
        }

        if (selectedLanguage != "" && selectedLanguage != null) {
            metadataValueDTO.push({
                textValue: selectedLanguage,
                textLang: '',
                metadataFieldId: 35, // date of issue
            });
        }
        additionalFields.forEach(field => {
            if (field.value.trim() != "" && field.metadataFieldId != "") {
                metadataValueDTO.push({
                    textValue: field.value,
                    textLang: '',
                    metadataFieldId: parseInt(field.metadataFieldId), // convert string to int
                });
            }
        });

        const formData = new FormData();
        formData.append('Discoverable', true);
        formData.append('CollectionId', collectionId);
        metadataValueDTO.forEach(item => {
            formData.append('metadataValueDTO', JSON.stringify(item));
        });
        multipleFileUploads.forEach(file => {
            formData.append('multipleFiles', file);
        });
        console.log(metadataValueDTO)
        try {
            const response = await axios.post(`${process.env.REACT_APP_BASE_URL}/api/Item/CreateSimpleItem`, formData, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    Authorization: header
                },
            });
            console.log(response.data);
            setCreatedItemId(response.data.objectResponse.itemId);
            setShowModal(true); // Hiện modal

            // setSuccessMessage('Item created successfully!');
        } catch (error) {
            console.error('There was an error creating the item!', error);
        } finally {
            setLoading(false); // Kết thúc loading
        }
    };
    const handleCreateMore = () => {
        setShowModal(false);
        // Reset form hoặc các trạng thái khác nếu cần thiết
    };

    const handleViewItem = () => {
        // Chuyển sang trang khác với id của item vừa tạo
        window.location.href = `/DSpace/ItemDetail/${createdItemId}`;
    };
    useEffect(() => {
        axios.get(`${process.env.REACT_APP_BASE_URL}/api/Author/getListOfAuthors`)
            .then(response => {
                setAuthors(response.data);
            })
            .catch(error => {
                console.error('There was an error fetching the authors!', error);
            });
    }, []);


    const filter = createFilterOptions();
    const [selectedAdvisors, setSelectedAdvisors] = useState([]);

    const handleAdvisorSelectChange = (selectedAdvisorIds) => {
        setSelectedAdvisors(selectedAdvisorIds);
        // const selectedAdvisorNames = selectedAdvisorIds.map(advisorId => {
        //     const advisor = authors.find(author => author.authorId === advisorId);
        //     return advisor ? `${advisor.fullName}` : '';
        // });
        // setitemData(prevData => ({
        //     ...prevData,
        //     author: selectedAdvisorNames,
        // }));
    };


    const AuthorSelect = ({ authors, selectedAuthors, handleAuthorSelectChange, setAuthors, isAdvisor }) => {
        const [value, setValue] = useState([]);
        const [open, toggleOpen] = useState(false);
        const [dialogValue, setDialogValue] = useState({
            fullName: '',
            jobTitle: '',
            uri: '',
            type: '',
        });
        const [errors, setErrors] = useState({});

        const options = authors.map(author => ({
            value: author.authorId,
            label: author.fullName,
        }));

        const selectedOptions = options.filter(option =>
            selectedAuthors.includes(option.value)
        );

        const handleClose = () => {
            setDialogValue({
                fullName: '',
                jobTitle: '',
                uri: '',
                type: '',
            });
            setErrors({});
            toggleOpen(false);
        };

        const handleSubmitAuthor = (event) => {
            event.preventDefault();

            // Validate fields
            const newErrors = {};
            if (!dialogValue.fullName) newErrors.firstName = 'Full name is required';
            if (!dialogValue.jobTitle) newErrors.jobTitle = 'Job title is required';
            if (!dialogValue.uri) newErrors.uri = 'URI is required';
            if (!dialogValue.type) newErrors.type = 'Type is required';

            if (Object.keys(newErrors).length > 0) {
                setErrors(newErrors);
                return;
            }

            const newAuthor = {
                fullName: dialogValue.fullName,
                jobTitle: dialogValue.jobTitle,
                dateAccessioned: new Date().toISOString(),
                dateAvailable: new Date().toISOString(),
                uri: dialogValue.uri,
                type: "",
            };

            axios.post(`${process.env.REACT_APP_BASE_URL}/api/Author/createAuthor`, newAuthor)
                .then(response => {
                    const createdAuthor = response.data;

                    // Update authors state with new author
                    setAuthors(prevAuthors => [...prevAuthors, createdAuthor]);

                    // Update selectedAuthors state to include new author's id
                    if (isAdvisor) {
                        setSelectedAdvisors(prevSelectedAdvisors => [
                            ...prevSelectedAdvisors,
                            createdAuthor.authorId,
                        ]);

                        setitemData(prevData => ({
                            ...prevData,
                            author: [
                                ...prevData.author,
                                `${createdAuthor.fullName}`
                            ],
                        }));
                    } else {
                        setSelectedAuthors(prevSelectedAuthors => [
                            ...prevSelectedAuthors,
                            createdAuthor.authorId,
                        ]);

                        setitemData(prevData => ({
                            ...prevData,
                            author: [
                                ...prevData.author,
                                `${createdAuthor.fullName}`
                            ],
                        }));
                    }

                    handleClose();
                })
                .catch(error => {
                    console.error('There was an error creating the author!', error);
                });
        };

        return (
            <React.Fragment>
                <Autocomplete
                    multiple
                    value={selectedOptions}
                    onChange={(event, newValue) => {
                        if (newValue && newValue.length && newValue[newValue.length - 1].inputValue && isAdvisor) {
                            // toggleOpen(true);
                            // setDialogValue({
                            //     fullName: newValue[newValue.length - 1].inputValue,
                            //     jobTitle: '',
                            //     uri: window.location.origin + "/Dspace/Author/AuthorDetail/",
                            //     type: '',
                            // });
                            addAdvisor(newValue[newValue.length - 1].inputValue);
                        } else if (newValue && newValue.length && newValue[newValue.length - 1].inputValue && !isAdvisor) {
                            addAuthor(newValue[newValue.length - 1].inputValue);

                        }else if(newValue && newValue.length && newValue[newValue.length - 1].inputValue && !isAdvisor){
                        } else {
                            handleAuthorSelectChange(newValue.map(option => option.value));
                        }
                    }}
                    filterOptions={(options, params) => {
                        const filtered = filter(options, params);

                        if (params.inputValue.trim() !== '') {
                            filtered.push({
                                inputValue: params.inputValue,
                                label: `Add "${params.inputValue}"`, 
                            });
                        }

                        return filtered;
                    }}
                    options={options}
                    getOptionLabel={(option) => {
                        if (typeof option === 'string') {
                            return option;
                        }
                        if (option.inputValue) {
                            return option.inputValue;
                        }
                        return option.label;
                    }}
                    renderOption={(props, option) => (
                        <li {...props}>
                            {option.label}
                        </li>
                    )}
                    sx={{ width: '100%' }}
                    renderInput={(params) => (
                        isAdvisor ? (
                            <TextField {...params} label="Search and select advisors" placeholder="Search and select advisors..." />
                        ) : (
                            <TextField {...params} label="Search and select authors" placeholder="Search and select authors..." />
                        )
                    )}
                />
                <Dialog open={open} onClose={handleClose}>
                    <form>
                        <DialogTitle>Add a new author</DialogTitle>
                        <DialogContent>
                            <DialogContentText>
                                {/* Did you miss any author in our list? Please, add them! */}
                            </DialogContentText>
                            <TextField
                                autoFocus
                                margin="dense"
                                id="fullName"
                                value={dialogValue.fullName}
                                onChange={(event) => setDialogValue({ ...dialogValue, fullName: event.target.value })}
                                label="Full Name"
                                type="text"
                                fullWidth
                                error={!!errors.fullName}
                                helperText={errors.fullName}
                            />
                            <TextField
                                margin="dense"
                                id="jobTitle"
                                value={dialogValue.jobTitle}
                                onChange={(event) => setDialogValue({ ...dialogValue, jobTitle: event.target.value })}
                                label="Job Title"
                                type="text"
                                fullWidth
                                error={!!errors.jobTitle}
                                helperText={errors.jobTitle}
                            />
                            <TextField
                                margin="dense"
                                id="uri"
                                value={dialogValue.uri}
                                onChange={(event) => setDialogValue({ ...dialogValue, uri: event.target.value })}
                                label="URI"
                                type="text"
                                fullWidth
                                error={!!errors.uri}
                                helperText={errors.uri}
                                hidden
                            />
                            <TextField
                                margin="dense"
                                id="type"
                                value={dialogValue.type}
                                onChange={(event) => setDialogValue({ ...dialogValue, type: event.target.value })}
                                label="Type"
                                type="text"
                                fullWidth
                                error={!!errors.type}
                                helperText={errors.type}
                            />
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={handleClose}>Cancel</Button>
                            <Button type="submit" onClick={(e) => {
                                e.stopPropagation();
                                handleSubmitAuthor(e);
                            }}>Add</Button>
                        </DialogActions>
                    </form>
                </Dialog>
            </React.Fragment>
        );
    };
    const handleAuthorSelectChange = (selectedAuthorIds) => {
        setSelectedAuthors(selectedAuthorIds);
        // const selectedAuthorNames = selectedAuthorIds.map(authorId => {
        //     const author = authors.find(author => author.authorId === authorId);
        //     return author ? `${author.fullName}` : '';
        // });
        // setitemData(prevData => ({
        //     ...prevData,
        //     author: selectedAuthorNames,
        // }));
    };
    const types = [
        'Animation', 'Article', 'Book', 'Book chapter', 'Dataset', 'Learning Object',
        'Image', 'Image, 3-D', 'Map', 'Musical Score', 'Plan or blueprint', 'Preprint', 'Presentation', 'Recording, acoustical', 'Recording, musical', 'Recording, oral'
        , 'Software', 'Technical Report', 'Thesis', 'Video', 'Working Paper', 'Other'
    ];
    const [selectedTypes, setSelectedTypes] = useState([]);
    const TypesSelect = ({ types, selectedTypes, setSelectedTypes }) => {
        const options = types.map(type => ({
            value: type,
            label: type
        }));

        const selectedOptions = options.filter(option =>
            selectedTypes.includes(option.value)
        );

        return (
            <Select
                isMulti
                options={options}
                value={selectedOptions}
                onChange={selectedOptions => handleTypeSelectChange(selectedOptions ? selectedOptions.map(option => option.value) : [])}
                placeholder="Search and select types"
                isClearable
                styles={{
                    container: (base) => ({ ...base, width: '100%' }),
                    menu: (provided) => ({
                        ...provided,
                        maxHeight: '200px',
                        overflowY: 'auto',
                    }),
                    menuList: (provided) => ({
                        ...provided,
                        maxHeight: '200px',
                        overflowY: 'auto',
                    }),
                }}
            />
        );
    };
    const handleTypeSelectChange = (selectedTypes) => {
        setSelectedTypes(selectedTypes);
        setitemData(prevData => ({
            ...prevData,
            type: selectedTypes
        }));
    };





    const languages = [
        'English (United States)', 'English', 'Spanish', 'German', 'French', 'Italian',
        'Japanese', 'Chinese', 'Portuguese', 'Turkish', '(Other)'
    ];
    const [selectedLanguage, setSelectedLanguage] = useState('');
    const LanguageSelect = ({ languages, selectedLanguage, setSelectedLanguage }) => {
        const options = languages.map(language => ({
            value: language,
            label: language
        }));

        return (
            <Select
                options={options}
                value={options.find(option => option.value === selectedLanguage)} // selectedLanguage là một mảng với một phần tử duy nhất
                onChange={selectedLanguage => handleLanguagesSelectChange(selectedLanguage ? selectedLanguage.value : null)} // Chỉ trả về giá trị của option được chọn
                placeholder="Search and select language..."
                isClearable
                isMulti={false}
                styles={{
                    container: (base) => ({ ...base, width: '100%' }),
                    menu: (provided) => ({
                        ...provided,
                        maxHeight: '200px',
                        overflowY: 'auto',
                    }),
                    menuList: (provided) => ({
                        ...provided,
                        maxHeight: '200px',
                        overflowY: 'auto',
                    }),
                }}
            />
        );
    };
    const handleLanguagesSelectChange = (selectedLanguage) => {
        setSelectedLanguage(selectedLanguage);
        setitemData(prevData => ({
            ...prevData,
            language: selectedLanguage
        }));
    };
    console.log(selectedLanguage)
    const handleKeywordsChange = (keywords) => {
        setSubjectKeywords(keywords)
        // setitemData(prevData => ({
        //     ...prevData,
        //     subjectKeywords: keywords
        // }));
    };
    const handleOtherTitleChange = (otherTitle) => {
        setOtherTitle(otherTitle);
        // setitemData(prevData => ({
        //     ...prevData,
        //     otherTitle: otherTitle
        // }));
    };
    const handleSeriesNoChange = (seriesNo) => {
        setSeriesNo(seriesNo)
        // setitemData(prevData => ({
        //     ...prevData,
        //     seriesNo: seriesNo
        // }));
    };


    const [itemData, setitemData] = useState({
        author: [],
        title: '',
        otherTitle: [],
        dateOfIssue: '',
        publisher: '',
        citation: '',
        seriesNo: [],
        identifiersISBN: [],
        identifiersISSN: [],
        identifiersSICI: [],
        identifiersISMN: [],
        identifiersOther: [],
        identifiersLCCN: [],
        identifiersURI: ['http://localhost:3000/DSpace/ItemDetail/'],
        type: [],
        language: '',
        subjectKeywords: [],
        abstract: '',
        sponsors: '',
        description: '',
        collectionId: collectionId
    });
    const [successMessage, setSuccessMessage] = useState(''); // State for success message
    const [multipleFileUploads, setMultipleFileUploads] = useState([]);

    console.log(itemData.author);
    console.log(selectedAuthors);
    const handleChange = (e) => {
        const { name, value, type, checked } = e.target;
        const newValue = type === 'checkbox' ? checked : value;
        setSelectedValueType(value);
        // setSelectedValueLanguage(value);
        if (name === 'type') {
            setitemData({
                ...itemData,
                [name]: e.target.value,
            });
        } else if (name === 'identifiersISBN') {
            setitemData(prevData => ({
                ...prevData,
                identifiersISBN: [newValue]
            }));
        } else if (name === 'identifiersISSN') {
            itemData.identifiersISSN.push(newValue)
        } else if (name === 'identifiersSICI') {
            itemData.identifiersSICI.push(newValue)
        } else if (name === 'identifiersISMN') {
            itemData.identifiersISMN.push(newValue)
        } else if (name === 'identifiersOther') {
            itemData.identifiersOther.push(newValue)
        } else if (name === 'identifiersLCCN') {
            itemData.identifiersLCCN.push(newValue)
        } else if (name === 'identifiersURI') {
            itemData.identifiersURI.push(newValue)
        } else {
            setitemData({
                ...itemData,
                [name]: newValue,
            });
        }
    };
    console.log(itemData.title);
    const handleSubmitForIdentifier = (e) => {
        e.preventDefault();
        console.log(itemData);
        identifierFields.forEach(field => {
            const key = getIdentifierKey(field.type);
            if (key) {
                itemData[key].push(field.value);
            }
        });
    }



    const handleFileUpload = (event) => {
        if (Array.from(formdata.keys()).length > 0 && formdata.has('multipleFiles')) {
            formdata.delete('multipleFiles');
        }
        console.log(event)
        const files = Array.from(event.target.files);
        console.log(files)
        files.forEach(element => {
            multipleFileUploads.push(element)
            formdata.append('multipleFiles', element)
        });
    }

    const getFileExtension = (filename) => {
        return filename.split('.').pop();
    };

    const getBitstreamFormat = (extension) => {
        switch (extension.toLowerCase()) {
            case 'docx':
                return 'Microsoft Word XML';
            // Thêm các định dạng tệp tin khác nếu cần
            default:
                return 'Unknown';
        }
    };
    const formatFileSize = (size) => {
        if (size === 0) return '0 Bytes';
        const k = 1024;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        const i = Math.floor(Math.log(size) / Math.log(k));
        return parseFloat((size / Math.pow(k, i)).toFixed(2)) + ' ' + sizes[i];
    };

    const [formdata, setFormData] = useState(new FormData());

    const convertToFormData = (data) => {
        if (Array.from(formdata.keys()).length > 0) {
            if (formdata.has('author')) {
                formdata.delete('author');
            }
            if (formdata.has('title')) {
                formdata.delete('title');
            }
            if (formdata.has('otherTitle')) {
                formdata.delete('otherTitle');
            }
            if (formdata.has('dateOfIssue')) {
                formdata.delete('dateOfIssue');
            }
            if (formdata.has('publisher')) {
                formdata.delete('publisher');
            }
            if (formdata.has('citation')) {
                formdata.delete('citation');
            }
            if (formdata.has('seriesNo')) {
                formdata.delete('seriesNo');
            }
            if (formdata.has('identifiersISBN')) {
                formdata.delete('identifiersISBN');
            }
            if (formdata.has('identifiersISSN')) {
                formdata.delete('identifiersISSN');
            }
            if (formdata.has('identifiersSICI')) {
                formdata.delete('identifiersSICI');
            }
            if (formdata.has('identifiersISMN')) {
                formdata.delete('identifiersISMN');
            }
            if (formdata.has('identifiersOther')) {
                formdata.delete('identifiersOther');
            }
            if (formdata.has('identifiersLCCN')) {
                formdata.delete('identifiersLCCN');
            }
            if (formdata.has('identifiersURI')) {
                formdata.delete('identifiersURI');
            }
            if (formdata.has('type')) {
                formdata.delete('type');
            }
            if (formdata.has('language')) {
                formdata.delete('language');
            }
            if (formdata.has('subjectKeywords')) {
                formdata.delete('subjectKeywords');
            }
            if (formdata.has('abstract')) {
                formdata.delete('abstract');
            }
            if (formdata.has('sponsors')) {
                formdata.delete('sponsors');
            }
            if (formdata.has('description')) {
                formdata.delete('description');
            }
            if (formdata.has('collectionId')) {
                formdata.delete('collectionId');
            }
        }
        Object.keys(data).forEach(key => {
            if (Array.isArray(data[key])) {
                data[key].forEach(item => {
                    formdata.append(key, item);
                });
            } else {
                formdata.append(key, data[key]);
            }
        });
        return formdata;
    };
    const [title, setTitle] = useState('');
    const [otherTitle, setOtherTitle] = useState([]);
    const [dateOfIssue, setDateOfIssue] = useState('');
    const [publisher, setPublisher] = useState('');
    const [citation, setCitation] = useState('');
    const [seriesNo, setSeriesNo] = useState([]);
    const [subjectKeywords, setSubjectKeywords] = useState([]);
    const [abstract, setAbstract] = useState('');
    const [sponsors, setSponsors] = useState('');
    const [description, setDescription] = useState('');
    const [year, setYear] = useState('');
    const [month, setMonth] = useState('');
    const [day, setDay] = useState('');
    const [errors, setErrors] = useState({});
    const currentYear = new Date().getFullYear();

    const handleDateChange = (e) => {
        setDateOfIssue(e.target.value);
    };
    const handleYearChange = (e) => {
        setYear(e.target.value);
    };

    const handleMonthChange = (e) => {
        setMonth(e.target.value);
    };

    const handleDayChange = (e) => {
        setDay(e.target.value);
    };
    useEffect(() => {
        validateDate();
    }, [year, month, day]);

    const isLeapYear = (year) => {
        return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
    };

    const getDaysInMonth = (year, month) => {
        return new Date(year, month, 0).getDate();
    };

    const validateDate = () => {
        const newErrors = {};

        // Validate year
        if (year && (year < 1900 || year > currentYear)) {
            newErrors.year = `Year must be between 1900 and ${currentYear}`;
        }

        // Validate month
        if (month) {
            if (!year) {
                newErrors.month = 'Enter the year first';
            } else if (month < 1 || month > 12) {
                newErrors.month = 'Month must be between 1 and 12';
            }
        }

        // Validate day
        if (day) {
            if (!year || !month) {
                newErrors.day = 'Enter the year and month first';
            } else {
                const daysInMonth = getDaysInMonth(year, month);
                if (day < 1 || day > daysInMonth) {
                    newErrors.day = `Day must be between 1 and ${daysInMonth}`;
                }
            }
        }

        setErrors(newErrors);

        if (Object.keys(newErrors).length === 0) {
            let dateStr = year;
            if (month) {
                dateStr += `-${String(month).padStart(2, '0')}`;
            }
            if (day) {
                dateStr += `-${String(day).padStart(2, '0')}`;
            }
            setDateOfIssue(dateStr);
        } else {
            setDateOfIssue('');
        }
    };
    const handlePublisherChange = (e) => {
        setPublisher(e.target.value);
    };
    const [showCollectionWarning, setShowCollectionWarning] = useState(false);

    const handleSubmit = (e) => {
        e.preventDefault();
        if (collectionId <= 0) {
            setShowCollectionWarning(true);
        } else {
            const data = {
                header,
                collectionId,
                authors: selectedAuthors,
                advisors1: selectedAdvisors,
                title,
                otherTitle,
                dateOfIssue,
                publisher,
                citation,
                seriesNo,
                identifierFields,
                selectedTypes,
                subjectKeywords,
                abstract,
                sponsors,
                description,
                selectedLanguage,
                advisorsMore: advisors,
                authorsMore,
                additionalFields
            };

            createSimpleItem({ data, multipleFileUploads });
        }
    };
    const handleDelete = (i) => {
        setitemData(prevData => ({
            ...prevData,
            otherTitle: prevData.otherTitle.filter((_, index) => index !== i)
        }));
    };

    const handleAddition = (tag) => {
        setitemData(prevData => ({
            ...prevData,
            otherTitle: [...prevData.otherTitle, tag.text]
        }));
    };


    // Thêm một cặp select và input mới
    const addIdentifierField = () => {
        setIdentifierFields([...identifierFields, { type: "", value: "" }]);
    };
    const [advisors, setAdvisors] = useState([]);

    const addAdvisor = (name) => {
        setAdvisors([...advisors, { id: Date.now(), fullName: name || '', jobTitle: '' }]);
    };

    const handleAdvisorChange = (id, field, value) => {
        setAdvisors(advisors.map(advisor =>
            advisor.id === id ? { ...advisor, [field]: value } : advisor
        ));
    };

    const deleteAdvisor = (id) => {
        setAdvisors(advisors.filter(advisor => advisor.id !== id));
    };

    const [authorsMore, setAuthorsMore] = useState([]);
    const addAuthor = (name) => {
        setAuthorsMore([...authorsMore, { id: Date.now(), fullName: name || '', jobTitle: '' }]);
    };

    const handleAuthorChange = (id, field, value) => {
        setAuthorsMore(prevAuthors => prevAuthors.map(author => author.id === id ? { ...author, [field]: value } : author));
    };
    const deleteAuthor = (id) => {
        setAuthorsMore(authorsMore.filter(author => author.id !== id));
    };
    console.log(advisors)
    console.log(authorsMore) 
    // Xóa một cặp select và input
    const removeIdentifierField = (index) => {
        const newIdentifierFields = [...identifierFields];
        newIdentifierFields.splice(index, 1);
        setIdentifierFields(newIdentifierFields);
    };
    const handleChangeIdentify = (index, event) => {
        const { name, value } = event.target;
        const newIdentifierFields = [...identifierFields];
        newIdentifierFields[index] = { ...newIdentifierFields[index], [name]: value };
        setIdentifierFields(newIdentifierFields);
        console.log(identifierFields)

        console.log(itemData);
    };
    const getIdentifierKey = (type) => {
        switch (type) {
            case "ISBN":
                return "identifiersISBN";
            case "ISSN":
                return "identifiersISSN";
            case "SICI":
                return "identifiersSICI";
            case "ISMN":
                return "identifiersISMN";
            case "Other":
                return "identifiersOther";
            case "LCCN":
                return "identifiersLCCN";
            case "URI":
                return "identifiersURI";
            default:
                return null;
        }
    };
    const MetadataSelect = ({ value, onChange }) => {
        const filteredOptions = MetadataField.filter(option =>
            ![1, 2, 57, 58, 12, 36, 26, 39, 27, 28, 29, 30, 31, 32, 33, 34, 35, 59, 51, 15, 17, 14].includes(option.metadataFieldId)
        );
        const handleChange = (event, newValue) => {
            onChange(newValue ? newValue.metadataFieldId : '');
        };

        return (
            <Autocomplete
                sx={{ width: '100%' }}
                options={filteredOptions}
                getOptionLabel={(option) => `${option.metadataFieldName}`}
                id="metadata-customized-option-demo"
                onChange={handleChange}
                value={filteredOptions.find(option => option.metadataFieldId === value) || null}
                renderInput={(params) => (
                    <TextField {...params} label="Choose a metadata" variant="standard" />
                )}
            />
        );
    };
    const [additionalFields, setAdditionalFields] = useState([]);

    const handleAddField = () => {
        setAdditionalFields([...additionalFields, { id: Date.now(), metadataFieldId: '', value: '' }]);
    };

    const handleFieldChange = (id, field, value) => {
        setAdditionalFields(additionalFields.map(fieldItem =>
            fieldItem.id === id ? { ...fieldItem, [field]: value } : fieldItem
        ));
    };

    const handleDeleteField = (id) => {
        setAdditionalFields(additionalFields.filter(fieldItem => fieldItem.id !== id));
    };

    const { ValueContainer, Placeholder } = components;

    const CustomValueContainer = ({ children, ...props }) => {
        return (
            <ValueContainer {...props}>
                <Placeholder {...props} isFocused={props.isFocused}>
                    {props.selectProps.placeholder}
                </Placeholder>
                {React.Children.map(children, child =>
                    child && child.key !== 'placeholder' ? child : null,
                )}
            </ValueContainer>
        );
    };
    const CollectionSelect = ({ collections, selectedValue, handleChangeCollection }) => {
        const [focused, setFocused] = useState(false);

        const options = [
            ...collections.map(collection => ({
                value: collection.collectionId.toString(),
                label: collection.collectionName
            }))
        ];

        return (
            <Select
                options={options}
                value={options.find(option => option.value === selectedValue)}
                onChange={selectedOption => handleChangeCollection(selectedOption ? selectedOption.value : '-1')}
                placeholder="Choose collections"
                isClearable
                components={{
                    ValueContainer: CustomValueContainer
                }}
                maxMenuHeight={300}
                onFocus={() => setFocused(true)}
                onBlur={() => setFocused(false)}
                isFocused={focused}
                theme={(theme) => ({
                    ...theme,
                    spacing: {
                        ...theme.spacing,
                        baseUnit: 2,
                        controlHeight: 46,
                        menuGutter: 8,
                    },
                })}
                styles={{
                    container: (provided) => ({
                        ...provided,
                        width: '160px',
                        // marginTop: 5
                    }),
                    control: (base) => ({
                        ...base,
                        border: '1px solid black',
                        borderRadius: '4px',
                    }),
                    valueContainer: (provided) => ({
                        ...provided,
                        overflow: "visible",

                    }),
                    placeholder: (base, state) => ({
                        ...base,
                        position: 'absolute',
                        top: (state.hasValue || state.selectProps.inputValue || state.selectProps.isFocused) ? '-120%' : '0%',
                        transition: 'top 0.2s, font-size 0.2s',
                        fontSize: (state.hasValue || state.selectProps.inputValue || state.selectProps.isFocused) && 14,

                    }),
                    indicatorSeparator: () => ({
                        display: 'none',
                    }),
                    dropdownIndicator: (base) => ({
                        ...base,
                        display: 'none',
                    }),
                }}
            />
        );
    };

    const handleChangeCollection = (e) => {
        // const { name, value, type, checked } = e.target;
        // const newValue = type === 'checkbox' ? checked : value;
        setSelectedValue(e)
        navigate(`/DSpace/CreateItem/${e}`);
    };
    return (
        <div>
            <Header />
            <Helmet>
                <meta charSet="utf-8" />
                <title>Create new Item - dSPACE</title>
            </Helmet>
            <div className="container mt-5">
                <h1 className="mb-4 text-center">Create New Item</h1>
                <div>
                    <CollectionSelect
                        collections={collections}
                        selectedValue={selectedValue}
                        handleChangeCollection={handleChangeCollection}
                    />
                </div>
                {successMessage && <div className="alert alert-success">{successMessage}</div>} {/* Success message */}
                <div className="row justify-content-center">
                    <div className="col-md-12">
                        <form onSubmit={handleSubmit}>
                            <div className="row">
                                {/* {authors.map((author, index) => ( */}
                                <div>
                                    <span className="add-field-container mt-3 mb-2" onClick={handleAddField}>
                                        <AddRounded className="add-rounded-icon" />
                                        <span className="add-field-text">Add New Field</span>
                                    </span>
                                </div>
                                <div>
                                    {additionalFields.map((field) => (
                                        <div key={field.id} className="form-group mb-3 d-flex align-items-center">
                                            <div className="flex-grow-1" style={{ flex: 2 }}>
                                                <MetadataSelect
                                                    value={field.metadataFieldId}
                                                    onChange={(value) => handleFieldChange(field.id, 'metadataFieldId', value)}
                                                />
                                            </div>
                                            <input
                                                type="text"
                                                className="form-control mx-2"
                                                value={field.value}
                                                onChange={(e) => handleFieldChange(field.id, 'value', e.target.value)}
                                                style={{ flex: 1 }}
                                                required
                                            />
                                            <button
                                                type="button"
                                                className="btn btn-danger"
                                                onClick={() => handleDeleteField(field.id)}
                                                style={{ flex: 0 }}
                                            >
                                                <DeleteRounded />
                                            </button>
                                        </div>
                                    ))}

                                </div>
                                <div>
                                    <label style={{ marginRight: '10px' }}>Advisor </label>
                                    <div className="form-group mb-3 d-flex align-items-center">
                                        <AuthorSelect
                                            authors={authors}
                                            selectedAuthors={selectedAdvisors}
                                            handleAuthorSelectChange={handleAdvisorSelectChange}
                                            setAuthors={setAuthors}
                                            isAdvisor={true}
                                        />
                                    </div>
                                    {advisors.map(advisor => (
                                        <div key={advisor.id} className="form-group mb-3 d-flex align-items-center justify-content-between">
                                            <TextField
                                                label="Full Name"
                                                variant="outlined"
                                                placeholder="Full Name"
                                                value={advisor.fullName}
                                                onChange={(e) => handleAdvisorChange(advisor.id, 'fullName', e.target.value)}
                                                // fullWidth
                                                style={{width: '47%'}}
                                            />
                                            <TextField
                                                label="Job Title"
                                                variant="outlined"
                                                placeholder="Job Title"
                                                value={advisor.jobTitle}
                                                onChange={(e) => handleAdvisorChange(advisor.id, 'jobTitle', e.target.value)}
                                                // fullWidth
                                                style={{width: '47%'}}
                                            />
                                            <button
                                                type="button"
                                                className="btn btn-danger"
                                                onClick={() => deleteAdvisor(advisor.id)}
                                                style={{height: '56px'}}
                                            >
                                                <DeleteRounded />
                                            </button>
                                        </div>
                                    ))}
                                    <div>
                                        <span className="add-field-container" onClick={() => addAdvisor('')}>
                                            <AddRounded className="add-rounded-icon" />
                                            <span>Add more advisor</span>
                                        </span>
                                    </div>

                                </div>
                                <div>
                                    <label style={{ marginRight: '10px' }}>Author </label>
                                    <div className="form-group mb-3 d-flex align-items-center">
                                        <AuthorSelect
                                            authors={authors}
                                            selectedAuthors={selectedAuthors}
                                            handleAuthorSelectChange={handleAuthorSelectChange}
                                            setAuthors={setAuthors}
                                            isAdvisor={false}
                                        />
                                    </div> 
                                    {authorsMore.map(author => (
                                        <div key={author.id} className="form-group mb-3 d-flex align-items-center justify-content-between">
                                            <TextField
                                                label="Full Name"
                                                variant="outlined"
                                                placeholder="Full Name"
                                                value={author.fullName}
                                                onChange={(e) => handleAuthorChange(author.id, 'fullName', e.target.value)}
                                                // fullWidth
                                                style={{width: '47%'}}
                                            />
                                            {/* <input
                                                type="text"
                                                placeholder="Job Title"
                                                value={author.jobTitle}
                                                onChange={(e) => handleAuthorChange(author.id, 'jobTitle', e.target.value)}
                                                className="form-control mx-2"
                                            /> */}
                                            <TextField
                                                label="Job Title"
                                                variant="outlined"
                                                placeholder="Job Title"
                                                value={author.jobTitle}
                                                onChange={(e) => handleAuthorChange(author.id, 'jobTitle', e.target.value)}
                                                // fullWidth
                                                style={{width: '47%'}}
                                            />
                                            <button
                                                type="button"
                                                className="btn btn-danger"
                                                onClick={() => deleteAuthor(author.id)}
                                                style={{height: '56px'}}
                                            >
                                                <DeleteRounded />
                                            </button>
                                        </div>
                                    ))}
                                    <div>
                                        <span className="add-field-container" onClick={() => addAuthor('')}>
                                            <AddRounded className="add-rounded-icon" />
                                            <span>Add more author</span>
                                        </span>
                                    </div>
                                </div>

                                <div className="form-group mb-3">
                                    <label>Title</label>
                                    <label style={{color: 'red', fontWeight: 'bold'}}> *</label>
                                    <TextareaAutosize
                                        className="form-control"
                                        aria-label="empty textarea"
                                        placeholder="Enter title"
                                        name="title"
                                        value={title}
                                        onChange={(e) => setTitle(e.target.value)}
                                        required
                                        style={{
                                            boxSizing: 'border-box',
                                            width: '100%',
                                            padding: '8px',
                                            borderRadius: '4px',
                                            border: '1px solid #ccc',
                                        }}
                                    />
                                </div>
                                <div>
                                    <label style={{ marginRight: '10px' }}>Other Title: Enter other titles separated by Enter Button </label>
                                    <div className="form-group mb-3 align-items-center">
                                        <TagsInput
                                            name='otherTitle'
                                            value={otherTitle}
                                            onChange={handleOtherTitleChange}
                                            placeHolder="Enter other titles separated by Enter Button"
                                        />
                                    </div>

                                </div>

                                <div className="form-group mb-3 col-md-3">
                                    <label>Date of Issue</label>
                                    <label style={{color: 'red', fontWeight: 'bold'}}> *</label>
                                    <div className='d-flex flex-column'>
                                        <div className='d-flex'>
                                            <div className="flex-fill me-2">
                                                <input
                                                    type="number"
                                                    className={`form-control ${errors.year ? 'is-invalid' : ''}`}
                                                    placeholder="Year"
                                                    value={year}
                                                    onChange={handleYearChange}
                                                    required
                                                />
                                                {errors.year && <div className="invalid-feedback">{errors.year}</div>}
                                            </div>
                                            <div className="flex-fill me-2">
                                                <input
                                                    type="number"
                                                    className={`form-control ${errors.month ? 'is-invalid' : ''}`}
                                                    placeholder="Month"
                                                    value={month}
                                                    onChange={handleMonthChange}
                                                    disabled={!year}
                                                />
                                                {errors.month && <div className="invalid-feedback">{errors.month}</div>}
                                            </div>
                                            <div className="flex-fill">
                                                <input
                                                    type="number"
                                                    className={`form-control ${errors.day ? 'is-invalid' : ''}`}
                                                    placeholder="Day"
                                                    value={day}
                                                    onChange={handleDayChange}
                                                    disabled={!year || !month}
                                                />
                                                {errors.day && <div className="invalid-feedback">{errors.day}</div>}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="form-group mb-3 col-md-9">
                                    <label>Publisher</label>
                                    <TextareaAutosize
                                        className="form-control"
                                        aria-label="empty textarea"
                                        placeholder="Enter publisher"
                                        name="publisher"
                                        value={publisher}
                                        onChange={handlePublisherChange}

                                        style={{
                                            boxSizing: 'border-box',
                                            width: '100%',
                                            padding: '8px',
                                            borderRadius: '4px',
                                            border: '1px solid #ccc',
                                        }}
                                    />
                                </div>

                                <div className="form-group mb-3">
                                    <label>Citation</label>
                                    <TextareaAutosize
                                        className="form-control"
                                        aria-label="empty textarea"
                                        placeholder="Enter citation"
                                        name="citation"
                                        value={citation}
                                        onChange={(e) => setCitation(e.target.value)}
                                        style={{
                                            boxSizing: 'border-box',
                                            width: '100%',
                                            padding: '8px',
                                            borderRadius: '4px',
                                            border: '1px solid #ccc',
                                        }}
                                    />
                                </div>

                                <div className="form-group mb-3">
                                    <label>Series No: Enter Series No separated by Enter Button</label>
                                    <TagsInput
                                        name="seriesNo"
                                        value={seriesNo}
                                        onChange={handleSeriesNoChange}
                                        placeHolder="Enter SeriesNo separated by Enter Button"
                                    />
                                </div>


                                {/* </div>

                                <div className="col-md-6"> */}
                                <div className="form-group mb-3">
                                    <label>Identifiers</label>
                                    {identifierFields.map((field, index) => (
                                        <div key={index} className="d-flex align-items-center mb-3">
                                            <select
                                                name="type"
                                                value={field.type}
                                                onChange={(e) => handleChangeIdentify(index, e)}
                                                className="form-select me-2"
                                                style={{ width: "135px" }} // Đặt chiều rộng của select
                                            >
                                                <option value="">Select Type</option>
                                                <option value="27">GOVDOC</option>
                                                <option value="28">ISBN</option>
                                                <option value="29">ISSN</option>
                                                <option value="30">SICI</option>
                                                <option value="31">ISMN</option>
                                                <option value="32">Other</option>
                                                <option value="33">LCCN</option>
                                                <option value="34">URI</option>
                                            </select>
                                            <input
                                                type="text"
                                                className="form-control me-2"
                                                name="value"
                                                value={field.value}
                                                onChange={(e) => handleChangeIdentify(index, e)}
                                                placeholder="Enter value"
                                                style={{ flex: 1 }} // Đặt input để dãn ra hết khoảng còn lại
                                            />
                                            {identifierFields.length > 1 && (
                                                <button
                                                    type="button"
                                                    className="btn btn-danger"
                                                    onClick={() => removeIdentifierField(index)}
                                                >
                                                    <DeleteRounded />
                                                </button>
                                            )}
                                        </div>
                                    ))}
                                    {/* <div
                                            type="button"
                                            className="d-flex"
                                            onClick={addIdentifierField}
                                        >
                                            <span>

                                            </span>
                                            <AddRounded />
                                            Add more identifier
                                        </div> */}
                                    <div>
                                        <span className="add-field-container" onClick={addIdentifierField}>
                                            <AddRounded className="add-rounded-icon" />
                                            <span>Add more identifier</span>
                                        </span>
                                    </div>
                                </div>

                                <div className="form-group mb-3">
                                    <label>Choose Type</label>
                                    <TypesSelect
                                        name="type"
                                        types={types}
                                        selectedTypes={selectedTypes}
                                        handleTypeSelectChange={handleTypeSelectChange}
                                    />

                                </div>

                                <div className="form-group mb-3">
                                    <label>Keywords: Enter keywords separated by Enter Button</label>
                                    <TagsInput
                                        name="subjectKeywords"
                                        value={subjectKeywords}
                                        onChange={handleKeywordsChange}
                                        placeHolder="Enter keywords separated by Enter Button"
                                    />
                                </div>

                                <div className="form-group mb-3">
                                    <label>Abstract</label>
                                    <textarea
                                        className="form-control"
                                        name="abstract"
                                        value={abstract}
                                        onChange={(e) => setAbstract(e.target.value)}
                                        rows={4}
                                        placeHolder="Enter abstract"
                                    />
                                </div>

                                <div className="form-group mb-3">
                                    <label>Sponsors</label>
                                    <textarea
                                        className="form-control"
                                        name="sponsors"
                                        value={sponsors}
                                        onChange={(e) => setSponsors(e.target.value)}
                                        rows={4}
                                        placeHolder="Enter sponsors"

                                    />
                                </div>

                                <div className="form-group mb-3">
                                    <label>Description</label>
                                    <textarea
                                        className="form-control"
                                        name="description"
                                        value={description}
                                        onChange={(e) => setDescription(e.target.value)}
                                        rows={6}
                                        placeHolder="Enter description"
                                    />
                                </div>

                                <div className="form-group mb-3">
                                    <label>Language</label>

                                    <LanguageSelect
                                        name='language'
                                        languages={languages}
                                        selectedLanguage={selectedLanguage}
                                        handleLanguagesSelectChange={handleLanguagesSelectChange}
                                    />
                                </div>
                                <label className='d-flex'>Upload files <p style={{color: 'red', fontWeight: 'bold', margin: '0'}}>*</p></label>

                                <div className="form-group mb-3">
                                    <label className="custom-file-upload">
                                        <input name="multipleFileUploads" type="file" multiple onChange={handleFileUpload} required />
                                        {multipleFileUploads.length > 0 && (
                                            <div>
                                                {multipleFileUploads.map((file, index) => (
                                                    <div key={index} style={{ display: 'flex', alignItems: 'center', marginTop: '10px' }}>
                                                        <p style={{ margin: 0 }}>{file.name} ({formatFileSize(file.size)})</p>
                                                        <p style={{ margin: 0, marginLeft: '10px' }}>Bitstream format: {getBitstreamFormat(getFileExtension(file.name))}</p>
                                                    </div>
                                                ))}
                                            </div>
                                        )}
                                    </label>
                                </div>
                                <div className='mt-4'>
                                    <button type="submit" className="btn btn-primary">Create Item</button>
                                    <button type="button" style={{ marginLeft: '10px' }} onClick={() => navigate(`/Dspace/Collection/ItemList/-1`)} class="btn btn-secondary">Back to List</button>
                                </div>
                            </div>
                            <div style={{ marginTop: '200px' }}></div>
                        </form>
                    </div>
                </div>
            </div>
            <Modal show={showModal} onHide={() => setShowModal(false)}>
                <Modal.Header closeButton>
                    <Modal.Title>Item Created Successfully</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <p>Item Created Successfully</p>
                </Modal.Body>
                <Modal.Footer>
                    <Button style={{ marginRight: '20px' }} variant="secondary" onClick={handleCreateMore}>
                        Create More
                    </Button>
                    <Button variant="primary" onClick={handleViewItem}>
                        View Item
                    </Button>
                </Modal.Footer>
            </Modal>
            <Modal show={showCollectionWarning} onHide={() => setShowCollectionWarning(false)}>
                <Modal.Header closeButton>
                    <Modal.Title>Warning</Modal.Title>
                </Modal.Header>
                <Modal.Body>Please select a collection.</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={() => setShowCollectionWarning(false)}>
                        OK
                    </Button>
                </Modal.Footer>
            </Modal>
            {loading && <LoadingOverlay />}
        </div>
    );
};

export default CreateCommunity;
