import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useParams, useNavigate, Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import AssessmentIcon from '@mui/icons-material/Assessment';
import Header from '../../components/Header/Header';
import Counter from '../../components/Statistics/counterView';
import { Button } from '@mui/material';

const CommunityDetailStaff = () => {
    const { communityId } = useParams();
    const [community, setCommunity] = useState(null);
    const [collections, setCollections] = useState([]);
    const [message, setMessage] = useState(null);
    const [messageType, setMessageType] = useState('');
    const navigate = useNavigate();
    const token = localStorage.getItem('Token');
    const header = `Bearer ${token}`;
    useEffect(() => {
        axios.get(`${process.env.REACT_APP_BASE_URL}/api/CommunityStaff/getCollection/${communityId}`,  {headers: { Authorization: header }})
            .then(response => {
                setCommunity(response.data);
            })
            .catch(error => {
                console.error('There was an error fetching the community!', error);
            });

           
    }, [communityId]);



    function getRelativePath(fullPath) {
        const splitPath = fullPath.split('public\\');
        return splitPath[1];
    }

    const handleRedirectStats = () => {
        navigate(`/Dspace/StatisticDetail?communityId=${communityId}`);
    };

    return (
        <div>
            <Header />
            
            {community !== null && (
                <Counter communityId={community.communityId} />
            )}
            <div className="container mt-5">
                <div className="py-1 bg-light">
                    <Link to="/HomepageAdmin" className="text-decoration-none">Home </Link>
                    <span className="text-dark" ><strong>| Community Detail</strong></span>
                </div>

                {message && <div className={`alert alert-${messageType}`}>{message}</div>}
                <h1 className="mb-4">Community Details
                    <Button
                        onClick={handleRedirectStats}
                        className='ms-2'
                        startIcon={<AssessmentIcon />}
                        sx={{
                            color: 'primary',
                            border: 'none',
                            boxShadow: 'none',
                            '&:hover': {
                                border: 'none',
                                boxShadow: 'none',
                            }
                        }}
                    >
                        Statistics
                    </Button></h1>

                <div className="card">
                    <div className="card-body">
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Name:</div>
                            <div className="col-sm-6 ">{community?.communityName}</div>
                        </div>
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Short Description:</div>
                            <div className="col-sm-6 ">{community?.shortDescription}</div>
                        </div>
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Created By:</div>
                            <div className="col-sm-6 ">{community?.createBy}</div>
                        </div>
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Updated By:</div>
                            <div className="col-sm-6">{community?.updateBy}</div>
                        </div>
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Create Time:</div>
                            <div className="col-sm-6 ">{new Date(community?.createTime).toLocaleString()}</div>
                        </div>
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Update Time:</div>
                            <div className="col-sm-6 ">{new Date(community?.updateTime).toLocaleString()}</div>
                        </div>
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Logo:</div>
                            <div className="col-sm-6 ">
                                {community?.logoUrl ? (
                                    <img src={`/${getRelativePath(community.logoUrl)}`} alt="logo" className='img-thumbnail' style={{ width: '200px' }} />
                                ) : (
                                    <div>No Image</div>
                                )}
                            </div>
                        </div>
                        <div className="row pb-2 my-3">
                            <div className="col-sm-2 text-end">Active:</div>
                            <div className="col-sm-6"><input type="checkbox" checked={community?.isActive} readOnly /></div>
                        </div>
                    </div>
                </div>
                <div className="mt-4">
                    <button className="btn btn-secondary" onClick={() => navigate('/Dspace/Community/CommunityListSatff')}>Back to List</button>
                </div>

               
            </div>
        </div>
    );
};

export default CommunityDetailStaff;
