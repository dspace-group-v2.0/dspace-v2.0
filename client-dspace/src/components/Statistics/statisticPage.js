import { useEffect, useState } from "react";
import { useSearchParams } from "react-router-dom";
import Header from "../Header/Header";
import axios from "axios";
import { Line } from "react-chartjs-2";
import Chart from "chart.js/auto";
import { CategoryScale } from "chart.js";
import { Box, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from "@mui/material";
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import { Helmet } from "react-helmet";
Chart.register(CategoryScale);
function StatisticPage() {
    const [searchParams, setSearchParams] = useSearchParams();
    const [error, setError] = useState(null);
    const communityId = searchParams.get('communityId');
    const collectionId = searchParams.get('collectionId');
    const itemId = searchParams.get('itemId');

    const [dataResponse, setDataResponse] = useState(null);
    const [data, setData] = useState(null);

    const [dataItem, setDataItem] = useState(null);
    const [dataCollection, setDataCollection] = useState(null);
    const [dataCommunity, setDataCommunity] = useState(null);

    const [chartItem, setChartItem] = useState(null);
    const [tableDataItem, setTableDataItem] = useState([]);
    const [totalViewItem, setTotalViewItem] = useState(0);

    const [chartCollection, setChartCollection] = useState(null);
    const [tableDataCollection, setTableDataCollection] = useState([]);
    const [totalViewCollection, setTotalViewCollection] = useState(0);

    const [chartCommunity, setChartCommunity] = useState(null);
    const [tableDataCommunity, setTableDataCommunity] = useState([]);
    const [totalViewCommunity, setTotalViewCommunity] = useState(0);


    const [value, setValue] = useState('1');

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    useEffect(() => {
        if (communityId != null) {
            axios.get(`${process.env.REACT_APP_BASE_URL}/api/Statistic/GetStatisticForCommunity/${communityId}`)
                .then(response => {
                    setDataResponse(response.data)
                }).catch(error => {
                    setError('There was an error fetching the statistic!');
                    console.error('There was an error fetching the statistic!', error);
                });

            setValue('1');
        } else if (collectionId != null) {
            axios.get(`${process.env.REACT_APP_BASE_URL}/api/Statistic/GetStatisticForCollection/${collectionId}`)
                .then(response => {
                    setDataResponse(response.data)
                }).catch(error => {
                    setError('There was an error fetching the statistic!');
                    console.error('There was an error fetching the statistic!', error);
                });
            setValue('2');
        } else if (itemId != null) {
            axios.get(`${process.env.REACT_APP_BASE_URL}/api/Statistic/GetStatisticForItem/${itemId}`)
                .then(response => {
                    setDataResponse(response.data)
                }).catch(error => {
                    setError('There was an error fetching the statistic!');
                    console.error('There was an error fetching the statistic!', error);
                });
            setValue('3');
        }

    }, [itemId], [collectionId], [communityId])

    useEffect(() => {
        if (dataResponse !== null) {
            if (dataResponse.community !== undefined) {
                setDataCommunity(dataResponse.community)
            }
            if (dataResponse.collection !== undefined) {
                setDataCollection(dataResponse.collection)
            }
            if (dataResponse.item !== undefined) {
                setDataItem(dataResponse.item)
            }
        }
    }, [dataResponse])

    const transformData = (data) => {
        const groupedData = data.reduce((acc, item) => {
            if (!acc[item.year]) {
                acc[item.year] = { months: Array(12).fill(0), total: 0 };
            }
            acc[item.year].months[item.month - 1] = item.totalCount;
            acc[item.year].total += item.totalCount;
            return acc;
        }, {});

        return Object.entries(groupedData).map(([year, { months, total }]) => [parseInt(year), ...months, total]);
    };

    useEffect(() => {
        if (dataItem !== null) {
            setChartItem({
                labels: dataItem.map((data) => data.month + "-" + data.year),
                datasets: [
                    {
                        label: "Item views gained",
                        data: dataItem.map((data) => data.totalCount),
                        backgroundColor: [
                            "rgba(75,192,192,1)",
                            "#50AF95",
                            "#f3ba2f",
                            "#2a71d0"
                        ],
                        borderColor: "black",
                        borderWidth: 2
                    }
                ]
            })
            setTotalViewItem(dataItem.reduce((sum, item) => sum + item.totalCount, 0));
            const tableData = transformData(dataItem);
            setTableDataItem(tableData);
        }
        if (dataCollection !== null) {
            setChartCollection({
                labels: dataCollection.map((data) => data.month + "-" + data.year),
                datasets: [
                    {
                        label: "Collection views gained",
                        data: dataCollection.map((data) => data.totalCount),
                        backgroundColor: [
                            "rgba(75,192,192,1)",
                            "#50AF95",
                            "#f3ba2f",
                            "#2a71d0"
                        ],
                        borderColor: "black",
                        borderWidth: 2
                    }
                ]
            })

            setTotalViewCollection(dataCollection.reduce((sum, item) => sum + item.totalCount, 0));
            const tableData = transformData(dataCollection);
            setTableDataCollection(tableData);
        }
        if (dataCommunity !== null) {
            setChartCommunity({
                labels: dataCommunity.map((data) => data.month + "-" + data.year),
                datasets: [
                    {
                        label: "Community views gained",
                        data: dataCommunity.map((data) => data.totalCount),
                        backgroundColor: [
                            "rgba(75,192,192,1)",
                            "#50AF95",
                            "#f3ba2f",
                            "#2a71d0"
                        ],
                        borderColor: "black",
                        borderWidth: 2
                    }
                ]
            })
            setTotalViewCommunity(dataCommunity.reduce((sum, item) => sum + item.totalCount, 0));
            const tableData = transformData(dataCommunity);
            setTableDataCommunity(tableData);
        }
    }, [dataItem], [dataCollection], [dataCommunity])
    const months = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec', 'Total'];

    console.log(tableDataCollection)

    return (
        <div className="App">
            <Header />
            <Helmet>
                <meta charSet="utf-8" />
                <title>Statistic Summary - dSPACE</title>
            </Helmet>
            <div className="d-flex mt-4 justify-content-center w-100">
                <Box className="d-flex flex-column w-50" sx={{ width: '100%', typography: 'body1' }}>
                    <Typography
                        variant="h4"
                        component="h4"
                        sx={{ textAlign: 'left', mb: 1, mt: 2 }}>
                        Summary Statistics
                    </Typography>
                    <TabContext value={value}>
                        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                            {
                                itemId !== null && (
                                    <TabList onChange={handleChange} aria-label="lab API tabs example">
                                        <Tab label="Item view count" value="3" />
                                    </TabList>
                                )
                            }
                            {
                                collectionId !== null && (
                                    <TabList onChange={handleChange} aria-label="lab API tabs example">
                                        <Tab label="Collection view count" value="2" />
                                        <Tab label="Item view count" value="3" />
                                    </TabList>
                                )
                            }
                            {
                                communityId !== null && (
                                    <TabList onChange={handleChange} aria-label="lab API tabs example">
                                        <Tab label="Community view count" value="1" />
                                        <Tab label="Collection view count" value="2" />
                                        <Tab label="Item view count" value="3" />
                                    </TabList>
                                )
                            }
                        </Box>
                        <TabPanel value="1">
                        <div className="chart-container flex-column">
                                <Typography
                                    variant="h5"
                                    component="h5"
                                    sx={{ textAlign: 'left' }}>
                                    Communities statistic
                                </Typography>
                                {chartCommunity ? (
                                    <Line
                                        data={chartCommunity}
                                        options={{
                                            plugins: {
                                                responsive: true,
                                                title: {
                                                    display: true,
                                                    text: "Number of users accessing the community"
                                                },
                                                legend: {
                                                    display: false
                                                }
                                            },

                                        }}
                                    />
                                ) : (
                                    <p>No data available to display the chart.</p>
                                )}
                            </div>

                            <div className="table-container mt-3 flex-column">

                                {dataCommunity ? (
                                    <TableContainer component={Paper}>
                                        <Table aria-label="spanning table">
                                            <TableHead>
                                                <TableRow>
                                                    {months.map((month, index) => (
                                                        <TableCell key={index}>{month}</TableCell>
                                                    ))}
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                {tableDataCommunity.map((row, rowIndex) => (
                                                    <TableRow key={rowIndex}>
                                                        {row.map((value, colIndex) => (
                                                            <TableCell key={colIndex}>{value}</TableCell>
                                                        ))}
                                                    </TableRow>
                                                ))}

                                                <TableRow>
                                                    <TableCell align="right" colSpan={13}>Total Views</TableCell>
                                                    <TableCell align="left" colSpan={1}>{totalViewCommunity}</TableCell>
                                                </TableRow>
                                            </TableBody>
                                        </Table>
                                    </TableContainer>
                                ) : (
                                    <p>No data available to display the chart.</p>
                                )}
                            </div>

                        </TabPanel>
                        <TabPanel value="2">
                            <div className="chart-container flex-column">
                                <Typography
                                    variant="h5"
                                    component="h5"
                                    sx={{ textAlign: 'left' }}>
                                    Collections statistic
                                </Typography>
                                {chartCollection ? (
                                    <Line
                                        data={chartCollection}
                                        options={{
                                            plugins: {
                                                responsive: true,
                                                title: {
                                                    display: true,
                                                    text: "Number of users accessing the collection"
                                                },
                                                legend: {
                                                    display: false
                                                }
                                            },

                                        }}
                                    />
                                ) : (
                                    <p>No data available to display the chart.</p>
                                )}
                            </div>

                            <div className="table-container mt-3 flex-column">

                                {dataCollection ? (
                                    <TableContainer component={Paper}>
                                        <Table aria-label="spanning table">
                                            <TableHead>
                                                <TableRow>
                                                    {months.map((month, index) => (
                                                        <TableCell key={index}>{month}</TableCell>
                                                    ))}
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                {tableDataCollection.map((row, rowIndex) => (
                                                    <TableRow key={rowIndex}>
                                                        {row.map((value, colIndex) => (
                                                            <TableCell key={colIndex}>{value}</TableCell>
                                                        ))}
                                                    </TableRow>
                                                ))}

                                                <TableRow>
                                                    <TableCell align="right" colSpan={13}>Total Views</TableCell>
                                                    <TableCell align="left" colSpan={1}>{totalViewCollection}</TableCell>
                                                </TableRow>
                                            </TableBody>
                                        </Table>
                                    </TableContainer>
                                ) : (
                                    <p>No data available to display the chart.</p>
                                )}
                            </div>

                        </TabPanel>
                        <TabPanel value="3">
                            <div className="chart-container flex-column">
                                <Typography
                                    variant="h5"
                                    component="h5"
                                    sx={{ textAlign: 'left' }}>
                                    Items statistic
                                </Typography>
                                {chartItem ? (
                                    <Line
                                        data={chartItem}
                                        options={{
                                            responsive: true,
                                            plugins: {
                                                title: {
                                                    display: true,
                                                    text: "Number of users accessing the item"
                                                },
                                                legend: {
                                                    display: false
                                                }
                                            },
                                        }}
                                    />
                                ) : (
                                    <p>No data available to display the chart.</p>
                                )}
                            </div>

                            <div className="table-container mt-3 flex-column">

                                {dataItem ? (
                                    <TableContainer component={Paper}>
                                        <Table aria-label="spanning table">
                                            <TableHead>
                                                <TableRow>
                                                    {months.map((month, index) => (
                                                        <TableCell key={index}>{month}</TableCell>
                                                    ))}
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                {tableDataItem.map((row, rowIndex) => (
                                                    <TableRow key={rowIndex}>
                                                        {row.map((value, colIndex) => (
                                                            <TableCell key={colIndex}>{value}</TableCell>
                                                        ))}
                                                    </TableRow>
                                                ))}

                                                <TableRow>
                                                    <TableCell align="right" colSpan={13}>Total Views</TableCell>
                                                    <TableCell align="left" colSpan={1}>{totalViewItem}</TableCell>
                                                </TableRow>
                                            </TableBody>
                                        </Table>
                                    </TableContainer>
                                ) : (
                                    <p>No data available to display the chart.</p>
                                )}
                            </div>

                        </TabPanel>
                    </TabContext>
                </Box>
            </div>

        </div>
    );
}
export default StatisticPage;