import React, { useState, useEffect } from 'react';
import axios from 'axios';

function Counter({ communityId, collectionId, itemId }) {
    const token = localStorage.getItem('Token');
    const header = `Bearer ${token}`;
    let timeoutId;

    useEffect(() => {
        if (localStorage.getItem('stopCount') === 'TRUE' || !localStorage.getItem('stopCount')) {
            localStorage.setItem('stopCount', 'FALSE');
            clearTimeout(timeoutId);
        }
    }, [])

    useEffect(() => {
        if (communityId !== undefined && collectionId !== undefined && itemId  !== undefined) {
            if (localStorage.getItem('stopCount') === 'FALSE') {
                localStorage.setItem('stopCount', 'TRUE');
                timeoutId = setTimeout(() => {
                    const date = new Date();
                    const month = date.getMonth() + 1;
                    const year = date.getFullYear();
                    const day = date.getDay();
                    const stats = {
                        Year: year,
                        Month: month,
                        Day: day,
                        ViewOnDay: 1,
                        ItemId: itemId,
                        CollectionId: collectionId,
                        CommunityId: communityId,
                        UserId: ""
                    }
                    axios.post(`${process.env.REACT_APP_BASE_URL}/api/Statistic/UploadStatisticIndividual`, stats,{
                        headers: { Authorization: header }
                    })
                        .then(response => {
                            console.log("Statistics updated successfully");
                        })
                        .catch(error => {
                            console.error("Error updating statistics:", error);
                        });
                }, 5000);
            }
        } else if (communityId !== undefined && collectionId !== undefined) {
            if (localStorage.getItem('stopCount') === 'FALSE') {
                localStorage.setItem('stopCount', 'TRUE');
                timeoutId = setTimeout(() => {
                    const date = new Date();
                    const month = date.getMonth() + 1;
                    const year = date.getFullYear();
                    const day = date.getDay();
                    const stats = {
                        Year: year,
                        Month: month,
                        Day: day,
                        ViewOnDay: 1,
                        ItemId: null,
                        CollectionId: collectionId,
                        CommunityId: communityId,
                        UserId: ""
                    }
                    axios.post(`${process.env.REACT_APP_BASE_URL}/api/Statistic/UploadStatisticIndividual`, stats,{
                        headers: { Authorization: header }
                    })
                        .then(response => {
                            console.log("Statistics updated successfully");
                        })
                        .catch(error => {
                            console.error("Error updating statistics:", error);
                        });
                }, 5000);
            }

        } else if (communityId !== undefined) {
            if (localStorage.getItem('stopCount') === 'FALSE') {
                localStorage.setItem('stopCount', 'TRUE');
                timeoutId = setTimeout(() => {
                    const date = new Date();
                    const month = date.getMonth() + 1;
                    const year = date.getFullYear();
                    const day = date.getDay();
                    const stats = {
                        Year: year,
                        Month: month,
                        Day: day,
                        ViewOnDay: 1,
                        ItemId: null,
                        CollectionId: null,
                        CommunityId: communityId,
                        UserId: ""
                    }
                    axios.post(`${process.env.REACT_APP_BASE_URL}/api/Statistic/UploadStatisticIndividual`, stats,{
                        headers: { Authorization: header }
                    })
                        .then(response => {
                            console.log("Statistics updated successfully");
                        })
                        .catch(error => {
                            console.error("Error updating statistics:", error);
                        });
                }, 5000);
            }


        }

    }, [communityId], [collectionId], [itemId]);

    return;
}

export default Counter;