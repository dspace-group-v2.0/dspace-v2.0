import React from 'react';
import { Container, Typography, Button } from '@mui/material';
import { Link, useNavigate } from 'react-router-dom'; // if using react-router
import { Helmet } from 'react-helmet';
import { jwtDecode } from 'jwt-decode';

const NotFound = () => {
  const navigate = useNavigate();
  const token = localStorage.getItem('Token');
  const role = token ? jwtDecode(token)["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"] : null;

  const handleClick = () => {
    if (role !== null) {
      if (role === "STUDENT" || role === "LECTURER") {
        navigate('/Homepage');
      } else if (role === "STAFF") {
        navigate('/HomepageStaff');
      } else if (role === "ADMIN") {
        navigate('/HomepageAdmin');
      }
    } else {
      navigate('/Login');
    }
  }
  
  return (
    <Container maxWidth="sm" style={{ marginTop: '4rem' }}>
      <Helmet>
        <meta charSet="utf-8" />
        <title>404 Not Found - dSPACE</title>
      </Helmet>
      <Typography variant="h1" align="center" gutterBottom>
        404
      </Typography>
      <Typography variant="h4" align="center" paragraph>
        Oops! Page not found.
      </Typography>
      <Typography variant="body1" align="center" paragraph>
        The page you are looking for might have been removed, had its name changed,
        or is temporarily unavailable.
      </Typography>
      <div style={{ textAlign: 'center', marginTop: '2rem' }}>
        <Button variant="contained" color="primary" onClick={handleClick}>
          Go to Home
        </Button>
      </div>
    </Container>
  );
};
export default NotFound;