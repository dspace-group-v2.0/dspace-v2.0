import { useNavigate, useParams } from "react-router-dom";
import Header from "./Header/Header";
import { Helmet } from "react-helmet";
import { jwtDecode } from "jwt-decode";

const PdfViewer = () => {
    const { fileId } = useParams();
    const processedFileId = fileId.split('@')[0];
    const itemId = fileId.split('@')[1];
    const token = localStorage.getItem('Token');
    const userRole = token ? jwtDecode(token)["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"] : null;
    const navigate = useNavigate();

    const handleClickBackToItem = (itemId) => {
        if (userRole !== null) {
            if (userRole === 'LECTURER' || userRole === 'STUDENT') {
                navigate(`/DSpace/ItemDetail/${itemId}`);
            } else if (userRole === 'STAFF') {
                navigate(`/DSpace/ItemDetail/${itemId}`);
            } else if (userRole === 'ADMIN') {
                navigate(`/DSpace/ItemDetail/${itemId}`);
            }
        }
    };
    return (
        <div>
            <Header />
            <Helmet>
                <meta charSet="utf-8" />
                <title>PDF Viewer - dSPACE</title>
            </Helmet>
            <div className="d-flex flex-column" style={{ backgroundColor: "darkkhaki" }}>
                <div className="d-flex align-items-start mt-2 ms-2">
                    <button type="button" className="btn btn-outline-dark" onClick={() => handleClickBackToItem(itemId)}>Back to Item</button>
                </div>

                <div className="d-flex align-items-center flex-column w-100 mt-2 " >
                    <iframe src={`https://drive.google.com/file/d/${processedFileId}/preview`} width="40%" height="860px" allow="autoplay"></iframe>
                </div>
            </div>

        </div>

    )
};
export default PdfViewer;
