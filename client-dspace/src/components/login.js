import React, { useState } from 'react';
import Container from '@mui/material/Container';
import Box from '@mui/material/Box';
import { GoogleLogin } from '@react-oauth/google';
import { jwtDecode } from 'jwt-decode';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { Helmet } from 'react-helmet';
const Login = () => {
    const navigate = useNavigate();
    const [selectedCampus, setSelectedCampus] = useState('');
    const handleChange = (event) => {
        setSelectedCampus(event.target.value);
    };
    const [formdata, setFormdata] = useState({
        email: '',
        given_name: '',
        family_name: ''
    });


    const handleGoogleLogin = async (credentialResponse) => {
        const credentialResponseDecoded = jwtDecode(credentialResponse.credential);
        console.log(credentialResponseDecoded);
        setFormdata({
            email: credentialResponseDecoded.email,
            given_name: credentialResponseDecoded.given_name,
            family_name: credentialResponseDecoded.family_name,
            hd: credentialResponseDecoded.hd,
        });

        try {
            const apiResponse = await axios.post(`${process.env.REACT_APP_BASE_URL}/api/Authentication/Login`, credentialResponseDecoded);

            if (apiResponse.data.role === 'STUDENT' || apiResponse.data.role === 'LECTURER') {
                navigate('/Homepage');
            } else if (apiResponse.data.role === 'STAFF') {
                navigate('/HomepageStaff');
            } else if (apiResponse.data.role === 'ADMIN') {
                navigate('/HomepageAdmin');
            }

            localStorage.setItem('Name', apiResponse.data.name);
            localStorage.setItem('Token', apiResponse.data.token);
            window.dispatchEvent(new Event("storageChanged"));
        } catch (error) {
            console.log('Login Failed:', error);
        }
    };
    const BASE_URL = process.env.REACT_APP_BASE_URL;
    console.log(BASE_URL)
    return (

        <Container
            maxWidth={false}
            disableGutters
            className="d-flex align-items-center justify-content-center vh-100"
            style={{ backgroundColor: '#e0e7f5' }}
        >
            <Helmet>
                <meta charSet="utf-8" />
                <title>Login - dSPACE</title>
            </Helmet>
            <Box
                className="w-100"
                sx={{
                    maxWidth: 1200,
                    mx: 'auto',
                    border: '1px solid #ccc',
                    borderRadius: 1,
                    overflow: 'hidden'
                }}
            >
                <Box
                    className="row no-gutters"
                >
                    <Box
                        className="col-md-6 d-flex flex-column align-items-center justify-content-center text-white"
                        sx={{
                            background: 'linear-gradient(90deg, #1e3c72, #2a5298)',
                            padding: '50px'
                        }}
                    >
                        <h3>Welcome to</h3>
                        <img src='../../fpt-logo.png' style={{ width: 220, height: 220, margin: 'auto' }} alt="Background" />
                        <h2>FPT Education DSpace</h2>
                        <p>*dSpace: Digital Signal Processing and Control Engineering is an open source repository software package typically used for creating open access repositories for scholarly and/or published digital content</p>
                    </Box>
                    <Box
                        className="col-md-6 d-flex align-items-center justify-content-center"
                        sx={{
                            background: 'white',
                            padding: '50px'
                        }}
                    >
                        <Box className='d-flex flex-column align-items-center' sx={{ width: '100%', maxWidth: 400 }}>
                            <img src='../../reader.png' style={{ width: 100, height: 100, margin: 'auto' }} alt="Background" />
                            <h4>Sign in with your Google Account</h4>
                            <div className='d-flex flex-row align-items-start w-100 mt-3 justify-content-center'>
                                {/* <FormControl sx={{ mt: 2, width: 180, margin: 0 }}>
                                        <InputLabel id="demo-simple-select-label" sx={{ fontSize: '0.9rem', mt:-0.8 }}>Select Campus</InputLabel>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="demo-simple-select"
                                            value={selectedCampus}
                                            label="Select Campus"
                                            onChange={handleChange}
                                            sx={{ height: 40 }}
                                        >

                                            <MenuItem value={'FU - Hòa Lạc'}>FU - Hòa Lạc</MenuItem>
                                            <MenuItem value={'FU - Hồ Chí Minh'}>FU - Hồ Chí Minh</MenuItem>
                                            <MenuItem value={'FU - Đà Nẵng'}>FU - Đà Nẵng</MenuItem>
                                            <MenuItem value={'FU - Cần Thơ'}>FU - Cần Thơ</MenuItem>
                                            <MenuItem value={'FU - Quy Nhơn'}>FU - Quy Nhơn</MenuItem>
                                        </Select>
                                    </FormControl> */}
                                <GoogleLogin
                                    onSuccess={handleGoogleLogin}
                                    render={renderProps => (
                                        <button onClick={renderProps.onClick} disabled={renderProps.disabled}>This is my custom Google button</button>
                                    )}
                                    onError={() => {
                                        console.log('Login Failed');
                                    }}
                                />

                            </div>

                            {/* <p className="fw-bolder mt-2" >or</p>
                            <button type="button" className="btn btn-primary">
                                Sign in with FeID
                            </button> */}

                        </Box>
                    </Box>
                </Box>
            </Box>
        </Container>
    );
};

export default Login;