import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { useNavigate } from 'react-router-dom';
import './Header.css';
import Menu from '../Menu/Menu';
import MenuStaff from '../Menu/MenuStaff';
import Counter from '../Statistics/counterView';
import Noti from "../../components/Statistics/noti.js";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css'
import { jwtDecode } from 'jwt-decode';

function Header() {
  const token = localStorage.getItem('Token');
  const userRole = token ? jwtDecode(token)["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"] : null;
  const navigate = useNavigate();
  return (

    <Navbar expand="lg" className="header-nav">
      <Counter />
      <div className="menu-icon">
        {userRole === "STAFF" && (<MenuStaff />)}
        {userRole === "ADMIN" && (<Menu />)}
      </div>
      <Container>
        {userRole === "ADMIN" && (<Navbar.Brand href="/HomepageAdmin">FPT Education on DSpace V2.0</Navbar.Brand>)}
        {userRole === "STAFF" && (<Navbar.Brand href="/HomepageStaff">FPT Education on DSpace V2.0</Navbar.Brand>)}
        {(userRole === "STUDENT" || userRole === "LECTURER") && (<Navbar.Brand href="/Homepage">FPT Education on DSpace V2.0</Navbar.Brand>)}

        {(userRole === "ADMIN" || userRole === "STAFF") && (
          <Navbar.Brand href="/Dspace/Community/DirectoryTree" style={{ marginLeft: '5%' }}>Directory tree</Navbar.Brand>
        )}

        <ToastContainer style={{ width: '600px' }} position="top-right" newestOnTop />
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
          </Nav>
          <Noti />
          <Nav style={{ paddingLeft: '5%' }}>
            {localStorage.getItem('Token') != null ? (
              <div>
                <Nav.Link href="#link">Welcome {localStorage.getItem("Name")} - {userRole}</Nav.Link>
                <Nav.Link onClick={
                  async () => {
                    localStorage.removeItem("Token");
                    localStorage.removeItem("Name");
                    window.dispatchEvent(new Event("storageChanged"));
                    navigate('/');
                  }
                }>Logout</Nav.Link>
              </div>
            ) : (
              <p><Nav.Link href="/Login">Login</Nav.Link></p>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default Header;
