import React, { useEffect, useState, useCallback } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import Header from './Header/Header';
import { Helmet } from 'react-helmet';
import ExpandCircleDownIcon from '@mui/icons-material/ExpandCircleDown';
import Badge from '@mui/material/Badge';
import { jwtDecode } from 'jwt-decode';
const CommunityTree = () => {
    const [data, setData] = useState([]);
    const [expandedCommunities, setExpandedCommunities] = useState({});
    const token = localStorage.getItem('Token');
    const navigate = useNavigate();

    useEffect(() => {
        fetch(`${process.env.REACT_APP_BASE_URL}/api/Community/GetCommunityTree`)
            .then(response => response.json())
            .then(data => setData(data))
            .catch(error => console.error('Error fetching data:', error));
    }, []);

    const toggleExpand = (id) => {
        setExpandedCommunities(prevState => ({
            ...prevState,
            [id]: !prevState[id],
        }));
    };

    const renderCollections = (collections) => {
        return (
            <ul>
                {collections.map(collection => (
                    <li key={collection.collectionId}>
                        <Badge sx={{
                                '& .MuiBadge-badge': {
                                },
                            }} color="primary" badgeContent={collection.totalItems} max={999}>
                        <span style={{marginRight: '5px'}} className="collection-name" onClick={() => handleCollectionClick(collection)}>
                            {collection.collectionName}
                        </span>
                        </Badge>
                    </li>
                ))}
            </ul>
        );
    };

    const renderSubCommunities = (subCommunities) => {
        return (
            <ul>
                {subCommunities.map(subCommunity => (
                    <li key={subCommunity.communityId}>
                        <strong className="community-name">
                            <Badge sx={{
                                '& .MuiBadge-badge': {
                                    // Bạn có thể tùy chỉnh vị trí của badge nếu cần
                                },
                            }} color="primary" badgeContent={subCommunity.totalItem} max={999}>
                                <Link to={`/Dspace/Community/CommunityDetail/${subCommunity.communityId}`} className="community-link">
                                    {subCommunity.communityName}
                                </Link>
                            </Badge>
    
                            <span
                                onClick={() => toggleExpand(subCommunity.communityId)}
                                className={`toggle-arrow ${expandedCommunities[subCommunity.communityId] ? 'expanded' : ''}`}
                            >
                                <ExpandCircleDownIcon />
                            </span>
                        </strong>
                        <div className={`sub-list ${expandedCommunities[subCommunity.communityId] ? 'expanded' : 'collapsed'}`}>
                            {subCommunity.subCommunities.length > 0 && renderSubCommunities(subCommunity.subCommunities)}
                            {subCommunity.collectionDTOs.length > 0 && renderCollections(subCommunity.collectionDTOs)}
                        </div>
                    </li>
                ))}
            </ul>
        );
    };
    

    const renderCommunities = (communities) => {
        return (
            <ul>
                {communities.map(community => (
                    <li key={community.communityId}>
                        <strong className="community-name">
                            <Badge sx={{
                                '& .MuiBadge-badge': {
                                    // transform: 'translateX(25px)',
                                },
                            }} color="primary" badgeContent={community.totalItem} max={999}>
                                <Link to={`/Dspace/Community/CommunityDetail/${community.communityId}`} className="community-link">
                                    {community.communityName}
                                </Link>
                            </Badge>

                            <span
                                onClick={() => toggleExpand(community.communityId)}
                                className={`toggle-arrow ${expandedCommunities[community.communityId] ? 'expanded' : ''}`}
                            >
                                <ExpandCircleDownIcon />
                            </span>
                        </strong>
                        <div className={`sub-list ${expandedCommunities[community.communityId] ? 'expanded' : 'collapsed'}`}>
                            {community.subCommunities.length > 0 && renderSubCommunities(community.subCommunities)}
                            {community.collectionDTOs.length > 0 && renderCollections(community.collectionDTOs)}
                        </div>
                    </li>
                ))}
            </ul>
        );
    };

    const handleCollectionClick = useCallback((collection) => {
        const role = token ? jwtDecode(token)["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"] : null;
        const url = role === 'ADMIN'
            ? `/Dspace/Collection/CollectionDetail/${collection.collectionId}?colName=${collection.collectionName}`
            : `/Dspace/Collection/CollectionDetail/${collection.collectionId}?colName=${collection.collectionName}`;
        navigate(url);
    }, [navigate]);

    return (
        <div className="homepage-reader">
            <Header />
            <Helmet>
                <meta charSet="utf-8" />
                <title>Directory Tree - dSPACE</title>
            </Helmet>
            <div className="content-container">
                <div className="container">
                    <div className="row">
                        <div style={{ marginBottom: '5%' }}>
                            <img
                                src="/FPT.jpg"
                                alt="Background"
                                className="background-image"
                            />
                        </div>
                        <div className="col-12">
                            <h1>Communities, Subcommunities, and Collections</h1>
                            {renderCommunities(data)}
                        </div>
                    </div>
                </div>
            </div>
            <div className="ft">
                <div className="container">
                    <div className="row">
                        <div className="col-12 text-center">
                            <span>&copy; 2023 - FPT Education | <Link to="/about-us">About us</Link></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CommunityTree;
