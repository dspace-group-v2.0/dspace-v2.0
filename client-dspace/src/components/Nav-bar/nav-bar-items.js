import React, { useState, useEffect } from 'react';
import { NavLink, useParams } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import {
    List, ListItem, ListItemIcon, ListItemText, Collapse
} from '@material-ui/core';
import {
    ExpandLess, ExpandMore, Add as NewIcon, Edit as EditIcon, RotateLeft, Search, VisibilityOff, Public,
    ImportExport as ImportIcon, GetApp as ExportIcon, DateRange as DateRangeIcon, Class, MenuBook, SwapVert, AssignmentInd, VpnKey,
    Search as AdminSearchIcon, Book as RegistriesIcon, RateReview,
    Assignment as CurationTaskIcon, Settings as ProcessesIcon,
    Build as WorkflowAdminIcon, LocalHospital as HealthIcon,
    NotificationImportant as SystemWideAlertIcon, Error
} from '@material-ui/icons';
import Chip from '@mui/material/Chip';
import IconButton from '@mui/material/IconButton';

import Select from 'react-select';
import { TagsInput } from "react-tag-input-component";
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Slider from '@mui/material/Slider';
import { pink } from '@mui/material/colors';
import { green } from '@mui/material/colors';
import Radio from '@mui/material/Radio';
import { FormControlLabel, FormGroup } from '@mui/material';
import Checkbox from '@mui/material/Checkbox';


const useStyles = makeStyles({
    root: {
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
        // backgroundColor: '#f5f5f5',
        // margin: '20px',
        // width: '250px', // Automatically adjust width on hover

    },
    listItem: {
        display: 'flex',
        alignItems: 'center',
        padding: '10px',
        marginBottom: '10px',
        borderRadius: '5px',
        cursor: 'pointer',
        backgroundColor: '#F8F9FA',
        // justifyContent: 'space-between', width: '100%'
    },
    listItem2: {
        display: 'flex',
        alignItems: 'center',
        borderRadius: '5px',
        cursor: 'pointer',
        backgroundColor: '#F8F9FA'
    },
    listItemIcon: {
        marginRight: '10px',
    },
    listItemText: {
        opacity: 1, // Show text immediately
    },

});

const ManagementMenu = ({ startDate, endDate, setStartDate, setEndDate, numPerPage, setNumPerPage, sortState, setSortState, selectedAuthors, setSelectedAuthors, selectedKeywords, setSelectedKeywords, selectedTypes, setSelectedTypes, searchQuery, setSearchQuery, filterSubmit, discoverable, setDiscoverable }) => {
    const classes = useStyles();
    const [openDate, setopenDate] = useState(false);
    const [openType, setOpenType] = useState(false);
    const [openNumPerPage, setOpenNumPerPage] = useState(false);
    const [openSort, setOpenSort] = useState(false);
    const [openAuthors, setOpenAuthors] = useState(false);
    const [openPublishers, setOpenPublishers] = useState(false);
    const [openKeywords, setOpenKeywords] = useState(false);
    // const [searchQuery, setSearchQuery] = useState('');
    const [authors, setAuthors] = useState([]);
    const [keywords, setKeywords] = useState([]);
    const { collectionId } = useParams();

    // const [selectedAuthors, setSelectedAuthors] = useState([]);

    useEffect(() => {
        async function fetchAuthors() {
            const response = await fetch(`${process.env.REACT_APP_BASE_URL}/api/Author/getListOfAuthors`);
            const data = await response.json();
            setAuthors(data);
        }
        fetchAuthors();
    }, []);
    // useEffect(() => {
    //     async function fetchKeywords() {
    //         const response = await fetch(`${process.env.REACT_APP_BASE_URL}/api/Author/getListOfAuthors`);
    //         const data = await response.json();
    //         setKeywords(data);
    //     }
    //     fetchKeywords();
    // }, []);
    // const [selectedValue, setSelectedValue] = useState('');

    // const [startDate, setStartDate] = useState(null);
    // const [endDate, setEndDate] = useState(null);
    const handleDateClick = () => {
        setopenDate(!openDate);
    };
    const handleTypeClick = () => {
        setOpenType(!openType);
    };
    const handleNumClick = () => {
        setOpenNumPerPage(!openNumPerPage);
    };
    const handleSortClick = () => {
        setOpenSort(!openSort);
    };
    const handleAuthorsClick = () => {
        setOpenAuthors(!openAuthors);
    };
    const handlePublishersClick = () => {
        setOpenPublishers(!openPublishers);
    };
    const handleKeywordsClick = () => {
        setOpenKeywords(!openKeywords);
    };
    const handleChange = (e) => {
        const { name, value, type, checked } = e.target;
        const newValue = type === 'checkbox' ? checked : value;
        setNumPerPage(e.target.value)
    };
    const handleSortChange = (e) => {
        const { name, value, type, checked } = e.target;
        const newValue = type === 'checkbox' ? checked : value;
        setSortState(e.target.value)
        // filterSubmit(true);
    };
    const handleAuthorsChange = (e) => {
        const { name, value, type, checked } = e.target;
        const newValue = type === 'checkbox' ? checked : value;
        // setSortState(e.target.value)
    };
    const handleKeywordsChange = (e) => {
        const { name, value, type, checked } = e.target;
        const newValue = type === 'checkbox' ? checked : value;
        // setSortState(e.target.value)
    };
    const handleAuthorCheckboxChange = (event, authorId) => {
        if (event.target.checked) {
            setSelectedAuthors([...selectedAuthors, authorId]);
        } else {
            setSelectedAuthors(selectedAuthors.filter((id) => id !== authorId));
        }
    };
    // console.log(selectedAuthors);
    const handlePublishersChange = (e) => {
        setSearchQuery(e.target.value);
    };
    const AuthorSelect = ({ authors, selectedAuthors, handleAuthorSelectChange }) => {
        const options = authors.map(author => {
            let label = author.fullName;
            if (author.jobTitle !== null) {
                if (author.jobTitle.trim() !== '') {
                    label += " - " + author.jobTitle;
                }
            } else {
                author.jobTitle = ''
            }
            return {
                value: author.authorId.toString(),
                label: label
            };
        });

        const selectedOptions = options.filter(option =>
            selectedAuthors.includes(option.value)
        );

        return (
            <Select
                isMulti
                options={options}
                value={selectedOptions}
                onChange={selectedOptions => handleAuthorSelectChange(selectedOptions ? selectedOptions.map(option => option.value) : [])}
                placeholder="Search and select contributors..."
                isClearable
                styles={{
                    container: (base) => ({ ...base, width: '100%' }),
                    menu: (provided) => ({
                        ...provided,
                        maxHeight: '200px',
                        overflowY: 'auto',
                    }),
                    menuList: (provided) => ({
                        ...provided,
                        maxHeight: '200px',
                        overflowY: 'auto',
                    }),
                }}
            />
        );
    };
    const handleAuthorSelectChange = (selectedAuthorIds) => {
        setSelectedAuthors(selectedAuthorIds);
    };
    console.log(selectedAuthors);

    const resetFilters = () => {
        setStartDate(null);
        setEndDate(null);
        setSelectedAuthors([]);
        setSelectedKeywords([]);
        setSearchQuery('');
        setSelectedTypes([]);
        setOpenKeywords(false);
        setOpenPublishers(false);
        setOpenAuthors(false);
        setOpenType(false);
        setopenDate(false);
        setValue([1900, getCurrentYear()]);
        setSelectedOptions({
            undiscoverable: true,
            published: true,
        });
        setDiscoverable(null)
        setResetting(true);
    };
    const types = [
        'Animation', 'Article', 'Book', 'Book chapter', 'Dataset', 'Learning Object',
        'Image', 'Image, 3-D', 'Map', 'Musical Score', 'Plan or blueprint', 'Preprint', 'Presentation', 'Recording, acoustical', 'Recording, musical', 'Recording, oral'
        , 'Software', 'Technical Report', 'Thesis', 'Video', 'Working Paper', 'Other'
    ];
    const TypesSelect = ({ types, selectedTypes, setSelectedTypes }) => {
        const options = types.map(type => ({
            value: type,
            label: type
        }));

        const selectedOptions = options.filter(option =>
            selectedTypes.includes(option.value)
        );

        return (
            <Select
                isMulti
                options={options}
                value={selectedOptions}
                onChange={selectedOptions => handleTypeSelectChange(selectedOptions ? selectedOptions.map(option => option.value) : [])}
                placeholder="Search and select types"
                isClearable
                styles={{
                    container: (base) => ({ ...base, width: '100%' }),
                    menu: (provided) => ({
                        ...provided,
                        maxHeight: '200px',
                        overflowY: 'auto',
                    }),
                    menuList: (provided) => ({
                        ...provided,
                        maxHeight: '200px',
                        overflowY: 'auto',
                    }),
                }}
            />
        );
    };
    const handleTypeSelectChange = (selectedTypes) => {
        setSelectedTypes(selectedTypes);
    };


    const KeywordSelect = ({ keywords, selectedKeywords, handleKeywordSelectChange }) => {
        const options = keywords.map(keyword => ({
            value: keyword.authorId,
            label: keyword.firstName
        }));

        const selectedOptions = options.filter(option =>
            selectedKeywords.includes(option.value)
        );

        return (
            <Select
                isMulti
                options={options}
                value={selectedOptions}
                onChange={selectedOptions => handleKeywordSelectChange(selectedOptions ? selectedOptions.map(option => option.value) : [])}
                placeholder="Select keywords..."
                isClearable
                styles={{
                    container: (base) => ({ ...base, width: '100%' }),
                    menu: (provided) => ({
                        ...provided,
                        maxHeight: '200px',
                        overflowY: 'auto',
                    }),
                    menuList: (provided) => ({
                        ...provided,
                        maxHeight: '200px',
                        overflowY: 'auto',
                    }),
                }}
            />
        );
    };
    const handleKeywordSelectChange = (newKeywords) => {
        setSelectedKeywords(newKeywords);
    };
    console.log(selectedKeywords);
    const [resetting, setResetting] = useState(false);
    useEffect(() => {
        if (resetting) {
            filterSubmit(true);
            setResetting(false); // Đặt trạng thái reset về false sau khi gọi filterSubmit
        }
    }, [resetting, filterSubmit]);
    // useEffect(() => {
    //     if(collectionId >= 0){
    //         filterSubmit(true);
    //     }
    // }, [sortState]);

    const handleClick = () => {
        resetFilters();
    };
    const getCurrentYear = () => new Date().getFullYear();

    function valuetext(value) {
        return `${value}`;
    }
    const [value, setValue] = useState([1900, getCurrentYear()]);

    const handleChangeSlider = (event, newValue) => {
        setValue(newValue);
        setStartDate(newValue[0]);
        setEndDate(newValue[1]);
    };

    const handleInputChange = (index) => (event) => {
        const newValue = event.target.value === '' ? '' : Number(event.target.value);
        const updatedValue = [...value];
        updatedValue[index] = newValue;
        setValue(updatedValue);
        if (index === 0) {
            setStartDate(newValue);
        } else {
            setEndDate(newValue);
        }
    };

    const handleBlur = () => {
        const updatedValue = value.map((v, i) => {
            if (v === '' || v < 1900) return 1900;
            if (v > getCurrentYear()) return getCurrentYear();
            return v;
        });
        if (updatedValue[0] > updatedValue[1]) {
            updatedValue[1] = updatedValue[0];
        }
        setValue(updatedValue);
        setStartDate(updatedValue[0]);
        setEndDate(updatedValue[1]);
    };

    console.log(value)
    console.log(startDate)
    console.log(endDate)


    const [selectedOptions, setSelectedOptions] = useState({
        undiscoverable: true,
        published: true,
    });

    const handleChangeStatus = (event) => {
        const { name, checked } = event.target;

        // Update state but ensure that at least one option is selected
        setSelectedOptions(prev => {
            const updatedOptions = { ...prev, [name]: checked };

            // Ensure at least one option is selected
            if (!updatedOptions.undiscoverable && !updatedOptions.published) {
                return prev; // Revert to previous state
            }

            // Update discoverable based on new options
            setDiscoverable(getStatus(updatedOptions));

            return updatedOptions;
        });
    };



    const getStatus = (options = selectedOptions) => {
        if (options.undiscoverable && options.published) {
            return null;
        } else if (options.undiscoverable) {
            return false;
        } else if (options.published) {
            return true;
        }
        return null;
    };
    console.log(selectedOptions)
    console.log(discoverable)
    function Status({ status, checked }) {
        const getStatusText = (status) => {
            if (status == true) {
                return "Published";
            } else {
                return "Undiscoverable";
            }
        };

        const getStatusColor = (status, checked) => {
            if (checked) {
                if (status === true) {
                    return "#28A745";
                } else {
                    return "#DC3545";
                }
            } else {
                return "#6c757d"; // Gray color for unchecked status
            }
        };

        const getBoxShadow = (status, checked) => {
            if (checked) {
                const color = getStatusColor(status, checked);
                return `0 0 10px 2px ${color}`;
            } else {
                return 'none'; // No shadow for unchecked status
            }
        };
        return (
            <p style={{ margin: '0' }}>
                <span style={{
                    margin: '0',
                    backgroundColor: getStatusColor(status, checked),
                    borderRadius: '3px',
                    color: 'white',
                    padding: '2px',
                    fontSize: 'small',
                    boxShadow: getBoxShadow(status, checked)

                }}>
                    {getStatusText(status)}
                </span>
            </p>
        );
    }
    return (
        <div className={classes.root}>
            {collectionId && (<>
                <h2 style={{ color: '#757575', marginBottom: '' }}>Filters</h2>
                <List>
                    <ListItem button onClick={handleDateClick} className={classes.listItem}>
                        <ListItemIcon className={classes.listItemIcon}>
                            <DateRangeIcon />
                        </ListItemIcon>
                        <ListItemText style={{ marginTop: '' }} primary="Date range" className={classes.listItemText} />
                        {openDate ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                    <Collapse in={openDate} timeout="auto" unmountOnExit>
                        <List component="div" disablePadding>
                            {/* <ListItem>
                            <ListItemIcon className='col-md-5'>
                                Start Date:
                            </ListItemIcon>
                            <ListItemText>
                                <input
                                    type="date"
                                    value={startDate ? startDate.toISOString().slice(0, 10) : ''}
                                    onChange={(e) => setStartDate(e.target.value ? new Date(e.target.value) : null)}
                                />
                            </ListItemText>
                        </ListItem>
                        <ListItem>
                            <ListItemIcon className='col-md-5'>
                                End Date:
                            </ListItemIcon>
                            <ListItemText>

                                <input
                                    type="date"
                                    value={endDate ? endDate.toISOString().slice(0, 10) : ''}
                                    onChange={(e) => setEndDate(e.target.value ? new Date(e.target.value) : null)}
                                />
                            </ListItemText>
                        </ListItem> */}
                            <Box sx={{ width: '100%' }}>
                                <Slider
                                    getAriaLabel={() => 'Year range'}
                                    value={value.map(v => (v === '' ? 0 : v))} // Temporary fix for displaying the slider when input is empty
                                    min={1900}
                                    max={getCurrentYear()}
                                    onChange={handleChangeSlider}
                                    valueLabelDisplay="auto"
                                    getAriaValueText={valuetext}
                                    color='primary'
                                />
                                <Box sx={{ display: 'flex', justifyContent: 'space-between', mt: 2, mb: 2 }}>
                                    <TextField
                                        label="Start Year"
                                        type="number"
                                        value={value[0]}
                                        onChange={handleInputChange(0)}
                                        onBlur={handleBlur}
                                        inputProps={{ min: 1900, max: value[1] }}
                                        sx={{ ml: 2, mr: 1 }}
                                    />
                                    <TextField
                                        label="End Year"
                                        type="number"
                                        value={value[1]}
                                        onChange={handleInputChange(1)}
                                        onBlur={handleBlur}
                                        inputProps={{ min: value[0], max: getCurrentYear() }}
                                        sx={{ ml: 1, mr: 2 }}
                                    />
                                </Box>
                            </Box>
                        </List>
                    </Collapse>



                    {/* ====================================================== */}

                    <ListItem button onClick={handleAuthorsClick} className={classes.listItem}>
                        <ListItemIcon className={classes.listItemIcon}>
                            <AssignmentInd />
                        </ListItemIcon>
                        <ListItemText primary="Authors" className={classes.listItemText} />
                        {openAuthors ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>


                    <Collapse in={openAuthors} timeout="auto" unmountOnExit>

                        <div style={{ padding: '0 16px' }}>
                            {/* <List component="div" disablePadding> */}
                            <AuthorSelect
                                authors={authors}
                                selectedAuthors={selectedAuthors}
                                handleAuthorSelectChange={handleAuthorSelectChange}
                            />
                            {/* </List> */}
                        </div>

                    </Collapse>
                    {/* ====================================================== */}

                    <ListItem button onClick={handlePublishersClick} className={classes.listItem}>
                        <ListItemIcon className={classes.listItemIcon}>
                            <RateReview />
                        </ListItemIcon>
                        <ListItemText primary="Publishers" className={classes.listItemText} />
                        {openPublishers ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                    <Collapse in={openPublishers} timeout="auto" unmountOnExit>
                        <List component="div" disablePadding>
                            <ListItem>
                                <ListItemText>
                                    {/* <select
                                    className="form-control"
                                    name="parentCommunityId"
                                    value={''}
                                    onChange={handlePublishersChange}
                                >
                                    <option value=''></option>
                                </select> */}
                                    <TextField id="outlined-basic" label="Search publisher" variant="outlined" value={searchQuery} onChange={handlePublishersChange} />

                                </ListItemText>
                            </ListItem>
                        </List>
                    </Collapse>
                    {/* ====================================================== */}
                    <ListItem button onClick={handleKeywordsClick} className={classes.listItem}>
                        <ListItemIcon className={classes.listItemIcon}>
                            <VpnKey />
                        </ListItemIcon>
                        <ListItemText primary="Keywords" className={classes.listItemText} />
                        {openKeywords ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                    <Collapse in={openKeywords} timeout="auto" unmountOnExit>
                        <div style={{ padding: '0 16px' }}>
                            {/* <KeywordSelect
                            keywords={keywords}
                            selectedKeywords={selectedKeywords}
                            handleKeywordSelectChange={handleKeywordSelectChange}
                        /> */}
                            <TagsInput
                                value={selectedKeywords}
                                onChange={handleKeywordSelectChange}
                                placeHolder="Input then click enter button"
                            />
                        </div>
                    </Collapse>

                    {/* ====================================================== */}
                    <ListItem button onClick={handleTypeClick} className={classes.listItem}>
                        <ListItemIcon className={classes.listItemIcon}>
                            <Class />
                        </ListItemIcon>
                        <ListItemText primary="Type" className={classes.listItemText} />
                        {openType ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                    <Collapse in={openType} timeout="auto" unmountOnExit>
                        <div style={{ padding: '0 16px' }}>
                            <TypesSelect
                                name="type"
                                types={types}
                                selectedTypes={selectedTypes}
                                handleTypeSelectChange={handleTypeSelectChange}
                                required
                            />
                        </div>
                    </Collapse>
                </List>
                <ListItem className={classes.listItem}>
                    <div>
                        <div className='d-flex'>
                            <ListItemIcon className={classes.listItemIcon}>
                                <Error />
                            </ListItemIcon>
                            <ListItemText primary="Status" className={classes.listItemText} />
                        </div>

                        <List component="div" disablePadding>
                            <ListItem>
                                <ListItemText>
                                    <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', width: '100%' }}>
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    hidden
                                                    checked={selectedOptions.undiscoverable}
                                                    onChange={handleChangeStatus}
                                                    name="undiscoverable"
                                                    sx={{
                                                        color: pink[800],
                                                        '&.Mui-checked': {
                                                            color: pink[600],
                                                        },
                                                    }}
                                                />
                                            }
                                            label={
                                                <IconButton
                                                    sx={{
                                                        color: selectedOptions.undiscoverable ? 'error.main' : 'default',
                                                        '&:hover': {
                                                            color: selectedOptions.undiscoverable ? 'error.dark' : 'default',
                                                        },
                                                    }}
                                                    aria-label="toggle undiscoverable"
                                                    onClick={() => handleChangeStatus({ target: { name: 'undiscoverable', checked: !selectedOptions.undiscoverable } })}
                                                >
                                                    <VisibilityOff />
                                                </IconButton>
                                            }
                                        />

                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    hidden
                                                    checked={selectedOptions.published}
                                                    onChange={handleChangeStatus}
                                                    name="published"
                                                    sx={{
                                                        color: green[800],
                                                        '&.Mui-checked': {
                                                            color: green[600],
                                                        },
                                                    }} />
                                            }
                                            label={
                                                <IconButton
                                                    sx={{
                                                        color: selectedOptions.published ? 'success.main' : 'default',
                                                        '&:hover': {
                                                            color: selectedOptions.published ? 'success.dark' : 'default',
                                                        },
                                                    }}
                                                    aria-label="toggle published"
                                                    onClick={() => handleChangeStatus({ target: { name: 'published', checked: !selectedOptions.published } })}
                                                >
                                                    <Public />
                                                </IconButton>
                                            }
                                        />
                                    </div>
                                </ListItemText>
                            </ListItem>
                        </List>
                    </div>
                </ListItem>

                <div className="d-flex mb-4 justify-content-between">
                    <div
                        className="btn d-flex"
                        style={{
                            backgroundColor: '#757575',
                            borderRadius: '4px',
                        }}
                        // onClick={resetFilters}
                        onClick={handleClick}
                    // onClick={() => handleButtonClick('list')}
                    >
                        <RotateLeft style={{ color: '#FFFFFF' }} />
                        <p style={{ margin: '0', color: 'white', fontWeight: 'bolder' }}>Reset filters</p>
                    </div>

                    <div
                        className="btn d-flex"
                        style={{
                            backgroundColor: '#757575',
                            borderRadius: '4px',
                        }}
                        onClick={filterSubmit}
                    // onClick={() => handleButtonClick('list')}
                    >
                        <AdminSearchIcon style={{ color: '#FFFFFF' }} />
                        <p style={{ margin: '0', color: 'white', fontWeight: 'bolder' }}>Filters</p>
                    </div>
                </div>
            </>)}
            <h2 style={{ color: '#757575', marginBottom: '' }}>Settings</h2>
            {/* ====================================================== */}
            <ListItem button className={classes.listItem}>
                <div>
                    <div className='d-flex'>
                        <ListItemIcon className={classes.listItemIcon}>
                            <SwapVert />
                        </ListItemIcon>
                        <ListItemText primary="Sort By" className={classes.listItemText} />
                    </div>

                    <List component="div" disablePadding>
                        <ListItem>
                            <ListItemText>
                                <select

                                    className="form-control"
                                    name="parentCommunityId"
                                    value={sortState}
                                    onChange={handleSortChange}
                                >
                                    <option value='1'>Title Ascending</option>
                                    <option value='2'>Title Descending</option>
                                    <option value='3'>Date Issue Ascending</option>
                                    <option value='4'>Date Issue Descending</option>
                                </select>
                            </ListItemText>
                        </ListItem>
                    </List>
                </div>
                {/* {openSort ? <ExpandLess /> : <ExpandMore />} */}
            </ListItem>

            {/* ====================================================== */}
            <ListItem button className={classes.listItem}>
                <div>
                    <div className='d-flex'>
                        <ListItemIcon className={classes.listItemIcon}>
                            <MenuBook />
                        </ListItemIcon>
                        <ListItemText primary="Items per page" className={classes.listItemText} />
                    </div>
                    <List component="div" disablePadding>
                        <ListItem>
                            <ListItemText>
                                <select
                                    style={{ width: '135%' }}
                                    className="form-control"
                                    name="parentCommunityId"
                                    value={numPerPage}
                                    onChange={handleChange}
                                >
                                    <option value='5'>5</option>
                                    <option value='10'>10</option>
                                    <option value='20'>20</option>
                                    <option value='30'>30</option>
                                    <option value='40'>40</option>
                                    <option value='50'>50</option>
                                </select>
                            </ListItemText>
                        </ListItem>
                    </List>
                </div>
                {/* {openNumPerPage ? <ExpandLess /> : <ExpandMore />} */}
            </ListItem>
            {/* <Collapse in={openNumPerPage} timeout="auto" unmountOnExit>
                
            </Collapse> */}
        </div>
    );
};

export default ManagementMenu;
