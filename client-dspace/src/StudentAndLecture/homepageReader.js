import React, { useEffect, useState, useCallback, useRef } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import Header from '../components/Header/Header';
import './homepageReader.css';
import { Search } from '@material-ui/icons';
import axios from 'axios';
import { Helmet } from 'react-helmet';
import ExpandCircleDownIcon from '@mui/icons-material/ExpandCircleDown';
import Badge from '@mui/material/Badge';
import { jwtDecode } from 'jwt-decode';

const HomepageReader = () => {
    const [searchQuery, setSearchQuery] = useState('');

    const token = localStorage.getItem('Token');
    const header = `Bearer ${token}`;
    const [data, setData] = useState([]);
    const [expandedCommunities, setExpandedCommunities] = useState({});
    const navigate = useNavigate();

    useEffect(() => {
        fetch(`${process.env.REACT_APP_BASE_URL}/api/CommunityUser/GetCommunityTree`)
            .then(response => response.json())
            .then(data => setData(data))
            .catch(error => console.error('Error fetching data:', error));
    }, []);

    const toggleExpand = (id) => {
        setExpandedCommunities(prevState => ({
            ...prevState,
            [id]: !prevState[id],
        }));
    };

    const renderCollections = (collections) => {
        return (
            <ul>
                {collections.map(collection => (
                    <li key={collection.collectionId}>
                        <Badge sx={{
                            '& .MuiBadge-badge': {
                                // transform: 'translateX(25px)', 
                            },
                        }} color="primary" badgeContent={collection.totalItems} max={999}>
                            <span style={{ marginRight: '5px' }} className="collection-name" onClick={() => handleCollectionClick(collection)}>
                                {collection.collectionName}
                            </span>
                        </Badge>
                    </li>
                ))}
            </ul>
        );
    };

    const renderSubCommunities = (subCommunities) => {
        return (
            <ul>
                {subCommunities.map(subCommunity => (
                    <li key={subCommunity.communityId}>
                        <strong className="community-name">
                            <Badge sx={{
                                '& .MuiBadge-badge': {
                                    // Bạn có thể tùy chỉnh vị trí của badge nếu cần
                                },
                            }} color="primary" badgeContent={subCommunity.totalItem} max={999}>
                                <Link to={`/Dspace/Community/CommunityDetail/${subCommunity.communityId}`} className="community-link">
                                    {subCommunity.communityName}
                                </Link>
                            </Badge>

                            <span
                                onClick={() => toggleExpand(subCommunity.communityId)}
                                className={`toggle-arrow ${expandedCommunities[subCommunity.communityId] ? 'expanded' : ''}`}
                            >
                                <ExpandCircleDownIcon />
                            </span>
                        </strong>
                        <div className={`sub-list ${expandedCommunities[subCommunity.communityId] ? 'expanded' : 'collapsed'}`}>
                            {subCommunity.subCommunities.length > 0 && renderSubCommunities(subCommunity.subCommunities)}
                            {subCommunity.collectionDTOs.length > 0 && renderCollections(subCommunity.collectionDTOs)}
                        </div>
                    </li>
                ))}
            </ul>
        );
    };


    const renderCommunities = (communities) => {
        return (
            <ul>
                {communities.map(community => (
                    <li key={community.communityId}>
                        <strong className="community-name">
                            <Badge sx={{
                                '& .MuiBadge-badge': {
                                    // transform: 'translateX(25px)',
                                },
                            }} color="primary" badgeContent={community.totalItem} max={999}>
                                <Link to={`/Dspace/Community/CommunityDetail/${community.communityId}`} className="community-link">
                                    {community.communityName}
                                </Link>
                            </Badge>

                            <span
                                onClick={() => toggleExpand(community.communityId)}
                                className={`toggle-arrow ${expandedCommunities[community.communityId] ? 'expanded' : ''}`}
                            >
                                <ExpandCircleDownIcon />
                            </span>
                        </strong>
                        <div className={`sub-list ${expandedCommunities[community.communityId] ? 'expanded' : 'collapsed'}`}>
                            {community.subCommunities.length > 0 && renderSubCommunities(community.subCommunities)}
                            {community.collectionDTOs.length > 0 && renderCollections(community.collectionDTOs)}
                        </div>
                    </li>
                ))}
            </ul>
        );
    };

    const handleCollectionClick = useCallback((collection) => {
        const role = token ? jwtDecode(token)["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"] : null;
        const url = role === 'ADMIN'
            ? `/Dspace/Collection/CollectionDetail/${collection.collectionId}?colName=${collection.collectionName}`
            : `/Dspace/Collection/CollectionDetail/${collection.collectionId}?colName=${collection.collectionName}`;
        navigate(url);
    }, [navigate]);

    const handleSearchChange = (event) => {
        setSearchQuery(event.target.value);
    };

    const handleSearchSubmit = () => {
        navigate(`/Dspace/Collection/ItemListReader/0?search=${searchQuery}`);
    };

    const handleViewSubscribedCollections = () => {
        navigate('/Dspace/SubscribedCollectionList');
    };
    const formatDate = (dateString) => {
        if (!dateString) return '';

        const parts = dateString.split('-');
        let formattedDate = '';

        if (parts.length === 3) {
            // Case: full date (YYYY-MM-DD)
            const [year, month, day] = parts;
            formattedDate = `${day.padStart(2, '0')}-${month.padStart(2, '0')}-${year}`;
        } else if (parts.length === 2) {
            // Case: year and month (YYYY-MM)
            const [year, month] = parts;
            formattedDate = `${month.padStart(2, '0')}-${year}`;
        } else if (parts.length === 1) {
            // Case: only year (YYYY)
            const [year] = parts;
            formattedDate = year;
        }

        return formattedDate;
    };
    return (
        <div className="homepage-reader">
            <Header />
            <Helmet>
                <meta charSet="utf-8" />
                <title>Homepage for User - dSPACE</title>
            </Helmet>
            <div className="content-container">
                <div className="container">
                    <div className="row">
                        <div style={{ marginBottom: '5%' }}>
                            <img
                                src="FPT.jpg"
                                alt="Background"
                                className="background-image"
                            />
                        </div>
                        <div className="col-12">
                            <p style={{ margin: '0', fontWeight: 'bold' }}>Search items by title</p>
                            <div className="search-container d-flex align-items-center mb-4">
                                <input
                                    type="text"
                                    className="form-control w-50"
                                    placeholder="Search items by title"
                                    style={{ borderRadius: '0', flex: '0 1 auto' }}
                                    value={searchQuery}
                                    onChange={handleSearchChange}
                                />
                                <button
                                    style={{ backgroundColor: '#43515F', color: 'white', borderRadius: '0 6px 6px 0' }}
                                    className="btn"
                                    onClick={handleSearchSubmit}
                                >
                                    <Search />
                                </button>
                            </div>

                            <h1>Communities in DSpace</h1>
                            <h4>Select a community to browse its collections</h4>
                            <div className="col-12">

                                {renderCommunities(data)}
                            </div>
                        </div>
                    </div>

                    <hr className="separator-line" />

                    <button
                        className="btn btn-primary mb-4"
                        onClick={handleViewSubscribedCollections}
                    >
                        View Subscribed Collections
                    </button>
                </div>
            </div>

            <div className="ft">
                <div className="container">
                    <div className="row">
                        <div className="col-12 text-center">
                            <span>&copy; 2023 - FPT Education | <Link to="/about-us">About us</Link></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default HomepageReader;