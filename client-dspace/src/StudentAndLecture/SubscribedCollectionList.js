import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Link, useNavigate } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from '../components/Header/Header';
import { Helmet } from 'react-helmet';
import { toast } from 'react-toastify';

const SubscribedCollectionList = () => {
    const [subscribedCollections, setSubscribedCollections] = useState([]);
    const [error, setError] = useState(null);
    const [searchQuery, setSearchQuery] = useState('');
    const [searchType, setSearchType] = useState('collectionName');
    const [currentPage, setCurrentPage] = useState(1);
    const itemsPerPage = 12;
    const token = localStorage.getItem('Token');
    const header = `Bearer ${token}`;
    const navigate = useNavigate();
    const addNotification = (message, type) => {
        toast(message, { type });
    };
    useEffect(() => {
        fetchSubscribedCollections();
    }, []);

    const fetchSubscribedCollections = () => {
        axios.get(`${process.env.REACT_APP_BASE_URL}/api/SubcribeUser/ViewListCollectionSubcribed`, {
            headers: { Authorization: header }
        })
            .then(response => {
                setSubscribedCollections(response.data);
            })
            .catch(error => {
                setError(error.message);
            });
    };

    const handleSearchChange = (e) => {
        setSearchQuery(e.target.value);
        setCurrentPage(1);
    };

    const handleSearchTypeChange = (e) => {
        setSearchType(e.target.value);
        setSearchQuery('');
    };

    const handleCollectionClick = (collection) => {
        navigate(`/Dspace/Collection/CollectionDetail/${collection.collectionId}?colName=${collection.collectionName}`);
    };

    const handleUnsubscribe = (collectionId) => {
        axios.post(`${process.env.REACT_APP_BASE_URL}/api/SubcribeUser/UnSubcribeCollection?collectionId=${collectionId}`, null, {
            headers: { Authorization: header }
        })
        .then(response => {
            addNotification(response.data, 'success');
            fetchSubscribedCollections();
        })
        .catch(error => {
            setError(error.message);
        });
    };

    const filteredCollections = subscribedCollections.filter(collection => {
        if (!searchQuery.trim()) return true;
        const normalizedSearchQuery = searchQuery.replace(/\s+/g, '').toLowerCase();

        switch (searchType) {
            case 'collectionName':
                return collection.collectionName.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery);
            case 'description':
                return collection.shortDescription.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery);
            case 'all':
                return Object.values(collection).some(value =>
                    typeof value === 'string' && value.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery)
                );
            default:
                return false;
        }
    });

    const indexOfLastItem = currentPage * itemsPerPage;
    const indexOfFirstItem = indexOfLastItem - itemsPerPage;
    const currentCollections = filteredCollections.slice(indexOfFirstItem, indexOfLastItem);

    const totalPages = Math.ceil(filteredCollections.length / itemsPerPage);

    const handlePageChange = (pageNumber) => {
        setCurrentPage(pageNumber);
    };

    const handleNextPage = () => {
        if (currentPage < totalPages) {
            setCurrentPage(prevPage => prevPage + 1);
        }
    };

    const handlePreviousPage = () => {
        if (currentPage > 1) {
            setCurrentPage(prevPage => prevPage - 1);
        }
    };
    function getRelativePath(fullPath) {
        const splitPath = fullPath.split('public\\');
        return splitPath[1];
    }
    return (
        <div>
            <Header />
            <Helmet>
                <meta charSet="utf-8" />
                <title>Subscribed Collections - dSPACE</title>
            </Helmet>
            <div className="container mt-5">
                {/* <div className="py-1 bg-light">
                    <Link to="/Homepage" className="text-decoration-none">Home </Link>
                    <span className="text-dark"><strong>| Subscribed Collections</strong></span>
                </div> */}
                <h1 className="mb-4">Subscribed Collections List</h1>
                <div className="row mb-4">
                    <div className="col-md-5">
                        <label htmlFor="searchInput" className="form-label fw-bold">Search</label>
                        <input
                            id="searchInput"
                            type="text"
                            className="form-control"
                            placeholder="Search collections..."
                            value={searchQuery}
                            onChange={handleSearchChange}
                        />
                    </div>
                    <div className="col-md-4">
                        <label htmlFor="searchTypeSelect" className="form-label fw-bold">Search By</label>
                        <select
                            id="searchTypeSelect"
                            className="form-select"
                            value={searchType}
                            onChange={handleSearchTypeChange}
                        >
                            <option value="all">Select...</option>
                            <option value="collectionName">Collection Name</option>
                            <option value="description">Description</option>
                        </select>
                    </div>
                </div>
                {error && <div className="alert alert-danger">{error}</div>}
                <table className="table table-striped table-bordered">
                    <thead className="thead-dark">
                        <tr>
                            <th>Id</th>
                            <th>Logo</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Subscribed</th>
                        </tr>
                    </thead>
                    <tbody>
                        {currentCollections.map((collection, index) => (
                            <tr key={collection.collectionId}>
                                <td>{index + 1 + (currentPage - 1) * itemsPerPage}</td>
                                <td>
                                {collection.logoUrl ? (
                                            // <img src={`/${getRelativePath(community.logoUrl)}`} alt="logo" className='img-thumbnail' style={{ width: '200px' }} />
                                            <img
                                                src={`/${getRelativePath(collection.logoUrl)}`}
                                                alt={collection.collectionName}
                                                style={{ width: '150px', height: '200px', objectFit: 'cover', marginRight: '20px', boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1)' }}
                                                onError={(e) => {
                                                    e.target.onerror = null;
                                                    e.target.src = '/No-Image-Placeholder.png';
                                                }}
                                            />
                                        ) : (
                                            <img
                                                src={`/No-Image-Placeholder.png`}
                                                alt={collection.collectionName}
                                                style={{ width: '150px', height: '200px', objectFit: 'cover', marginRight: '20px', boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1)' }}

                                            />
                                        )}
                                </td>
                                <td>
                                    <button
                                        className="btn btn-link p-0"
                                        style={{ textDecoration: 'none', color: 'black' }}
                                        onClick={() => handleCollectionClick(collection)}
                                    >
                                        {collection.collectionName}
                                    </button>
                                </td>
                                <td>
                                    <button
                                        className="btn btn-link p-0"
                                        style={{ textDecoration: 'none', color: 'black' }}
                                        onClick={() => handleCollectionClick(collection)}
                                    >
                                        {collection.shortDescription}
                                    </button>
                                </td>
                                <td>
                                    <button
                                        className="btn btn-secondary"
                                        onClick={() => handleUnsubscribe(collection.collectionId)}
                                    >
                                        Unsubscribe
                                    </button>
                                </td>
                            </tr>
                        ))}
                        {currentCollections.length === 0 && (
                            <tr>
                                <td colSpan="5" className="text-center">No collections found</td>
                            </tr>
                        )}
                    </tbody>
                </table>
                <div className="d-flex justify-content-center">
                    <nav>
                        <ul className="pagination">
                            <li className={`page-item ${currentPage === 1 ? 'disabled' : ''}`}>
                                <button className="page-link" onClick={handlePreviousPage}>Previous</button>
                            </li>
                            {[...Array(totalPages)].map((_, i) => (
                                <li key={i} className={`page-item ${currentPage === i + 1 ? 'active' : ''}`}>
                                    <button className="page-link" onClick={() => handlePageChange(i + 1)}>
                                        {i + 1}
                                    </button>
                                </li>
                            ))}
                            <li className={`page-item ${currentPage === totalPages ? 'disabled' : ''}`}>
                                <button className="page-link" onClick={handleNextPage}>Next</button>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    );
};

export default SubscribedCollectionList;
