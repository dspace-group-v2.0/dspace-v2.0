import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useParams, Link, useNavigate, useLocation } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from '../components/Header/Header';
import { VerticalAlignBottom } from '@material-ui/icons';
import { saveAs } from 'file-saver';
import Counter from '../components/Statistics/counterView';
import { Modal, Button, Form } from "react-bootstrap";
import { Helmet } from 'react-helmet';
import { jwtDecode } from 'jwt-decode';
const ItemDetails = () => {
    const { itemId } = useParams(); // Get the ID from the URL
    const [item, setItem] = useState(null);
    const [loading, setLoading] = useState(true);
    const navigate = useNavigate();
    const location = useLocation();
    const searchParams = new URLSearchParams(location.search);
    const colId = searchParams.get('colId');
    const [reason, setReason] = useState("");
    const token = localStorage.getItem('Token');
    const userRole = token ? jwtDecode(token)["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"] : null;
    const header = `Bearer ${token}`;
    useEffect(() => {
        // Fetch the community data from your API using the id from the URL
        axios.get(`${process.env.REACT_APP_BASE_URL}/api/Item/GetItemSimpleById/${itemId}`) // Replace with your actual API endpoint
            .then(response => {
                const fetchedItem = response.data;
                setItem(fetchedItem);
                setLoading(false);
            })
            .catch(error => {
                console.error('There was an error fetching the item!', error);
                setLoading(false);
            });

    }, [itemId]);
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const [showAccept, setShowAccept] = useState(false);
    const [showSubmitToReview, setShowSubmitToReview] = useState(false);
    const handleCloseAccept = () => setShowAccept(false);
    const handleShowAccept = () => setShowAccept(true);
    const handleCloseSubmitToReview = () => setShowSubmitToReview(false);
    const handleShowSubmitToReview = () => setShowSubmitToReview(true);
    const handleSubmitDecline = async () => {
        try {
            await axios.put(`${process.env.REACT_APP_BASE_URL}/api/ItemStatus/RejectItem?itemId=${itemId}&message=${reason}`, null, {
                headers: { Authorization: header }
            });
            setShow(false);
            window.location.reload();
        } catch (error) {
            console.error("There was an error submitting the decline reason!", error);
        }
    };
    const handleAccept = async () => {
        try {
            await axios.put(
                `${process.env.REACT_APP_BASE_URL}/api/ItemStatus/AcceptItem?itemId=${itemId}`, null, {
                headers: { Authorization: header }
            }
            );
            setShowAccept(false);
            window.location.reload();
        } catch (error) {
            console.error("There was an error accepting the item!", error);
        }
    };
    const handleSubmitToReview = async () => {
        try {
            await axios.put(
                `${process.env.REACT_APP_BASE_URL}/api/ItemStatus/SubmitToReview?itemId=${itemId}`,
                null,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data',
                        Authorization: header
                    }
                }
            );
            setShowSubmitToReview(false);
            window.location.reload();
        } catch (error) {
            console.error("There was an error submitting to review!", error);
        }
    };
    const handleClickFile = (id) => {
        // const fileUrl = item.file[0].fileUrl; // Lấy URL của file PDF
        navigate(`/DSpace/PdfViewer/${id}`);
    };
    const handleDeleteItem = () => {
        const confirmDelete = window.confirm("Are you sure you want to delete this item?");
        if (confirmDelete) {
            // Gọi API để xóa item
            fetch(`${process.env.REACT_APP_BASE_URL}/api/Item/deleteItem/${itemId}`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json'
                    // Các headers khác nếu cần
                }
            })
                .then(response => {
                    if (!response.ok) {
                        throw new Error('Failed to delete item.');
                    }
                    window.alert('Item deleted successfully!');
                    navigate('/Dspace/Collection/ItemList/0');
                    // Xử lý khi xóa thành công (vd: redirect, refresh danh sách, ...)
                })
                .catch(error => {
                    console.error('Error deleting item:', error);
                    // Xử lý khi có lỗi xảy ra
                });
        }
    };
    const handleDownloadFile = (fileKeyId, fileName) => {
        const downloadUrl = `${process.env.REACT_APP_BASE_URL}/api/FileUpload/downloadFile/${fileKeyId}`;

        fetch(downloadUrl)
            .then(response => response.blob())
            .then(blob => {
                // Sử dụng file-saver để lưu tệp
                saveAs(blob, fileName);
            })
            .catch(error => {
                console.error('Error downloading file:', error);
            });
    };
    const handleAuthorClick = (id) => {
        // Xử lý sự kiện khi tác giả được click
        navigate(`/Dspace/Author/AuthorDetail/${id}`)
    };
    const formatDate = (dateString) => {
        if (!dateString) return '';

        const parts = dateString.split('-');
        let formattedDate = '';

        if (parts.length === 3) {
            // Case: full date (YYYY-MM-DD)
            const [year, month, day] = parts;
            formattedDate = `${day.padStart(2, '0')}-${month.padStart(2, '0')}-${year}`;
        } else if (parts.length === 2) {
            // Case: year and month (YYYY-MM)
            const [year, month] = parts;
            formattedDate = `${month.padStart(2, '0')}-${year}`;
        } else if (parts.length === 1) {
            // Case: only year (YYYY)
            const [year] = parts;
            formattedDate = year;
        }

        return formattedDate;
    };
    function Status({ status }) {
        const getStatusText = (status) => {
            switch (status) {
                case 0:
                    return "Not submitted yet";
                case 1:
                    return "Pending";
                case 2:
                    return "Rejected";
                case 3:
                    return "Published";
                default:
                    return "";
            }
        };

        const getStatusColor = (status) => {
            switch (status) {
                case 0:
                    return "gray";
                case 1:
                    return "#FFC107";
                case 2:
                    return "#DC3545";
                case 3:
                    return "#28A745";
                default:
                    return "transparent";
            }
        };
        const getBoxShadow = (status) => {
            const color = getStatusColor(status);
            return `0 0 10px 2px ${color}`;
        };
        return (
            <>
                <p style={{ margin: '0' }}>
                    <span style={{
                        margin: '0',
                        backgroundColor: getStatusColor(status),
                        borderRadius: '3px',
                        color: 'white',
                        padding: '2px',
                        fontSize: 'medium',
                        boxShadow: getBoxShadow(status)

                    }}>
                        {getStatusText(status)}
                    </span>
                </p>
                {status === 2 && (
                    <div className="alert alert-danger" role="alert" style={{ whiteSpace: 'pre-wrap', wordWrap: 'break-word' }}>
                        {item.message}
                    </div>
                )}

            </>
        );
    }

    if (loading) {
        return (
            <div>
                <Header />
                <Helmet>
                    <meta charSet="utf-8" />
                    <title>Item unknown... - dSPACE</title>
                </Helmet>
                <div className="container mt-5 d-flex justify-content-center align-items-center" style={{ height: '60vh' }}>
                    <div class="load-4">
                        {/* <p>Loading 4</p> */}
                        <div class="ring-1"></div>
                        <Counter itemId={itemId} />
                    </div>
                </div>
            </div>
        );
    }
    // console.log(item?.metadata?.identifier);
    const handleReasonChange = (e) => {
        const value = e.target.value;
        const lines = value.split('\n');

        // Giới hạn chỉ cho phép 3 dòng
        if (lines.length <= 3) {
            setReason(value);
        } else {
            // Nếu vượt quá 3 dòng, chỉ lấy 3 dòng đầu tiên
            setReason(lines.slice(0, 3).join('\n'));
        }
    };


    return (
        <div>
            <Header />
            <Helmet>
                <meta charSet="utf-8" />
                <title>{`${item?.title}`} - dSPACE</title>
            </Helmet>
            <div className="container mt-5">
                <h1 className="mb-5 text-center">{item?.title} <Status status={item?.status} /></h1>

                <div className="row justify-content-center">
                    <div className="col-md-11">
                        <div className="row">
                            <Modal show={show} onHide={handleClose}>
                                <Modal.Header closeButton>
                                    <Modal.Title>Enter reason for decline</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <Form>
                                        <Form.Group>
                                            <Form.Label>Reason</Form.Label>
                                            <Form.Control
                                                as="textarea"
                                                rows={3}
                                                placeholder="Enter reason"
                                                value={reason}
                                                onChange={handleReasonChange}
                                            />
                                        </Form.Group>
                                    </Form>
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button variant="secondary" onClick={handleClose}>
                                        Cancel
                                    </Button>
                                    <Button variant="primary" onClick={handleSubmitDecline}>
                                        Submit Decline
                                    </Button>
                                </Modal.Footer>
                            </Modal>

                            <Modal show={showAccept} onHide={handleCloseAccept}>
                                <Modal.Header closeButton>
                                    <Modal.Title>Confirm Accept</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <p>Are you sure you want to accept this item?</p>
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button variant="secondary" onClick={handleCloseAccept}>
                                        Cancel
                                    </Button>
                                    <Button variant="primary" onClick={handleAccept}>
                                        Accept
                                    </Button>
                                </Modal.Footer>
                            </Modal>
                            <Modal show={showSubmitToReview} onHide={handleCloseSubmitToReview}>
                                <Modal.Header closeButton>
                                    <Modal.Title>Submit to Review</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <p>Are you sure you want to submit this item for review?</p>
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button variant="secondary" onClick={handleCloseSubmitToReview}>
                                        Cancel
                                    </Button>
                                    <Button variant="primary" onClick={handleSubmitToReview}>
                                        Submit to Review
                                    </Button>
                                </Modal.Footer>
                            </Modal>
                            <div className="col-md-5">
                                <div>
                                    <label style={{ marginRight: '10px', fontSize: '1.4rem', marginBottom: '3px' }}>Author</label>
                                    <div className="form-group mb-3 d-flex flex-column">
                                        {item.authorItems.map((authorItem, index) => (
                                            <div key={index} className="author-container">
                                                <span
                                                    onClick={() => handleAuthorClick(authorItem.authorId)}
                                                    className="author-item"
                                                >
                                                    {authorItem.fullName}
                                                </span>
                                            </div>
                                        ))}
                                    </div>
                                </div>
                                <div>
                                    <label style={{ marginRight: '10px', fontSize: '1.4rem', marginBottom: '3px' }}>Date Of Issue</label>
                                    <div className="form-group mb-3 d-flex flex-column">
                                        {formatDate(item.dateOfIssue)}
                                    </div>
                                </div>
                                <div>
                                    <label style={{ marginRight: '10px', fontSize: '1.4rem', marginBottom: '3px' }}>Publisher</label>
                                    <div className="form-group mb-3 d-flex flex-column">
                                        {item.publisher}
                                    </div>
                                </div>
                                <div>
                                    <label style={{ marginRight: '10px', fontSize: '1.4rem', marginBottom: '3px' }}>Keywords</label>
                                    <div className="form-group mb-3 d-flex flex-column">
                                        {item.subjectKeywords.map((keywordItem, index) => (
                                            <div key={index} className="author-container">

                                                {keywordItem}
                                            </div>
                                        ))}
                                    </div>
                                </div>
                                <div>
                                    <label style={{ marginRight: '10px', fontSize: '1.4rem', marginBottom: '3px' }}>Collection</label>
                                    <div className="form-group mb-3 d-flex flex-column">
                                        {item.collectionName}
                                    </div>
                                </div>
                                <div>
                                    <div className="mt-2">
                                        <button
                                            type="button"
                                            style={{ marginRight: '10px' }}
                                            onClick={() => navigate(`/Dspace/Metadata/MetadataDetail/${itemId}`)}
                                            className="btn btn-info"
                                        >
                                            Show full item
                                        </button>
                                        {colId ? (
                                            <button
                                                type="button"
                                                style={{ marginRight: '10px' }}
                                                onClick={() => navigate(`/Dspace/Collection/CollectionDetail/${colId}`)}
                                                className="btn btn-secondary"
                                            >
                                                Back to List
                                            </button>
                                        ) : (
                                            <button
                                                type="button"
                                                style={{ marginRight: '10px' }}
                                                onClick={() => navigate(`/Dspace/Collection/ItemListReader/-1`)}
                                                className="btn btn-secondary"
                                            >
                                                Back to List
                                            </button>
                                        )}
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-7">
                                <div>
                                    <label style={{ marginRight: '10px', fontSize: '1.4rem', marginBottom: '3px' }}>Description</label>
                                    <div className="form-group mb-3 d-flex flex-column">
                                        {item.description}
                                    </div>
                                </div>
                                <div className="form-group mb-3">
                                    <div className="custom-file-uploadx">
                                        <div>
                                            <div>Files:</div>
                                            <div>
                                                {item?.file && item.file.length > 0 ? (
                                                    <div>
                                                        {item.file.filter(file => file.isActive).map((file, index) => (
                                                            <div className="file-item" key={index} style={{ display: 'flex', alignItems: 'center', marginBottom: '10px' }}>
                                                                <div
                                                                    className="fileName"
                                                                    onClick={() => handleClickFile(file.fileKeyId + "@" + itemId)}
                                                                    style={{ cursor: 'pointer', color: '#207698', marginRight: '10px', flexGrow: 1 }}
                                                                >
                                                                    {file.fileName}
                                                                </div>
                                                                {userRole === "ADMIN" || userRole === "STAFF" ? (
                                                                    <>
                                                                        <VerticalAlignBottom className="download-icon" onClick={() => handleDownloadFile(file.fileKeyId, file.fileName)} style={{ cursor: 'pointer' }} />
                                                                    </>
                                                                ) : null}
                                                            </div>
                                                        ))}
                                                    </div>
                                                ) : (
                                                    <p>No file available</p>
                                                )}

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style={{ marginTop: '200px' }}></div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ItemDetails;
