import { Search } from '@material-ui/icons';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useCallback, useEffect, useRef, useState } from 'react';
import { Link, useLocation, useNavigate, useSearchParams } from 'react-router-dom';
import Select from 'react-select';
import Header from '../components/Header/Header';
import { Helmet } from 'react-helmet';

const removeDuplicates = (items) => {
    const seen = new Set();
    return items.filter(item => {
        const key = `${item.label}-${item.url}`;
        if (seen.has(key)) {
            return false;
        } else {
            seen.add(key);
            return true;
        }
    });
};

const SubCommunity = () => {
    const navigate = useNavigate();
    const location = useLocation();
    const [searchParams, setSearchParams] = useSearchParams();
    let communityIdRef = useRef(searchParams.getAll('cid'));
    const collectionId = searchParams.get('colid') ?? "0";
    const [community, setCommunity] = useState({});
    const [collections, setCollections] = useState([]);
    const [collection, setCollection] = useState({});
    const [communities, setCommunities] = useState([]);
    const [searchTerm, setSearchTerm] = useState('');
    const queryParams = new URLSearchParams(location.search);
    const initialButton = queryParams.get('button') || 'All';
    const colName = searchParams.get('colName');
    const [activeButton, setActiveButton] = useState(initialButton);
    const [selectedYear, setSelectedYear] = useState('');
    const [selectedMonth, setSelectedMonth] = useState('');
    const [selectedDate, setSelectedDate] = useState('');
    const [years, setYears] = useState([]);
    const handleDateChange = (e) => setSelectedDate(e.target.value);
    const [searchQuery, setSearchQuery] = useState('');
    const [subscriptions, setSubscriptions] = useState({});
    const communityId = searchParams.get('cid');
    const [currentPageCommunities, setCurrentPageCommunities] = useState(1);
    const [itemsPerPageCommunities, setItemsPerPageCommunities] = useState(5);
    const [currentPageCollections, setCurrentPageCollections] = useState(1);
    const [itemsPerPageCollections, setItemsPerPageCollections] = useState(5);

    const filterData = (year, month, date) => {

        let filteredCommunities = communities.filter(community => {
            const createDate = new Date(community.createDate);
            if (
                (year === '' || createDate.getFullYear() === parseInt(year, 10)) &&
                (month === '' || createDate.getMonth() + 1 === parseInt(month, 10)) &&
                (date === '' || createDate.getDate() === parseInt(date, 10))
            ) {
                return true;
            }
            return false;
        });

        setCommunities(filteredCommunities);
    };

    const token = localStorage.getItem('Token');
    const header = `Bearer ${token}`;
    const buttons = [
        'All',
        'By Name',
        'By Description'
    ];

    useEffect(() => {

        communityIdRef.current = searchParams.getAll('cid');
    }, [location.search, searchParams]);

    const months = [
        'January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December'
    ];
    useEffect(() => {
        const uniqueYears = Array.from(new Set(communities.map(community => new Date(community.createTime).getFullYear())));
        uniqueYears.sort((a, b) => b - a);
        setYears(uniqueYears);
    }, [community, searchParams]);
    useEffect(() => {
        setActiveButton(initialButton);
    }, [initialButton]);


    useEffect(() => {
        communityIdRef.current = searchParams.getAll('cid');
        axios.get(`${process.env.REACT_APP_BASE_URL}/api/Collection/getListOfCollectionByCommunityId/${communityIdRef?.current[communityIdRef?.current.length - 1]}`)
            .then(response => {
                setCollections(response.data);
            })
            .catch(error => {
                console.error('Error fetching collections:', error);
            });
    }, [communityIdRef, searchParams]);

    useEffect(() => {
        axios.get(`${process.env.REACT_APP_BASE_URL}/api/Collection/getCollection/${collectionId}`)
            .then(response => {
                setCollection(response.data);
            })
            .catch(error => {
                console.error('Error fetching collection:', error);
            });
    }, [collectionId]);
    useEffect(() => {
        communityIdRef.current = searchParams.getAll('cid');
        axios.get(`${process.env.REACT_APP_BASE_URL}/api/Community/getListOfCommunitiesByParentId/${communityIdRef?.current[communityIdRef?.current.length - 1]}`)
            .then(response => {
                setCommunities(response.data);
            })
            .catch(error => {
                console.error('Error fetching communities:', error);
            });
    }, [communityIdRef, searchParams]);

    useEffect(() => {
        communityIdRef.current = searchParams.getAll('cid');
        axios.get(`${process.env.REACT_APP_BASE_URL}/api/Community/getCommunity/${communityIdRef?.current[communityIdRef?.current.length - 1]}`)
            .then(response => {
                setCommunity(response.data);
                // setBreadcrumbLinks(removeDuplicates(updatedLinks));
            })
            .catch(error => {
                console.error('Error fetching community:', error);
            });
    }, [communityIdRef, searchParams]);

    useEffect(() => {
        axios.get(`${process.env.REACT_APP_BASE_URL}/api/CollectionUser/GetAllCollection/${communityId}`, {
            headers: { Authorization: header }
        })
            .then(response => {
                const subscriptionsData = response.data.reduce((acc, item) => {
                    acc[item.collectionId] = item.isSubcribed;
                    return acc;
                }, {});
                setSubscriptions(subscriptionsData);
            })
            .catch(error => {
                console.error('Error fetching subscription status:', error);
            });
    }, []);

    const handleCommunityClick = (community) => {
        let arr = [];
        arr?.push(community.communityId);
        searchParams.set("cid", arr);
        setSearchParams(searchParams);
    };

    const handleCollectionClick = useCallback((collection) => {
        {
            localStorage.getItem("Role") === "ADMIN" ? (navigate(`/Dspace/Collection/CollectionDetail/${collection.collectionId}?colName=${collection.collectionName}`))
                : navigate(`/Dspace/Collection/CollectionDetail/${collection.collectionId}?colName=${collection.collectionName}`);
        }

    }, []);
    useEffect(() => {
        console.log('render')
    }, [handleCollectionClick])

    // const handleShowMoreCommunities = () => {
    //     setVisibleCommunityCount(prevCount => prevCount + 3);
    // };

    // const handleShowMoreCollections = () => {
    //     setVisibleCollectionCount(prevCount => prevCount + 3);
    // };

    const handleYearChange = (e) => {
        const year = e.target.value;
        setSelectedYear(year);
        filterData(year, selectedMonth, selectedDate);
    };

    const handleMonthChange = (e) => {
        const month = e.target.value;
        setSelectedMonth(month);

        // Lọc dữ liệu ngay khi người dùng chọn năm và tháng
        filterData(selectedYear, month, selectedDate);
    };
    const performSearch = () => {
        const normalizedSearchQuery = searchQuery.replace(/\s+/g, '').toLowerCase();

        if (activeButton === 'By Name') {
            setCommunities(prevCommunities =>
                prevCommunities.filter(community =>
                    community.communityName.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery)
                )
            );
            setCollections(prevCollections =>
                prevCollections.filter(collection =>
                    collection.collectionName.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery)
                )
            );
        } else if (activeButton === 'By Description') {
            setCommunities(prevCommunities =>
                prevCommunities.filter(community =>
                    community.shortDescription.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery)
                )
            );
            setCollections(prevCollections =>
                prevCollections.filter(collection =>
                    collection.shortDescription.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery)
                )
            );
        }
        else if (activeButton === 'By issue date') {
            let filteredCommunities = communities.filter(community => {
                const createDate = new Date(community.createDate); // Thay thế bằng trường tương ứng từ API
                if (
                    (selectedYear === '' || createDate.getFullYear() === parseInt(selectedYear, 10)) &&
                    (selectedMonth === '' || createDate.getMonth() + 1 === parseInt(selectedMonth, 10)) &&
                    (selectedDate === '' || createDate.getDate() === parseInt(selectedDate, 10))
                ) {
                    return true;
                }
                return false;
            });

            setCommunities(filteredCommunities);
        }


    };

    const handleSearchChange = (e) => {
        setSearchQuery(e.target.value);
    };


    const handleButtonClick = (buttonName) => {
        setActiveButton(buttonName);
        navigate(`?cid=${community.communityId}&button=${buttonName}`);
        setSearchQuery('');
        setCurrentPageCommunities(1);
        setCurrentPageCollections(1);
    };
    // Communities pagination
    const handleNextPageCommunities = () => {
        setCurrentPageCommunities(prevPage => prevPage + 1);
    };

    const handlePrevPageCommunities = () => {
        setCurrentPageCommunities(prevPage => prevPage - 1);
    };

    const indexOfLastCommunity = currentPageCommunities * itemsPerPageCommunities;
    const indexOfFirstCommunity = indexOfLastCommunity - itemsPerPageCommunities;
    const currentCommunitiesPaginated = communities.slice(indexOfFirstCommunity, indexOfLastCommunity);
    const totalPagesCommunities = Math.ceil(communities.length / itemsPerPageCommunities);

    // Collections pagination
    const handleNextPageCollections = () => {
        setCurrentPageCollections(prevPage => prevPage + 1);
    };

    const handlePrevPageCollections = () => {
        setCurrentPageCollections(prevPage => prevPage - 1);
    };

    const indexOfLastCollection = currentPageCollections * itemsPerPageCollections;
    const indexOfFirstCollection = indexOfLastCollection - itemsPerPageCollections;
    const currentCollectionsPaginated = collections.slice(indexOfFirstCollection, indexOfLastCollection);
    const totalPagesCollections = Math.ceil(collections.length / itemsPerPageCollections);

    const handleSubscribe = (collectionId) => {
        axios.post(`${process.env.REACT_APP_BASE_URL}/api/SubcribeUser/SubcribeCollection?collectionId=${collectionId}`, null, {
            headers: { Authorization: header }
        })
            .then(response => {
                console.log('Subscribed successfully!', response.data);
                setSubscriptions(prev => ({ ...prev, [collectionId]: true }));
                alert('Subscribed successfully');
            })
            .catch(error => {
                console.error('Error subscribing to collection:', error);
                alert('Failed to subscribe');
            });
    };

    const handleUnsubscribe = (collectionId) => {
        axios.post(`${process.env.REACT_APP_BASE_URL}/api/SubcribeUser/UnSubcribeCollection?collectionId=${collectionId}`, null, {
            headers: { Authorization: header }
        })
            .then(response => {
                console.log('Unsubscribed successfully!', response.data);
                setSubscriptions(prev => ({ ...prev, [collectionId]: false }));
                alert('Unsubscribed successfully');
            })
            .catch(error => {
                console.error('Error unsubscribing to collection:', error);
                alert('Failed to unsubscribe');
            });
    };

    const NameSelect = ({ Names, selectedNames, handleNameSelectChange }) => {
        const options = Names.map(Name => ({
            value: Name.NameId,
            label: Name.firstName
        }));
        const selectedOptions = options.filter(option =>
            selectedNames.includes(option.value)
        );

        return (
            <Select
                isMulti
                options={options}
                value={selectedOptions}
                onChange={selectedOptions => handleNameSelectChange(selectedOptions ? selectedOptions.map(option => option.value) : [])}
                placeholder="Search and select Names..."
                isClearable
                styles={{
                    container: (base) => ({ ...base, width: '100%' }),
                    menu: (provided) => ({
                        ...provided,
                        maxHeight: '200px',
                        overflowY: 'auto',
                    }),
                    menuList: (provided) => ({
                        ...provided,
                        maxHeight: '200px',
                        overflowY: 'auto',
                    }),
                    indicatorSeparator: () => ({
                        display: 'none',
                    }),
                    dropdownIndicator: (base) => ({
                        ...base,
                        display: 'none',
                    }),
                }}
            />
        );
    };
    function getRelativePath(fullPath) {
        const splitPath = fullPath.split('public\\');
        return splitPath[1];
    }


    return (
        <div>
            <Header />
            <Helmet>
                <meta charSet="utf-8" />
                <title>need to checking... - dSPACE</title>
            </Helmet>
            <div className="container mt-5">
                <h1 className="mb-4">{community.communityName} Community</h1>
                <div className="card">
                    <div className="card-body">
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Name:</div>
                            <div className="col-sm-6 ">{community?.communityName}</div>
                        </div>
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Short Description:</div>
                            <div className="col-sm-6 ">{community?.shortDescription}</div>
                        </div>
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Created By:</div>
                            <div className="col-sm-6 ">{community?.createBy}</div>
                        </div>
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Updated By:</div>
                            <div className="col-sm-6">{community?.updateBy}</div>
                        </div>
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Create Time:</div>
                            <div className="col-sm-6 ">{new Date(community?.createTime).toLocaleString()}</div>
                        </div>
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Update Time:</div>
                            <div className="col-sm-6 ">{new Date(community?.updateTime).toLocaleString()}</div>
                        </div>
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Logo:</div>
                            <div className="col-sm-6 ">
                                {community?.logoUrl ? (
                                    <img src={`/${getRelativePath(community.logoUrl)}`} alt="logo" className='img-thumbnail' style={{ width: '200px' }} />
                                ) : (
                                    <div>No Image</div>
                                )}
                            </div>
                        </div>
                        <div className="row pb-2 my-3">
                            <div className="col-sm-2 text-end">Active:</div>
                            <div className="col-sm-6"><input type="checkbox" checked={community?.isActive} readOnly /></div>
                        </div>
                    </div>
                </div>
                <br></br>
                <div className='d-flex mb-4'>
                    {buttons.map((buttonName, index) => (
                        <div
                            key={index}
                            className="btn"
                            style={{
                                border: '1px solid black',
                                borderRadius: index === 0 ? '4px 0 0 4px' : index === buttons.length - 1 ? '0 4px 4px 0' : '0',
                                padding: '13px',
                                fontWeight: 'bolder',
                                backgroundColor: activeButton === buttonName ? '#43515F' : 'white',
                                color: activeButton === buttonName ? 'white' : '#207698',
                                cursor: 'pointer'
                            }}
                            onClick={() => handleButtonClick(buttonName)}
                        >
                            {buttonName}
                        </div>
                    ))}
                </div>
                {activeButton === 'By Name' && (
                    <div className="d-flex mb-4">
                        <input
                            type="text"
                            className="form-control w-50"
                            placeholder="Search By Name"
                            style={{ borderRadius: '0 0 0 0', flex: '0 1 auto' }}
                            value={searchQuery}
                            onChange={handleSearchChange}
                        />
                        <button
                            style={{ backgroundColor: '#43515F', color: 'white', borderRadius: '0 6px 6px 0' }}
                            className="btn"
                            onClick={performSearch}
                        >
                            <Search />
                        </button>
                    </div>
                )}
                {activeButton === 'By Description' && (
                    <div className="d-flex mb-4">
                        <input
                            type="text"
                            className="form-control w-50"
                            placeholder="Search By Description"
                            style={{ borderRadius: '0 0 0 0', flex: '0 1 auto' }}
                            value={searchQuery}
                            onChange={handleSearchChange}
                        />
                        <button
                            style={{ backgroundColor: '#43515F', color: 'white', borderRadius: '0 6px 6px 0' }}
                            className="btn"
                            onClick={performSearch}
                        >
                            <Search />
                        </button>
                    </div>
                )}


                <h3>Sub-Communities of {community.communityName}</h3>
                <ul className="list-group">
                    {currentCommunitiesPaginated.map((community, index) => (
                        <li key={index} className="list-group-item clickable-row border-0">
                            <div className="d-flex justify-content-between align-items-center" onClick={() => handleCommunityClick(community)}>
                                <div className="d-flex align-items-center">
                                    {/* <img src={community.logoUrl} alt="logo" style={{ width: '200px', height: '150px', marginRight: '100px', cursor: 'pointer' }} /> */}
                                    {community.logoUrl ? (
                                        <img src={`/${getRelativePath(community.logoUrl)}`} alt="logo" className='img-thumbnail' style={{ width: '200px' }} />
                                    ) : (
                                        <div>No Image</div>
                                    )}
                                    <div style={{ marginBottom: '80px' }}>
                                        <span className="font-weight-bold h4" style={{ cursor: 'pointer' }}>Name: {community.communityName}</span>
                                        <p className="mb-0">Description: {community.shortDescription}</p>
                                        <span className="font-weight-bold">Total Items: </span>{community.totalItems}

                                    </div>
                                </div>
                                <div>
                                </div>
                            </div>
                        </li>
                    ))}
                </ul>
                <div className="d-flex justify-content-center align-items-center">
                    <button
                        className="btn btn-outline-primary mx-2"
                        onClick={handlePrevPageCommunities}
                        disabled={currentPageCommunities === 1}
                    >
                        Prev
                    </button>
                    <span className="btn btn-outline-primary mx-2">
                        {currentPageCommunities}
                    </span>
                    <button
                        className="btn btn-outline-primary mx-2"
                        onClick={handleNextPageCommunities}
                        disabled={indexOfLastCommunity >= communities.length}
                    >
                        Next
                    </button>
                </div>
                <div className="d-flex justify-content-center mt-2">
                    Page {currentPageCommunities} of {totalPagesCommunities}
                </div>
            </div>

            <div className="container mt-5">
                <h3>Collections of {community.communityName}</h3>
                <ul className="list-group">
                    {currentCollectionsPaginated.map((collection, index) => (
                        <li key={index} className="list-group-item clickable-row border-0">
                            <div className="d-flex justify-content-between align-items-center" >
                                <div className="d-flex align-items-center">
                                    {/* <img src={collection.logoUrl} alt="logo" style={{ width: '200px', height: '150px', marginRight: '100px', cursor: 'pointer' }} /> */}
                                    {collection.logoUrl ? (
                                        <img src={`/${getRelativePath(collection.logoUrl)}`} alt="logo" className='img-thumbnail' style={{ width: '200px' }} />
                                    ) : (
                                        <div>No Image</div>
                                    )}
                                    <div style={{ marginBottom: '80px' }} >
                                        <span className="font-weight-bold h4" style={{ cursor: 'pointer' }} onClick={() => handleCollectionClick(collection)}>Name: {collection.collectionName} </span>
                                        <p className="mb-0" onClick={() => handleCollectionClick(collection)}>Description: {collection.shortDescription}</p>
                                        <span className="font-weight-bold" >Total Items: </span>{collection.totalItems}
                                        <div>
                                            {subscriptions[collection.collectionId] ? (
                                                <button className="btn btn-secondary mt-2" onClick={() => handleUnsubscribe(collection.collectionId)}>Unsubscribe</button>
                                            ) : (
                                                <button className="btn btn-danger mt-2" onClick={() => handleSubscribe(collection.collectionId)}>Subscribe</button>
                                            )}
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </li>
                    ))}

                </ul>

                <div className="d-flex justify-content-center align-items-center">
                    <button
                        className="btn btn-outline-primary mx-2"
                        onClick={handlePrevPageCollections}
                        disabled={currentPageCollections === 1}
                    >
                        Prev
                    </button>
                    <span className="btn btn-outline-primary mx-2">
                        {currentPageCollections}
                        
                    </span>
                    <button
                        className="btn btn-outline-primary mx-2"
                        onClick={handleNextPageCollections}
                        disabled={indexOfLastCollection >= collections.length}
                    >
                        Next
                    </button>

                </div>
                <div className="d-flex justify-content-center mt-2">
                    Page {currentPageCollections} of {totalPagesCollections}
                </div>
            </div>
        </div>
    );
};

export default SubCommunity;
