import React, { useEffect, useState } from 'react';
import { Link, useNavigate, useParams, useLocation } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from '../components/Header/Header';
import axios from 'axios';
import '../Admin/Items/CollectionList.css'; // Import file CSS
import Select from 'react-select';
import { styled } from '@material-ui/core';
import { FormatListBulleted, GridOn, Search } from '@material-ui/icons';
import { TagsInput } from "react-tag-input-component";
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';
import {
    ExpandLess, ExpandMore
} from '@material-ui/icons';
const CollectionList = () => {
    const location = useLocation();
    const searchParams = new URLSearchParams(location.search);
    const colName = searchParams.get('colName');

    const queryParams = new URLSearchParams(location.search);
    const initialButton = queryParams.get('button') || 'Recent submissions';

    const [selectedValue, setSelectedValue] = useState('');
    const [items, setItems] = useState([]);
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(true);
    const [searchQuery, setSearchQuery] = useState('');
    const navigate = useNavigate();
    const { collectionId } = useParams();
    const [startDate, setStartDate] = useState(null);
    const [endDate, setEndDate] = useState(null);
    const [numPerPage, setNumPerPage] = useState(5);
    const [sortState, setSortState] = useState(null);
    const [selectedAuthors, setSelectedAuthors] = useState([]);
    const [selectedKeywords, setSelectedKeywords] = useState([]);
    const [objItemsPerPage, setobjItemsPerPage] = useState([]);
    const [page, setPage] = React.useState(1);
    const handleChangePage = (event, value) => {
        setPage(value);
    };
    // const [activeButton, setActiveButton] = useState('list'); // Default active button is 'list'
    const [activeButton, setActiveButton] = useState(initialButton);

    useEffect(() => {
        setActiveButton(initialButton);
    }, [initialButton]);

    const handleButtonClick = (buttonName) => {
        navigate(`?colName=${colName}&button=${buttonName}`);
        window.location.reload();
    };

    const buttons = [
        'Recent submissions',
        'By issue date',
        'By authors',
        'By title',
        'By subject keywords',
        // 'By subject category'
    ];
    const [expandedAbstracts, setExpandedAbstracts] = useState({});
    const toggleAbstract = (itemId) => {
        setExpandedAbstracts(prevState => ({
            ...prevState,
            [itemId]: !prevState[itemId]
        }));
    };

    const handleSearchSubmit = () => {
        let payload = {};
        console.log(payload);
        let api = ``;

        if (activeButton == 'Recent submissions') {
            return;
        } else if (activeButton == 'By issue date') {
            api = `${process.env.REACT_APP_BASE_URL}/api/ItemUser/SearchItemByDate/${page}/${numPerPage}`;
            payload = {
                year: selectedYear,
                month: Number(selectedMonth) !== 0 ? selectedMonth.padStart(2, '0') : null,
                day: Number(selectedDate) !== 0 ? selectedDate.padStart(2, '0') : null,
                collectionId: parseInt(collectionId, 10) || 0,
                discoverable: true,
                sortType: 0
            }
        } else if (activeButton == 'By authors') {
            api = `${process.env.REACT_APP_BASE_URL}/api/ItemUser/SearchItemByAuthor/${page}/${numPerPage}?collectionId=${collectionId}`;
            payload = {
                listAuthor: selectedAuthors,
                collectionId: parseInt(collectionId, 10) || 0,
                discoverable: true,
                sortType: 0

            }
        } else if (activeButton == 'By title') {
            if (searchQuery.trim() == "") {
                api = `${process.env.REACT_APP_BASE_URL}/api/ItemUser/SearchItemBySubjectKeyword/${page}/${numPerPage}?collectionId=${collectionId}`;
                payload = {
                    listSubjectKeyword: selectedKeywords,
                    collectionId: parseInt(collectionId, 10) || 0,
                    discoverable: true,
                    sortType: 0
                }
            } else {
                api = `${process.env.REACT_APP_BASE_URL}/api/ItemUser/SearchItemByTitle/${page}/${numPerPage}?title=${normalizeText(searchQuery.trim())}&collectionId=${collectionId}`;
                payload = {
                    title: normalizeText(searchQuery.trim()),
                    collectionId: parseInt(collectionId, 10) || 0,
                    discoverable: true,
                    sortType: 0
                }
            }
        } else if (activeButton == 'By subject keywords') {
            api = `${process.env.REACT_APP_BASE_URL}/api/ItemUser/SearchItemBySubjectKeyword/${page}/${numPerPage}`;
            payload = {
                listSubjectKeyword: selectedKeywords,
                collectionId: parseInt(collectionId, 10) || 0,
                discoverable: true,
                sortType: 0
            }

        }
        axios.post(api, payload)
            .then(({ data }) => {
                setItems(data);
                setobjItemsPerPage(data);
                setLoading(false);
            })
            .catch(error => {
                setError(error.message);
                setLoading(false);
            });
    };
    useEffect(() => {
        // Lấy dữ liệu từ API
        if (collectionId < 0 || activeButton == 'Recent submissions') { setobjItemsPerPage([]) }
        else {
            handleSearchSubmit()
        }

    }, [collectionId, numPerPage, page]);
    const [currentPage, setCurrentPage] = useState(1);
    // const numPerPage = 12;
    const [collections, setCollections] = useState([]);

    useEffect(() => {
        // Lấy dữ liệu từ API
        if (collectionId != 0) {
            if (activeButton == 'By issue date' || activeButton == 'By authors' || activeButton == 'By title' || activeButton == 'By subject keywords') {

            } else {
                axios.get(`${process.env.REACT_APP_BASE_URL}/api/ItemUser/Get5ItemRecentInACollection/${collectionId}`)
                    .then(({ data }) => {
                        setItems(data);
                        // setobjItemsPerPage(data);
                        // objItemsPerPage.objmap = items;
                        setLoading(false);
                    })
                    .catch(error => {
                        setError(error.message);
                        setLoading(false);
                    });
            }

        } else {
        }
    }, [collectionId]);

    const handleSearchChange = (e) => {
        setSearchQuery(e.target.value);
    };
    const handleRowClick = (itemId) => {
        // Chuyển hướng sang trang chi tiết bộ sưu tập
        navigate(`/DSpace/ItemDetail/${itemId}?colId=${collectionId}`);
    };



    const normalizeText = (text) => {
        // Chuyển đổi tất cả các ký tự thành chữ thường
        text = text.toLowerCase();

        // Loại bỏ dấu câu và dấu cách
        text = text.replace(/[\s\p{P}]/gu, '');

        // Loại bỏ dấu tiếng Việt
        text = text.replace(/[áàảãạăắằẳẵặâấầẩẫậ]/g, 'a');
        text = text.replace(/[đ]/g, 'd');
        text = text.replace(/[éèẻẽẹêếềểễệ]/g, 'e');
        text = text.replace(/[íìỉĩị]/g, 'i');
        text = text.replace(/[óòỏõọôốồổỗộơớờởỡợ]/g, 'o');
        text = text.replace(/[úùủũụưứừửữự]/g, 'u');
        text = text.replace(/[ýỳỷỹỵ]/g, 'y');

        // Loại bỏ các dấu thanh và âm cuối
        text = text.normalize('NFD').replace(/[\u0300-\u036f]/g, '');

        return text;
    };
    const [selectedYear, setSelectedYear] = useState('');
    const [selectedMonth, setSelectedMonth] = useState('');
    const [selectedDate, setSelectedDate] = useState('');
    const [years, setYears] = useState([]);

    const months = [
        'January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December'
    ];
    // useEffect(() => {
    //     const currentYear = new Date().getFullYear();
    //     const startYear = 2000;
    //     const uniqueYears = [];

    //     for (let year = startYear; year <= currentYear; year++) {
    //         uniqueYears.push(year);
    //     }

    //     uniqueYears.sort((a, b) => b - a); // Sắp xếp từ năm mới nhất đến cũ nhất
    //     setYears(uniqueYears);
    // }, []);
    const handleYearChange = (e) => setSelectedYear(e.target.value);
    const handleMonthChange = (e) => setSelectedMonth(e.target.value);
    const handleDateChange = (e) => setSelectedDate(e.target.value);

    const handleSearch = () => {
        // Your search logic here
        console.log('Searching for:', { year: selectedYear, month: selectedMonth, date: selectedDate });
    };

    // const filteredCollections = items
    //     .filter(item => {
    //         const itemDate = new Date(item.dateOfIssue);
    //         const normalizedTitle = normalizeText(item.title);
    //         const normalizedQuery = normalizeText(searchQuery.trim());
    //         // const authorIds = item.authorItems.map(authorItem => authorItem.author.authorId);
    //         // const containsAllSelectedAuthors = selectedAuthors.every(id => authorIds.includes(id));

    //         // Check if active button is 'By issue date' and filter by year and month
    //         const filterByIssueDate = (activeButton === 'By issue date') &&
    //             (!selectedYear || itemDate.getFullYear() === parseInt(selectedYear)) &&
    //             (!selectedMonth || itemDate.getMonth() === (parseInt(selectedMonth) - 1)) &&
    //             (!selectedDate || itemDate.getDate() === (parseInt(selectedDate)))

    //         // Handle asterisk (*) in search query
    //         const asteriskIndex = searchQuery.indexOf('*');
    //         let filteredTitle = normalizedQuery;

    //         // Handle phrase search
    //         const phraseMatch = searchQuery.match(/"([^"]+)"/);
    //         let phraseWords = [];
    //         if (phraseMatch) {
    //             phraseWords = phraseMatch[1].split(' ').map(word => normalizeText(word));
    //         }

    //         const containsAllPhraseWords = phraseWords.every(word => normalizedTitle.includes(word));

    //         // Handle exclude words (using - or NOT)
    //         const excludeWords = searchQuery
    //             .match(/(?:-|NOT\s+)(\w+)/gi)
    //             ?.map(word => normalizeText(word.replace(/(?:-|NOT\s+)/, ''))) || [];
    //         const containsAnyExcludeWord = excludeWords.some(word => normalizedTitle.includes(word));

    //         if (asteriskIndex !== -1) {
    //             filteredTitle = normalizedQuery.substring(0, asteriskIndex);
    //             return (
    //                 // item.isActive &&
    //                 normalizedTitle.startsWith(filteredTitle) &&
    //                 // (selectedAuthors.length === 0 || containsAllSelectedAuthors) &&
    //                 (activeButton !== 'By issue date' || filterByIssueDate)  // Apply filter by issue date if active button is 'By issue date'
    //             );
    //         } else {
    //             return (
    //                 // item.isActive &&
    //                 !containsAnyExcludeWord &&
    //                 ((phraseWords.length > 0 && containsAllPhraseWords) || (phraseWords.length === 0 && normalizedTitle.includes(normalizedQuery))) &&
    //                 // (selectedAuthors.length === 0 || containsAllSelectedAuthors) &&
    //                 (activeButton !== 'By issue date' || filterByIssueDate)  // Apply filter by issue date if active button is 'By issue date'
    //             );
    //         }
    //     })
    //     .sort((a, b) => {
    //         if (sortState === '1') {
    //             return a.metadata.title.localeCompare(b.metadata.title); // Sort by title ascending
    //         } else if (sortState === '2') {
    //             return b.metadata.title.localeCompare(a.metadata.title); // Sort by title descending
    //         } else if (sortState === '3') {
    //             return new Date(a.dateOfIssue) - new Date(b.dateOfIssue); // Sort by date ascending
    //         } else if (sortState === '4') {
    //             return new Date(b.dateOfIssue) - new Date(a.dateOfIssue); // Sort by date descending
    //         }
    //         return 0; // Default no sorting
    //     });

    // console.log(filteredCollections);

    const CollectionSelect = ({ collections, selectedValue, handleChange }) => {
        const options = collections.map(collection => ({
            value: collection.collectionId,
            label: collection.collectionName
        }));

        return (
            <Select
                options={options}
                value={options.find(option => option.value === selectedValue)}
                onChange={selectedOption => handleChange(selectedOption ? selectedOption.value : '0')}
                placeholder="All collections"
                isClearable
                styles={{
                    container: (base) => ({ ...base, width: 'auto' }),
                    control: (base) => ({
                        ...base,
                        border: '1px solid black',
                        borderRadius: '4px 0 0 4px',
                    }),
                    indicatorSeparator: () => ({
                        display: 'none',
                    }),
                    dropdownIndicator: (base) => ({
                        ...base,
                        display: 'none',
                    }),
                }}
            />

        );
    };

    const handleChange = (e) => {
        // const { name, value, type, checked } = e.target;
        // const newValue = type === 'checkbox' ? checked : value;
        setSelectedValue(e)
        navigate(`/Dspace/Collection/ItemList/${e}`);
    };

    function Status({ status }) {
        const getStatusText = (status) => {
            //   switch (status) {
            //     case 0:
            //       return "Not submitted yet";
            //     case 1:
            //       return "Pending";
            //     case 2:
            //       return "Rejected";
            //     case 3:
            //       return "Published";
            //     default:
            //       return "";
            //   }
            if (status == true) {
                return "Published";
            } else {
                return "Undiscoverable";
            }
        };

        const getStatusColor = (status) => {
            //   switch (status) {
            //     case 0:
            //       return "gray";
            //     case 1:
            //       return "#FFC107";
            //     case 2:
            //       return "#DC3545";
            //     case 3:
            //       return "#28A745";
            //     default:
            //       return "transparent";
            //   }
            if (status == true) {
                return "#28A745";
            } else {
                return "#DC3545";
            }
        };
        const getBoxShadow = (status) => {
            const color = getStatusColor(status);
            return `0 0 10px 2px ${color}`;
        };
        return (
            <p style={{ margin: '0' }}>
                <span style={{
                    margin: '0',
                    backgroundColor: getStatusColor(status),
                    borderRadius: '3px',
                    color: 'white',
                    padding: '2px',
                    fontSize: 'small',
                    boxShadow: getBoxShadow(status)

                }}>
                    {getStatusText(status)}
                </span>
            </p>
        );
    }
    const [authors, setAuthors] = useState([]);
    useEffect(() => {
        async function fetchAuthors() {
            const response = await fetch(`${process.env.REACT_APP_BASE_URL}/api/Author/getListOfAuthors`);
            const data = await response.json();
            setAuthors(data);
        }
        fetchAuthors();
    }, []);
    const AuthorSelect = ({ authors, selectedAuthors, handleAuthorSelectChange }) => {
        const options = authors.map(author => ({
            value: author.authorId.toString(),
            label: author.fullName + " - " + author.jobTitle
        }));

        const selectedOptions = options.filter(option =>
            selectedAuthors.includes(option.value)
        );

        return (
            <Select
                isMulti
                options={options}
                value={selectedOptions}
                onChange={selectedOptions => handleAuthorSelectChange(selectedOptions ? selectedOptions.map(option => option.value) : [])}
                placeholder="Search and select contributors..."
                isClearable
                styles={{
                    container: (base) => ({ ...base, width: '100%' }),
                    menu: (provided) => ({
                        ...provided,
                        maxHeight: '200px',
                        overflowY: 'auto',
                    }),
                    menuList: (provided) => ({
                        ...provided,
                        maxHeight: '200px',
                        overflowY: 'auto',
                    }),
                    indicatorSeparator: () => ({
                        display: 'none',
                    }),
                    dropdownIndicator: (base) => ({
                        ...base,
                        display: 'none',
                    }),
                }}
            />
        );
    };
    const handleAuthorSelectChange = (selectedAuthorIds) => {
        setSelectedAuthors(selectedAuthorIds);
    };

    // Pagination logic
    // const indexOfLastCommunity = currentPage * numPerPage;
    // const indexOfFirstCommunity = indexOfLastCommunity - numPerPage;
    // const currentItems = items.slice(indexOfFirstCommunity, indexOfLastCommunity);

    // const totalPages = Math.ceil(items.length / numPerPage);

    // const paginate = (pageNumber) => setCurrentPage(pageNumber);

    // const handleNext = () => {
    //     if (currentPage < totalPages) {
    //         setCurrentPage(currentPage + 1);
    //     }
    // };

    // const handlePrevious = () => {
    //     if (currentPage > 1) {
    //         setCurrentPage(currentPage - 1);
    //     }
    // };
    const formatDate = (dateString) => {
        if (!dateString) return '';

        const parts = dateString.split('-');
        let formattedDate = '';

        if (parts.length === 3) {
            // Case: full date (YYYY-MM-DD)
            const [year, month, day] = parts;
            formattedDate = `${day.padStart(2, '0')}-${month.padStart(2, '0')}-${year}`;
        } else if (parts.length === 2) {
            // Case: year and month (YYYY-MM)
            const [year, month] = parts;
            formattedDate = `${month.padStart(2, '0')}-${year}`;
        } else if (parts.length === 1) {
            // Case: only year (YYYY)
            const [year] = parts;
            formattedDate = year;
        }

        return formattedDate;
    };
    // if (currentItems.length != 0) {
    //     setLoading(false)
    // }
    // console.log({ currentItems });
    // useEffect(() => {
    //     filterAndSortItems();
    // }, [searchQuery, selectedAuthors, selectedYear, selectedMonth, sortState, activeButton, items]);
    // const filterAndSortItems = () => {filteredCollections}
    if (loading) {
        return (
            <div>
                <Header />
                <div className="container mt-5 d-flex justify-content-center align-items-center" style={{ height: '60vh' }}>
                    <div class="load-4">
                        {/* <p>Loading 4</p> */}
                        <div class="ring-1"></div>
                    </div>
                </div>
            </div>
        );
    }
    const handleKeywordSelectChange = (newKeywords) => {
        setSelectedKeywords(newKeywords);
    };
    return (
        <div>
            <Header />
            <div className="main-content d-flex flex-column align-items-center">
                <div className="container mt-5 col-md-8">
                    <h1 style={{ marginBottom: '3%' }}>{colName}</h1>
                    <h5 style={{ marginBottom: '' }}>Browse</h5>
                    <div className='d-flex mb-4'>
                        {buttons.map((buttonName, index) => (
                            <div
                                key={index}
                                className="btn"
                                style={{
                                    border: '1px solid black',
                                    borderRadius: index === 0 ? '4px 0 0 4px' : index === buttons.length - 1 ? '0 4px 4px 0' : '0',
                                    padding: '13px',
                                    fontWeight: 'bolder',
                                    backgroundColor: activeButton === buttonName ? '#43515F' : 'white',
                                    color: activeButton === buttonName ? 'white' : '#207698',
                                    cursor: 'pointer'
                                }}
                                onClick={() => handleButtonClick(buttonName)}
                            >
                                {buttonName}
                            </div>
                        ))}

                    </div>


                    {error && <div className="alert alert-danger">{error}</div>}

                    {activeButton === 'Recent submissions' && (
                        items.length > 0 ? (
                            <div className='col-md-12'>
                                <h6>Now showing 5 new items</h6>
                                {items.map((item, index) => (
                                    <div key={index} style={{  display: 'flex', alignItems: 'flex-start', marginBottom: '20px' }}>
                                        <img
                                            src={item.imageURL || '/The_logo_of_Little_Black_Book.png'} // Sử dụng URL ảnh từ item hoặc ảnh placeholder
                                            alt={item.title}
                                            style={{ width: '150px', height: '200px', objectFit: 'cover', marginRight: '20px' }}
                                        />
                                        <div>
                                            <Status status={true} />


                                            <h3 style={{ margin: '0' }}>
                                                <span className="author-item" onClick={() => handleRowClick(item.itemId)}>{item.title}</span>
                                            </h3>
                                            <p style={{ margin: '0' }}>{item.dateOfIssue && (<>({formatDate(item.dateOfIssue)})</>)} {item.publisher && (<> - {item.publisher}</>)}</p>
                                            {/* <p className='d-flex' style={{margin: '0'}}><p style={{margin: '0', color: '#4b9cdb', fontWeight: 'bolder', marginRight: '10px'}}>Publisher: </p>{item.publisher}</p> */}
                                            <p className='d-flex' style={{ margin: '0' }}><p style={{ margin: '0', color: '#207f9e', fontWeight: 'bolder', marginRight: '10px' }}>Contributors: </p>{item.authorItems
                                                .filter((value, index, self) =>
                                                    index === self.findIndex((t) => t.authorId === value.authorId)
                                                )
                                                .map(authorItem => authorItem.fullName)
                                                .join(', ')
                                            }</p>
                                            {/* <p className='d-flex' style={{margin: '0'}}><p style={{margin: '0', color: '#4b9cdb', fontWeight: 'bolder', marginRight: '10px'}}>Abstract: </p></p> */}
                                            <div>
                                                {expandedAbstracts[item.itemId] ? (
                                                    <div>
                                                        <div>{item.abstract}</div>
                                                        <button onClick={() => toggleAbstract(item.itemId)} style={{ background: 'none', color: '#207f9e', border: 'none', padding: '0', cursor: 'pointer', fontWeight: 'bolder' }}>
                                                            <ExpandLess /> Collapse
                                                        </button>
                                                    </div>
                                                ) : (
                                                    <div>
                                                        <div style={{ overflow: 'hidden', textOverflow: 'ellipsis', display: '-webkit-box', WebkitLineClamp: 3, WebkitBoxOrient: 'vertical' }}>
                                                            {item.abstract}
                                                        </div>
                                                        {item.abstract.split(' ').length > 20 && (
                                                            <button onClick={() => toggleAbstract(item.itemId)} style={{ background: 'none', color: '#207f9e', border: 'none', padding: '0', cursor: 'pointer', fontWeight: 'bolder' }}>
                                                                <ExpandMore />  Show more
                                                            </button>
                                                        )}
                                                    </div>
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        ) : <h6>Not found</h6>
                    )}

                    {activeButton === 'By issue date' && (
                        <>
                            <div className="d-flex flex-column">
                                <div className="d-flex">
                                    <p style={{ fontWeight: 'bolder', margin: '0px', width: '200px' }}>Choose year</p>
                                    <p style={{ fontWeight: 'bolder', margin: '0px', width: '150px' }}>Choose month</p>
                                    <p style={{ fontWeight: 'bolder', margin: '0px' }}>Choose date</p>
                                </div>
                                <div className="d-flex mb-4">
                                    <div className="d-flex flex-column align-items-start" style={{ width: '200px' }}>
                                        <input
                                            style={{ borderRadius: '6px', width: '100%' }}
                                            type="number"
                                            placeholder="Choose year"
                                            value={selectedYear}
                                            onChange={handleYearChange}
                                            className="form-control"
                                        />
                                    </div>
                                    <div className="d-flex flex-column align-items-start mx-2" style={{ width: '150px' }}>
                                        <select value={selectedMonth} onChange={handleMonthChange} className="form-select">
                                            <option value="">(Choose month)</option>
                                            {months.map((month, index) => (
                                                <option key={index} value={index + 1}>{month}</option>
                                            ))}
                                        </select>
                                    </div>
                                    <div className="d-flex flex-column align-items-start">
                                        <input
                                            style={{ borderRadius: '6px 0 0 6px' }}
                                            type="number"
                                            placeholder="Filter results by date"
                                            value={selectedDate}
                                            onChange={handleDateChange}
                                            className="form-control"
                                        />
                                    </div>
                                    <div className="d-flex flex-column align-items-start">
                                        <button onClick={handleSearchSubmit} style={{ backgroundColor: '#43515F', color: 'white', borderRadius: '0 6px 6px 0', height: '100%' }} className="btn">
                                            <Search />
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </>
                    )}
                    {activeButton === 'By authors' && (
                        <>
                            <p style={{ fontWeight: 'bolder', margin: '0px' }}>Choose contributors</p>

                            <div className="d-flex mb-4 w-50" style={{ padding: '' }}>
                                <AuthorSelect
                                    authors={authors}
                                    selectedAuthors={selectedAuthors}
                                    handleAuthorSelectChange={handleAuthorSelectChange}
                                />
                                <button onClick={handleSearchSubmit} style={{ backgroundColor: '#43515F', color: 'white', borderRadius: '0 6px 6px 0' }} className="btn">
                                    <Search />
                                </button>
                            </div>
                        </>
                    )}
                    {activeButton === 'By title' && (
                        <>
                            <p style={{ fontWeight: 'bolder', margin: '0px' }}>Search by title</p>
                            <div className="d-flex mb-4">
                                <input
                                    type="text"
                                    className="form-control w-50"
                                    placeholder="Search By Title"
                                    style={{ borderRadius: '0 0 0 0', flex: '0 1 auto' }}
                                    value={searchQuery}
                                    onChange={handleSearchChange}
                                />
                                <button onClick={handleSearchSubmit} style={{ backgroundColor: '#43515F', color: 'white', borderRadius: '0 6px 6px 0' }} className="btn">
                                    <Search />
                                </button>
                            </div>
                        </>
                    )}
                    {activeButton === 'By subject keywords' &&
                        (
                            <>
                                <p style={{ fontWeight: 'bolder', margin: '0px' }}>Search by title</p>
                                <div className="d-flex mb-4 w-50" style={{ padding: '' }}>
                                    <TagsInput
                                        style={{ borderRadius: '0 0 0 0' }}
                                        value={selectedKeywords}
                                        onChange={handleKeywordSelectChange}
                                        placeHolder="Enter keywords separated by Enter Button"
                                    />
                                    <button onClick={handleSearchSubmit} style={{ backgroundColor: '#43515F', color: 'white', borderRadius: '0 6px 6px 0' }} className="btn">
                                        <Search />
                                    </button>
                                </div>
                            </>
                        )
                    }
                    {activeButton === 'By subject category' && <p>Content for By subject category</p>}


                    {activeButton !== 'Recent submissions' ? (
                        objItemsPerPage.objmap?.length > 0 ? (
                            <div className='col-md-12'>
                                <h6>Now showing {(page - 1) * numPerPage + 1} - {objItemsPerPage.objmap?.length + (page - 1) * numPerPage} of {objItemsPerPage.totalAmount}</h6>
                                {objItemsPerPage.objmap.map((item, index) => (
                                    <div key={index} style={{  display: 'flex', alignItems: 'flex-start', marginBottom: '20px' }}>
                                        <img
                                            src={item.imageURL || '/The_logo_of_Little_Black_Book.png'} // Sử dụng URL ảnh từ item hoặc ảnh placeholder
                                            alt={item.title}
                                            style={{ width: '150px', height: '200px', objectFit: 'cover', marginRight: '20px' }}
                                        />
                                        <div>
                                            <Status status={true} />


                                            <h3 style={{ margin: '0' }}>
                                                <span className="author-item" onClick={() => handleRowClick(item.itemId)}>{item.title}</span>
                                            </h3>
                                            <p style={{ margin: '0' }}>{item.dateOfIssue && (<>({formatDate(item.dateOfIssue)})</>)} {item.publisher && (<> - {item.publisher}</>)}</p>
                                            {/* <p className='d-flex' style={{margin: '0'}}><p style={{margin: '0', color: '#4b9cdb', fontWeight: 'bolder', marginRight: '10px'}}>Publisher: </p>{item.publisher}</p> */}
                                            <p className='d-flex' style={{ margin: '0' }}><p style={{ margin: '0', color: '#207f9e', fontWeight: 'bolder', marginRight: '10px' }}>Contributors: </p>{item.authorItems
                                                .filter((value, index, self) =>
                                                    index === self.findIndex((t) => t.authorId === value.authorId)
                                                )
                                                .map(authorItem => authorItem.fullName)
                                                .join(', ')
                                            }</p>
                                            {/* <p className='d-flex' style={{margin: '0'}}><p style={{margin: '0', color: '#4b9cdb', fontWeight: 'bolder', marginRight: '10px'}}>Abstract: </p></p> */}
                                            <div>
                                                {expandedAbstracts[item.itemId] ? (
                                                    <div>
                                                        <div>{item.abstract}</div>
                                                        <button onClick={() => toggleAbstract(item.itemId)} style={{ background: 'none', color: '#207f9e', border: 'none', padding: '0', cursor: 'pointer', fontWeight: 'bolder' }}>
                                                            <ExpandLess /> Collapse
                                                        </button>
                                                    </div>
                                                ) : (
                                                    <div>
                                                        <div style={{ overflow: 'hidden', textOverflow: 'ellipsis', display: '-webkit-box', WebkitLineClamp: 3, WebkitBoxOrient: 'vertical' }}>
                                                            {item.abstract}
                                                        </div>
                                                        {item.abstract.split(' ').length > 20 && (
                                                            <button onClick={() => toggleAbstract(item.itemId)} style={{ background: 'none', color: '#207f9e', border: 'none', padding: '0', cursor: 'pointer', fontWeight: 'bolder' }}>
                                                                <ExpandMore />  Show more
                                                            </button>
                                                        )}
                                                    </div>
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        ) : <h6></h6>
                    ) : <h6></h6>}


                    {/* <nav>
                        <ul className="pagination justify-content-center">
                            <li className={`page-item ${currentPage === 1 ? 'disabled' : ''}`}>
                                <button style={{ color: '#207698' }} className="page-link" onClick={handlePrevious}>
                                    Previous
                                </button>
                            </li>
                            {Array.from({ length: totalPages }, (_, index) => (
                                <li key={index + 1} className={`page-item ${index + 1 === currentPage ? 'active' : ''}`}>
                                    <button className="page-link" style={{
                                        backgroundColor: index + 1 === currentPage ? '#207698' : '',
                                        color: index + 1 === currentPage ? '' : '#207698'
                                    }} onClick={() => paginate(index + 1)}>
                                        {index + 1}
                                    </button>
                                </li>
                            ))}
                            <li className={`page-item ${currentPage === totalPages ? 'disabled' : ''}`}>
                                <button style={{ color: '#207698' }} className="page-link" onClick={handleNext}>
                                    Next
                                </button>
                            </li>
                        </ul>
                    </nav> */}
                    {activeButton !== 'Recent submissions' && (
                        <Stack spacing={2} alignItems="center">
                            {/* <Typography>Page: {page}</Typography> */}
                            <Pagination count={objItemsPerPage.totalPage} page={page} onChange={handleChangePage} size="large" />
                        </Stack>)}
                </div>
            </div>
        </div>
    );
};
export default CollectionList;
