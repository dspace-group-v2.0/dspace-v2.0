import React, { useState, useEffect } from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';
import HeaderAdmin from '../../components/Header/Header-admin';
import { Helmet } from 'react-helmet';
import { toast } from 'react-toastify';

const Addauthor = () => {
    const [authorData, setAuthorData] = useState({
        fullName: '',
        jobTitle: '',
        uri: '',
        type: '',
    });
    const [successMessage, setSuccessMessage] = useState('');
    const [errors, setErrors] = useState({});

    useEffect(() => {
        setAuthorData((prevData) => ({
            ...prevData,
            uri: window.location.origin + "/Dspace/Author/AuthorDetail/",
        }));
    }, []);
    const addNotification = (message, type) => {
        toast(message, { type });
    };
    const handleChange = (e) => {
        const { name, value } = e.target;
        setAuthorData({
            ...authorData,
            [name]: value,
        });

        // Clear error for the field when user starts typing
        setErrors({
            ...errors,
            [name]: '',
        });
    };

    const validate = () => {
        let valid = true;
        const newErrors = {};

        if (!authorData.fullName.trim()) {
            newErrors.fullName = 'Full Name is required';
            valid = false;
        }

        if (!authorData.jobTitle.trim()) {
            newErrors.jobTitle = 'Job Title is required';
            valid = false;
        }

        // if (!authorData.type.trim()) {
        //     newErrors.type = 'Type is required';
        //     valid = false;
        // }

        setErrors(newErrors);
        return valid;
    };

    const handleSubmit = (e) => {
        e.preventDefault();

        if (!validate()) {
            return;
        }

        axios.post(`${process.env.REACT_APP_BASE_URL}/api/Author/createAuthor`, authorData)
            .then(response => {
                console.log('Author created successfully!', response.data);
                setSuccessMessage('Author added successfully!');
                addNotification(response.data, 'success');
                // Reset form after successful submission
                setAuthorData({
                    fullName: '',
                    jobTitle: '',
                    uri: window.location.origin + "/Dspace/Author/AuthorDetail/",
                    type: '',
                });
            })
            .catch(error => {
                console.error('There was an error creating the author!', error);
                addNotification(error.response.data, 'error'); 
            });
    };

    return (
        <div>
            <HeaderAdmin />
            <Helmet>
                <meta charSet="utf-8" />
                <title>Create new Author - dSPACE</title>
            </Helmet>
            <div className="container mt-5">
                <h1>Create New Author</h1>
                {/* {successMessage && <div className="alert alert-success">{successMessage}</div>} */}
                <div className="row">
                    <div className="col-md-12">
                        <form onSubmit={handleSubmit}>
                            <div className="form-group row w-75">
                                <div className="col">
                                    <label>Full Name</label>
                                    <input
                                        type="text"
                                        className={`form-control ${errors.fullName && 'is-invalid'}`}
                                        name="fullName"
                                        value={authorData.fullName}
                                        onChange={handleChange}
                                        
                                    />
                                    {errors.fullName && <div className="invalid-feedback">{errors.fullName}</div>}
                                </div>
                                <div className="col">
                                    <label>Job Title</label>
                                    <input
                                        type="text"
                                        className={`form-control ${errors.jobTitle && 'is-invalid'}`}
                                        name="jobTitle"
                                        value={authorData.jobTitle}
                                        onChange={handleChange}
                                        
                                    />
                                    {errors.jobTitle && <div className="invalid-feedback">{errors.jobTitle}</div>}
                                </div>
                            </div>
                            <div className="form-group row w-75">
                                <div className="col">
                                    <label>Type</label>
                                    <input
                                        type="text"
                                        className={`form-control ${errors.type && 'is-invalid'}`}
                                        name="type"
                                        value={authorData.type}
                                        onChange={handleChange}
                                        
                                    />
                                    {errors.type && <div className="invalid-feedback">{errors.type}</div>}
                                </div>
                                <div className="col">
                                    <input
                                        type="hidden"
                                        className="form-control"
                                        name="uri"
                                        value={authorData.uri}
                                        onChange={handleChange}
                                        
                                    />
                                </div>
                            </div>
                            <button type="submit" className="btn btn-primary mt-3">Add</button>
                        </form>
                    </div>
                </div>
                <div className="mt-3">
                    <Link to="/Dspace/Author/AuthorList">Back to List</Link>
                </div>
            </div>
        </div>
    );
};

export default Addauthor;
