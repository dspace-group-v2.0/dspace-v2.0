import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useParams, useNavigate, Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from '../../components/Header/Header';
import { Helmet } from 'react-helmet';
import {
    ExpandLess, ExpandMore
} from '@material-ui/icons';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';
import { Button, Modal } from 'react-bootstrap';
import { toast } from 'react-toastify';
import { jwtDecode } from 'jwt-decode';

const AuthorDetails = () => {
    const { authorId } = useParams();
    const [author, setAuthor] = useState(null);
    const [message, setMessage] = useState(null);
    const [error, setError] = useState(null);
    const navigate = useNavigate();
    const [numPerPage, setNumPerPage] = useState(5);
    const [page, setPage] = React.useState(1);
    const [items, setItems] = useState([]);
    const [objItemsPerPage, setobjItemsPerPage] = useState([]);
    const [expandedAbstracts, setExpandedAbstracts] = useState({});
    const token = localStorage.getItem('Token');
    const userRole = token ? jwtDecode(token)["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"] : null;
    const toggleAbstract = (itemId) => {
        setExpandedAbstracts(prevState => ({
            ...prevState,
            [itemId]: !prevState[itemId]
        }));
    };
    const addNotification = (message, type) => {
        toast(message, { type });
    };
    const handleChangePage = (event, value) => {
        setPage(value);
    };
    const formatDate = (dateString) => {
        if (!dateString) return '';

        const parts = dateString.split('-');
        let formattedDate = '';

        if (parts.length === 3) {
            // Case: full date (YYYY-MM-DD)
            const [year, month, day] = parts;
            formattedDate = `${day.padStart(2, '0')}-${month.padStart(2, '0')}-${year}`;
        } else if (parts.length === 2) {
            // Case: year and month (YYYY-MM)
            const [year, month] = parts;
            formattedDate = `${month.padStart(2, '0')}-${year}`;
        } else if (parts.length === 1) {
            // Case: only year (YYYY)
            const [year] = parts;
            formattedDate = year;
        }

        return formattedDate;
    };
    useEffect(() => {
        const fetchAuthorDetails = async () => {
            try {
                const response = await axios.get(`${process.env.REACT_APP_BASE_URL}/api/Author/getAuthor/${authorId}`);
                setAuthor(response.data);
            } catch (error) {
                setError('There was an error fetching the author details!');
                console.error('Error fetching author details:', error);
                addNotification(error.response.data, 'error');
            }
        };

        fetchAuthorDetails();
    }, [authorId]);
    const DeleteButtonWithModal = ({ handleDelete }) => {
        const [show, setShow] = useState(false);

        const handleClose = () => setShow(false);
        const handleShow = () => setShow(true);

        const handleConfirmDelete = () => {
            handleDelete();
            handleClose();
        };

        return (
            <>
                <button variant="danger" className="btn btn-danger me-2" onClick={handleShow}>
                    Delete
                </button>

                <Modal show={show} onHide={handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Confirm Deletion</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>Are you sure you want to delete this contributor?</Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Cancel
                        </Button>
                        <Button variant="danger" onClick={handleConfirmDelete}>
                            Delete
                        </Button>
                    </Modal.Footer>
                </Modal>
            </>
        );
    };
    const handleDelete = () => {
        axios.delete(`${process.env.REACT_APP_BASE_URL}/api/Author/DeleteAuthor/${authorId}`)
            .then(response => {
                setMessage({ text: 'Author deleted successfully!', type: 'success' });
                addNotification(response.data, 'success');
                setTimeout(() => navigate('/Dspace/Author/AuthorList'), 2000);
            })
            .catch(error => {
                console.error('There was an error deleting the author!', error);
                setMessage({ text: 'Author cannot be deleted as it may have associated records. Please remove them first.', type: 'danger' });
                addNotification(error.response.data, 'error');
            });
    };
    useEffect(() => {

        handleSearchSubmit()

    }, [authorId, numPerPage, page]);

    const handleSearchSubmit = () => {
        let payload = {};
        console.log(payload);
        let api = ``;


        api = `${process.env.REACT_APP_BASE_URL}/api/Item/SearchItemByAuthor/${page}/${numPerPage}?collectionId=0`;
        payload = [authorId]

        axios.post(api, payload)
            .then(({ data }) => {
                setItems(data);
                setobjItemsPerPage(data);
            })
            .catch(error => {
                setError(error.message);
                addNotification(error.message, 'error');
            });
    };
    if (error) {
        return (
            <div>
                <Header />
                <Helmet>
                    <meta charSet="utf-8" />
                    <title>Author unknown... - dSPACE</title>
                </Helmet>
                <div className="container mt-5">
                    <h1 className="mb-4">Author Details</h1>
                    <div className="alert alert-danger">{error}</div>
                    <button className="btn btn-secondary" onClick={() => navigate('/Dspace/Author/AuthorList')}>Back to List</button>
                </div>
            </div>
        );
    }

    if (!author) {
        return (
            <div>
                <Header />
                <Helmet>
                    <meta charSet="utf-8" />
                    <title>Author unknown... - dSPACE</title>
                </Helmet>
                <div className="container mt-5">
                    <h1 className="mb-4">Author Details</h1>
                    <p>Loading...</p>
                </div>
            </div>
        );
    }
    function Status({ status }) {
        const getStatusText = (status) => {
            if (status == true) {
                return "Published";
            } else {
                return "Undiscoverable";
            }
        };

        const getStatusColor = (status) => {
            if (status == true) {
                return "#28A745";
            } else {
                return "#DC3545";
            }
        };
        const getBoxShadow = (status) => {
            const color = getStatusColor(status);
            return `0 0 10px 2px ${color}`;
        };
        return (
            <p style={{ margin: '0' }}>
                <span style={{
                    margin: '0',
                    backgroundColor: getStatusColor(status),
                    borderRadius: '3px',
                    color: 'white',
                    padding: '2px',
                    fontSize: 'small',
                    boxShadow: getBoxShadow(status)

                }}>
                    {getStatusText(status)}
                </span>
            </p>
        );
    }
    const handleRowClick = (itemId) => {
        // Chuyển hướng sang trang chi tiết bộ sưu tập
        navigate(`/DSpace/ItemDetail/${itemId}`);
    };

    return (
        <div>
            <Header />
            <Helmet>
                <meta charSet="utf-8" />
                <title>Author {`${author?.fullName}`} - dSPACE</title>
            </Helmet>
            <div className="container mt-5">
                <h1 className="mb-4">Author Details</h1>
                <div className="row justify-content-center">
                    <div className="col-md-12">
                        <div className="card">
                            <div className="card-body">
                                <div className="row border-bottom pb-2 my-3">
                                    <div className="col-sm-4 text-end">Full Name:</div>
                                    <div className="col-sm-6">{author.fullName}</div>
                                </div>
                                <div className="row border-bottom pb-2 my-3">
                                    <div className="col-sm-4 text-end">Job Title:</div>
                                    <div className="col-sm-6">{author.jobTitle}</div>
                                </div>
                                <div className="row border-bottom pb-2 my-3">
                                    <div className="col-sm-4 text-end">Date Accessioned:</div>
                                    <div className="col-sm-6">{`${author.dateAccessioned}`.split('T')[0]}</div>
                                </div>
                                <div className="row border-bottom pb-2 my-3">
                                    <div className="col-sm-4 text-end">Date Available:</div>
                                    <div className="col-sm-6">{`${author.dateAvailable}`.split('T')[0]}</div>
                                </div>
                                <div className="row border-bottom pb-2 my-3">
                                    <div className="col-sm-4 text-end">URI:</div>
                                    <div className="col-sm-6">
                                        <a href={author.uri}>{author.uri}</a>
                                    </div>
                                </div>
                                <div className="row pb-2 my-3">
                                    <div className="col-sm-4 text-end">Type:</div>
                                    <div className="col-sm-6">{author.type}</div>
                                </div>
                            </div>
                        </div>
                        {(userRole !== "STUDENT" && userRole !== "LECTURER") ? (
                            
                            <div className="mt-4">
                                <button className="btn btn-primary me-2" onClick={() => navigate(`/Dspace/Author/EditAuthor/${author.authorId}`)}>Edit</button>
                                <DeleteButtonWithModal handleDelete={handleDelete} />
                                <button className="btn btn-secondary" onClick={() => navigate('/Dspace/Author/AuthorList')}>Back to List</button>
                            </div>
                        ) : null}
                    </div>
                </div>
                <div className="row justify-content-center mt-5">
                    <div className="col-md-11">
                        {objItemsPerPage.objmap?.length > 0 ? (
                            <div>
                                <h6>Now showing {(page - 1) * numPerPage + 1} - {objItemsPerPage.objmap?.length + (page - 1) * numPerPage} of {objItemsPerPage.totalAmount}</h6>
                                {objItemsPerPage.objmap.map((item, index) => (
                                    <div key={index} style={{ display: 'flex', alignItems: 'flex-start', marginBottom: '20px' }}>
                                        <img
                                            src={item.imageURL || '/The_logo_of_Little_Black_Book.png'} // Sử dụng URL ảnh từ item hoặc ảnh placeholder
                                            alt={item.title}
                                            style={{ width: '150px', height: '200px', objectFit: 'cover', marginRight: '20px' }}
                                        />
                                        <div>
                                            <Status status={item.discoverable} />
                                            <h3 style={{ margin: '0' }}>
                                                <span className="author-item" onClick={() => handleRowClick(item.itemId)}>{item.title}</span>
                                            </h3>
                                            <p style={{ margin: '0' }}>{item.dateOfIssue && (<>({formatDate(item.dateOfIssue)})</>)} {item.publisher && (<> - {item.publisher}</>)}</p>
                                            <p className='d-flex' style={{ margin: '0' }}>
                                                <p style={{ margin: '0', color: '#207f9e', fontWeight: 'bolder', marginRight: '10px' }}>Contributors: </p>
                                                {item.authorItems.filter((value, index, self) => index === self.findIndex((t) => t.authorId === value.authorId))
                                                    .map(authorItem => authorItem.fullName)
                                                    .join(', ')
                                                }
                                            </p>
                                            <div>
                                                {expandedAbstracts[item.itemId] ? (
                                                    <div>
                                                        <div>{item.abstract}</div>
                                                        <button onClick={() => toggleAbstract(item.itemId)} style={{ background: 'none', color: '#207f9e', border: 'none', padding: '0', cursor: 'pointer', fontWeight: 'bolder' }}>
                                                            <ExpandLess /> Collapse
                                                        </button>
                                                    </div>
                                                ) : (
                                                    <div>
                                                        <div style={{ overflow: 'hidden', textOverflow: 'ellipsis', display: '-webkit-box', WebkitLineClamp: 3, WebkitBoxOrient: 'vertical' }}>
                                                            {item.abstract}
                                                        </div>
                                                        {item.abstract.split(' ').length > 20 && (
                                                            <button onClick={() => toggleAbstract(item.itemId)} style={{ background: 'none', color: '#207f9e', border: 'none', padding: '0', cursor: 'pointer', fontWeight: 'bolder' }}>
                                                                <ExpandMore /> Show more
                                                            </button>
                                                        )}
                                                    </div>
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        ) : <h6></h6>}
                    </div>
                </div>
            </div>

            <Stack spacing={2} alignItems="center">
                {/* <Typography>Page: {page}</Typography> */}
                <Pagination count={objItemsPerPage.totalPage} page={page} onChange={handleChangePage} size="large" />
            </Stack>
        </div>
    );
};

export default AuthorDetails;
