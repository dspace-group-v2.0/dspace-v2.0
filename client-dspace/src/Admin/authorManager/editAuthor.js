import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useParams, Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import HeaderAdmin from '../../components/Header/Header-admin';
import { Helmet } from 'react-helmet';
import { toast } from 'react-toastify';

const EditAuthor = () => {
  const { authorId } = useParams();

  const [formData, setFormData] = useState({
    fullName: '',
    jobTitle: '',
    uri: '',
    type: '',
  });
  const addNotification = (message, type) => {
    toast(message, { type });
};
  const [successMessage, setSuccessMessage] = useState('');
  const [authorHeader, setauthorHeader] = useState('');
  const [errors, setErrors] = useState({});

  useEffect(() => {
    axios.get(`${process.env.REACT_APP_BASE_URL}/api/Author/getAuthor/${authorId}`)
      .then(response => {
        setFormData(response.data);
        setauthorHeader(response.data.fullName);
      })
      .catch(error => {
        console.error('There was an error fetching the author!', error);
        addNotification(error.response.data, 'error'); 
      });
  }, [authorId]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });

    // Clear error for the field when user starts typing
    setErrors({
      ...errors,
      [name]: '',
    });
  };

  const validate = () => {
    let valid = true;
    const newErrors = {};

    if (!formData.fullName.trim()) {
      newErrors.fullName = 'Full Name is required';
      valid = false;
    }

    if (!formData.jobTitle.trim()) {
      newErrors.jobTitle = 'Job Title is required';
      valid = false;
    }

    // if (!formData.type.trim()) {
    //   newErrors.type = 'Type is required';
    //   valid = false;
    // }

    setErrors(newErrors);
    return valid;
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (!validate()) {
      return;
    }

    axios.put(`${process.env.REACT_APP_BASE_URL}/api/Author/updateAuthor/${authorId}`, formData)
      .then(response => {
        console.log('Author updated successfully!', response.data);
        setSuccessMessage('Author updated successfully!');
        addNotification(response.data, 'success'); 
      })
      .catch(error => {
        console.error('There was an error updating the author!', error);
        addNotification(error.response.data, 'error');       });
  };

  return (
    <div>
      <HeaderAdmin />
      <Helmet>
        <meta charSet="utf-8" />
        <title>Modify Author - {`${formData?.fullName}`} - dSPACE</title>
      </Helmet>
      <div className="container mt-5">
        <h2>Edit Author - {authorHeader}</h2>
        {/* {successMessage && <div className="alert alert-success">{successMessage}</div>} */}
        <form onSubmit={handleSubmit}>
          <div className="row">
            <div className="col-md-6 mb-3">
              <div className="form-group">
                <label htmlFor="fullName">Full Name</label>
                <input
                  type="text"
                  className={`form-control ${errors.fullName && 'is-invalid'}`}
                  name="fullName"
                  value={formData.fullName}
                  onChange={handleChange}
                />
                {errors.fullName && <div className="invalid-feedback">{errors.fullName}</div>}
              </div>
            </div>
            <div className="col-md-6 mb-3">
              <div className="form-group">
                <label htmlFor="jobTitle">Job Title</label>
                <input
                  type="text"
                  className={`form-control ${errors.jobTitle && 'is-invalid'}`}
                  name="jobTitle"
                  value={formData.jobTitle}
                  onChange={handleChange}

                />
                {errors.jobTitle && <div className="invalid-feedback">{errors.jobTitle}</div>}
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6 mb-3">
              <div className="form-group">
                <label htmlFor="type">Type</label>
                <input
                  type="text"
                  className={`form-control ${errors.type && 'is-invalid'}`}
                  name="type"
                  value={formData.type}
                  onChange={handleChange}

                />
                {errors.type && <div className="invalid-feedback">{errors.type}</div>}
              </div>
            </div>
          </div>
          <div className="d-flex justify-content-start">
            <button type="submit" className="btn btn-primary">Save</button>
          </div>
        </form>
        <div className="mt-3">
          <Link to="/Dspace/Author/AuthorList">Back to List</Link>
        </div>
      </div>
    </div>
  );
};

export default EditAuthor;
