import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from '../../components/Header/Header';
import { Helmet } from 'react-helmet';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemButton from '@mui/material/ListItemButton';
import Divider from '@mui/material/Divider';
import Typography from '@mui/material/Typography';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';

const ListAuthor = () => {
    const [authors, setAuthors] = useState([]);
    const [searchQuery, setSearchQuery] = useState('');
    const [searchType, setSearchType] = useState('');
    const [error, setError] = useState(null);
    const [currentPage, setCurrentPage] = useState(1);
    const itemsPerPage = 10; // Adjust the number of items per page as needed

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await axios.get(`${process.env.REACT_APP_BASE_URL}/api/Author/getListOfAuthors`);
                setAuthors(response.data);
            } catch (error) {
                setError(error.message);
                console.error("Error fetching data:", error);
            }
        };

        fetchData();
    }, []);

    const handleSearchChange = (e) => {
        setSearchQuery(e.target.value);
    };

    const handleSearchTypeChange = (e) => {
        setSearchType(e.target.value);
        setSearchQuery('');
    };

    const filteredAuthors = authors.filter(author => {
        if (!searchQuery.trim()) return true;

        const normalizedSearchQuery = searchQuery.replace(/\s+/g, '').toLowerCase();
        switch (searchType) {
            case 'fullName':
                return (
                    `${author.fullName}`.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery)
                );
            case 'jobTitle':
                return author.jobTitle.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery);
            default:
                if (author.jobTitle === null) {
                    author.jobTitle = ''
                }
                return (
                    `${author.fullName}`?.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery) ||
                    author?.jobTitle.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery)
                );
        }
    });

    const indexOfLastItem = currentPage * itemsPerPage;
    const indexOfFirstItem = indexOfLastItem - itemsPerPage;
    const currentAuthors = filteredAuthors.slice(indexOfFirstItem, indexOfLastItem);

    const totalPages = Math.ceil(filteredAuthors.length / itemsPerPage);

    const handlePageChange = (pageNumber) => {
        setCurrentPage(pageNumber);
    };

    const handleNextPage = () => {
        if (currentPage < totalPages) {
            setCurrentPage(prevPage => prevPage + 1);
        }
    };

    const handlePreviousPage = () => {
        if (currentPage > 1) {
            setCurrentPage(prevPage => prevPage - 1);
        }
    };

    return (
        <div>
            <Header />
            <Helmet>
                <meta charSet="utf-8" />
                <title>List of Authors - dSPACE</title>
            </Helmet>
            <div className="container mt-5">
                <h1 className="mb-4">Authors List</h1>
                <div className="row mb-4 align-items-center">
                    <div className="col-md-4">
                        <label htmlFor="searchInput" className="form-label fw-bold">Search</label>
                        <input
                            id="searchInput"
                            type="text"
                            className="form-control"
                            placeholder="Search"
                            value={searchQuery}
                            onChange={handleSearchChange}
                        />
                    </div>
                    <div className="col-md-4">
                        <label htmlFor="searchTypeSelect" className="form-label fw-bold">Search By</label>
                        <select
                            id="searchTypeSelect"
                            className="form-select"
                            value={searchType}
                            onChange={handleSearchTypeChange}
                        >
                            <option value="">Select...</option>
                            <option value="fullName">Full Name</option>
                            <option value="jobTitle">Job Title</option>
                        </select>
                    </div>
                    <div className="col-md-4 d-flex align-items-center justify-content-end">
                        <Link to="/Dspace/Author/AddAuthor" className="btn btn-primary">Create new author</Link>
                    </div>
                </div>
                {error && <div className="alert alert-danger">Error fetching data: {error}</div>}
                <List sx={{ width: '100%', maxWidth: '100%', bgcolor: 'background.paper' }}>
                    {currentAuthors.length > 0 ? (
                        currentAuthors.map((author, index) => (
                            <React.Fragment key={author.authorId}>
                                <ListItem alignItems="flex-start" sx={{ margin: 0 }}>
                                    <ListItemButton component={Link} to={`/Dspace/Author/AuthorDetail/${author.authorId}`}>
                                        <ListItemAvatar>
                                            <Avatar alt="Author" src="/user.png" />
                                        </ListItemAvatar>
                                        <ListItemText
                                            primary={
                                                <Typography variant="h6" component="div">
                                                    {author.fullName}
                                                </Typography>
                                            }
                                            secondary={author.jobTitle}
                                        />
                                    </ListItemButton>
                                </ListItem>
                                <Divider component="li" />
                            </React.Fragment>
                        ))
                    ) : (
                        <ListItem>
                            <ListItemText
                                primary="No authors available"
                                primaryTypographyProps={{ align: 'center' }}
                            />
                        </ListItem>
                    )}
                </List>
                <div className="d-flex justify-content-center mt-4">
                    <nav>
                        <ul className="pagination">
                            <li className={`page-item ${currentPage === 1 ? 'disabled' : ''}`}>
                                <button className="page-link" onClick={handlePreviousPage}>Previous</button>
                            </li>
                            {[...Array(totalPages)].map((_, i) => (
                                <li key={i} className={`page-item ${currentPage === i + 1 ? 'active' : ''}`}>
                                    <button className="page-link" onClick={() => handlePageChange(i + 1)}>
                                        {i + 1}
                                    </button>
                                </li>
                            ))}
                            <li className={`page-item ${currentPage === totalPages ? 'disabled' : ''}`}>
                                <button className="page-link" onClick={handleNextPage}>Next</button>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    );
};

export default ListAuthor;
