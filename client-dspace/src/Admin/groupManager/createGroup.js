import React, { useState } from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from '../../components/Header/Header';
import { Link } from 'react-router-dom'; // Assuming you're using react-router-dom
import HeaderAdmin from '../../components/Header/Header-admin';
import { Helmet } from 'react-helmet';
import { toast } from 'react-toastify';

const CreateGroup = () => {
    const [groupData, setGroupData] = useState({
        title: '',
        description: '',
        isActive: true,
    });
    const [successMessage, setSuccessMessage] = useState('');
    const [errors, setErrors] = useState({}); // State for errors

    const handleChange = (e) => {
        const { name, value, type, checked } = e.target;
        setGroupData({
            ...groupData,
            [name]: type === 'checkbox' ? checked : value,
        });
    };
    const addNotification = (message, type) => {
        toast(message, { type });
    };
    const validate = () => {
        let newErrors = {};

        if (!groupData.title.trim()) newErrors.title = 'Title is required';
        if (!groupData.description.trim()) newErrors.description = 'Description is required';

        setErrors(newErrors);
        return Object.keys(newErrors).length === 0;
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        if (validate()) {
            // Send data to your API
            axios.post(`${process.env.REACT_APP_BASE_URL}/api/Group/createGroup`, groupData)
                .then(response => {
                    console.log('Group created successfully!', response.data);
                    setSuccessMessage('Group added successfully!');
                    // Reset form after successful submission
                    setGroupData({
                        title: '',
                        description: '',
                        isActive: true,
                    });
                    addNotification('Group added successfully', 'success');
                })
                .catch(error => {
                    console.error('There was an error creating the group!', error);
                    addNotification('There was an error creating the group!', 'error');
                });
        }
    };

    return (
        <div>
            <HeaderAdmin />
            <Helmet>
                <meta charSet="utf-8" />
                <title>Create new Group - dSPACE</title>
            </Helmet>
            <div className="container mt-5">
                {/* <div className="py-1 bg-light">
                    <Link to="/HomepageAdmin" className="text-decoration-none">Home </Link>
                    <span className="text-dark"><strong>| Add Group</strong></span>
                </div> */}
                <h1 className="mb-4">Create New Group</h1>
                {/* {successMessage && <div className="alert alert-success">{successMessage}</div>} */}
                <form onSubmit={handleSubmit}>
                    <div className="col-md-6 mb-3">
                        <div className="form-group">
                            <label>Title</label>
                            <input
                                type="text"
                                className={`form-control ${errors.title ? 'is-invalid' : ''}`}
                                name="title"
                                value={groupData.title}
                                onChange={handleChange}
                                
                            />
                            {errors.title && <div className="invalid-feedback">{errors.title}</div>}
                        </div>
                    </div>
                    <div className="col-md-6 mb-3">
                        <div className="form-group">
                            <label>Description</label>
                            <textarea
                                className={`form-control ${errors.description ? 'is-invalid' : ''}`}
                                name="description"
                                value={groupData.description}
                                onChange={handleChange}
                            />
                            {errors.description && <div className="invalid-feedback">{errors.description}</div>}
                        </div>
                    </div>
                    <div className="form-group form-check mb-3">
                        <input
                            type="checkbox"
                            className="form-check-input"
                            name="isActive"
                            checked={groupData.isActive}
                            onChange={handleChange}
                        />
                        <label className="form-check-label">Is Active</label>
                    </div>
                    <button type="submit" className="btn btn-primary">Add</button>
                </form>
                <div className="mt-3">
                    <Link to="/Dspace/Group/GroupList">Back to List</Link>
                </div>
            </div>
        </div>
    );
};

export default CreateGroup;
