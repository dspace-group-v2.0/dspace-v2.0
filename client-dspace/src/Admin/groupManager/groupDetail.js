import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useParams, useNavigate, Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import HeaderAdmin from '../../components/Header/Header-admin';
import { Helmet } from 'react-helmet';
import { Button, Modal } from 'react-bootstrap';
import { toast } from 'react-toastify';

const GroupDetails = () => {
    const { groupId } = useParams();
    const navigate = useNavigate();
    const [group, setGroup] = useState(null);
    const [error, setError] = useState(null);
    const [successMessage, setSuccessMessage] = useState('');
    const [failureMessage, setFailureMessage] = useState('');
    const [currentPage, setCurrentPage] = useState(1);
    const [perPage] = useState(4);
    const [showModal, setShowModal] = useState(false);
    const [modalContent, setModalContent] = useState({});
    const handleShowModal = (content) => {
        setModalContent(content);
        setShowModal(true);
    };
    const indexOfLast = currentPage * perPage;
    const indexOfFirst = indexOfLast - perPage;

    const [currentCommunityPage, setCurrentCommunityPage] = useState(1);
    const [communitiesPerPage] = useState(4);
    const indexOfLastCommunity = currentCommunityPage * communitiesPerPage;
    const indexOfFirstCommunity = indexOfLastCommunity - communitiesPerPage;


    const [currentCollectionPage, setCurrentCollectionPage] = useState(1);
    const [collectionsPerPage] = useState(4);
    const indexOfLastCollection = currentCollectionPage * collectionsPerPage;
    const indexOfFirstCollection = indexOfLastCollection - collectionsPerPage;

    const handleCloseModal = () => setShowModal(false);
    const fetchGroupDetails = async () => {
        try {
            const response = await axios.get(`${process.env.REACT_APP_BASE_URL}/api/Group/getGroup/${groupId}`);
            setGroup(response.data);
        } catch (error) {
            setError(error.message);
            console.error('Error fetching group details:', error);
        }
    };

    useEffect(() => {
        fetchGroupDetails();
    }, [groupId]);
    const addNotification = (message, type) => {
        toast(message, { type });
    };
    const handleDeleteGroup = async () => {
        try {
            const response = await axios.delete(`${process.env.REACT_APP_BASE_URL}/api/Group/deleteGroup/${groupId}`);
            if (response.status === 200) {
                setSuccessMessage('Group deleted successfully');
                setFailureMessage('');
                addNotification('Group deleted successfully', 'success');
                setTimeout(() => {
                    navigate('/Dspace/Group/GroupList');
                }, 2000);
                handleCloseModal();
            } else {
                setFailureMessage('Failed to delete group');
                addNotification('Failed to delete group', 'error');
                setSuccessMessage('');
                handleCloseModal();
            }
        } catch (error) {
            console.error('Error deleting group:', error);
            setFailureMessage('Failed to delete group');
            addNotification('Empty group before delete!', 'error');
            setSuccessMessage('');
            handleCloseModal();
        }
    };

    const handleDeleteMember = async (userId, groupId) => {
        // if (window.confirm('Are you sure you want to delete this member?')) {
        try {
            const response = await axios.put(`${process.env.REACT_APP_BASE_URL}/api/GroupPeople/DeletePeopleInGroup`, {
                "groupId": groupId + '',
                "peopleId": userId + '',
            });
            if (response.status === 200) {
                setSuccessMessage('Member deleted successfully');
                setFailureMessage('');
                addNotification('Member deleted successfully', 'success');
                setGroup({
                    ...group,
                    listPeopleInGroup: group.listPeopleInGroup.filter(member => member.peopleId !== userId)
                });
                handleCloseModal();
            } else {
                setFailureMessage('Failed to delete member');
                addNotification('Failed to delete member', 'error');
                setSuccessMessage('');
                handleCloseModal();
            }
        } catch (error) {
            console.error('Error deleting member:', error);
            setFailureMessage('Failed to delete member');
            addNotification('Failed to delete member', 'error');
            setSuccessMessage('');
            handleCloseModal();
        }
        // }
    };

    const handleDeleteCollection = async (collectionId) => {
        // if (window.confirm('Are you sure you want to delete this collection?')) {
        try {
            const response = await axios.delete(`${process.env.REACT_APP_BASE_URL}/api/CollectionGroup/DeleteCollectionGroup?id=${collectionId}`);
            if (response.status === 200) {
                setSuccessMessage('Collection deleted successfully');
                addNotification('Collection deleted successfully', 'success');
                setFailureMessage('');
                setGroup({
                    ...group,
                    listCollectionGroup: group.listCollectionGroup.filter(collection => collection.id !== collectionId)
                });
                handleCloseModal();
            } else {
                setFailureMessage('Failed to delete collection');
                addNotification('Failed to delete collection', 'error');
                setSuccessMessage('');
                handleCloseModal();
            }
        } catch (error) {
            console.error('Error deleting collection:', error);
            setFailureMessage('Failed to delete collection');
            addNotification('Failed to delete collection', 'error');
            setSuccessMessage('');
            handleCloseModal();
        }
        // }
    };

    const handleDeleteCommunity = async (communityId) => {
        // if (window.confirm('Are you sure you want to delete this community?')) {
        try {
            const response = await axios.delete(`${process.env.REACT_APP_BASE_URL}/api/CommunityGroup/DeleteCommunityGroup?id=${communityId}`);
            if (response.status === 200) {
                setSuccessMessage('Community deleted successfully');
                addNotification('Community deleted successfully', 'success');
                setFailureMessage('');
                fetchGroupDetails();
                handleCloseModal();
            } else {
                setFailureMessage('Failed to delete community');
                addNotification('Failed to delete community', 'error');
                setSuccessMessage('');
                handleCloseModal();
            }
        } catch (error) {
            console.error('Error deleting community:', error);
            setFailureMessage('Failed to delete community');
            addNotification('Failed to delete community', 'error');
            setSuccessMessage('');
            handleCloseModal();
        }
        // }
    };
    const currentMembers = group ? group.listPeopleInGroup.slice(indexOfFirst, indexOfLast) : [];
    const currentCommunities = group ? group.listCommunityGroup.slice(indexOfFirstCommunity, indexOfLastCommunity) : [];
    const currentCollections = group ? group.listCollectionGroup.slice(indexOfFirstCollection, indexOfLastCollection) : [];

    const paginate = (pageNumber) => setCurrentPage(pageNumber);
    const paginateCommunities = (pageNumber) => setCurrentCommunityPage(pageNumber);
    const paginateCollections = (pageNumber) => setCurrentCollectionPage(pageNumber);
    if (!group) {
        return <div>Loading...</div>;
    }


    return (
        <div>
            <HeaderAdmin />
            <Helmet>
                <meta charSet="utf-8" />
                <title>{`${group?.title}`} - dSPACE</title>
            </Helmet>
            <div className="container mt-5">
                <h1 className="mb-4">Group Details</h1>
                <div className="card" style={{ maxWidth: '50%' }}>
                    <div className="card-body">
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-3 text-end">Title:</div>
                            <div className="col-sm-6">{group.title}</div>
                        </div>
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-3 text-end">Description:</div>
                            <div className="col-sm-6">{group.description}</div>
                        </div>
                        <div className="row pb-2 my-3">
                            <div className="col-sm-3 text-end">Active:</div>
                            <div className="col-sm-6"><input type="checkbox" checked={group.isActive} readOnly /></div>
                        </div>
                    </div>
                </div>
                <div className="mt-4">
                    <h4>Group Members</h4>
                    <table className="table table-striped table-bordered">
                        <thead className="thead-dark">
                            <tr>
                                <th>Full Name</th>
                                <th>Address</th>
                                <th>Phone Number</th>
                                <th>Email</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {currentMembers.map(member => (
                                <tr key={member.peopleId}>
                                    <td>{`${(member.firstName || '')} ${(member.lastName || '')}`.trim() || 'N/A'}</td>
                                    <td>{member.address || 'N/A'}</td>
                                    <td>{member.phoneNumber || 'N/A'}</td>
                                    <td>{member.email || 'N/A'}</td>
                                    <td>
                                        <button className="btn btn-danger me-2" onClick={() => handleShowModal({ type: 'member', id: member.peopleId, groupId })}>
                                            Delete
                                        </button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                    <div className="d-flex justify-content-center">
                        <nav>
                            <ul className="pagination">
                                {Array.from({ length: Math.ceil(group.listPeopleInGroup.length / perPage) }, (_, i) => (
                                    <li key={i + 1} className={`page-item ${currentPage === i + 1 ? 'active' : ''}`}>
                                        <button onClick={() => paginate(i + 1)} className="page-link">
                                            {i + 1}
                                        </button>
                                    </li>
                                ))}
                            </ul>
                        </nav>
                    </div>
                </div>

                <div className="col-12 mt-4">
                    <h4>Communities</h4>
                    <table className="table table-striped table-bordered">
                        <thead className="thead-dark">
                            <tr>
                                <th>Community Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {currentCommunities.map(community => (
                                <tr key={community.id}>
                                    <td>{community.communityName}</td>
                                    <td>
                                        <button className="btn btn-danger me-2" onClick={() => handleShowModal({ type: 'community', id: community.id })}>
                                            Delete
                                        </button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                    <div className="d-flex justify-content-center">
                        <nav>
                            <ul className="pagination">
                                {Array.from({ length: Math.ceil(group.listCommunityGroup.length / communitiesPerPage) }, (_, i) => (
                                    <li key={i + 1} className={`page-item ${currentCommunityPage === i + 1 ? 'active' : ''}`}>
                                        <button onClick={() => paginateCommunities(i + 1)} className="page-link">
                                            {i + 1}
                                        </button>
                                    </li>
                                ))}
                            </ul>
                        </nav>
                    </div>
                </div>
                <div className="col-12 mt-4">
                    <h4>Collections</h4>
                    <table className="table table-striped table-bordered">
                        <thead className="thead-dark">
                            <tr>
                                <th>Collection Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {currentCollections.map(collection => (
                                <tr key={collection.id}>
                                    <td>
                                        <Link to={`/Dspace/Collection/CollectionDetail/${collection.collectionId}`}>
                                            {collection.collectionName}
                                        </Link>
                                    </td>
                                    <td>
                                        <button className="btn btn-danger me-2" onClick={() => handleShowModal({ type: 'collection', id: collection.id })}>
                                            Delete
                                        </button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                    <div className="d-flex justify-content-center">
                        <nav>
                            <ul className="pagination">
                                {Array.from({ length: Math.ceil(group.listCollectionGroup.length / collectionsPerPage) }, (_, i) => (
                                    <li key={i + 1} className={`page-item ${currentCollectionPage === i + 1 ? 'active' : ''}`}>
                                        <button onClick={() => paginateCollections(i + 1)} className="page-link">
                                            {i + 1}
                                        </button>
                                    </li>
                                ))}
                            </ul>
                        </nav>
                    </div>
                </div>

                <div className="mt-4">
                    <button className="btn btn-success me-2" onClick={() => navigate(`/Dspace/Group/UserToGroup/${group.groupId}`)}>Add people to group</button>
                    <button className="btn btn-success me-2" onClick={() => navigate(`/Dspace/Group/CollectionToGroup/${group.groupId}`)}>Add collection to group</button>
                    <button className="btn btn-success me-2" onClick={() => navigate(`/Dspace/Group/CommunityToGroup/${group.groupId}`)}>Add community to group</button>
                    <button className="btn btn-primary me-2" onClick={() => navigate(`/Dspace/Group/EditGroup/${group.groupId}`)}>Edit</button>
                    <button className="btn btn-danger me-2" onClick={() => handleShowModal({ type: 'group', id: group.groupId })}>
                        Delete
                    </button>
                    <button className="btn btn-secondary" onClick={() => navigate('/Dspace/Group/GroupList')}>Back to List</button>
                </div>
            </div>
            <Modal show={showModal} onHide={handleCloseModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Confirm Delete</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    Are you sure you want to delete this {modalContent.type}?
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleCloseModal}>
                        Cancel
                    </Button>
                    <Button variant="danger" onClick={() => {
                        switch (modalContent.type) {
                            case 'member':
                                handleDeleteMember(modalContent.id, modalContent.groupId);
                                break;
                            case 'community':
                                handleDeleteCommunity(modalContent.id);
                                break;
                            case 'collection':
                                handleDeleteCollection(modalContent.id);
                                break;
                            case 'group':
                                handleDeleteGroup(modalContent.id);
                                break;
                            default:
                                break;
                        }
                    }}>
                        Delete
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
};

export default GroupDetails;
