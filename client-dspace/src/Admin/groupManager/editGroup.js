import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useParams, Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from '../../components/Header/Header';
import HeaderAdmin from '../../components/Header/Header-admin';
import { Helmet } from 'react-helmet';
import { toast } from 'react-toastify';

const EditGroup = () => {
    const { groupId } = useParams();
    const [groupData, setGroupData] = useState({
        title: '',
        description: '',
        isActive: false,
    });
    const [successMessage, setSuccessMessage] = useState('');
    const [groupNameHeader, setgroupNameHeader] = useState('');
    const [errors, setErrors] = useState({}); // State for errors
    const addNotification = (message, type) => {
        toast(message, { type });
    };
    useEffect(() => {
        // Fetch the group data from your API using the id from the URL
        axios.get(`${process.env.REACT_APP_BASE_URL}/api/Group/getGroup/${groupId}`)
            .then(response => {
                setGroupData(response.data);
                setgroupNameHeader(response.data.title)
            })
            .catch(error => {
                console.error('There was an error fetching the group!', error);
                addNotification('There was an error fetching the group!', 'error');
            });
    }, [groupId]);

    const handleChange = (e) => {
        const { name, value, type, checked } = e.target;
        setGroupData({
            ...groupData,
            [name]: type === 'checkbox' ? checked : value,
        });
    };

    const validate = () => {
        let newErrors = {};

        if (!groupData.title.trim()) newErrors.title = 'Title is required';
        if (!groupData.description.trim()) newErrors.description = 'Description is required';

        setErrors(newErrors);
        return Object.keys(newErrors).length === 0;
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        if (validate()) {
            axios.put(`${process.env.REACT_APP_BASE_URL}/api/Group/updateGroup/${groupId}`, groupData)
                .then(response => {
                    console.log('Group updated successfully!', response.data);
                    setSuccessMessage('Group updated successfully!');
                    addNotification('Group updated successfully!', 'success');
                })
                .catch(error => {
                    console.error('There was an error updating the group!', error);
                    addNotification('There was an error updating the group!', 'error');
                });
        }
    };

    return (
        <div>
            <HeaderAdmin />
            <Helmet>
                <meta charSet="utf-8" />
                <title>Modify Group - {`${groupData?.title}`} - dSPACE</title>
            </Helmet>
            <div className="container mt-5">
                {/* <div className="py-1 bg-light">
                    <Link to="/HomepageAdmin" className="text-decoration-none">Home </Link>
                    <span className="text-dark" ><strong>| Edit Group</strong></span>
                </div> */}
                <h1 className="mb-4">Edit Group - {groupNameHeader}</h1>
                {/* {successMessage && <div className="alert alert-success">{successMessage}</div>} */}
                <form onSubmit={handleSubmit}>
                    <div className="col-md-6 mb-3">
                        <div className="form-group">
                            <label>Title</label>
                            <input
                                type="text"
                                className={`form-control ${errors.title ? 'is-invalid' : ''}`}
                                name="title"
                                value={groupData.title}
                                onChange={handleChange}
                            />
                            {errors.title && <div className="invalid-feedback">{errors.title}</div>}
                        </div>
                    </div>
                    <div className="col-md-6 mb-3">
                        <div className="form-group">
                            <label>Description</label>
                            <textarea
                                className={`form-control ${errors.description ? 'is-invalid' : ''}`}
                                name="description"
                                value={groupData.description}
                                onChange={handleChange}
                            />
                            {errors.description && <div className="invalid-feedback">{errors.description}</div>}
                        </div>
                    </div>
                    <div className="form-group form-check mb-3">
                        <input
                            type="checkbox"
                            className="form-check-input"
                            name="isActive"
                            checked={groupData.isActive}
                            onChange={handleChange}
                        />
                        <label className="form-check-label">Is Active</label>
                    </div>
                    <button type="submit" className="btn btn-primary">Save</button>
                </form>
                <div className="mt-3">
                    <Link to="/Dspace/Group/GroupList">Back to List</Link>
                </div>
            </div>
        </div>
    );
};

export default EditGroup;
