import React, { useState, useEffect } from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link, useParams } from 'react-router-dom';
import HeaderAdmin from '../../components/Header/Header-admin';
import { Helmet } from 'react-helmet';
import { toast } from 'react-toastify';

const AddCollectionGroup = () => {
    const { groupId } = useParams();

    const [groupData, setGroupData] = useState([]);


    const [collections, setCollections] = useState([]);
    const [existingCollections, setExistingCollections] = useState([]);
    const [successMessage, setSuccessMessage] = useState('');
    const [searchQuery, setSearchQuery] = useState('');
    const [searchType, setSearchType] = useState('collectionName');
    const [currentPage, setCurrentPage] = useState(1);
    const itemsPerPage = 12;
    const token = localStorage.getItem('Token');
    const header = `Bearer ${token}`;
    const addNotification = (message, type) => {
        toast(message, { type });
    };
    useEffect(() => {
        axios.get(`${process.env.REACT_APP_BASE_URL}/api/Collection/getListOfCollections`)
            .then(response => {
                setCollections(response.data);
            })
            .catch(error => {
                console.error('There was an error fetching the collections!', error);
                addNotification('There was an error fetching the collections!', 'error');
            });

        axios.get(`${process.env.REACT_APP_BASE_URL}/api/Group/getGroup/${groupId}`)
            .then(response => {
                setExistingCollections(response.data.listCollectionGroup.map(collection => collection.collectionId));
            })
            .catch(error => {
                console.error('There was an error fetching the group data!', error);
                addNotification('There was an error fetching the group data!', 'error');
            });
    }, [groupId]);

    const handleSelectCollection = (e, collectionId) => {
        e.preventDefault();
        setGroupData(prevData => {
            const collectionIndex = prevData.findIndex(data => data.collectionId === collectionId);
            if (collectionIndex === -1) {
                // Nếu collectionId chưa có trong mảng, thêm vào mảng
                return [...prevData, { collectionId }];
            } else {
                // Nếu collectionId đã tồn tại trong mảng, xóa nó khỏi mảng
                const newData = [...prevData];
                newData.splice(collectionIndex, 1);
                return newData;
            }
        });
    };


    const handleSubmit = (e) => {
        e.preventDefault();
        const payload = groupData.map(item => ({
            ...item,
            groupId,
            isActive: true
        }));
        axios.post(`${process.env.REACT_APP_BASE_URL}/api/CollectionGroup/AddCollectionGroup`, payload, {
            headers: { Authorization: header }
        })
            .then(response => {
                setSuccessMessage('Collection group added successfully!');
                addNotification('Collection group added successfully!', 'success');
                setCollections(collections.filter(collection => !groupData.some(data => data.collectionId === collection.collectionId)));
                setGroupData([]); // Reset groupData sau khi thêm thành công
            })
            .catch(error => {
                console.error('There was an error creating the collection group!', error);
                addNotification('There was an error creating the collection group!', 'error');
            });
    };


    const handleSearchChange = (e) => {
        setSearchQuery(e.target.value);
        setCurrentPage(1);
    };

    const handleSearchTypeChange = (e) => {
        setSearchType(e.target.value);
        setSearchQuery('');
    };

    const filteredCollections = collections.filter(collection => {
        if (!searchQuery.trim()) return true;
        const normalizedSearchQuery = searchQuery.replace(/\s+/g, '').toLowerCase();

        switch (searchType) {
            case 'collectionName':
                return collection.collectionName.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery);
            case 'description':
                return collection.shortDescription.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery);
            default:
                return false;
        }
    });

    const indexOfLastItem = currentPage * itemsPerPage;
    const indexOfFirstItem = indexOfLastItem - itemsPerPage;
    const currentCollections = filteredCollections.slice(indexOfFirstItem, indexOfLastItem);

    const totalPages = Math.ceil(filteredCollections.length / itemsPerPage);

    const handlePageChange = (pageNumber) => {
        setCurrentPage(pageNumber);
    };

    const handleNextPage = () => {
        if (currentPage < totalPages) {
            setCurrentPage(prevPage => prevPage + 1);
        }
    };

    const handlePreviousPage = () => {
        if (currentPage > 1) {
            setCurrentPage(prevPage => prevPage - 1);
        }
    };

    return (
        <div>
            <HeaderAdmin />
            <Helmet>
                <meta charSet="utf-8" />
                <title>Reference Collection to Group - dSPACE</title>
            </Helmet>
            <div className="container mt-5">
                {/* <div className="py-1 bg-light">
                    <Link to="/HomepageAdmin" className="text-decoration-none">Home </Link>
                    <span className="text-dark"><strong>| Add Collection Group</strong></span>
                </div> */}
                <h1 className="mb-4">Create New Collection Group</h1>
                {/* {successMessage && <div className="alert alert-success">{successMessage}</div>} */}
                <form onSubmit={handleSubmit}>

                    <div className="row mb-4">
                        <div className="col-md-5">
                            <label htmlFor="searchInput" className="form-label fw-bold">Search</label>
                            <input
                                id="searchInput"
                                type="text"
                                className="form-control"
                                placeholder="Search collections..."
                                value={searchQuery}
                                onChange={handleSearchChange}
                            />
                        </div>
                        <div className="col-md-4">
                            <label htmlFor="searchTypeSelect" className="form-label fw-bold">Search By</label>
                            <select
                                id="searchTypeSelect"
                                className="form-select"
                                value={searchType}
                                onChange={handleSearchTypeChange}
                            >
                                <option value="all">Select...</option>
                                <option value="collectionName">Collection Name</option>
                                <option value="description">Description</option>
                            </select>
                        </div>
                    </div>
                    <div className="mt-3">
                        <h2>List of Collections</h2>
                        <table className="table table-striped table-bordered">
                            <thead className="thead-dark">
                                <tr>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Select</th>
                                </tr>
                            </thead>
                            <tbody>
                                {currentCollections.length > 0 ? (
                                    currentCollections.filter(collection => !existingCollections.includes(collection.collectionId)).map((collection) => (
                                        <tr key={collection.collectionId}>
                                            <td>
                                                <Link to={`/Dspace/Collection/CollectionDetail/${collection.collectionId}`}>
                                                    {collection.collectionName}
                                                </Link>
                                            </td>
                                            <td>{collection.shortDescription}</td>
                                            <td>
                                                <button
                                                    className={`btn ${groupData.some(data => data.collectionId === collection.collectionId) ? 'btn-primary' : 'btn-outline-primary'}`}
                                                    onClick={(e) => handleSelectCollection(e, collection.collectionId)}
                                                >
                                                    {groupData.some(data => data.collectionId === collection.collectionId) ? 'Selected' : 'Select'}
                                                </button>
                                            </td>
                                        </tr>
                                    ))
                                ) : (
                                    <tr>
                                        <td colSpan="3" className="text-center">No collections available</td>
                                    </tr>
                                )}

                            </tbody>
                        </table>
                        <div className="d-flex justify-content-center">
                            <nav>
                                <ul className="pagination">
                                    <li className={`page-item ${currentPage === 1 ? 'disabled' : ''}`}>
                                        <button type='button' className="page-link" onClick={handlePreviousPage}>Previous</button>
                                    </li>
                                    {[...Array(totalPages)].map((_, i) => (
                                        <li key={i} className={`page-item ${currentPage === i + 1 ? 'active' : ''}`}>
                                            <button type='button' className="page-link" onClick={() => handlePageChange(i + 1)}>
                                                {i + 1}
                                            </button>
                                        </li>
                                    ))}
                                    <li className={`page-item ${currentPage === totalPages ? 'disabled' : ''}`}>
                                        <button type='button' className="page-link" onClick={handleNextPage}>Next</button>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <button type="submit" className="btn btn-primary me-2">Add</button>
                        <Link to={`/Dspace/Group/GroupDetail/${groupId}`} className="btn btn-secondary">Back to Group Details</Link>
                    </div>
                </form>
            </div>
        </div>
    );
};

export default AddCollectionGroup;
