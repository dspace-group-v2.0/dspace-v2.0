import React, { useState, useEffect } from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link, useParams } from 'react-router-dom';
import HeaderAdmin from '../../components/Header/Header-admin';
import { Helmet } from 'react-helmet';
import { toast } from 'react-toastify';

const AddCommunityGroup = () => {
    const { groupId } = useParams();

    const [groupData, setGroupData] = useState({
        communityIds: [],
    });
    const addNotification = (message, type) => {
        toast(message, { type });
    };
    const [communities, setCommunities] = useState([]);
    const [existingCommunities, setExistingCommunities] = useState([]);
    const [successMessage, setSuccessMessage] = useState('');
    const [searchQuery, setSearchQuery] = useState('');
    const [searchType, setSearchType] = useState('communityName');
    const [currentPage, setCurrentPage] = useState(1);
    const itemsPerPage = 12;
    const token = localStorage.getItem('Token');
    const header = `Bearer ${token}`;

    useEffect(() => {
        axios.get(`${process.env.REACT_APP_BASE_URL}/api/Community/getListOfCommunities`)
            .then(response => {
                setCommunities(response.data);
            })
            .catch(error => {
                console.error('There was an error fetching the communities!', error);
                addNotification('There was an error fetching the communities!', 'error');
            });

        axios.get(`${process.env.REACT_APP_BASE_URL}/api/Group/getGroup/${groupId}`)
            .then(response => {
                setExistingCommunities(response.data.listCommunityGroup.map(community => community.communityId));
            })
            .catch(error => {
                console.error('There was an error fetching the group data!', error);
                addNotification('There was an error fetching the communities!', 'error');
            });
    }, [groupId]);

    const handleSelectCommunity = (e, communityId) => {
        e.preventDefault();
        setGroupData(prevState => {
            const { communityIds } = prevState;
            if (communityIds.includes(communityId)) {
                // Nếu communityId đã tồn tại trong mảng, xóa nó ra khỏi mảng
                return {
                    ...prevState,
                    communityIds: communityIds.filter(id => id !== communityId)
                };
            } else {
                // Nếu communityId chưa tồn tại, thêm nó vào mảng
                return {
                    ...prevState,
                    communityIds: [...communityIds, communityId]
                };
            }
        });
    };

    // console.log(groupData.length)
    const handleSubmit = (e) => {
        e.preventDefault();
        const payload = groupData.communityIds.map(communityId => ({
            communityId,
            groupId
        }));
        axios.post(`${process.env.REACT_APP_BASE_URL}/api/CommunityGroup/AddCommunityGroup`, payload, {
            headers: { Authorization: header }
        })
            .then(response => {
                setSuccessMessage('Community group added successfully!');
                addNotification('Community group added successfully!', 'success');
                setCommunities(communities.filter(community => !groupData.communityIds.includes(community.communityId)));
                setGroupData({
                    communityIds: [],
                });
            })
            .catch(error => {
                console.error('There was an error creating the community group!', error);
                addNotification('There was an error creating the community group!', 'error');
            });
    };


    const handleSearchChange = (e) => {
        setSearchQuery(e.target.value);
        setCurrentPage(1);
    };

    const handleSearchTypeChange = (e) => {
        setSearchType(e.target.value);
        setSearchQuery('');
    };

    const filteredCommunities = communities.filter(community => {
        if (!searchQuery.trim()) return true;
        const normalizedSearchQuery = searchQuery.replace(/\s+/g, '').toLowerCase();

        switch (searchType) {
            case 'communityName':
                return community.communityName.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery);
            case 'description':
                return community.shortDescription.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery);
            default:
                return false;
        }
    });

    const indexOfLastItem = currentPage * itemsPerPage;
    const indexOfFirstItem = indexOfLastItem - itemsPerPage;
    const currentCommunities = filteredCommunities.slice(indexOfFirstItem, indexOfLastItem);

    const totalPages = Math.ceil(filteredCommunities.length / itemsPerPage);

    const handlePageChange = (pageNumber) => {
        setCurrentPage(pageNumber);
    };

    const handleNextPage = () => {
        if (currentPage < totalPages) {
            setCurrentPage(prevPage => prevPage + 1);
        }
    };

    const handlePreviousPage = () => {
        if (currentPage > 1) {
            setCurrentPage(prevPage => prevPage - 1);
        }
    };

    return (
        <div>
            <HeaderAdmin />
            <Helmet>
                <meta charSet="utf-8" />
                <title>Reference Community to Group - dSPACE</title>
            </Helmet>
            <div className="container mt-5">
                {/* <div className="py-1 bg-light">
                    <Link to="/HomepageAdmin" className="text-decoration-none">Home </Link>
                    <span className="text-dark"><strong>| Add Community Group</strong></span>
                </div> */}
                <h1 className="mb-4">Create New Community Group</h1>
                {/* {successMessage && <div className="alert alert-success">{successMessage}</div>} */}
                <form onSubmit={handleSubmit}>
                    <div className="row mb-4">
                        <div className="col-md-5">
                            <label htmlFor="searchInput" className="form-label fw-bold">Search</label>
                            <input
                                id="searchInput"
                                type="text"
                                className="form-control"
                                placeholder="Search communities..."
                                value={searchQuery}
                                onChange={handleSearchChange}
                            />
                        </div>
                        <div className="col-md-4">
                            <label htmlFor="searchTypeSelect" className="form-label fw-bold">Search By</label>
                            <select
                                id="searchTypeSelect"
                                className="form-select"
                                value={searchType}
                                onChange={handleSearchTypeChange}
                            >
                                <option value="all">Select...</option>
                                <option value="communityName">Community Name</option>
                                <option value="description">Description</option>
                            </select>
                        </div>
                    </div>
                    <div className="mt-3">
                        <h2>List of Communities</h2>
                        <table className="table table-striped table-bordered">
                            <thead className="thead-dark">
                                <tr>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Select</th>
                                </tr>
                            </thead>
                            <tbody>
                                {currentCommunities.length > 0 ? (
                                    currentCommunities.filter(community => !existingCommunities.includes(community.communityId)).map((community) => (
                                        <tr key={community.communityId}>
                                            <td>
                                                <Link to={`/Dspace/Community/CommunityDetail/${community.communityId}`}>
                                                    {community.communityName}
                                                </Link>
                                            </td>
                                            <td>{community.shortDescription}</td>
                                            <td>
                                                <button
                                                    className={`btn ${groupData.communityIds.includes(community.communityId) ? 'btn-primary' : 'btn-outline-primary'}`}
                                                    onClick={(e) => handleSelectCommunity(e, community.communityId)}
                                                >
                                                    {groupData.communityIds.includes(community.communityId) ? 'Selected' : 'Select'}
                                                </button>
                                            </td>
                                        </tr>
                                    ))
                                ) : (
                                    <tr>
                                        <td colSpan="3" className="text-center">No communities available</td>
                                    </tr>
                                )}

                            </tbody>
                        </table>
                        <div className="d-flex justify-content-center">
                            <nav>
                                <ul className="pagination">
                                    <li className={`page-item ${currentPage === 1 ? 'disabled' : ''}`}>
                                        <button type='button' className="page-link" onClick={handlePreviousPage}>Previous</button>
                                    </li>
                                    {[...Array(totalPages)].map((_, i) => (
                                        <li key={i} className={`page-item ${currentPage === i + 1 ? 'active' : ''}`}>
                                            <button type='button' className="page-link" onClick={() => handlePageChange(i + 1)}>
                                                {i + 1}
                                            </button>
                                        </li>
                                    ))}
                                    <li className={`page-item ${currentPage === totalPages ? 'disabled' : ''}`}>
                                        <button type='button' className="page-link" onClick={handleNextPage}>Next</button>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <button type="submit" className="btn btn-primary me-2">Add</button>
                        <Link to={`/Dspace/Group/GroupDetail/${groupId}`} className="btn btn-secondary">Back to Group Details</Link>
                    </div>
                </form>
            </div>
        </div>
    );
};

export default AddCommunityGroup;
