import React, { useEffect, useState } from 'react';
import { Link, useNavigate, useParams, useLocation } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from '../../components/Header/Header';
import axios from 'axios';
import Select from 'react-select';
import { Search } from '@material-ui/icons';
import { TagsInput } from "react-tag-input-component";
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';
import Counter from '../../components/Statistics/counterView';
import AssessmentIcon from '@mui/icons-material/Assessment';
import { Button } from '@mui/material';
import { Modal } from 'react-bootstrap';
import {
    ExpandLess, ExpandMore
} from '@material-ui/icons';
import { toast } from 'react-toastify';
import { Helmet } from 'react-helmet';
import { AddRounded } from '@material-ui/icons';
import DangerousIcon from '@mui/icons-material/Dangerous';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import Tooltip from '@mui/material/Tooltip';
import IconButton from '@mui/material/IconButton';
import { jwtDecode } from 'jwt-decode';

function Status({ status }) {
    const getStatusText = (status) => {
        if (status == true) {
            return "Published";
        } else {
            return "Undiscoverable";
        }
    };

    const getStatusColor = (status) => {
        if (status == true) {
            return "#28A745";
        } else {
            return "#DC3545";
        }
    };
    const getBoxShadow = (status) => {
        const color = getStatusColor(status);
        return `0 0 10px 2px ${color}`;
    };
    return (
        <p style={{ margin: '0' }}>
            <span style={{
                margin: '0',
                backgroundColor: getStatusColor(status),
                borderRadius: '3px',
                color: 'white',
                padding: '2px',
                fontSize: 'small',
                boxShadow: getBoxShadow(status)

            }}>
                {getStatusText(status)}
            </span>
        </p>
    );
}

const CollectionDetails = () => {
    const location = useLocation();
    const searchParams = new URLSearchParams(location.search);

    const queryParams = new URLSearchParams(location.search);
    const initialButton = queryParams.get('button') || 'Recent submissions';

    const [items, setItems] = useState([]);
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(true);
    const [searchQuery, setSearchQuery] = useState('');
    const navigate = useNavigate();
    const [collection, setCollection] = useState(null);
    const [isSubscribed, setIsSubscribed] = useState(null);

    const { collectionId } = useParams();
    const [numPerPage, setNumPerPage] = useState(5);
    const [selectedAuthors, setSelectedAuthors] = useState([]);
    const [selectedKeywords, setSelectedKeywords] = useState([]);
    const [objItemsPerPage, setobjItemsPerPage] = useState([]);
    const [page, setPage] = React.useState(1);
    const [message, setMessage] = useState(null);
    const token = localStorage.getItem('Token');
    const userRole = token ? jwtDecode(token)["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"] : null;
    const header = `Bearer ${token}`;

    const handleChangePage = (event, value) => {
        setPage(value);
    };

    const [activeButton, setActiveButton] = useState(initialButton);
    const addNotification = (message, type) => {
        toast(message, { type });
    };

    useEffect(() => {
        setActiveButton(initialButton);
    }, [initialButton]);

    const handleButtonClick = (buttonName) => {
        navigate(`?button=${buttonName}`);
    };

    const buttons = [
        'Recent submissions',
        'By issue date',
        'By authors',
        'By title',
        'By subject keywords',
        // 'By subject category'
    ];
    const [expandedAbstracts, setExpandedAbstracts] = useState({});
    const toggleAbstract = (itemId) => {
        setExpandedAbstracts(prevState => ({
            ...prevState,
            [itemId]: !prevState[itemId]
        }));
    };

    const handleSearchSubmit = () => {
        let payload = {};
        console.log(payload);
        let api = ``;
        if (userRole === "ADMIN") {
            if (activeButton == 'Recent submissions') {
                return;
            } else if (activeButton == 'By issue date') {
                api = `${process.env.REACT_APP_BASE_URL}/api/Item/SearchItemByDate/${page}/${numPerPage}`;
                payload = {
                    year: selectedYear,
                    month: Number(selectedMonth) !== 0 ? selectedMonth.padStart(2, '0') : null,
                    day: Number(selectedDate) !== 0 ? selectedDate.padStart(2, '0') : null,
                    collectionId: parseInt(collectionId, 10) || 0,
                    discoverable: null,
                    sortType: 0
                }
            } else if (activeButton == 'By authors') {
                api = `${process.env.REACT_APP_BASE_URL}/api/Item/SearchItemByAuthor/${page}/${numPerPage}?collectionId=${collectionId}`;
                payload = selectedAuthors
            } else if (activeButton == 'By title') {
                if (searchQuery.trim() == "") {
                    api = `${process.env.REACT_APP_BASE_URL}/api/Item/SearchItemBySubjectKeyword/${page}/${numPerPage}?collectionId=${collectionId}`;
                    payload = selectedKeywords
                } else {
                    api = `${process.env.REACT_APP_BASE_URL}/api/Item/SearchItemByTitle/${page}/${numPerPage}?title=${normalizeText(searchQuery.trim())}&collectionId=${collectionId}`;
                }
            } else if (activeButton == 'By subject keywords') {
                api = `${process.env.REACT_APP_BASE_URL}/api/Item/SearchItemBySubjectKeyword/${page}/${numPerPage}?collectionId=${collectionId}`;
                payload = selectedKeywords

            }
        } else if (userRole === "STAFF") {
            if (activeButton == 'Recent submissions') {
                return;
            } else if (activeButton == 'By issue date') {
                api = `${process.env.REACT_APP_BASE_URL}/api/ItemStaff/SearchItemByDate/${page}/${numPerPage}`;
                payload = {
                    year: selectedYear,
                    month: Number(selectedMonth) !== 0 ? selectedMonth.padStart(2, '0') : null,
                    day: Number(selectedDate) !== 0 ? selectedDate.padStart(2, '0') : null,
                    collectionId: parseInt(collectionId, 10) || 0,
                    discoverable: null,
                    sortType: 0
                }
            } else if (activeButton == 'By authors') {
                api = `${process.env.REACT_APP_BASE_URL}/api/ItemStaff/SearchItemByAuthor/${page}/${numPerPage}`;
                payload = {
                    listAuthor: selectedAuthors,
                    collectionId: parseInt(collectionId, 10) || 0,
                    discoverable: null,
                    sortType: 0
                }
            } else if (activeButton == 'By title') {
                if (searchQuery.trim() == "") {
                    api = `${process.env.REACT_APP_BASE_URL}/api/ItemStaff/SearchItemBySubjectKeyword/${page}/${numPerPage}`;
                    payload = {
                        listSubjectKeyword: selectedKeywords,
                        collectionId: parseInt(collectionId, 10) || 0,
                        discoverable: null,
                        sortType: 0
                    }
                } else {
                    api = `${process.env.REACT_APP_BASE_URL}/api/ItemStaff/SearchItemByTitle/${page}/${numPerPage}`;
                    payload = {
                        title: normalizeText(searchQuery.trim()),
                        collectionId: parseInt(collectionId, 10) || 0,
                        discoverable: null,
                        sortType: 0
                    }
                }
            } else if (activeButton == 'By subject keywords') {
                api = `${process.env.REACT_APP_BASE_URL}/api/ItemStaff/SearchItemBySubjectKeyword/${page}/${numPerPage}`;
                payload = {
                    listSubjectKeyword: selectedKeywords,
                    collectionId: parseInt(collectionId, 10) || 0,
                    discoverable: null,
                    sortType: 0
                }
            }
        } else {
            if (activeButton == 'Recent submissions') {
                return;
            } else if (activeButton == 'By issue date') {
                api = `${process.env.REACT_APP_BASE_URL}/api/ItemUser/SearchItemByDate/${page}/${numPerPage}`;
                payload = {
                    year: selectedYear,
                    month: Number(selectedMonth) !== 0 ? selectedMonth.padStart(2, '0') : null,
                    day: Number(selectedDate) !== 0 ? selectedDate.padStart(2, '0') : null,
                    collectionId: parseInt(collectionId, 10) || 0,
                    discoverable: null,
                    sortType: 0
                }
            } else if (activeButton == 'By authors') {
                api = `${process.env.REACT_APP_BASE_URL}/api/ItemUser/SearchItemByAuthor/${page}/${numPerPage}`;
                payload = {
                    listAuthor: selectedAuthors,
                    collectionId: parseInt(collectionId, 10) || 0,
                    discoverable: null,
                    sortType: 0
                }
            } else if (activeButton == 'By title') {
                if (searchQuery.trim() == "") {
                    api = `${process.env.REACT_APP_BASE_URL}/api/ItemUser/SearchItemBySubjectKeyword/${page}/${numPerPage}`;
                    payload = {
                        listSubjectKeyword: selectedKeywords,
                        collectionId: parseInt(collectionId, 10) || 0,
                        discoverable: null,
                        sortType: 0
                    }
                } else {
                    api = `${process.env.REACT_APP_BASE_URL}/api/ItemUser/SearchItemByTitle/${page}/${numPerPage}`;
                    payload = {
                        title: normalizeText(searchQuery.trim()),
                        collectionId: parseInt(collectionId, 10) || 0,
                        discoverable: null,
                        sortType: 0
                    }
                }
            } else if (activeButton == 'By subject keywords') {
                api = `${process.env.REACT_APP_BASE_URL}/api/ItemUser/SearchItemBySubjectKeyword/${page}/${numPerPage}`;
                payload = {
                    listSubjectKeyword: selectedKeywords,
                    collectionId: parseInt(collectionId, 10) || 0,
                    discoverable: null,
                    sortType: 0
                }
            }
        }

        axios.post(api, payload, {
            headers: {
                Authorization: header
            }
        })
            .then(({ data }) => {
                // setItems(data);
                setobjItemsPerPage(data);
                setLoading(false);
            })
            .catch(error => {
                setError(error.message);
                setLoading(false);
            });
    };
    useEffect(() => {
        // Lấy dữ liệu từ API
        if (collectionId < 0 || activeButton === 'Recent submissions') { setobjItemsPerPage([]) }
        else {
            handleSearchSubmit()
            setLoading(false);
        }

    }, [collectionId, numPerPage, page]);

    useEffect(() => {
        setobjItemsPerPage([])
    }, [activeButton]);

    useEffect(() => {
        // Lấy dữ liệu từ API
        if (collectionId != 0) {
            if (activeButton == 'By issue date' || activeButton == 'By authors' || activeButton == 'By title' || activeButton == 'By subject keywords') {

            } else {
                if (userRole === "ADMIN" || userRole === "STAFF") {
                    axios.get(`${process.env.REACT_APP_BASE_URL}/api/Item/Get5ItemRecentInACollection?collectionId=${collectionId}`)
                        .then(({ data }) => {
                            setItems(data);
                            setLoading(false);
                        })
                        .catch(error => {
                            setError(error.message);
                            setLoading(false);
                        });
                } else {
                    axios.get(`${process.env.REACT_APP_BASE_URL}/api/ItemUser/Get5ItemRecentInACollection/${collectionId}`, {
                        headers: {
                            Authorization: header
                        }
                    })
                        .then(({ data }) => {
                            setItems(data);
                            // setobjItemsPerPage(data);
                            // objItemsPerPage.objmap = items;
                            setLoading(false);
                        })
                        .catch(error => {
                            setError(error.message);
                            setLoading(false);
                        });
                }

            }

        } else {
        }
    }, [collectionId, activeButton]);
    useEffect(() => {
        if (userRole == 'STUDENT' || userRole == 'LECTURER') {
            axios.get(`${process.env.REACT_APP_BASE_URL}/api/CollectionUser/GetCollectionById/${collectionId}`, { headers: { Authorization: header } })
                .then(response => {
                    if(response.status === 204){
                        setLoading(true);
                    }else{
                        setCollection(response.data);
                        setIsSubscribed(response.data.isSubcribed)
                        console.log(isSubscribed)
                    }
                })
                .catch(error => {
                    setError('There was an error fetching the collection!');
                    console.error('There was an error fetching the collection!', error);
                });
        } else if (userRole == 'STAFF') {
            axios.get(`${process.env.REACT_APP_BASE_URL}/api/CollectionStaff/getCollection/${collectionId}`, { headers: { Authorization: header } })
                .then(response => {
                    if(response.status === 204){
                        setLoading(true);
                    }else{
                        setCollection(response.data);
                    }
                })
                .catch(error => {
                    setError('There was an error fetching the collection!');
                    console.error('There was an error fetching the collection!', error);
                });
        } else {
            axios.get(`${process.env.REACT_APP_BASE_URL}/api/Collection/getCollection/${collectionId}`, { headers: { Authorization: header } })
                .then(response => {
                    if(response.status === 204){
                        setLoading(true);
                    }else{
                        setCollection(response.data);
                    }
                })
                .catch(error => {
                    setError('There was an error fetching the collection!');
                    console.error('There was an error fetching the collection!', error);
                });
        }

    }, [collectionId]);

    const handleSearchChange = (e) => {
        setSearchQuery(e.target.value);
    };
    const handleRowClick = (itemId) => {
        if (userRole !== null) {
            navigate(`/DSpace/ItemDetail/${itemId}?colId=${collectionId}`);
        } else {
            navigate('/Login');
        }
    };

    const normalizeText = (text) => {
        text = text.toLowerCase();
        text = text.replace(/\s/g, '');

        return text;
    };
    const [selectedYear, setSelectedYear] = useState('');
    const [selectedMonth, setSelectedMonth] = useState('');
    const [selectedDate, setSelectedDate] = useState('');
    const [years, setYears] = useState([]);

    const months = [
        'January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December'
    ];

    const handleYearChange = (e) => setSelectedYear(e.target.value);
    const handleMonthChange = (e) => setSelectedMonth(e.target.value);
    const handleDateChange = (e) => setSelectedDate(e.target.value);

    const CollectionSelect = ({ collections, selectedValue, handleChange }) => {
        const options = collections.map(collection => ({
            value: collection.collectionId,
            label: collection.collectionName
        }));

        return (
            <Select
                options={options}
                value={options.find(option => option.value === selectedValue)}
                onChange={selectedOption => handleChange(selectedOption ? selectedOption.value : '0')}
                placeholder="All collections"
                isClearable
                styles={{
                    container: (base) => ({ ...base, width: 'auto' }),
                    control: (base) => ({
                        ...base,
                        border: '1px solid black',
                        borderRadius: '4px 0 0 4px',
                    }),
                    indicatorSeparator: () => ({
                        display: 'none',
                    }),
                    dropdownIndicator: (base) => ({
                        ...base,
                        display: 'none',
                    }),
                }}
            />

        );
    };

    const [authors, setAuthors] = useState([]);
    useEffect(() => {
        async function fetchAuthors() {
            const response = await fetch(`${process.env.REACT_APP_BASE_URL}/api/Author/getListOfAuthors`);
            const data = await response.json();
            setAuthors(data);
        }
        fetchAuthors();
    }, []);
    const AuthorSelect = ({ authors, selectedAuthors, handleAuthorSelectChange }) => {
        const options = authors.map(author => {
            let label = author.fullName;
            if (author.jobTitle !== null) {
                if (author.jobTitle.trim() !== '') {
                    label += " - " + author.jobTitle;
                }
            } else {
                author.jobTitle = ''
            }
            return {
                value: author.authorId.toString(),
                label: label
            };
        });

        const selectedOptions = options.filter(option =>
            selectedAuthors.includes(option.value)
        );

        return (
            <Select
                isMulti
                options={options}
                value={selectedOptions}
                onChange={selectedOptions => handleAuthorSelectChange(selectedOptions ? selectedOptions.map(option => option.value) : [])}
                placeholder="Search and select contributors..."
                isClearable
                styles={{
                    container: (base) => ({ ...base, width: '100%' }),
                    menu: (provided) => ({
                        ...provided,
                        maxHeight: '200px',
                        overflowY: 'auto',
                    }),
                    menuList: (provided) => ({
                        ...provided,
                        maxHeight: '200px',
                        overflowY: 'auto',
                    }),
                    indicatorSeparator: () => ({
                        display: 'none',
                    }),
                    dropdownIndicator: (base) => ({
                        ...base,
                        display: 'none',
                    }),
                }}
            />
        );
    };
    const handleAuthorSelectChange = (selectedAuthorIds) => {
        setSelectedAuthors(selectedAuthorIds);
    };

    const formatDate = (dateString) => {
        if (!dateString) return '';

        const parts = dateString.split('-');
        let formattedDate = '';

        if (parts.length === 3) {
            // Case: full date (YYYY-MM-DD)
            const [year, month, day] = parts;
            formattedDate = `${day.padStart(2, '0')}-${month.padStart(2, '0')}-${year}`;
        } else if (parts.length === 2) {
            // Case: year and month (YYYY-MM)
            const [year, month] = parts;
            formattedDate = `${month.padStart(2, '0')}-${year}`;
        } else if (parts.length === 1) {
            // Case: only year (YYYY)
            const [year] = parts;
            formattedDate = year;
        }

        return formattedDate;
    };

    if (loading) {
        return (
            <div>
                <Header />
                <div className="container mt-5 d-flex justify-content-center align-items-center" style={{ height: '60vh' }}>
                    <div class="load-4">
                        {/* <p>Loading 4</p> */}
                        <div class="ring-1"></div>
                    </div>
                </div>
            </div>
        );
    }
    const handleKeywordSelectChange = (newKeywords) => {
        setSelectedKeywords(newKeywords);
    };

    const DeleteButtonWithModal = ({ handleDelete }) => {
        const [show, setShow] = useState(false);

        const handleClose = () => setShow(false);
        const handleShow = () => setShow(true);

        const handleConfirmDelete = () => {
            handleDelete();
            handleClose();
        };

        return (
            <>
                <button variant="danger" className="btn btn-danger me-2" onClick={handleShow}>
                    Delete
                </button>

                <Modal show={show} onHide={handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Confirm Deletion</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>Are you sure you want to delete this collection?</Modal.Body>
                    <Modal.Footer>
                        <button className="btn btn-secondary me-2" variant="secondary" onClick={handleClose}>
                            Cancel
                        </button>
                        <button className="btn btn-danger me-2" variant="danger" onClick={handleConfirmDelete}>
                            Delete
                        </button>
                    </Modal.Footer>
                </Modal>
            </>
        );
    };

    const handleDelete = () => {
        if (userRole === "ADMIN") {
            axios.delete(`${process.env.REACT_APP_BASE_URL}/api/Collection/DeleteCollection/${collectionId}`, {
                headers: {
                    Authorization: header
                }
            })
                .then(response => {
                    setMessage({ text: 'Collection deleted successfully!', type: 'success' });
                    addNotification(response.data, 'success');
                    setTimeout(() => navigate('/Dspace/Collection/CollectionList'), 2000);
                })
                .catch(error => {
                    console.error('There was an error deleting the collection!', error);
                    setMessage({ text: 'Collection cannot be deleted as it contains items or sub-collections. Please remove them first.', type: 'danger' });
                    addNotification(error.response.data, 'error');
                });
        } else if (userRole === "STAFF") {
            axios.delete(`${process.env.REACT_APP_BASE_URL}/api/CollectionStaff/DeleteCollection/${collectionId}`, {
                headers: {
                    Authorization: header
                }
            })
                .then(response => {
                    setMessage({ text: 'Collection deleted successfully!', type: 'success' });
                    addNotification(response.data, 'success');
                    setTimeout(() => navigate('/Dspace/Collection/CollectionList'), 2000);
                })
                .catch(error => {
                    console.error('There was an error deleting the collection!', error);
                    setMessage({ text: 'Collection cannot be deleted as it contains items or sub-collections. Please remove them first.', type: 'danger' });
                    addNotification(error.response.data, 'error');
                });
        }

    };
    function getRelativePath(fullPath) {
        const splitPath = fullPath.split('public\\');
        return splitPath[1];
    }

    const handleRedirectStats = () => {
        navigate(`/Dspace/StatisticDetail?collectionId=${collectionId}`);
    };

    const handleUnsubscribe = (collectionId) => {
        axios.post(`${process.env.REACT_APP_BASE_URL}/api/SubcribeUser/UnSubcribeCollection?collectionId=${collectionId}`, null, {
            headers: { Authorization: header }
        })
            .then(response => {
                console.log('Unsubscribed successfully!', response.data);
                setIsSubscribed(false); // Cập nhật trạng thái
                // alert('Unsubscribed successfully');
                addNotification(response.data, 'success');
            })
            .catch(error => {
                console.error('Error unsubscribing to collection:', error);
                // alert('Failed to unsubscribe');
                addNotification(error.response.data, 'error');
            });
    };

    const handleSubscribe = (collectionId) => {
        axios.post(`${process.env.REACT_APP_BASE_URL}/api/SubcribeUser/SubcribeCollection?collectionId=${collectionId}`, null, {
            headers: { Authorization: header }
        })
            .then(response => {
                console.log('Subscribed successfully!', response.data);
                setIsSubscribed(true); // Cập nhật trạng thái
                // alert('Subscribed successfully');
                addNotification(response.data, 'success');
            })
            .catch(error => {
                console.error('Error subscribing to collection:', error);
                // alert('Failed to subscribe');
                addNotification(error.response.data, 'error');
            });
    };
    // console.log(isSubscribed)
    return (
        <div>
            <Header />
            <Helmet>
                <meta charSet="utf-8" />
                <title>{`${collection?.collectionName}`} - dSPACE</title>
            </Helmet>

            <div className="container mt-5">
                {collection !== null && (
                    <Counter communityId={collection.communityId} collectionId={collectionId} />
                )}
                {/* <div className="py-1 bg-light">
                    <Link to="#link" className="text-decoration-none">Home </Link>
                    <span className="text-dark"><strong>| Collection Detail</strong></span>
                </div> */}
                <h1 className="mb-4">Collection Details
                    <Button
                        onClick={handleRedirectStats}
                        className='ms-2'
                        startIcon={<AssessmentIcon />}
                        sx={{
                            color: 'primary',
                            border: 'none',
                            boxShadow: 'none',
                            '&:hover': {
                                border: 'none',
                                boxShadow: 'none',
                            }
                        }}
                    >
                        Statistics
                    </Button>
                </h1>

                {/* {message && (
                    <div className={`alert alert-${message.type} text-center`}>{message.text}</div>
                )} */}
                <div className="card">
                    <div className="card-body">
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Name:</div>
                            <div className="col-sm-6">{collection?.collectionName}</div>
                        </div>
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Short Description:</div>
                            <div className="col-sm-6">{collection?.shortDescription}</div>
                        </div>
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Parent Community:</div>
                            {(userRole === "STUDENT" || userRole === "LECTURER") ? (
                                <div className="col-sm-6">{collection?.communityDTOForSelectOfUser?.communityName}</div>
                            ) : <div className="col-sm-6">{collection?.communityDTOForSelect?.communityName}</div>
                            }

                        </div>
                        {(userRole !== "STUDENT" && userRole !== "LECTURER") && (
                            <>
                                <div className="row border-bottom pb-2 my-3">
                                    <div className="col-sm-2 text-end">Group Name:</div>
                                    <div className="col-sm-6">{collection?.collectionGroupDTOForDetails[0]?.groupName}</div>
                                </div>

                                <div className="row border-bottom pb-2 my-3">
                                    <div className="col-sm-2 text-end">Create Time:</div>
                                    <div className="col-sm-6">{new Date(collection?.createTime).toLocaleString()}</div>
                                </div>
                                <div className="row border-bottom pb-2 my-3">
                                    <div className="col-sm-2 text-end">Update Time:</div>
                                    <div className="col-sm-6">{new Date(collection?.updateTime).toLocaleString()}</div>
                                </div>
                                <div className="row border-bottom pb-2 my-3">
                                    <div className="col-sm-2 text-end">Created By:</div>
                                    <div className="col-sm-6">{collection?.createBy}</div>
                                </div>
                                <div className="row border-bottom pb-2 my-3">
                                    <div className="col-sm-2 text-end">Updated By:</div>
                                    <div className="col-sm-6">{collection?.updateBy}</div>
                                </div>
                                <div className="row border-bottom pb-2 my-3">
                                    <div className="col-sm-2 text-end">Active:</div>
                                    <div className="col-sm-6"><Status status={collection?.isActive} /></div>
                                </div>
                            </>
                        )}
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">License:</div>
                            <div className="col-sm-6">{collection?.license}</div>
                        </div>
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Entity Type Name:</div>
                            <div className="col-sm-6">{collection?.entityTypeName}</div>
                        </div>
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Logo:</div>
                            <div className="col-sm-6">
                                {collection?.logoUrl ? (
                                    <img src={`/${getRelativePath(collection.logoUrl)}`} alt="logo" className='img-thumbnail' style={{ width: '200px' }}
                                        onError={(e) => {
                                            e.target.onerror = null;
                                            e.target.src = '/No-Image-Placeholder.png';
                                        }} />
                                ) : (
                                    <img
                                        src={`/No-Image-Placeholder.png`}
                                        alt={collection?.collectionName}
                                        style={{ width: '150px', height: '200px', objectFit: 'cover', marginRight: '20px', boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1)' }}

                                    />
                                )}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="mt-4">
                    {(userRole === "ADMIN" || (userRole === "STAFF" && collection?.canEdit)) && (
                        <>
                            <button className="btn btn-primary me-2" onClick={() => navigate(`/Dspace/Collection/EditCollection/${collectionId}`)}>Edit</button>
                            <DeleteButtonWithModal handleDelete={handleDelete} />
                        </>
                    )}
                    {((userRole === 'STUDENT' || userRole === 'LECTURER') && isSubscribed) && (
                        <button style={{ marginRight: '1%' }} className="btn btn-secondary" onClick={() => handleUnsubscribe(collection.collectionId)}>
                            Unsubscribe
                        </button>
                    )}

                    {((userRole === 'STUDENT' || userRole === 'LECTURER') && !isSubscribed) &&
                        (
                            <button style={{ marginRight: '1%' }} className="btn btn-danger" onClick={() => handleSubscribe(collection.collectionId)}>
                                Subscribe
                            </button>
                        )}
                    <button className="btn btn-secondary" onClick={() => navigate('/Dspace/Collection/CollectionList')}>Back to List</button>
                </div>
            </div>
            <div className="main-content d-flex flex-column align-items-center">
                <div className="container mt-5">
                    {((userRole === "STAFF" && collection?.canEdit) || (userRole === "ADMIN")) && (
                        <div>
                            <span className="add-field-container mb-2" onClick={() => navigate(`/Dspace/CreateItem/${collectionId}`)}>
                                <AddRounded className="add-rounded-icon" />
                                <span className="add-field-text">Create new item</span>
                            </span>
                        </div>
                    )}
                    <div className='d-flex mb-4'>
                        {buttons.map((buttonName, index) => (
                            <div
                                key={index}
                                className="btn"
                                style={{
                                    border: '1px solid black',
                                    borderRadius: index === 0 ? '4px 0 0 4px' : index === buttons.length - 1 ? '0 4px 4px 0' : '0',
                                    padding: '13px',
                                    fontWeight: 'bolder',
                                    backgroundColor: activeButton === buttonName ? '#43515F' : 'white',
                                    color: activeButton === buttonName ? 'white' : '#207698',
                                    cursor: 'pointer'
                                }}
                                onClick={() => handleButtonClick(buttonName)}
                            >
                                {buttonName}
                            </div>
                        ))}

                    </div>


                    {error && <div className="alert alert-danger">{error}</div>}

                    {activeButton === 'Recent submissions' && (
                        items.length > 0 ? (
                            <div className='col-md-12'>
                                <h6>Now showing 5 new items</h6>
                                {items.map((item, index) => (
                                    <div key={index} style={{ display: 'flex', alignItems: 'flex-start', marginBottom: '20px' }}>
                                        <img
                                            src={item.imageURL || '/The_logo_of_Little_Black_Book.png'} // Sử dụng URL ảnh từ item hoặc ảnh placeholder
                                            alt={item.title}
                                            style={{ width: '150px', height: '200px', objectFit: 'cover', marginRight: '20px' }}
                                        />
                                        <div>
                                            <div className='d-flex' style={{ alignItems: 'center' }}>
                                                {(userRole === "STAFF" || userRole === "ADMIN") && (
                                                    <Status status={item.discoverable} style={{ display: 'flex', alignItems: 'center' }} />
                                                )}
                                                {(userRole === "STAFF" && collection?.canEdit) && (
                                                    <Tooltip color='success' placement="top" title="You are assigned to this item">
                                                        <IconButton style={{ display: 'flex', alignItems: 'center' }}>
                                                            <CheckCircleIcon />
                                                        </IconButton>
                                                    </Tooltip>
                                                )}
                                                {(userRole === "STAFF" && !collection?.canEdit) && (
                                                    <Tooltip placement="top" title="You are not assigned to this item">
                                                        <IconButton color='error' style={{ display: 'flex', alignItems: 'center' }}>
                                                            <DangerousIcon />
                                                        </IconButton>
                                                    </Tooltip>
                                                )}
                                            </div>

                                            <h3 style={{ margin: '0' }}>
                                                <span className="author-item" onClick={() => handleRowClick(item.itemId)}>{item.title}</span>
                                            </h3>
                                            <p style={{ margin: '0' }}>{item.dateOfIssue && (<>({formatDate(item.dateOfIssue)})</>)} {item.publisher && (<> - {item.publisher}</>)}</p>
                                            {/* <p className='d-flex' style={{margin: '0'}}><p style={{margin: '0', color: '#4b9cdb', fontWeight: 'bolder', marginRight: '10px'}}>Publisher: </p>{item.publisher}</p> */}
                                            <p className='d-flex' style={{ margin: '0' }}><p style={{ margin: '0', color: '#207f9e', fontWeight: 'bolder', marginRight: '10px' }}>Contributors: </p>{item.authorItems
                                                .filter((value, index, self) =>
                                                    index === self.findIndex((t) => t.authorId === value.authorId)
                                                )
                                                .map(authorItem => authorItem.fullName)
                                                .join(', ')
                                            }</p>
                                            {/* <p className='d-flex' style={{margin: '0'}}><p style={{margin: '0', color: '#4b9cdb', fontWeight: 'bolder', marginRight: '10px'}}>Abstract: </p></p> */}
                                            <div>
                                                {expandedAbstracts[item.itemId] ? (
                                                    <div>
                                                        <div>{item.abstract}</div>
                                                        <button onClick={() => toggleAbstract(item.itemId)} style={{ background: 'none', color: '#207f9e', border: 'none', padding: '0', cursor: 'pointer', fontWeight: 'bolder' }}>
                                                            <ExpandLess /> Collapse
                                                        </button>
                                                    </div>
                                                ) : (
                                                    <div>
                                                        <div style={{ overflow: 'hidden', textOverflow: 'ellipsis', display: '-webkit-box', WebkitLineClamp: 3, WebkitBoxOrient: 'vertical' }}>
                                                            {item.abstract}
                                                        </div>
                                                        {item.abstract.split(' ').length > 20 && (
                                                            <button onClick={() => toggleAbstract(item.itemId)} style={{ background: 'none', color: '#207f9e', border: 'none', padding: '0', cursor: 'pointer', fontWeight: 'bolder' }}>
                                                                <ExpandMore />  Show more
                                                            </button>
                                                        )}
                                                    </div>
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        ) : <h6>Not found</h6>
                    )}

                    {activeButton === 'By issue date' && (
                        <>
                            <div className="d-flex flex-column">
                                <div className="d-flex">
                                    <p style={{ fontWeight: 'bolder', margin: '0px', width: '200px' }}>Choose year</p>
                                    <p style={{ fontWeight: 'bolder', margin: '0px', width: '150px' }}>Choose month</p>
                                    <p style={{ fontWeight: 'bolder', margin: '0px' }}>Choose date</p>
                                </div>
                                <div className="d-flex mb-4">
                                    <div className="d-flex flex-column align-items-start" style={{ width: '200px' }}>
                                        <input
                                            style={{ borderRadius: '6px', width: '100%' }}
                                            type="number"
                                            placeholder="Choose year"
                                            value={selectedYear}
                                            onChange={handleYearChange}
                                            className="form-control"
                                        />
                                    </div>
                                    <div className="d-flex flex-column align-items-start mx-2" style={{ width: '150px' }}>
                                        <select value={selectedMonth} onChange={handleMonthChange} className="form-select">
                                            <option value="">(Choose month)</option>
                                            {months.map((month, index) => (
                                                <option key={index} value={index + 1}>{month}</option>
                                            ))}
                                        </select>
                                    </div>
                                    <div className="d-flex flex-column align-items-start">
                                        <input
                                            style={{ borderRadius: '6px 0 0 6px' }}
                                            type="number"
                                            placeholder="Filter results by date"
                                            value={selectedDate}
                                            onChange={handleDateChange}
                                            className="form-control"
                                        />
                                    </div>
                                    <div className="d-flex flex-column align-items-start">
                                        <button onClick={handleSearchSubmit} style={{ backgroundColor: '#43515F', color: 'white', borderRadius: '0 6px 6px 0', height: '100%' }} className="btn">
                                            <Search />
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </>
                    )}
                    {activeButton === 'By authors' && (
                        <>
                            <p style={{ fontWeight: 'bolder', margin: '0px' }}>Choose contributors</p>

                            <div className="d-flex mb-4 w-50" style={{ padding: '' }}>
                                <AuthorSelect
                                    authors={authors}
                                    selectedAuthors={selectedAuthors}
                                    handleAuthorSelectChange={handleAuthorSelectChange}
                                />
                                <button onClick={handleSearchSubmit} style={{ backgroundColor: '#43515F', color: 'white', borderRadius: '0 6px 6px 0' }} className="btn">
                                    <Search />
                                </button>
                            </div>
                        </>
                    )}
                    {activeButton === 'By title' && (
                        <>
                            <p style={{ fontWeight: 'bolder', margin: '0px' }}>Search by title</p>
                            <div className="d-flex mb-4">
                                <input
                                    type="text"
                                    className="form-control w-50"
                                    placeholder="Search By Title"
                                    style={{ borderRadius: '0 0 0 0', flex: '0 1 auto' }}
                                    value={searchQuery}
                                    onChange={handleSearchChange}
                                />
                                <button onClick={handleSearchSubmit} style={{ backgroundColor: '#43515F', color: 'white', borderRadius: '0 6px 6px 0' }} className="btn">
                                    <Search />
                                </button>
                            </div>
                        </>
                    )}
                    {activeButton === 'By subject keywords' &&
                        (
                            <>
                                <p style={{ fontWeight: 'bolder', margin: '0px' }}>Search by keywords</p>
                                <div className="d-flex mb-4 w-50" style={{ padding: '' }}>
                                    <TagsInput
                                        style={{ borderRadius: '0 0 0 0' }}
                                        value={selectedKeywords}
                                        onChange={handleKeywordSelectChange}
                                        placeHolder="Enter keywords separated by Enter Button"
                                    />
                                    <button onClick={handleSearchSubmit} style={{ backgroundColor: '#43515F', color: 'white', borderRadius: '0 6px 6px 0' }} className="btn">
                                        <Search />
                                    </button>
                                </div>
                            </>
                        )
                    }
                    {activeButton === 'By subject category' && <p>Content for By subject category</p>}


                    {activeButton !== 'Recent submissions' ? (
                        objItemsPerPage.objmap?.length > 0 ? (
                            <div className='col-md-12'>
                                <h6>Now showing {(page - 1) * numPerPage + 1} - {objItemsPerPage.objmap?.length + (page - 1) * numPerPage} of {objItemsPerPage.totalAmount}</h6>
                                {objItemsPerPage.objmap.map((item, index) => (
                                    <div key={index} style={{ display: 'flex', alignItems: 'flex-start', marginBottom: '20px' }}>
                                        <img
                                            src={item.imageURL || '/The_logo_of_Little_Black_Book.png'} // Sử dụng URL ảnh từ item hoặc ảnh placeholder
                                            alt={item.title}
                                            style={{ width: '150px', height: '200px', objectFit: 'cover', marginRight: '20px' }}
                                        />
                                        <div>
                                            <div className='d-flex' style={{ alignItems: 'center' }}>
                                                {(userRole === "STAFF" || userRole === "ADMIN") && (
                                                    <Status status={item.discoverable} style={{ display: 'flex', alignItems: 'center' }} />
                                                )}
                                                {(userRole === "STAFF" && collection?.canEdit) && (
                                                    <Tooltip color='success' placement="top" title="You are assigned to this item">
                                                        <IconButton style={{ display: 'flex', alignItems: 'center' }}>
                                                            <CheckCircleIcon />
                                                        </IconButton>
                                                    </Tooltip>
                                                )}
                                                {(userRole === "STAFF" && !collection?.canEdit) && (
                                                    <Tooltip placement="top" title="You are not assigned to this item">
                                                        <IconButton color='error' style={{ display: 'flex', alignItems: 'center' }}>
                                                            <DangerousIcon />
                                                        </IconButton>
                                                    </Tooltip>
                                                )}
                                            </div>


                                            <h3 style={{ margin: '0' }}>
                                                <span className="author-item" onClick={() => handleRowClick(item.itemId)}>{item.title}</span>
                                            </h3>
                                            <p style={{ margin: '0' }}>{item.dateOfIssue && (<>({formatDate(item.dateOfIssue)})</>)} {item.publisher && (<> - {item.publisher}</>)}</p>
                                            {/* <p className='d-flex' style={{margin: '0'}}><p style={{margin: '0', color: '#4b9cdb', fontWeight: 'bolder', marginRight: '10px'}}>Publisher: </p>{item.publisher}</p> */}
                                            <p className='d-flex' style={{ margin: '0' }}><p style={{ margin: '0', color: '#207f9e', fontWeight: 'bolder', marginRight: '10px' }}>Contributors: </p>{item.authorItems
                                                .filter((value, index, self) =>
                                                    index === self.findIndex((t) => t.authorId === value.authorId)
                                                )
                                                .map(authorItem => authorItem.fullName)
                                                .join(', ')
                                            }</p>
                                            {/* <p className='d-flex' style={{margin: '0'}}><p style={{margin: '0', color: '#4b9cdb', fontWeight: 'bolder', marginRight: '10px'}}>Abstract: </p></p> */}
                                            <div>
                                                {expandedAbstracts[item.itemId] ? (
                                                    <div>
                                                        <div>{item.abstract}</div>
                                                        <button onClick={() => toggleAbstract(item.itemId)} style={{ background: 'none', color: '#207f9e', border: 'none', padding: '0', cursor: 'pointer', fontWeight: 'bolder' }}>
                                                            <ExpandLess /> Collapse
                                                        </button>
                                                    </div>
                                                ) : (
                                                    <div>
                                                        <div style={{ overflow: 'hidden', textOverflow: 'ellipsis', display: '-webkit-box', WebkitLineClamp: 3, WebkitBoxOrient: 'vertical' }}>
                                                            {item.abstract}
                                                        </div>
                                                        {item.abstract.split(' ').length > 20 && (
                                                            <button onClick={() => toggleAbstract(item.itemId)} style={{ background: 'none', color: '#207f9e', border: 'none', padding: '0', cursor: 'pointer', fontWeight: 'bolder' }}>
                                                                <ExpandMore />  Show more
                                                            </button>
                                                        )}
                                                    </div>
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        ) : <h6></h6>
                    ) : <h6></h6>}


                    {/* <nav>
                        <ul className="pagination justify-content-center">
                            <li className={`page-item ${currentPage === 1 ? 'disabled' : ''}`}>
                                <button style={{ color: '#207698' }} className="page-link" onClick={handlePrevious}>
                                    Previous
                                </button>
                            </li>
                            {Array.from({ length: totalPages }, (_, index) => (
                                <li key={index + 1} className={`page-item ${index + 1 === currentPage ? 'active' : ''}`}>
                                    <button className="page-link" style={{
                                        backgroundColor: index + 1 === currentPage ? '#207698' : '',
                                        color: index + 1 === currentPage ? '' : '#207698'
                                    }} onClick={() => paginate(index + 1)}>
                                        {index + 1}
                                    </button>
                                </li>
                            ))}
                            <li className={`page-item ${currentPage === totalPages ? 'disabled' : ''}`}>
                                <button style={{ color: '#207698' }} className="page-link" onClick={handleNext}>
                                    Next
                                </button>
                            </li>
                        </ul>
                    </nav> */}
                    {activeButton !== 'Recent submissions' && (
                        <Stack spacing={2} alignItems="center">
                            {/* <Typography>Page: {page}</Typography> */}
                            <Pagination count={objItemsPerPage.totalPage} page={page} onChange={handleChangePage} size="large" />
                        </Stack>)}
                </div>
            </div>
        </div>
    );
};
export default CollectionDetails;
