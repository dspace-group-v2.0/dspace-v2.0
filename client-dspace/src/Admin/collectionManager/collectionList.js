import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from '../../components/Header/Header';
import { Helmet } from 'react-helmet';
import {
    ExpandLess, ExpandMore
} from '@material-ui/icons';
import Badge from '@mui/material/Badge';
import { toast } from 'react-toastify';
import { jwtDecode } from 'jwt-decode';

const CollectionList = () => {
    const [collections, setCollections] = useState([]);
    const [error, setError] = useState(null);
    const [searchQuery, setSearchQuery] = useState('');
    const [searchType, setSearchType] = useState('collectionName'); // State to track the current search type
    const [currentPage, setCurrentPage] = useState(1);
    const itemsPerPage = 12;
    const token = localStorage.getItem('Token');
    const userRole = token ? jwtDecode(token)["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"] : null;
    const header = `Bearer ${token}`;
    const [expandedAbstracts, setExpandedAbstracts] = useState({});
    const toggleAbstract = (itemId) => {
        setExpandedAbstracts(prevState => ({
            ...prevState,
            [itemId]: !prevState[itemId]
        }));
    };

    useEffect(() => {
        if (userRole === "ADMIN") {
            axios.get(`${process.env.REACT_APP_BASE_URL}/api/Collection/getListOfCollections`)
                .then(response => {
                    setCollections(response.data);
                })
                .catch(error => {
                    setError(error.message);
                    addNotification(error.response.data, 'error');
                });
        } else if (userRole === "STAFF") {
            axios.get(`${process.env.REACT_APP_BASE_URL}/api/CollectionStaff/GetAllCollections`, {
                headers: {
                    Authorization: header
                }
            })
                .then(response => {
                    setCollections(response.data.filter(collection => collection.canEdit === true));
                    if (response.data.filter(collection => collection.canEdit === true).length == 0) {
                        addNotification('You are not assigned any collection', 'error');
                    }
                })
                .catch(error => {
                    setError("You are not assigned any collection");
                    addNotification('You are not assigned any collection', 'error');
                });
        }
    }, []);
    const addNotification = (message, type) => {
        toast(message, { type });
    };
    const handleSearchChange = (e) => {
        setSearchQuery(e.target.value);
        setCurrentPage(1); // Reset to first page on new search
    };

    const handleSearchTypeChange = (e) => {
        setSearchType(e.target.value);
        setSearchQuery(''); // Clear search query when changing search type
    };

    const filteredCollections = collections.filter(collection => {
        if (!searchQuery.trim()) return true;
        const normalizedSearchQuery = searchQuery.replace(/\s+/g, '').toLowerCase(); // Normalize search query

        switch (searchType) {
            case 'collectionName':
                return collection.collectionName.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery);
            case 'parentCommunity':
                return collection.communityDTOForSelect.communityName.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery);
            case 'description':
                return collection.shortDescription.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery);
            case 'all':
                return Object.values(collection).some(value =>
                    typeof value === 'string' && value.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery)
                );
            default:
                return false;
        }
    });

    const indexOfLastItem = currentPage * itemsPerPage;
    const indexOfFirstItem = indexOfLastItem - itemsPerPage;
    const currentCollections = filteredCollections.slice(indexOfFirstItem, indexOfLastItem);

    const totalPages = Math.ceil(filteredCollections.length / itemsPerPage);

    const handlePageChange = (pageNumber) => {
        setCurrentPage(pageNumber);
    };

    const handleNextPage = () => {
        if (currentPage < totalPages) {
            setCurrentPage(prevPage => prevPage + 1);
        }
    };

    const handlePreviousPage = () => {
        if (currentPage > 1) {
            setCurrentPage(prevPage => prevPage - 1);
        }
    };

    function getRelativePath(fullPath) {
        const splitPath = fullPath.split('public\\');
        return splitPath[1];
    }
    function Status({ status }) {
        const getStatusText = (status) => {
            if (status == true) {
                return "Published";
            } else {
                return "Undiscoverable";
            }
        };

        const getStatusColor = (status) => {
            if (status == true) {
                return "#28A745";
            } else {
                return "#DC3545";
            }
        };
        const getBoxShadow = (status) => {
            const color = getStatusColor(status);
            return `0 0 10px 2px ${color}`;
        };
        return (
            <p style={{ margin: '0' }}>
                <span style={{
                    margin: '0',
                    backgroundColor: getStatusColor(status),
                    borderRadius: '3px',
                    color: 'white',
                    padding: '2px',
                    fontSize: 'small',
                    boxShadow: getBoxShadow(status)

                }}>
                    {getStatusText(status)}
                </span>
            </p>
        );
    }
    return (
        <div>
            <Header />
            <Helmet>
                <meta charSet="utf-8" />
                <title>List of Collections - dSPACE</title>
            </Helmet>
            <div className="container mt-5">
                {/* <div className="py-1 bg-light">
                    <Link to="/HomepageAdmin" className="text-decoration-none">Home </Link>
                    <span className="text-dark" ><strong>| Collection</strong></span>
                </div> */}
                <h1 className="mb-4">Collections List</h1>
                <div className="row mb-4">
                    <div className="col-md-5">
                        <label htmlFor="searchInput" className="form-label fw-bold">Search</label>
                        <input
                            id="searchInput"
                            type="text"
                            className="form-control"
                            placeholder="Search collections..."
                            value={searchQuery}
                            onChange={handleSearchChange}
                        />
                    </div>
                    <div className="col-md-4">
                        <label htmlFor="searchTypeSelect" className="form-label fw-bold">Search By</label>
                        <select
                            id="searchTypeSelect"
                            className="form-select"
                            value={searchType}
                            onChange={handleSearchTypeChange}
                        >
                            <option value="all">Select...</option> {/* Option for searching by all fields */}
                            <option value="collectionName">Collection Name</option>
                            {/* <option value="parentCommunity">Parent Community</option> */}
                            <option value="description">Description</option> {/* New option for searching by description */}
                        </select>
                    </div>
                    <div className="col-md-3 d-flex align-items-end justify-content-end">
                        <Link to="/Dspace/Collection/CreateCollection" className="btn btn-primary">Create new collection</Link>
                    </div>
                </div>

                <ul className="list-group">
                    {currentCollections.map((collection, index) => (
                        <li key={index} className="list-group-item clickable-row border-0">
                            <div className="d-flex justify-content-between align-items-center">
                                <div className="d-flex">
                                    
                                    {collection?.logoUrl ? (
                                        <img src={`/${getRelativePath(collection.logoUrl)}`} alt="logo" className='img-thumbnail'
                                        style={{ width: '150px', height: '200px', objectFit: 'cover', marginRight: '20px', boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1)' }}
                                        onError={(e) => {
                                            e.target.onerror = null;
                                            e.target.src = '/No-Image-Placeholder.png';
                                          }} />
                                    ) : (
                                        <img
                                            src={`/No-Image-Placeholder.png`}
                                            alt={collection?.collectionName}
                                            style={{ width: '150px', height: '200px', objectFit: 'cover', marginRight: '20px', boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1)' }}

                                        />
                                    )}

                                    <div style={{}}>
                                    <Status status={collection.isActive} />
                                        <h3 style={{ margin: '0' }}>
                                            <Badge sx={{
                                                '& .MuiBadge-badge': {
                                                    transform: 'translateX(25px)',
                                                },
                                            }} color="primary" badgeContent={collection.totalItems} showZero max={999}>
                                                <Link style={{ textDecoration: 'none' }} to={`/Dspace/Collection/CollectionDetail/${collection.collectionId}`}>
                                                    <span className="author-item" style={{ cursor: 'pointer' }}>{collection.collectionName}</span>
                                                </Link>
                                            </Badge>
                                        </h3>
                                        {/* <p className="mb-0">{community.shortDescription}</p> */}
                                        <div>
                                            {expandedAbstracts[`collection_${collection.collectionId}`] ? (
                                                <div>
                                                    <div>{collection.shortDescription}</div>
                                                    <button onClick={() => toggleAbstract(`collection_${collection.collectionId}`)} style={{ background: 'none', color: '#207f9e', border: 'none', padding: '0', cursor: 'pointer', fontWeight: 'bolder' }}>
                                                        <ExpandLess /> Collapse
                                                    </button>
                                                </div>
                                            ) : (
                                                <div>
                                                    <div style={{ overflow: 'hidden', textOverflow: 'ellipsis', display: '-webkit-box', WebkitLineClamp: 5, WebkitBoxOrient: 'vertical' }}>
                                                        {collection.shortDescription}
                                                    </div>
                                                    {collection.shortDescription.split(' ').length > 20 && (
                                                        <button onClick={() => toggleAbstract(`collection_${collection.collectionId}`)} style={{ background: 'none', color: '#207f9e', border: 'none', padding: '0', cursor: 'pointer', fontWeight: 'bolder' }}>
                                                            <ExpandMore /> Show more
                                                        </button>
                                                    )}
                                                </div>
                                            )}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    ))}
                </ul>

                <div className="d-flex justify-content-center">
                    <nav>
                        <ul className="pagination">
                            <li className={`page-item ${currentPage === 1 ? 'disabled' : ''}`}>
                                <button className="page-link" onClick={handlePreviousPage}>Previous</button>
                            </li>
                            {[...Array(totalPages)].map((_, i) => (
                                <li key={i} className={`page-item ${currentPage === i + 1 ? 'active' : ''}`}>
                                    <button className="page-link" onClick={() => handlePageChange(i + 1)}>
                                        {i + 1}
                                    </button>
                                </li>
                            ))}
                            <li className={`page-item ${currentPage === totalPages ? 'disabled' : ''}`}>
                                <button className="page-link" onClick={handleNextPage}>Next</button>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    );
};

export default CollectionList;
