import React, { useState, useEffect, useCallback } from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from '../../components/Header/Header';
import { Link } from 'react-router-dom';
import { useDropzone } from 'react-dropzone';
import { Helmet } from 'react-helmet';
import { toast } from 'react-toastify';
import { Autocomplete, TextField } from '@mui/material';
import { jwtDecode } from 'jwt-decode';


const CreateCollection = () => {
    const [collectionData, setCollectionData] = useState({
        logoUrl: 'nothing',
        collectionName: '',
        shortDescription: '',
        isActive: true,
        license: '',
        entityTypeId: 1
    });

    const [communities, setCommunities] = useState([]);
    const [entityTypes, setEntityTypes] = useState([]);
    const [successMessage, setSuccessMessage] = useState('');
    const [errors, setErrors] = useState({});
    const token = localStorage.getItem('Token');
    const header = `Bearer ${token}`;
    const role = token ? jwtDecode(token)["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"] : null;

    useEffect(() => {
        if (role === "ADMIN") {
            axios.get(`${process.env.REACT_APP_BASE_URL}/api/Community/getListOfCommunities`)
                .then(response => {
                    setCommunities(response.data);
                })
                .catch(error => {
                    console.error('There was an error fetching the parent communities!', error);
                    addNotification(error.response.data, 'error');
                });
        } else if (role === "STAFF") {
            axios.get(`${process.env.REACT_APP_BASE_URL}/api/CommunityStaff/GetAllCommunities`, {
                headers: {
                    Authorization: header
                }
            })
                .then(response => {
                    setCommunities(response.data);
                })
                .catch(error => {
                    console.error('There was an error fetching the parent communities!', error);
                    addNotification(error.response.data, 'error');
                });
        }

    }, []);
    const addNotification = (message, type) => {
        toast(message, { type });
    };
    const handleChange = (e) => {
        const { name, value, type, checked } = e.target;
        setCollectionData({
            ...collectionData,
            [name]: type === 'checkbox' ? checked : value,
        });
    };

    const validate = () => {
        let newErrors = {};
        if (!collectionData.collectionName.trim()) newErrors.collectionName = 'Collection Name is required';
        if (!collectionData.shortDescription.trim()) newErrors.shortDescription = 'Short Description is required';
        if (!collectionData.license.trim()) newErrors.license = 'License is required';
        if (!collectionData.entityTypeId) newErrors.entityTypeId = 'Entity Type is required';
        setErrors(newErrors);
        return Object.keys(newErrors).length === 0;
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        const formData = new FormData();
        formData.append('file', file);
        Object.keys(collectionData).forEach(key => {
            formData.append(key, collectionData[key]);
        });
        formData.append('communityId', selectedCommunity.communityId)
        if (validate()) {
            if (role === "ADMIN") {
                axios.post(`${process.env.REACT_APP_BASE_URL}/api/Collection/CreateCollection`, formData, {
                    'Content-Type': 'multipart/form-data',
                    headers: { Authorization: header }
                })
                    .then(response => {
                        console.log('Collection created successfully!', response.data);
                        setSuccessMessage('Collection added successfully!');
                        addNotification('Collection added successfully!', 'success');
                        // Reset form or redirect to collections list
                    })
                    .catch(error => {
                        console.error('There was an error creating the collection!', error);
                        addNotification('There was an error creating the collection!', 'error');
                    });
            }
            else if (role === "STAFF") {
                axios.post(`${process.env.REACT_APP_BASE_URL}/api/CollectionStaff/CreateCollection`, formData, {
                    'Content-Type': 'multipart/form-data',
                    headers: { Authorization: header }
                })
                    .then(response => {
                        console.log('Collection created successfully!', response.data);
                        setSuccessMessage('Collection added successfully!');
                        addNotification('Collection added successfully!', 'success');
                        // Reset form or redirect to collections list
                    })
                    .catch(error => {
                        console.error('There was an error creating the collection!', error);
                        addNotification('There was an error creating the collection!', 'error');
                    });
            }
        }
    };

    const [file, setFile] = useState(null);
    const [preview, setPreview] = useState(null);

    const onDrop = useCallback((acceptedFiles) => {
        const file = acceptedFiles[0];
        setFile(file);

        const reader = new FileReader();
        reader.onloadend = () => {
            setPreview(reader.result);
        };
        reader.readAsDataURL(file);
        console.log(file)
    }, []);

    const { getRootProps, getInputProps, isDragActive } = useDropzone({
        onDrop,
        multiple: false,
        accept: 'image/*',
    });

    const [selectedCommunity, setSelectedCommunity] = useState(null);
    const handleCommunityChange = (event, newValue) => {
        setSelectedCommunity(newValue);
        console.log(newValue);
    };

    return (

        <div>
            <Header />
            <Helmet>
                <meta charSet="utf-8" />
                <title>Create new Collection - dSPACE</title>
            </Helmet>
            <div className="container mt-5">
                {/* <div className="py-1 bg-light">
                    <Link to="/HomepageAdmin" className="text-decoration-none">Home </Link>
                    <span className="text-dark"><strong>| Add Collection</strong></span>
                </div> */}
                <h1 className="mb-4">Create New Collection</h1>
                {/* {successMessage && <div className="alert alert-success">{successMessage}</div>} */}
                {(role === "STAFF" && communities.length == 0) ? (<></>) : (
                    <form onSubmit={handleSubmit}>
                        <div className='d-flex justify-content-between'>
                            <div className='row w-50'>
                                <div
                                    {...getRootProps()}
                                    className={`dropzone border d-flex flex-column rounded p-5 ${isDragActive ? 'bg-light' : ''}`}
                                    style={{ cursor: 'pointer' }}
                                >
                                    <input {...getInputProps()} />
                                    {isDragActive ? (
                                        <p className="text-center">Drop the files here...</p>
                                    ) : (
                                        <p className="text-center">Drag 'n' drop a file here, or click to select one</p>
                                    )}
                                    {preview ? (
                                        <div className="mt-3">
                                            <h5>Preview:</h5>
                                            <img src={preview} alt="Preview" className="img-thumbnail" />
                                        </div>
                                    ) : (
                                        <img src='../../dropbox.png' className="img-thumbnail" style={{ width: 300, height: 300, margin: 'auto' }} alt="Background" />
                                    )}
                                </div>
                            </div>

                            <div className="row w-50 flex-column justify-content-center">
                                <div className="col-md-6 mb-3 w-75">
                                    <div className="form-group">
                                        <label>Collection Name</label>
                                        <input
                                            type="text"
                                            className={`form-control ${errors.collectionName ? 'is-invalid' : ''}`}
                                            name="collectionName"
                                            value={collectionData.collectionName}
                                            onChange={handleChange}
                                        />
                                        {errors.collectionName && <div className="invalid-feedback">{errors.collectionName}</div>}
                                    </div>
                                </div>
                                <div className="col-md-6 mb-3 w-75">
                                    <div className="form-group">
                                        <label>Short Description</label>
                                        <textarea
                                            style={{ height: 200 }}
                                            className={`form-control ${errors.shortDescription ? 'is-invalid' : ''}`}
                                            name="shortDescription"
                                            value={collectionData.shortDescription}
                                            onChange={handleChange}
                                        />
                                        {errors.shortDescription && <div className="invalid-feedback">{errors.shortDescription}</div>}
                                    </div>
                                </div>
                                <div className="col-md-6 mb-3 w-75">
                                    <div className="form-group">
                                        <label>Community</label>
                                        <Autocomplete
                                            options={communities}
                                            getOptionLabel={(option) => option.communityName}
                                            onChange={handleCommunityChange}
                                            renderInput={(params) => (
                                                <TextField
                                                    {...params}
                                                    variant="outlined"
                                                    placeholder="Select Parent Community"
                                                    required={true}
                                                />
                                            )}
                                        />
                                        {errors.communityId && <div className="invalid-feedback">{errors.communityId}</div>}
                                    </div>
                                </div>
                                <div className="col-md-6 mb-3 w-75">
                                    <div className="form-group">
                                        <label>License</label>
                                        <textarea
                                            type="text"
                                            className={`form-control ${errors.license ? 'is-invalid' : ''}`}
                                            name="license"
                                            value={collectionData.license}
                                            onChange={handleChange}
                                        />
                                        {errors.license && <div className="invalid-feedback">{errors.license}</div>}
                                    </div>
                                </div>
                                <div className="col-md-6 mb-3">
                                    <div className="form-group form-check">
                                        <input
                                            type="checkbox"
                                            className="form-check-input"
                                            name="isActive"
                                            checked={collectionData.isActive}
                                            onChange={handleChange}
                                        />
                                        <label className="form-check-label">Is Active</label>
                                    </div>
                                </div>
                                <div className="col-md-6 mb-3">
                                    <button type="submit" className="btn btn-primary mt-3">Add</button>
                                </div>
                            </div>
                        </div>

                    </form>)}
                <div className="mt-3">
                    <Link to="/Dspace/Collection/CollectionList">Back to List</Link>
                </div>
            </div>
        </div>
    );
};

export default CreateCollection;
