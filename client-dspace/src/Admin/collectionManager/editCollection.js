import React, { useCallback, useEffect, useState } from 'react';
import axios from 'axios';
import { useParams, Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from '../../components/Header/Header';
import { Helmet } from 'react-helmet';
import { useDropzone } from 'react-dropzone';
import { Autocomplete, TextField } from '@mui/material';
import { toast } from 'react-toastify';
import { jwtDecode } from 'jwt-decode';

const EditCollection = () => {
    const { collectionId } = useParams();

    const [collectionData, setCollectionData] = useState({
        logoUrl: '',
        collectionName: '',
        shortDescription: '',
        license: '',
        isActive: true,
    });

    const [parentCommunities, setParentCommunities] = useState([]);
    const [collectionNameHeader, setCollectionNameHeader] = useState('');
    const [parentCommunityId, setParentCommunityId] = useState(0);
    const [successMessage, setSuccessMessage] = useState('');
    const [errors, setErrors] = useState({});
    const [parentCommunity, setParentCommunity] = useState({});

    const [file, setFile] = useState(null);
    const [preview, setPreview] = useState(null);
    const addNotification = (message, type) => {
        toast(message, { type });
    };
    const token = localStorage.getItem('Token');
    const header = `Bearer ${token}`;
    const role = token ? jwtDecode(token)["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"] : null;
    useEffect(() => {
        if (role === "ADMIN") {
            axios.get(`${process.env.REACT_APP_BASE_URL}/api/Collection/getCollection/${collectionId}`)
                .then(async (response) => {
                    setCollectionData({
                        logoUrl: response.data.logoUrl || '',
                        collectionName: response.data.collectionName || '',
                        shortDescription: response.data.shortDescription || '',
                        license: response.data.license || '',
                        isActive: response.data.isActive || '',
                    });
                    setCollectionNameHeader(response.data.collectionName);
                    setParentCommunityId(response.data.communityId)
                    if (response.data.logoUrl !== '') {
                        const relativePath = getRelativePath(response.data.logoUrl);
                        try {
                            const responseFile = await fetch(`/${relativePath}`); 
                            const blob = await responseFile.blob();
                            const fileName = relativePath.split('/').pop();
                            const fileObject = new File([blob], fileName, { type: blob.type });
                            console.log(fileObject)
                            setFile(fileObject);
                            setPreview(`/${relativePath}`);
                        } catch (error) {
                            console.error('Error fetching the logo file:', error);
                        }
                    }
                })
                .catch(error => {
                    console.error('There was an error fetching the collection!', error);
                    addNotification('There was an error fetching the collection!', 'error');
                });

            axios.get(`${process.env.REACT_APP_BASE_URL}/api/Community/getListOfCommunities`)
                .then(response => {
                    setParentCommunities(response.data);
                })
                .catch(error => {
                    console.error('There was an error fetching the list of communities!', error);
                    addNotification('There was an error fetching the list of communities!', 'error');
                });
        } else if (role === "STAFF") {
            axios.get(`${process.env.REACT_APP_BASE_URL}/api/CollectionStaff/getCollection/${collectionId}`, {
                headers: {
                    Authorization: header
                }
            })
                .then(async (response) => {
                    setCollectionData({
                        logoUrl: response.data.logoUrl || '',
                        collectionName: response.data.collectionName || '',
                        shortDescription: response.data.shortDescription || '',
                        license: response.data.license || '',
                        isActive: response.data.isActive || '',
                    });
                    setCollectionNameHeader(response.data.collectionName);
                    setParentCommunityId(response.data.communityId)
                    setParentCommunity(response.data.communityDTOForSelect)
                    if (response.data.logoUrl !== '') {
                        const relativePath = getRelativePath(response.data.logoUrl);
                        try {
                            const responseFile = await fetch(`/${relativePath}`); 
                            const blob = await responseFile.blob();
                            const fileName = relativePath.split('/').pop();
                            const fileObject = new File([blob], fileName, { type: blob.type });
                            console.log(fileObject)
                            setFile(fileObject);
                            setPreview(`/${relativePath}`);
                        } catch (error) {
                            console.error('Error fetching the logo file:', error);
                        }
                    }
                })
                .catch(error => {
                    console.error('There was an error fetching the collection!', error);
                    addNotification('There was an error fetching the collection!', 'error');
                });

            axios.get(`${process.env.REACT_APP_BASE_URL}/api/CommunityStaff/GetAllCommunities`, {
                headers: {
                    Authorization: header
                }
            })
                .then(response => {
                    setParentCommunities(response.data);
                })
                .catch(error => {
                    console.error('There was an error fetching the list of communities!', error);
                    addNotification('There was an error fetching the list of communities!', 'error');
                });
        }
    }, [collectionId, successMessage]);

    useEffect(() => {
        var checkCommunity = parentCommunities.find(c => c.communityId === parentCommunityId)
        if (checkCommunity !== undefined) {
            console.log(checkCommunity)
            setSelectedCommunity(checkCommunity)
        } else {

        }
    }, [parentCommunityId]);

    const handleChange = (e) => {
        const { name, value, type, checked } = e.target;

        setCollectionData({
            ...collectionData,
            [name]: type === 'checkbox' ? checked : value,
        });

        setErrors({
            ...errors,
            [name]: '',
        });
    };

    const validate = () => {
        let valid = true;
        const newErrors = {};

        if (!collectionData.collectionName.trim()) {
            newErrors.collectionName = 'Collection Name is required';
            valid = false;
        }

        // Validate shortDescription
        if (!collectionData.shortDescription.trim()) {
            newErrors.shortDescription = 'Short Description is required';
            valid = false;
        }

        setErrors(newErrors);
        return valid;
    };

    function getRelativePath(fullPath) {
        const splitPath = fullPath.split('public\\');
        return splitPath[1].replace(/\\/g, '/');
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        if (!validate()) {
            return;
        }
        const formData = new FormData();
        formData.append('file', file);
        Object.keys(collectionData).forEach(key => {
            formData.append(key, collectionData[key]);
        });
        if (role === "ADMIN" || (role === "STAFF" && parentCommunity.canEdit)) {
            formData.append('communityId', selectedCommunity.communityId)
        } else {
            formData.append('communityId', parentCommunityId)
        }
        if (role === "ADMIN") {
            axios.put(`${process.env.REACT_APP_BASE_URL}/api/Collection/updateCollection/${collectionId}`, formData, {
                headers: { Authorization: `Bearer ${localStorage.getItem('Token')}` }
            })
                .then(response => {
                    console.log('Collection updated successfully!', response.data);
                    setSuccessMessage('Collection updated successfully!');
                    addNotification('Collection updated successfully!', 'success');
                })
                .catch(error => {
                    console.error('There was an error updating the collection!', error);
                    addNotification('There was an error updating the collection!', 'error');
                });
        } else if (role === "STAFF") {
            axios.put(`${process.env.REACT_APP_BASE_URL}/api/CollectionStaff/updateCollection/${collectionId}`, formData, {
                headers: { Authorization: `Bearer ${localStorage.getItem('Token')}` }
            })
                .then(response => {
                    console.log('Collection updated successfully!', response.data);
                    setSuccessMessage('Collection updated successfully!');
                    addNotification('Collection updated successfully!', 'success');
                })
                .catch(error => {
                    console.error('There was an error updating the collection!', error);
                    addNotification('There was an error updating the collection!', 'error');
                });
        }
    };



    const onDrop = useCallback((acceptedFiles) => {
        const file = acceptedFiles[0];
        setFile(file);

        const reader = new FileReader();
        reader.onloadend = () => {
            setPreview(reader.result);
        };
        reader.readAsDataURL(file);
        console.log(file)
    }, []);

    const { getRootProps, getInputProps, isDragActive } = useDropzone({
        onDrop,
        multiple: false,
        accept: 'image/*',
    });

    const [selectedCommunity, setSelectedCommunity] = useState(null);
    const handleCommunityChange = (event, newValue) => {
        setSelectedCommunity(newValue);
        console.log(newValue);
    };



    return (
        <div>
            <Header />
            <Helmet>
                <meta charSet="utf-8" />
                <title>Modify Collection - {`${collectionData?.collectionName}`} - dSPACE</title>
            </Helmet>
            <div className="container mt-5">
                <h1 className="mb-4">Edit Collection - {collectionNameHeader}</h1>
                {/* {successMessage && <div className="alert alert-success">{successMessage}</div>} */}
                <form onSubmit={handleSubmit}>
                    <div className='d-flex justify-content-between'>
                        <div className='row w-50'>
                            <div
                                {...getRootProps()}
                                className={`dropzone border d-flex flex-column rounded p-5 ${isDragActive ? 'bg-light' : ''}`}
                                style={{ cursor: 'pointer' }}
                            >
                                <input {...getInputProps()} />
                                {isDragActive ? (
                                    <p className="text-center">Drop the files here...</p>
                                ) : (
                                    <p className="text-center">Drag 'n' drop a file here, or click to select one</p>
                                )}
                                {preview ? (
                                    <div className="mt-3">
                                        <h5>Preview:</h5>
                                        <img src={preview} alt="Preview" className="img-thumbnail" />
                                    </div>
                                ) : (
                                    <img src='/dropbox.png' className="img-thumbnail" style={{ width: 300, height: 300, margin: 'auto' }} alt="Background" />
                                )}
                            </div>
                        </div>
                        <div className="row w-50 flex-column justify-content-center">
                            <div className="col-md-5 w-100">
                                <div className="form-group mb-3">
                                    <label>Name</label>
                                    <input
                                        type="text"
                                        className={`form-control ${errors.collectionName ? 'is-invalid' : ''}`}
                                        name="collectionName"
                                        value={collectionData.collectionName}
                                        onChange={handleChange}
                                    />
                                    {errors.collectionName && <div className="invalid-feedback">{errors.collectionName}</div>}
                                </div>
                                <div className="form-group mb-3">
                                    <label>Short Description</label>
                                    <textarea
                                        style={{ height: 250 }}
                                        className={`form-control ${errors.shortDescription ? 'is-invalid' : ''}`}
                                        name="shortDescription"
                                        value={collectionData.shortDescription}
                                        onChange={handleChange}
                                    />
                                    {errors.shortDescription && <div className="invalid-feedback">{errors.shortDescription}</div>}
                                </div>
                                <div className="form-group mb-3">
                                    <label>Parent Community</label>
                                    {(role === "ADMIN" || (role === "STAFF" && parentCommunity.canEdit)) ? (
                                        <Autocomplete
                                            options={parentCommunities}
                                            getOptionLabel={(option) => option.communityName}
                                            value={selectedCommunity}
                                            onChange={handleCommunityChange}
                                            renderInput={(params) => (
                                                <TextField
                                                    {...params}
                                                    variant="outlined"
                                                    placeholder="Select Parent Community"
                                                    required={true}
                                                />
                                            )}
                                        />
                                    ) :
                                        <Autocomplete
                                            readOnly
                                            options={parentCommunities}
                                            getOptionLabel={(option) => option.communityName}
                                            value={parentCommunity}
                                            onChange={handleCommunityChange}
                                            renderInput={(params) => (
                                                <TextField
                                                    {...params}
                                                    variant="outlined"
                                                    placeholder="Select Parent Community"
                                                    required={true}
                                                />
                                            )}
                                        />
                                    }

                                </div>
                                <div className="form-group mb-3">
                                    <label>Active</label>
                                    <input
                                        type="checkbox"
                                        className="form-check-input ms-2"
                                        name="isActive"
                                        checked={collectionData.isActive}
                                        onChange={handleChange}
                                    />
                                </div>
                                <button type="submit" className="btn btn-primary mt-4">Update</button>
                            </div>
                        </div>
                    </div>
                </form>
                <div className="mt-3">
                    <Link to="/Dspace/Collection/CollectionList">Back to List</Link>
                </div>
            </div>
        </div>
    );
};

export default EditCollection;
