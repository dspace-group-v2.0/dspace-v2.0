import * as React from 'react';
import { useState, useEffect } from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import RestoreIcon from '@mui/icons-material/Restore';
import DeleteIcon from '@mui/icons-material/DeleteOutlined';
import {
    GridRowModes,
    DataGrid,
    GridToolbarContainer,
    GridActionsCellItem,
    GridRowEditStopReasons,
    useGridApiRef,
    gridClasses
} from '@mui/x-data-grid';
import {
    randomId,
} from '@mui/x-data-grid-generator';
import Autocomplete from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';
import { useNavigate, useParams } from 'react-router-dom';
import axios from 'axios';
import MetadataField from '../../components/metadataFields'
import Header from '../../components/Header/Header';
import { Accordion, AccordionDetails, AccordionSummary, AppBar, Card, CardContent, Chip, Container, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, FormControl, IconButton, InputLabel, Link, List, ListItem, ListItemAvatar, ListItemButton, ListItemText, MenuItem, Paper, Select, Slide, styled, TableBody, TableCell, tableCellClasses, TableContainer, TableHead, TableRow, Toolbar, Typography } from '@mui/material';
import { Tabs, Tab, Table } from 'react-bootstrap';
import Save from '@mui/icons-material/Save';
import Close from '@mui/icons-material/Close';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import '../Items/ModifyItem.css'
import LoadingButton from '@mui/lab/LoadingButton';
import FileDownloadIcon from '@mui/icons-material/FileDownload';
import ModeIcon from '@mui/icons-material/Mode';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
import UploadFileIcon from '@mui/icons-material/UploadFile';
import { darken } from '@mui/material/styles';
import SaveIcon from '@mui/icons-material/Save';
import { Helmet } from 'react-helmet';
import CloseIcon from '@mui/icons-material/Close';
import { useDropzone } from 'react-dropzone';
import DisabledByDefaultIcon from '@mui/icons-material/DisabledByDefault';
import VisibilityIcon from '@mui/icons-material/Visibility';
import Avatar from '@mui/material/Avatar';
import Divider from '@mui/material/Divider';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { Select as SelectMui } from '@mui/material';
import { toast } from 'react-toastify';
import { jwtDecode } from 'jwt-decode';

const PageLink = ({ href, text }) => {
    return (
        <a href={href}>
            {text}
        </a>
    );
};

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const addNotification = (message, type) => {
    toast(message, { type });
};
function MetadataSelect(props) {
    const { id, field, value, api, colDef, row } = props;
    console.log(props)
    const handleChange = (event, newValue) => {
        if (newValue !== null) {
            api.setEditCellValue({ id, field, value: newValue.metadataFieldName }, event);
        }
        console.log(newValue)
    };
    return (
        <Autocomplete
            sx={{
                width: '100%'
            }}
            options={MetadataField}
            getOptionLabel={(option) => `${option.metadataFieldName}`}
            id="metadata-customized-option-demo"
            onChange={handleChange}
            renderInput={(params) => (
                <TextField {...params} label="Choose a metadata" variant="standard" />
            )}
        />
    );
}

function ConvertDateTime(datetime) {
    const date = new Date(datetime);

    // Get the components
    const day = date.toLocaleString('en-US', { weekday: 'short' });
    const month = date.toLocaleString('en-US', { month: 'short' });
    const dayOfMonth = date.getDate();
    const year = date.getFullYear();
    const hours = date.getHours().toString().padStart(2, '0');
    const minutes = date.getMinutes().toString().padStart(2, '0');
    const seconds = date.getSeconds().toString().padStart(2, '0');
    const timezoneOffset = -date.getTimezoneOffset() / 60;
    const timezone = `GMT${timezoneOffset >= 0 ? '+' : ''}${timezoneOffset.toString().padStart(2, '0')}00 (Indochina Time)`;
    return `${day} ${month} ${dayOfMonth} ${year} ${hours}:${minutes}:${seconds} ${timezone}`;
}

export default function ModifyItem() {
    const { itemId } = useParams();
    const [items, setItems] = React.useState([]);
    const [collections, setCollections] = React.useState([]);
    const [error, setError] = React.useState(null);
    const token = localStorage.getItem('Token');
    const userRole = token ? jwtDecode(token)["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"] : null;
    const header = `Bearer ${token}`;
    const [modifyMetadata, setModifyFields] = React.useState([]);

    const navigate = useNavigate();
    const [fileContains, setFileContains] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    function LoadingOverlay() {
        return (
            <div className="loading-overlay">
                <div className="spinner-border text-light" role="status">
                    <span className="sr-only"></span>
                </div>
            </div>
        );
    }
    // Initialize fileContains when items change
    useEffect(() => {
        if (items && items.listFilesDTOForSelect) {
            setFileContains(items.listFilesDTOForSelect);
        }
    }, [items]);

    React.useEffect(() => {
        setIsLoading(true)
        if (itemId != 0) {
            setIsLoading(true)
            axios.get(`${process.env.REACT_APP_BASE_URL}/api/Item/GetItemFullById/${itemId}`)
                .then(({ data }) => {
                    setItems(data);
                    setModifyFields(data.listMetadataValueDTOForSelect)
                })
                .catch(error => {
                    setError(error.message);
                }).finally(() => {
                    setIsLoading(false);
                });
            if (userRole === "STAFF") {
                setIsLoading(true)
                axios.get(`${process.env.REACT_APP_BASE_URL}/api/CollectionStaff/GetAllCollections`, {
                    headers: {
                        Authorization: header
                    }
                })
                    .then(({ data }) => {
                        setCollections(data.filter(collection => collection.canEdit === true));
                    })
                    .catch(error => {
                        setError(error.message);
                    }).finally(() => {
                        setIsLoading(false);
                    });
            } else {
                setIsLoading(true)
                axios.get(`${process.env.REACT_APP_BASE_URL}/api/Collection/getListOfCollections`)
                    .then(({ data }) => {
                        setCollections(data)
                    })
                    .catch(error => {
                        setError(error.message);
                    }).finally(() => {
                        setIsLoading(false);
                    });
            }
        }
    }, [itemId]);

    const handleNonDiscoverableClick = () => {
        // const userConfirmed = window.confirm("Do you want to make this item become non-discoverable");
        // if (userConfirmed) {
        axios.put(`${process.env.REACT_APP_BASE_URL}/api/Item/ChangeDiscoverableOfItem`, null, {
            params: {
                itemId: itemId,
                discoverable: 'false'
            },
            headers: {
                Authorization: header
            }
        })
            .then(response => {
                console.log('This item is now setting to non-discoverable');
                addNotification('This item is now setting to non-discoverable', 'success')
                setItems(prevItems => ({ ...prevItems, discoverable: false }));
            })
            .catch(error => {
                console.error('Error:', error);
                addNotification('Error', 'error')
            });
        // setTimeout(() => {
        //     window.location.reload();
        // }, 2000);
        // window.location.reload();
        // } else {
        //     console.log('Request canceled by user');
        // }
    };

    const handleSendRequestUndiscoverable = () => {
        axios.put(`${process.env.REACT_APP_BASE_URL}/api/FileUpload/UndiscoverableFileWithFileId/${fileModifyChoose}`, null, {
            headers: {
                Authorization: header
            }
        })
            .then(response => {
                console.log('This file is now setting to non-discoverable');
                handleCloseDialogDiscoverable();
                setFileContains(prevFileContains =>
                    prevFileContains.map(file =>
                        file.fileId === fileModifyChoose ? { ...file, isActive: !file.isActive } : file
                    )
                );
            })
            .catch(error => {
                console.error('Error:', error);
            });
    };

    const handleSendRequestDelete = () => {
        axios.delete(`${process.env.REACT_APP_BASE_URL}/api/FileUpload/DeleteFileWithFileId/${fileModifyChoose}`, null, {
            headers: {
                Authorization: header
            }
        })
            .then(response => {
                console.log('This file is now deleted');
                addNotification('This file is now deleted', 'success')
                handleCloseDialogDelete();
                setFileContains(prevFileContains =>
                    prevFileContains.filter(file => file.fileId !== fileModifyChoose)
                );
            })
            .catch(error => {
                console.error('Error:', error);
                addNotification('Error', 'error')
            });
    };

    const handleDeleteItem = () => {
        const confirmDelete = window.confirm("Are you sure you want to delete this item?");
        if (confirmDelete) {
            fetch(`${process.env.REACT_APP_BASE_URL}/api/Item/deleteItem/${itemId}`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                .then(response => {
                    if (!response.ok) {
                        throw new Error('Failed to delete item.');
                    }
                    // window.alert('Item deleted successfully!');
                    addNotification('Item deleted successfully!', 'success')
                    setTimeout(() => {
                        if (userRole !== null) {
                            if (userRole === "ADMIN") {
                                navigate('/Dspace/Collection/ItemList/0');
                            } else if (userRole === "STAFF") {
                                navigate('/Dspace/Collection/ItemStaffList/0');
                            }
                        } else {
                            navigate('/Login');
                        }

                    }, 2000);

                })
                .catch(error => {
                    console.error('Error deleting item:', error);
                    addNotification('Error deleting item:', 'error')
                });
        }
    };


    const handleDiscoverableClick = () => {
        // const userConfirmed = window.confirm("Do you want to make this item become discoverable");
        // if (userConfirmed) {
        axios.put(`${process.env.REACT_APP_BASE_URL}/api/Item/ChangeDiscoverableOfItem`, null, {
            params: {
                itemId: itemId,
                discoverable: 'true'
            },
            headers: {
                Authorization: header
            }
        })
            .then(response => {
                console.log('This item is now setting to discoverable');
                addNotification('This item is now setting to discoverable', 'success')
                setItems(prevItems => ({ ...prevItems, discoverable: true }));
            })
            .catch(error => {
                console.error('Error:', error);
                addNotification('Error', 'error')
            });
        // setTimeout(() => {
        //     window.location.reload();
        // }, 2000);
        // window.location.reload();
        // } else {
        //     console.log('Request canceled by user');
        // }
    };

    const handleRedirectToDetail = () => {
        navigate(`/DSpace/ItemDetail/` + itemId);
    };

    const [selectedValue, setSelectedValue] = React.useState('');
    const [popup, setPop] = React.useState(false)
    const handleClickOpen = () => {
        setPop(!popup)
    }
    const closePopup = () => {
        setPop(false)
    }
    const handleSaveCollection = () => {
        if (selectedValue) {
            axios.put(`${process.env.REACT_APP_BASE_URL}/api/Item/ChangeCollectionOfItem`, null, {
                params: {
                    itemId: itemId,
                    collectionId: selectedValue.collectionId,
                },
                headers: {
                    Authorization: header
                }
            })
                .then(response => {
                    console.log('Success:', response.data);
                    addNotification('Success', 'success')
                })
                .catch(error => {
                    console.error('Error:', error);
                    addNotification('Error', 'error')
                });

            setTimeout(() => {
                window.location.reload();
            }, 2000);
        } else {
            console.error('No collection selected');
            addNotification('No collection selected', 'error')
        }
    };



    const [open, setOpen] = React.useState(false);
    const handleClickOpenMeta = () => {
        setOpen(true);
    };
    const handleCloseMeta = () => {
        setOpen(false);
    };
    const apiRef = useGridApiRef();
    const [hasUnsavedRows, setHasUnsavedRows] = React.useState(false);
    const [isSaving, setIsSaving] = React.useState(false);
    const [addFields, setAddFields] = React.useState([{
        textValue: '',
        textLang: '',
        metadataFieldId: '',
        itemId: itemId
    }]);

    const handleChangeInputAdd = (index, event) => {
        const values = [...addFields];
        const { name, value } = event.target;

        values[index][name] = value;
        setAddFields(values);
    };
    const handleAddFields = () => {
        setAddFields([...addFields, { textValue: '', textLang: '', metadataFieldId: '', itemId: itemId }]);
    };
    const handleRemoveFields = (index) => {
        const values = [...addFields];
        values.splice(index, 1);
        setAddFields(values);
    };

    const handleAutocompleteChange = (index, newValue) => {
        const values = [...addFields];
        values[index].metadataFieldId = newValue ? newValue.metadataFieldId : '';
        setAddFields(values);
    };

    const unsavedChangesRef = React.useRef({
        unsavedRows: {},
        rowsBeforeChange: {},
    });

    const columns = React.useMemo(() => {
        return [
            {
                field: 'metadataFieldId',
                headerName: 'Field ID',
                width: 180,
            },
            {
                field: 'metadataFieldName',
                headerName: 'Field',
                width: 240,
                editable: true,
                renderEditCell: (params) => (
                    <MetadataSelect {...params} />
                )
            },
            {
                field: 'textValue',
                headerName: 'Value',
                width: 420,
                editable: true
            },
            {
                field: 'textLang',
                headerName: 'Lang',
                width: 120,
                editable: true
            },
            {
                headerName: 'Actions',
                field: 'actions',
                type: 'actions',
                cellClassName: 'actions',
                getActions: ({ id, row }) => {
                    return [
                        <GridActionsCellItem
                            icon={<RestoreIcon />}
                            label="Discard changes"
                            disabled={unsavedChangesRef.current.unsavedRows[id] === undefined}
                            onClick={() => {
                                apiRef.current.updateRows([
                                    unsavedChangesRef.current.rowsBeforeChange[id],
                                ]);
                                delete unsavedChangesRef.current.rowsBeforeChange[id];
                                delete unsavedChangesRef.current.unsavedRows[id];
                                setHasUnsavedRows(
                                    Object.keys(unsavedChangesRef.current.unsavedRows).length > 0,
                                );
                            }}
                        />,
                        <GridActionsCellItem
                            icon={<DeleteIcon />}
                            label="Delete"
                            onClick={() => {
                                unsavedChangesRef.current.unsavedRows[id] = {
                                    ...row,
                                    _action: 'delete',
                                };
                                if (!unsavedChangesRef.current.rowsBeforeChange[id]) {
                                    unsavedChangesRef.current.rowsBeforeChange[id] = row;
                                }
                                setHasUnsavedRows(true);
                                apiRef.current.updateRows([row]); // to trigger row render
                            }}
                        />,
                    ];
                },
            },
        ];
    }, [unsavedChangesRef, apiRef]);

    const processRowUpdate = React.useCallback((newRow, oldRow) => {
        const rowId = newRow.metadataValueId;

        unsavedChangesRef.current.unsavedRows[rowId] = newRow;
        console.log(newRow)
        if (!unsavedChangesRef.current.rowsBeforeChange[rowId]) {
            unsavedChangesRef.current.rowsBeforeChange[rowId] = oldRow;
        }
        setHasUnsavedRows(true);
        return newRow;
    }, []);

    const discardChanges = React.useCallback(() => {
        setHasUnsavedRows(false);
        Object.values(unsavedChangesRef.current.rowsBeforeChange).forEach((row) => {
            apiRef.current.updateRows([row]);
        });
        unsavedChangesRef.current = {
            unsavedRows: {},
            rowsBeforeChange: {},
        };
    }, [apiRef]);

    const saveChanges = React.useCallback(async () => {
        try {
            const unsavedChangesMetadata = Object.values(unsavedChangesRef.current.unsavedRows);
            console.log('unsavedChangesMetadata:', unsavedChangesMetadata);

            const { deleteFields: newDeleteFields, updateFields: newUpdateFields } = unsavedChangesMetadata.reduce((acc, item) => {
                if (item._action !== undefined && item._action === 'delete') {
                    acc.deleteFields.push({ metadataValueId: item.metadataValueId });
                } else {
                    acc.updateFields.push({
                        metadataValueId: item.metadataValueId,
                        textValue: item.textValue,
                        textLang: item.textLang,
                        metadataFieldId: item.metadataFieldId,
                        itemId: itemId
                    });
                }
                return acc;
            }, { deleteFields: [], updateFields: [] });

            const modifiedFields = newUpdateFields.map(field => {
                if (field.metadataFieldId === 1 || field.metadataFieldId === 2) {
                    if (typeof field.textValue == 'string') {
                        return { ...field, textValue: `${window.location.origin}_${field.textValue}` };
                    }
                }
                return field;
            });

            const payload = {
                update: modifiedFields,
                delete: newDeleteFields
            };
            try {
                await axios.put(`${process.env.REACT_APP_BASE_URL}/api/Item/ModifyItem/${itemId}`, payload, {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: header
                    },
                    body: JSON.stringify(payload),
                }).then(function (response) {
                    // alert(response.data.message);
                    addNotification(response.data.message, 'success');
                    handleCloseMeta();
                }).catch(function (error) {
                    if (error.response) {
                        console.log(error.response)
                        // alert(error.response.data.message);
                        addNotification(error.response.data.message, 'error');
                    }
                });
            } catch (error) {
                console.error('There was an error adding the metadata:', error);
                // alert('Failed to add metadata');
                addNotification('Failed to add metadata', 'error');
            }

            // Persist updates in the database
            setIsSaving(true);
            await new Promise((resolve) => {
                setTimeout(resolve, 1000);
            });

            setIsSaving(false);
            const rowsToDelete = Object.values(
                unsavedChangesRef.current.unsavedRows,
            ).filter((row) => row._action === 'delete');
            if (rowsToDelete.length > 0) {
                rowsToDelete.forEach((row) => {
                    apiRef.current.updateRows([row]);
                });
            }

            setHasUnsavedRows(false);
            unsavedChangesRef.current = {
                unsavedRows: {},
                rowsBeforeChange: {},
            };
        } catch (error) {
            setIsSaving(false);
        }
    }, [apiRef]);

    const getRowClassName = React.useCallback(({ id }) => {
        const unsavedRow = unsavedChangesRef.current.unsavedRows[id];
        if (unsavedRow) {
            if (unsavedRow._action === 'delete') {
                return 'row--removed';
            }
            return 'row--edited';
        }
        return '';
    }, []);

    const StyledTableCell = styled(TableCell)(({ theme }) => ({
        [`&.${tableCellClasses.head}`]: {
            backgroundColor: theme.palette.common.black,
            color: theme.palette.common.white,
            padding: 13,
        },
        [`&.${tableCellClasses.body}`]: {
            fontSize: 14,
        },
    }));

    const StyledTableRow = styled(TableRow)(({ theme }) => ({
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
        // hide last border
        '&:last-child td, &:last-child th': {
            border: 0,
        },
    }));

    // function createData(fileName, creationTime, mimeType, actions) {
    //     return { fileName, creationTime, mimeType, actions };
    // }
    const SquareButton = styled(Button)(({ theme }) => ({
        marginLeft: 7,
        width: 30,
        height: 30,
        minWidth: 30,
        minHeight: 30,
    }));

    const [openDialogDiscoverable, setOpenDialogDiscoverable] = React.useState(false);
    const [openDialogDelete, setOpenDialogDelete] = React.useState(false);
    const [openDialogModify, setOpenDialogModify] = React.useState(false);
    const [openDialogAddItem, setOpenDialogAddItem] = React.useState(false);

    const handleClickDialogDiscoverable = (fileId) => {
        setFileModifyChoose(fileId)
        setOpenDialogDiscoverable(true);
    };

    const handleCloseDialogDiscoverable = () => {
        setOpenDialogDiscoverable(false);
    };

    const handleClickDialogDelete = (fileId) => {
        setFileModifyChoose(fileId)
        setOpenDialogDelete(true);
    };

    const handleCloseDialogDelete = () => {
        setOpenDialogDelete(false);
    };


    const handleClickDialogModify = (fileId, accessType, role) => {
        setFileModifyChoose(fileId)
        setAccessTypeSelect(accessType)
        setRoleFile(role)
        setOpenDialogModify(true);
    };

    const handleCloseDialogModify = () => {
        setOpenDialogModify(false);
        setFileModify([]);
    };

    const handleClickDialogAddItem = () => {
        setOpenDialogAddItem(true);
    };

    const handleCloseDialogAddItem = () => {
        setOpenDialogAddItem(false);
        setFileAdd([]);
    };

    const handleSendRequestAddNewFile = async () => {
        setIsLoading(true)
        var formdata = new FormData();
        formdata.append('role', roleFile);
        fileAdd.forEach(file => {
            formdata.append('multipleFiles', file);
        });
        try {
            const response = await axios.post(`${process.env.REACT_APP_BASE_URL}/api/FileUpload/AddNewFileToItem/${itemId}`, formdata, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                    Authorization: header
                },
            });
            console.log(response.data);
        } catch (error) {
            console.error('There was an error creating the item!', error);
        }finally {
            setIsLoading(false); 
            setOpenDialogAddItem(false);
            window.location.reload();
        }
    };

    const [fileModifyChoose, setFileModifyChoose] = React.useState(0);

    const [accessTypeSelect, setAccessTypeSelect] = React.useState(0);

    const handleAccessTypeChange = (event) => {
        setAccessTypeSelect(event.target.value);
    };

    const [roleFile, setRoleFile] = React.useState('ALL');

    const handleRoleChange = (event) => {
        setRoleFile(event.target.value);
    };

    const [fileModify, setFileModify] = React.useState([]);

    const onDrop = React.useCallback((acceptedFiles) => {
        setFileModify(acceptedFiles);
    }, []);

    const removeFile = (file) => () => {
        setFileModify((prevFiles) => prevFiles.filter((f) => f !== file));
    };

    const { getRootProps, getInputProps } = useDropzone({
        onDrop,
        accept: 'image/jpeg, image/png, text/*, application/*',
    });

    const handleMultipleFilesDrop = React.useCallback((acceptedFiles) => {
        setFileAdd((prevFiles) => [
            ...prevFiles,
            ...acceptedFiles.map((file) =>
                Object.assign(file, {
                    preview: file.type.startsWith('image/') ? URL.createObjectURL(file) : null,
                })
            ),
        ]);
    }, []);

    const { getRootProps: getMultipleRootProps, getInputProps: getMultipleInputProps } = useDropzone({
        onDrop: handleMultipleFilesDrop,
        accept: 'image/jpeg, image/png, text/*, application/*',
    });

    function getRelativePath(fullPath) {
        const splitPath = fullPath.split('public\\');
        return splitPath[1];
    }
    const [fileAdd, setFileAdd] = React.useState([]);

    const collection = items && items.collection ? items.collection : [];
    const community = items && items.collection ? items.collection.communityDTOForSelectOfUser : [];
    // const fileContains = items && items.listFilesDTOForSelect ? items.listFilesDTOForSelect : [];
    const advisorList = items && items.listAdvisorDtoForSelect ? items.listAdvisorDtoForSelect : [];
    const authorList = items && items.listAuthorDtoForSelect ? items.listAuthorDtoForSelect : [];
    const metadataTitle = items && items.listMetadataValueDTOForSelect ? items.listMetadataValueDTOForSelect.find((x) => x.metadataFieldId === 57)?.textValue : '';
    const fileToModify = fileContains.find(file => file.fileId === fileModifyChoose);
    const isDiscoverable = fileToModify ? fileToModify.isActive : false;
    return (
        <div>
            <Header />
            <Helmet>
                <meta charSet="utf-8" />
                <title>Modify Item - {`${metadataTitle}`} - dSPACE</title>
            </Helmet>
            <React.Fragment>
                <Dialog
                    fullScreen
                    open={openDialogAddItem}
                    onClose={handleCloseDialogAddItem}
                    TransitionComponent={Transition}
                >
                    <AppBar sx={{ position: 'relative' }}>
                        <Toolbar>
                            <IconButton
                                edge="start"
                                color="inherit"
                                onClick={handleCloseDialogAddItem}
                                aria-label="close"
                            >
                                <CloseIcon />
                            </IconButton>
                            <Typography sx={{ ml: 2, flex: 1 }} variant="h6" component="div">
                                Add New File
                            </Typography>
                            <Button autoFocus color="inherit" onClick={handleSendRequestAddNewFile}>
                                save
                            </Button>
                        </Toolbar>
                    </AppBar>
                    <div className="container mt-3">
                        <label className='d-flex align-items-center'>Choose the Access Role:
                            <FormControl sx={{ m: 1, minWidth: 120 }} size="small">
                                <InputLabel id="demo-select-small-label">Role</InputLabel>
                                <SelectMui
                                    labelId="demo-select-small-label"
                                    id="demo-select-small"
                                    value={roleFile}
                                    label="Role"
                                    onChange={handleRoleChange}
                                >
                                    <MenuItem value="ALL">
                                        <em>LECTURER & STUDENT</em>
                                    </MenuItem>
                                    <MenuItem value={'LECTURER'}>Only LECTURER</MenuItem>
                                    <MenuItem value={'STUDENT'}>Only STUDENT</MenuItem>
                                </SelectMui>
                            </FormControl>
                            <em><small>*The files that you add will grant readers permission to VIEW them. To change file access permissions, please edit them in the item editing section.</small></em>
                        </label>
                        <div
                            {...getMultipleRootProps({ className: 'dropzone' })}
                            className=" border-dashed d-flex flex-column align-items-center justify-content-center"
                            style={{ height: '300px', borderRadius: '8px', border: '2px dashed #dee2e6', backgroundColor: '#f8f9fa' }}
                        >
                            <input {...getMultipleInputProps()} />
                            <img class=" mb-3" style={{ width: '100px', height: '100px' }} src="../../oc-browse.svg" alt="Image Description" data-hs-theme-appearance="default" />
                            <p>Drag and drop your file here</p>
                            <button type="button" className="btn btn-primary mt-2">Browse files</button>
                        </div>
                        <div className="mt-4">
                            {fileAdd.map((file) => (
                                <div key={file.name} className="d-flex align-items-center mb-2" style={{ border: '1px solid #dee2e6', borderRadius: '8px', padding: '10px' }}>
                                    {file.preview ? (
                                        <img src={file.preview} alt={file.name} style={{ width: '50px', height: '50px', objectFit: 'cover', marginRight: '10px' }} />
                                    ) : (
                                        <div className="file-icon" style={{ width: '50px', height: '50px', display: 'flex', alignItems: 'center', justifyContent: 'center', marginRight: '10px' }}>
                                            <i className="bi bi-file-earmark-text" style={{ fontSize: '30px' }}></i>
                                        </div>
                                    )}
                                    <div className="flex-grow-1">
                                        <p className="mb-0">{file.name}</p>
                                        <p className="text-muted mb-0">{(file.size / 1024).toFixed(2)} KB - {file.type}</p>
                                    </div>
                                    <Button startIcon={<DisabledByDefaultIcon />} onClick={removeFile(file)}>
                                        <i className="bi bi-trash"></i>
                                    </Button>
                                </div>
                            ))}
                        </div>
                    </div>
                </Dialog>
            </React.Fragment>

            <React.Fragment>
                <Dialog
                    open={openDialogModify}
                    onClose={handleCloseDialogModify}
                    PaperProps={{
                        component: 'form',
                        onSubmit: async (event) => {
                            setIsLoading(true)
                            event.preventDefault();
                            const payload = new FormData();
                            payload.append('FileId', fileModifyChoose);
                            payload.append('AccessType', accessTypeSelect);
                            payload.append('Role', roleFile);
                            if (fileModify[0] !== undefined) {
                                payload.append('File', fileModify[0])
                            }
                            console.log(fileModify)
                            try {
                                await axios.put(`${process.env.REACT_APP_BASE_URL}/api/FileUpload/ModifyFileWithFileId`, payload, {
                                    headers: {
                                        'Content-Type': 'multipart/form-data',
                                        Authorization: header
                                    }
                                })
                                setFileContains((prevFileContains) =>
                                    prevFileContains.map((file) =>
                                        file.fileId === fileModifyChoose
                                            ? { ...file, fileAccess: [{ ...file.fileAccess[0], accessType: accessTypeSelect }] }
                                            : file
                                    )
                                );
                                handleCloseDialogModify();
                                addNotification("Update file successfully", 'success')
                            } catch (error) {
                                console.log('There was an error creating the item!', error);
                            } finally {
                                setIsLoading(false); 
                            }
                        },
                    }}
                >
                    <DialogTitle>Modify Item's file</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Select file, edit access permissions, and roles with those access permissions. <em>*Note: If you want to keep the current file and only edit access permissions; do not select a file.</em>
                        </DialogContentText>
                        <div className="container mt-3">
                            <div
                                {...getRootProps({ className: 'dropzone' })}
                                className=" border-dashed d-flex flex-column align-items-center justify-content-center"
                                style={{ height: '150px', borderRadius: '8px', border: '2px dashed #dee2e6', backgroundColor: '#f8f9fa' }}
                            >
                                <input {...getInputProps()} />
                                <p>Drag and drop your file here</p>
                                <button type="button" className="btn btn-primary mt-2">Browse files</button>
                            </div>
                            <div className="mt-4">
                                {fileModify.map((file) => (
                                    <div key={file.name} className="d-flex align-items-center mb-2" style={{ border: '1px solid #dee2e6', borderRadius: '8px', padding: '10px' }}>

                                        {file.preview ? (
                                            <img src={file.preview} alt={file.name} style={{ width: '50px', height: '50px', objectFit: 'cover', marginRight: '10px' }} />
                                        ) : (
                                            <div className="file-icon" style={{ width: '50px', height: '50px', display: 'flex', alignItems: 'center', justifyContent: 'center', marginRight: '10px' }}>
                                                <i className="bi bi-file-earmark-text" style={{ fontSize: '30px' }}></i>
                                            </div>
                                        )}
                                        <div className="flex-grow-1">
                                            <p className="mb-0">{file.name}</p>
                                            <p className="text-muted mb-0">{(file.size / 1024).toFixed(2)} KB - {file.type}</p>
                                        </div>
                                        <Button startIcon={<DisabledByDefaultIcon />} onClick={removeFile(file)}>
                                            <i className="bi bi-trash"></i>
                                        </Button>
                                    </div>
                                ))}

                            </div>
                            <div className='d-flex flex-row mt-2'>
                                <Box sx={{ minWidth: 150, marginLeft: 4, marginRight: 7 }}>
                                    <FormControl fullWidth>
                                        <InputLabel id="demo-simple-select-label">Access Type</InputLabel>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="demo-simple-select"
                                            value={accessTypeSelect}
                                            label="Access Type"
                                            onChange={handleAccessTypeChange}
                                        >
                                            <MenuItem value={0}>
                                                <em>View and download</em>
                                            </MenuItem>
                                            {/* <MenuItem value={1}>Only Download</MenuItem> */}
                                            <MenuItem value={2}>Only view</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Box>

                                <Box sx={{ minWidth: 120 }}>
                                    <FormControl fullWidth>
                                        <InputLabel id="demo-simple-select-label">Role</InputLabel>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="demo-simple-select"
                                            value={roleFile}
                                            label="Role"
                                            onChange={handleRoleChange}
                                        >
                                            <MenuItem value="ALL">
                                                <em>LECTURER & STUDENT</em>
                                            </MenuItem>
                                            <MenuItem value={'LECTURER'}>Only LECTURER</MenuItem>
                                            <MenuItem value={'STUDENT'}>Only STUDENT</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Box>
                            </div>
                        </div>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleCloseDialogModify}>Cancel</Button>
                        <Button type="submit">Save changes</Button>
                    </DialogActions>
                </Dialog>
            </React.Fragment>

            <React.Fragment>
                <Dialog
                    open={openDialogDiscoverable}
                    onClose={handleCloseDialogDiscoverable}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">
                        {"Notification"}
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            {isDiscoverable ?
                                "Make this file become undiscoverable?" :
                                "Make this file become discoverable?"}
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleCloseDialogDiscoverable}>Disagree</Button>
                        <Button onClick={handleSendRequestUndiscoverable} autoFocus>
                            Agree
                        </Button>
                    </DialogActions>
                </Dialog>
            </React.Fragment>

            <React.Fragment>
                <Dialog
                    open={openDialogDelete}
                    onClose={handleCloseDialogDelete}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">
                        {"Alert"}
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            Are you sure you want to DELETE this file?
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleCloseDialogDelete}>Disagree</Button>
                        <Button onClick={handleSendRequestDelete} autoFocus>
                            Agree
                        </Button>
                    </DialogActions>
                </Dialog>
            </React.Fragment>
            <div>
                {
                    popup ?
                        <div className="main">
                            <div className="popup">
                                <div className="popup-header mt-2 ms-2">
                                    <Typography
                                        variant="h5"
                                        component="h5"
                                        sx={{ textAlign: 'left', mt: 2, mb: 2 }}>
                                        Choose collection...
                                    </Typography>
                                    <Button className='ms-10' onClick={closePopup} startIcon={<Close />} >
                                    </Button>
                                </div>
                                <div className='mt-2 ms-4 me-4'>Select the collection you want to save the item to.</div>
                                <div className='d-flex justify-content-between mt-4 ms-4 me-4'>
                                    <div className='w-75'>
                                        <Autocomplete
                                            freeSolo
                                            id="free-solo-2-demo"
                                            disableClearable
                                            options={collections} // Sử dụng toàn bộ đối tượng collection thay vì chỉ collectionName
                                            getOptionDisabled={(option) => option.collectionId === items.collectionId} // Truy cập đúng collectionId từ đối tượng collection
                                            onInputChange={(event, newValue) => {
                                                const selected = collections.find(collect => collect.collectionName === newValue);
                                                setSelectedValue(selected);
                                            }}
                                            renderInput={(params) => (
                                                <TextField
                                                    {...params}
                                                    label="Collection..."
                                                    InputProps={{
                                                        ...params.InputProps,
                                                        type: 'search',
                                                    }}
                                                />
                                            )}
                                            getOptionLabel={(option) => option.collectionName}
                                        />
                                    </div>
                                    <Button variant="outlined" className='ms-10' onClick={handleSaveCollection} startIcon={<Save />} >
                                        Save
                                    </Button>
                                </div>
                            </div>
                        </div> : ""
                }
            </div>
            <Container>
                <Typography
                    variant="h4"
                    component="h4"
                    sx={{ textAlign: 'left', mt: 3 }}>
                    Edit Item: {metadataTitle}
                </Typography>
                <hr
                    style={{
                        color: 'black',
                        backgroundColor: 'black',
                        height: 1
                    }}
                />
                <Tabs defaultActiveKey="status">
                    <Tab eventKey="status" title="Status">
                        <div className='mt-1'>Welcome to the item management page. From here you can move or delete the item. You may also update or add new metadata on the other tabs.</div>
                        <Table className='mt-3'>
                            <tbody>
                                <tr>
                                    <td style={{ border: "none" }}>Item Internal ID</td>
                                    <td style={{ border: "none" }}>{items.itemId}</td>
                                </tr>
                                <tr>
                                    <td style={{ border: "none" }}>Last Modified:</td>
                                    <td style={{ border: "none" }}>{ConvertDateTime(items.lastModified)}</td>
                                </tr>
                                <tr>
                                    <td style={{ border: "none" }}>Item Page:</td>
                                    <td style={{ border: "none" }}><PageLink href={window.location.origin + `/DSpace/ItemDetail/` + items.itemId} text={`/DSpace/ItemDetail/` + items.itemId} /></td>
                                </tr>
                                <tr><td style={{ border: "none" }}></td></tr>
                                <tr>
                                    <td style={{ border: "none" }}>Change item status</td>

                                    {popup ?
                                        <div style={{ padding: '14px' }}>Hidden button</div> : (
                                            items.discoverable ? (
                                                <td style={{ border: "none" }}>
                                                    <Button variant="outlined" onClick={handleNonDiscoverableClick}>Make it non-discoverable...</Button>
                                                </td>
                                            ) : (
                                                <td style={{ border: "none" }}>
                                                    <Button variant="outlined" onClick={handleDiscoverableClick}>Make it discoverable...</Button>
                                                </td>
                                            )
                                        )
                                    }
                                </tr>
                                <tr>
                                    <td style={{ border: "none" }}>Move item to another collection</td>
                                    <td style={{ border: "none" }}>
                                        {popup ?
                                            <div style={{ padding: '6.5px' }}>Hidden button</div>
                                            : <Button variant="outlined" onClick={handleClickOpen}>Move this Item to a different Collection</Button>}
                                    </td>
                                </tr>
                                <tr>
                                    <td style={{ border: "none" }}>Completely expunge item</td>
                                    <td style={{ border: "none" }}>
                                        {popup ?
                                            <div style={{ padding: '6.5px' }}>Hidden button</div>
                                            : <Button variant="outlined" onClick={handleDeleteItem}>permanently delete</Button>}
                                    </td>
                                </tr>
                            </tbody>
                        </Table>

                        <Button variant="outlined" className="text-black mt-2 border-secondary mb-5" onClick={handleRedirectToDetail} startIcon={<ArrowBackIcon />} >
                            Back
                        </Button>
                    </Tab>
                    <Tab eventKey="metadata" title="Metadata">
                        <Box
                            sx={{
                                height: '100%',
                                width: '100%',
                                marginTop: 2,
                                '& .actions': {
                                    color: 'text.secondary',
                                },
                                '& .textPrimary': {
                                    color: 'text.primary',
                                },
                            }}
                        >
                            <div style={{ width: '100%' }}>

                                <Button variant="outlined" onClick={handleClickOpenMeta}>
                                    Add new Metadata
                                </Button>
                                <Dialog
                                    open={open}
                                    onClose={handleCloseMeta}
                                    PaperProps={{
                                        component: 'form',
                                        onSubmit: async (event) => {
                                            var resultAdd = null;
                                            event.preventDefault();
                                            const modifiedFields = addFields.map(field => {
                                                if (field.metadataFieldId === 1 || field.metadataFieldId === 2) {
                                                    if (typeof field.textValue == 'string') {
                                                        return { ...field, textValue: `${window.location.origin}_${field.textValue}` };
                                                    }
                                                }
                                                return field;
                                            });
                                            const payload = {
                                                add: modifiedFields,
                                            };
                                            resultAdd = await axios.put(`${process.env.REACT_APP_BASE_URL}/api/Item/ModifyItem/${itemId}`, payload, {
                                                method: 'PUT',
                                                headers: {
                                                    'Content-Type': 'application/json',
                                                    Authorization: header
                                                },
                                                body: JSON.stringify(payload),
                                            }).then(function (response) {
                                                // alert(response.data.message);
                                                // window.location.reload();
                                                addNotification(response.data.message, 'success');
                                                handleCloseMeta();
                                                setTimeout(() => {
                                                    window.location.reload();
                                                }, 2000);
                                            }).catch(function (error) {
                                                if (error.response) {
                                                    console.log(error.response)
                                                    // alert(error.response.data.message);
                                                    addNotification(error.response.data.message, 'error');
                                                }
                                            });
                                        },
                                    }}
                                >
                                    <DialogTitle>Add new metadata</DialogTitle>
                                    <DialogContent>
                                        <DialogContentText>
                                            Input the necessary information to create the item's metadata information.
                                        </DialogContentText>
                                        {addFields.map((field, index) => (
                                            <div key={index}>
                                                <Autocomplete
                                                    options={MetadataField}
                                                    getOptionLabel={(option) => option.metadataFieldName}
                                                    onChange={(event, newValue) => handleAutocompleteChange(index, newValue)}
                                                    renderInput={(params) => (
                                                        <TextField
                                                            {...params}
                                                            required
                                                            margin="dense"
                                                            label="Metadata Field"
                                                            fullWidth
                                                            variant="standard"
                                                        />
                                                    )}
                                                />
                                                <TextField
                                                    autoFocus
                                                    required
                                                    margin="dense"
                                                    name="textValue"
                                                    label="Text Value"
                                                    type="text"
                                                    sx={{ width: 300, mr: 3 }}
                                                    variant="standard"
                                                    value={field.textValue}
                                                    onChange={(event) => handleChangeInputAdd(index, event)}
                                                />
                                                <TextField
                                                    margin="dense"
                                                    name="textLang"
                                                    label="Text Language"
                                                    type="text"
                                                    variant="standard"
                                                    value={field.textLang}
                                                    onChange={(event) => handleChangeInputAdd(index, event)}
                                                />
                                                <br />
                                                <Button onClick={() => handleRemoveFields(index)}>Remove</Button>
                                            </div>
                                        ))}
                                        <Button onClick={handleAddFields}>Add More</Button>
                                    </DialogContent>
                                    <DialogActions>
                                        <Button onClick={handleCloseMeta}>Cancel</Button>
                                        <Button type="submit">Add Metadata</Button>
                                    </DialogActions>

                                </Dialog>
                                <div className='mt-3'>
                                    <DataGrid
                                        rows={modifyMetadata}
                                        columns={columns}
                                        apiRef={apiRef}
                                        disableRowSelectionOnClick
                                        processRowUpdate={processRowUpdate}
                                        getRowId={(row) => row.metadataValueId}
                                        ignoreValueFormatterDuringExport
                                        sx={{
                                            [`& .${gridClasses.row}.row--edited`]: {
                                                backgroundColor: (theme) => {
                                                    if (theme.palette.mode === 'light') {
                                                        return 'rgba(255, 254, 176, 0.3)';
                                                    }
                                                    return darken('rgba(255, 254, 176, 1)', 0.6);
                                                },
                                            },
                                            [`& .${gridClasses.row}.row--removed`]: {
                                                backgroundColor: (theme) => {
                                                    if (theme.palette.mode === 'light') {
                                                        return 'rgba(255, 170, 170, 0.3)';
                                                    }
                                                    return darken('rgba(255, 170, 170, 1)', 0.7);
                                                },
                                            },
                                        }}
                                        loading={isSaving}
                                        getRowClassName={getRowClassName}
                                        columnVisibilityModel={{
                                            metadataFieldId: false
                                        }}
                                        initialState={{
                                            pagination: {
                                                paginationModel: { pageSize: 10, page: 0 },
                                            },
                                        }}
                                        pageSizeOptions={[10, 15, 25]}
                                    />
                                </div>
                            </div>
                        </Box>
                        <div className='d-flex flex-column align-items-end'>
                            <div className='mb-1 mt-2' >
                                <LoadingButton
                                    disabled={!hasUnsavedRows}
                                    loading={isSaving}
                                    onClick={saveChanges}
                                    startIcon={<SaveIcon />}
                                    loadingPosition="start"
                                >
                                    <span>Save</span>
                                </LoadingButton>
                                <Button
                                    disabled={!hasUnsavedRows || isSaving}
                                    onClick={discardChanges}
                                    startIcon={<RestoreIcon />}
                                >
                                    Discard all changes
                                </Button>
                            </div>
                            <Button variant="outlined" className="text-black border-secondary mb-5" onClick={handleRedirectToDetail} startIcon={<ArrowBackIcon />} >
                                Back
                            </Button>
                        </div>

                    </Tab>

                    <Tab eventKey="bitstreams" title="Bitstreams">
                        <Button variant="contained" startIcon={<UploadFileIcon />} color="success" className='mt-4 mb-2' onClick={handleClickDialogAddItem}>Upload</Button>
                        <TableContainer component={Paper}>
                            <Table sx={{ minWidth: 700 }} aria-label="customized table">
                                <TableHead>
                                    <TableRow>
                                        <StyledTableCell>Name</StyledTableCell>
                                        <StyledTableCell align="center">Creation Time</StyledTableCell>
                                        <StyledTableCell align="center">Mime Type</StyledTableCell>
                                        <StyledTableCell align="center">Actions</StyledTableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody  >
                                    {fileContains.map((row) => (
                                        <StyledTableRow key={row.fileId}>
                                            <StyledTableCell component="th" scope="row">
                                                {row.fileName}
                                            </StyledTableCell>
                                            <StyledTableCell align="center">{ConvertDateTime(row.creationTime)}</StyledTableCell>
                                            <StyledTableCell align="center">{row.mimeType}</StyledTableCell>
                                            <StyledTableCell align="center">
                                                <SquareButton className='border border-secondary text-dark' onClick={() => handleClickDialogModify(row.fileId, row.fileAccess[0].accessType, row.fileAccess[0].role)}>
                                                    <ModeIcon />
                                                </SquareButton>
                                                {!row.isActive ?
                                                    (<SquareButton color='primary' className='border border-primary' onClick={() => handleClickDialogDiscoverable(row.fileId)}>
                                                        <VisibilityOffIcon />
                                                    </SquareButton>) :
                                                    (<SquareButton color='primary' className='border border-primary' onClick={() => handleClickDialogDiscoverable(row.fileId)}>
                                                        <VisibilityIcon />
                                                    </SquareButton>)
                                                }

                                                <SquareButton color='error' className='border border-danger' onClick={() => handleClickDialogDelete(row.fileId)} >
                                                    <DeleteForeverIcon />
                                                </SquareButton>
                                            </StyledTableCell>
                                        </StyledTableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </Tab>

                    <Tab eventKey="relationships" title="Relationships">
                        <div className='mt-2 mb-2'>
                            <Accordion defaultExpanded sx={{ width: '45%' }}>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1-content"
                                    id="panel1-header"
                                >
                                    <Typography
                                        variant="h5"
                                        component="h5"
                                        sx={{ textAlign: 'left' }}>
                                        Advisors
                                    </Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <List sx={{ width: '90%', maxWidth: '100%', bgcolor: 'background.paper' }}>
                                        {advisorList.length > 0 ? (
                                            advisorList.map((author, index) => (
                                                <div className='border border-dashed border-1 mb-2'>
                                                    <React.Fragment key={author.authorId}>
                                                        <ListItem alignItems="flex-start" sx={{ margin: 0 }}>
                                                            <ListItemButton component={Link} to={`/Dspace/Author/AuthorDetail/${author.authorId}`}>
                                                                <ListItemAvatar>
                                                                    <Avatar alt="Author" src="/user.png" />
                                                                </ListItemAvatar>
                                                                <ListItemText
                                                                    primary={
                                                                        <Typography variant="h6" component="div">
                                                                            {author.authorId} - {author.fullName}
                                                                        </Typography>
                                                                    }
                                                                    secondary={author.jobTitle}
                                                                />
                                                            </ListItemButton>
                                                        </ListItem>
                                                        <Divider component="li" />
                                                    </React.Fragment>
                                                </div>

                                            ))
                                        ) : (
                                            <ListItem>
                                                <ListItemText
                                                    primary="No authors available"
                                                    primaryTypographyProps={{ align: 'center' }}
                                                />
                                            </ListItem>
                                        )}
                                    </List>
                                </AccordionDetails>
                            </Accordion>
                        </div>
                        <div className='mt-2 mb-2'>
                            <Accordion defaultExpanded sx={{ width: '45%' }}>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1-content"
                                    id="panel1-header"
                                >
                                    <Typography
                                        variant="h5"
                                        component="h5"
                                        sx={{ textAlign: 'left' }}>
                                        Authors
                                    </Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <List sx={{ width: '90%', maxWidth: '100%', bgcolor: 'background.paper' }}>
                                        {authorList.length > 0 ? (
                                            authorList.map((author, index) => (
                                                <div className='border border-dashed border-1 mb-2'>
                                                    <React.Fragment key={author.authorId}>
                                                        <ListItem alignItems="flex-start" sx={{ margin: 0 }}>
                                                            <ListItemButton component={Link} to={`/Dspace/Author/AuthorDetail/${author.authorId}`}>
                                                                <ListItemAvatar>
                                                                    <Avatar alt="Author" src="/user.png" />
                                                                </ListItemAvatar>
                                                                <ListItemText
                                                                    primary={
                                                                        <Typography variant="h6" component="div">
                                                                            {author.authorId} - {author.fullName}
                                                                        </Typography>
                                                                    }
                                                                    secondary={author.jobTitle}
                                                                />
                                                            </ListItemButton>
                                                        </ListItem>
                                                        <Divider component="li" />
                                                    </React.Fragment>
                                                </div>

                                            ))
                                        ) : (
                                            <ListItem>
                                                <ListItemText
                                                    primary="No authors available"
                                                    primaryTypographyProps={{ align: 'center' }}
                                                />
                                            </ListItem>
                                        )}
                                    </List>
                                </AccordionDetails>
                            </Accordion>
                        </div>
                        <div>
                            <Typography
                                variant="h5"
                                component="h5"
                                sx={{ textAlign: 'left', mt: 3, mb: 2 }}>
                                Related Collection
                            </Typography>
                            <Card sx={{ display: 'flex', alignItems: 'center', padding: 2, width: '45%' }}>
                                {
                                    collection.logoUrl ? (
                                        <img src={`/${getRelativePath(collection.logoUrl)}`} alt="Description of image" style={{ width: '180px', height: 'auto' }} />
                                    ) : (
                                        <img src="https://cdn1.polaris.com/globalassets/pga/accessories/my20-orv-images/no_image_available6.jpg" alt="Description of image" style={{ width: '180px', height: 'auto' }} />
                                    )
                                }
                                <CardContent sx={{ display: 'flex', flexDirection: 'column' }}>
                                    <Chip label="Collection" color="primary" sx={{ marginBottom: 1 }} />
                                    <Typography variant="h6" color="primary" style={{ wordWrap: "break-word" }}>
                                        <Link href={window.location.origin + `/Dspace/Collection/CollectionDetail/` + collection.collectionId} underline='hover'>
                                            {collection.collectionName}
                                        </Link>
                                    </Typography>
                                    <Typography variant="body2" color="textSecondary">
                                        {collection.shortDescription}
                                    </Typography>
                                </CardContent>
                            </Card>
                        </div>
                        <div>
                            <Typography
                                variant="h5"
                                component="h5"
                                sx={{ textAlign: 'left', mt: 3, mb: 2 }}>
                                Related Community
                            </Typography>
                            <Card sx={{ display: 'flex', alignItems: 'center', padding: 2, width: '45%' }}>
                                {
                                    community?.logoUrl ? (
                                        <img src={`/${getRelativePath(community?.logoUrl)}`} alt="Description of image" style={{ width: '180px', height: 'auto' }} />
                                    ) : (
                                        <img src="https://cdn1.polaris.com/globalassets/pga/accessories/my20-orv-images/no_image_available6.jpg" alt="Description of image" style={{ width: '180px', height: 'auto' }} />
                                    )
                                }
                                <CardContent sx={{ display: 'flex', flexDirection: 'column' }}>
                                    <Chip label="Community" color="primary" sx={{ marginBottom: 1 }} />
                                    <Typography variant="h6" color="primary" style={{ wordWrap: "break-word" }}>
                                        <Link href={window.location.origin + `/Dspace/Community/CommunityDetail/` + community?.communityId} underline='hover'>
                                            {community?.communityName}
                                        </Link>
                                    </Typography>
                                    <Typography variant="body2" color="textSecondary">
                                        {community?.shortDescription}
                                    </Typography>
                                </CardContent>
                            </Card>
                        </div>


                    </Tab>
                </Tabs>
            </Container>
            {isLoading && <LoadingOverlay />}
        </div>

    );
}