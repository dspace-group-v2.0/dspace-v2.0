import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useParams, Link, useNavigate, useLocation } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from '../../components/Header/Header';
import { VerticalAlignBottom } from '@material-ui/icons';
import { saveAs } from 'file-saver';
import Counter from '../../components/Statistics/counterView';
import AssessmentIcon from '@mui/icons-material/Assessment';
import { Modal, Button, Form } from "react-bootstrap";
import { Box, Button as CustomButton, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, IconButton, Backdrop, Paper } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import { Helmet } from 'react-helmet';
import { jwtDecode } from 'jwt-decode';

function b64toBlob(b64Data, sliceSize) {
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays);
    return blob;
}
const ItemDetails = () => {
    const { itemId } = useParams(); // Get the ID from the URL
    const [item, setItem] = useState(null);
    const [loading, setLoading] = useState(true);
    const navigate = useNavigate();
    const location = useLocation();
    const searchParams = new URLSearchParams(location.search);
    const colId = searchParams.get('colId');
    const token = localStorage.getItem('Token');
    const header = `Bearer ${token}`;
    const userRole = token ? jwtDecode(token)["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"] : null;
    const [fileOpen, setFileOpen] = useState(false);
    const [fileId, setFileId] = useState('');

    const handleFileOpen = (fileKeyId) => {
        setFileId(fileKeyId);
        setFileOpen(true);
    };

    const handleFileClose = () => {
        setFileOpen(false);
        setFileId('');
    };

    useEffect(() => {
        async function getItemDetail() {
            if (userRole === "ADMIN") {
                await axios.get(`${process.env.REACT_APP_BASE_URL}/api/Item/GetItemSimpleById/${itemId}`) // Replace with your actual API endpoint
                    .then(response => {
                        if(response.status === 204){
                            setLoading(true)
                        }else{
                            const fetchedItem = response.data;
                            setItem(fetchedItem);
                            setLoading(false);
                        }
                    })
                    .catch(error => {
                        console.error('There was an error fetching the community!', error);
                        setLoading(false);
                    });
            } else if (userRole === "STAFF") {
                await axios.get(`${process.env.REACT_APP_BASE_URL}/api/ItemStaff/GetItemSimpleById/${itemId}`, {
                    headers: {
                        Authorization: header
                    }
                })
                    .then(response => {
                        if(response.status === 204){
                            setLoading(true)
                        }else{
                            const fetchedItem = response.data;
                            setItem(fetchedItem);
                            setLoading(false);
                        }
                    })
                    .catch(error => {
                        console.error('There was an error fetching the community!', error);
                        setLoading(false);
                    });
            } else {
                await axios.get(`${process.env.REACT_APP_BASE_URL}/api/ItemUser/GetItemSimpleById/${itemId}`, {
                    headers: {
                        Authorization: header
                    }
                })
                    .then(response => {
                        if(response.status === 204){
                            setLoading(true)
                        }else{
                            const fetchedItem = response.data;
                            setItem(fetchedItem);
                            setLoading(false);
                        }
                    })
                    .catch(error => {
                        console.error('There was an error fetching the community!', error);
                        setLoading(false);
                    });
            }

        }
        // Fetch the community data from your API using the id from the URL
        getItemDetail();
    }, [itemId]);
    const handleClickFile = (id) => {
        // const fileUrl = item.file[0].fileUrl; // Lấy URL của file PDF
        navigate(`/DSpace/PdfViewer/${id}`);
    };

    const [open, setOpen] = React.useState(false);
    const [fileLocationDownloaded, setfileLocationDownloaded] = React.useState('');

    const handleClose = () => {
        setOpen(false);
    };
    const handleDownloadFile = (fileKeyId, fileName) => {
        axios.get(`${process.env.REACT_APP_BASE_URL}/api/FileUpload/downloadFile/${fileKeyId}`)
            .then(response => {
                saveAs(b64toBlob(response.data), fileName)
                setOpen(true);
                setfileLocationDownloaded(response.data)
            })
            .catch(error => {
                console.error('There was an error while download file!', error);
            });

    };

    const handleAuthorClick = (id) => {
        // Xử lý sự kiện khi tác giả được click
        navigate(`/Dspace/Author/AuthorDetail/${id}`)
    };
    const formatDate = (dateString) => {
        if (!dateString) return '';

        const parts = dateString.split('-');
        let formattedDate = '';

        if (parts.length === 3) {
            // Case: full date (YYYY-MM-DD)
            const [year, month, day] = parts;
            formattedDate = `${day.padStart(2, '0')}-${month.padStart(2, '0')}-${year}`;
        } else if (parts.length === 2) {
            // Case: year and month (YYYY-MM)
            const [year, month] = parts;
            formattedDate = `${month.padStart(2, '0')}-${year}`;
        } else if (parts.length === 1) {
            // Case: only year (YYYY)
            const [year] = parts;
            formattedDate = year;
        }

        return formattedDate;
    };
    function Status({ status }) {
        const getStatusText = (status) => {
            if (status == true) {
                return "Published";
            } else {
                return "Undiscoverable";
            }
        };

        const getStatusColor = (status) => {
            if (status == true) {
                return "#28A745";
            } else {
                return "#DC3545";
            }
        };
        const getBoxShadow = (status) => {
            const color = getStatusColor(status);
            return `0 0 10px 2px ${color}`;
        };
        return (
            <>
                <p style={{ margin: '0' }}>
                    <span style={{
                        margin: '0',
                        backgroundColor: getStatusColor(status),
                        borderRadius: '3px',
                        color: 'white',
                        padding: '2px',
                        fontSize: 'medium',
                        boxShadow: getBoxShadow(status)

                    }}>
                        {getStatusText(status)}
                    </span>
                </p>
                {status === 2 && (
                    <div className="alert alert-danger" role="alert" style={{ whiteSpace: 'pre-wrap', wordWrap: 'break-word' }}>
                        {item.message}
                    </div>
                )}

            </>
        );
    }

    if (loading) {
        return (
            <div>
                <Header />
                <div className="container mt-5 d-flex justify-content-center align-items-center" style={{ height: '60vh' }}>
                    <div class="load-4">
                        {/* <p>Loading 4</p> */}
                        <div class="ring-1"></div>
                    </div>
                </div>
            </div>
        );
    }

    const handleRedirectStats = () => {
        navigate(`/Dspace/StatisticDetail?itemId=${itemId}`);
    };



    return (
        <div>
            <Header />
            <Helmet>
                <meta charSet="utf-8" />
                <title>{`${item?.title}`} - dSPACE</title>
            </Helmet>
            <Counter communityId={item.communityId} collectionId={item.collectionId} itemId={item.itemId} />

            {fileOpen && (
                <IconButton
                    onClick={handleFileClose}
                    sx={{
                        position: 'absolute',
                        top: 8,
                        right: 8,
                        zIndex: 3,

                    }}
                >
                    <CloseIcon variant="outlined" color='error' />
                </IconButton>
            )}

            {fileOpen && (
                <>
                    <Backdrop
                        sx={{
                            zIndex: 1,
                            color: '#fff',
                            backdropFilter: 'blur(10px)', // Blur effect
                            backgroundColor: 'rgba(0, 0, 0, 0.5)', // Optional dark overlay
                        }}
                        open={true}
                    />

                    <Paper
                        sx={{
                            position: 'absolute',
                            zIndex: 2,
                            top: '50%',
                            left: '50%',
                            transform: 'translate(-50%, -50%)',
                            boxShadow: 3,
                            textAlign: 'center',
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                            width: '43.5%', // 50% of the screen width
                            height: '100%', // Full height
                        }}
                    >

                        <iframe
                            src={`https://drive.google.com/file/d/${fileId}/preview`}
                            width="100%"
                            height="100%"
                            allow="autoplay"
                            title="File Preview"
                            style={{ border: 'none', borderRadius: '8px' }}
                        ></iframe>
                    </Paper>
                </>
            )}

            <React.Fragment>
                <Dialog
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">
                        {"Notification"}
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            Download successful! You can close this dialog now.
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <CustomButton onClick={handleClose}>Cancel</CustomButton>
                    </DialogActions>
                </Dialog>
            </React.Fragment>

            <div className="container mt-5">
                <h1 className="mb-5 text-center">{item.title}
                    <CustomButton
                        onClick={handleRedirectStats}
                        className='ms-2'
                        startIcon={<AssessmentIcon />}
                        sx={{
                            color: 'primary',
                            border: 'none',
                            boxShadow: 'none',
                            '&:hover': {
                                border: 'none',
                                boxShadow: 'none',
                            }
                        }}
                    >
                        Statistics
                    </CustomButton>
                    {(userRole === "ADMIN" || userRole === "STAFF") && (
                        <Status status={item.discoverable} />
                    )}</h1>

                <div className="row justify-content-center">
                    <div className="col-md-11">
                        <div className="row">
                            <div className="col-md-5">
                                <div>
                                    <label style={{ marginRight: '10px', fontSize: '1.4rem', marginBottom: '3px' }}>Contributors</label>
                                    <div className="form-group mb-3 d-flex flex-column">
                                        {item.authorItems.map((authorItem, index) => (
                                            <div key={index} className="author-container">
                                                <span
                                                    onClick={() => handleAuthorClick(authorItem.authorId)}
                                                    className="author-item"
                                                >
                                                    {authorItem.fullName}
                                                </span>
                                            </div>
                                        ))}
                                    </div>
                                </div>
                                <div>
                                    <label style={{ marginRight: '10px', fontSize: '1.4rem', marginBottom: '3px' }}>Date Of Issue</label>
                                    <div className="form-group mb-3 d-flex flex-column">
                                        {formatDate(item.dateOfIssue)}
                                    </div>
                                </div>
                                <div>
                                    <label style={{ marginRight: '10px', fontSize: '1.4rem', marginBottom: '3px' }}>Publisher</label>
                                    <div className="form-group mb-3 d-flex flex-column">
                                        {item.publisher}
                                    </div>
                                </div>
                                <div>
                                    <label style={{ marginRight: '10px', fontSize: '1.4rem', marginBottom: '3px' }}>Keywords</label>
                                    <div className="form-group mb-3 d-flex flex-column">
                                        {item.subjectKeywords.map((keywordItem, index) => (
                                            <div key={index} className="author-container">

                                                {keywordItem}
                                            </div>
                                        ))}
                                    </div>
                                </div>
                                <div>
                                    <label style={{ marginRight: '10px', fontSize: '1.4rem', marginBottom: '3px' }}>Collection</label>
                                    <div className="form-group mb-3 d-flex flex-column">
                                        {item.collectionName}
                                    </div>
                                </div>

                                <div>
                                    <div className="mt-2">

                                        {(userRole === "ADMIN" || (userRole === "STAFF" && item.canEdit)) ? (
                                            <>
                                                <button type="button" style={{ marginRight: '10px' }} onClick={() => navigate(`/DSpace/ModifyItem/${itemId}`)} class="btn btn-primary">Edit</button>
                                            </>
                                        ) :
                                            <button
                                                type="button"
                                                style={{ marginRight: '10px' }}
                                                onClick={() => navigate(`/Dspace/Metadata/MetadataDetail/${itemId}`)}
                                                className="btn btn-info"
                                            >
                                                Show full item
                                            </button>
                                        }

                                        {colId ? (
                                            <button
                                                type="button"
                                                style={{ marginRight: '10px' }}
                                                onClick={() => navigate(`/Dspace/Collection/CollectionDetail/${colId}`)}
                                                className="btn btn-secondary"
                                            >
                                                Back to List
                                            </button>
                                        ) : ((userRole === "ADMIN") ? (
                                            <button
                                                type="button"
                                                style={{ marginRight: '10px' }}
                                                onClick={() => navigate(`/Dspace/Collection/ItemList/-1`)}
                                                className="btn btn-secondary"
                                            >
                                                Back to List
                                            </button>
                                        ) : (userRole === "STAFF") ? (
                                            <button
                                                type="button"
                                                style={{ marginRight: '10px' }}
                                                onClick={() => navigate(`/Dspace/Collection/ItemStaffList/-1`)}
                                                className="btn btn-secondary"
                                            >
                                                Back to List
                                            </button>
                                        ) : (userRole === "STUDENT" || userRole === "LECTURER") ? (
                                            <button
                                                type="button"
                                                style={{ marginRight: '10px' }}
                                                onClick={() => navigate(`/Dspace/Collection/ItemListReader/-1`)}
                                                className="btn btn-secondary"
                                            >
                                                Back to List
                                            </button>
                                        ) : (
                                            <button
                                                type="button"
                                                style={{ marginRight: '10px' }}
                                                onClick={() => navigate(`/Login`)}
                                                className="btn btn-secondary"
                                            >
                                                Back to Login
                                            </button>
                                        ))}
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-7">
                                <div>
                                    <label style={{ marginRight: '10px', fontSize: '1.4rem', marginBottom: '3px' }}>Description</label>
                                    <div className="form-group mb-3 d-flex flex-column">
                                        {item.description}
                                    </div>
                                </div>
                                <div className="form-group mb-3">
                                    <div className="custom-file-uploadx">
                                        <div>
                                            <div>Files:</div>
                                            <div>
                                                {item?.file && item.file.length > 0 ? (
                                                    <div>
                                                        {item.file.map((file, index) => {
                                                            const accessType = file.fileAccess[0]?.accessType;
                                                            const role = file.fileAccess[0]?.role;
                                                            console.log(accessType + ' ' + role);
                                                            const canView = (accessType === 0 || accessType === 2) &&
                                                                (role === 'ALL' || role === userRole);

                                                            const canDownload = (accessType === 0 || accessType === 1) &&
                                                                (role === 'ALL' || role === userRole);

                                                            return (
                                                                <div className="file-item" key={index} style={{ display: 'flex', alignItems: 'center', marginBottom: '10px' }}>
                                                                    {
                                                                        ((userRole === "ADMIN" || userRole === "STAFF") && (file.mimeType === "application/zip" || file.mimeType === "application/x-rar")) && (
                                                                            <div
                                                                                className="fileName"
                                                                                style={{ cursor: 'pointer', color: '#000000', marginRight: '10px', flexGrow: 1 }}
                                                                            >
                                                                                {file.fileName}
                                                                            </div>

                                                                        )
                                                                    }
                                                                    {
                                                                        ((userRole === "ADMIN" || userRole === "STAFF") && (file.mimeType !== "application/zip" && file.mimeType !== "application/x-rar")) && (
                                                                            <div
                                                                                className="fileName"
                                                                                onClick={() => handleFileOpen(file.fileKeyId)}
                                                                                style={{ cursor: 'pointer', color: '#207698', marginRight: '10px', flexGrow: 1 }}
                                                                            >
                                                                                {file.fileName}
                                                                            </div>

                                                                        )
                                                                    }
                                                                    {
                                                                        (userRole === "ADMIN" || userRole === "STAFF") && (
                                                                            <VerticalAlignBottom className="download-icon" onClick={() => handleDownloadFile(file.fileKeyId, file.fileName)} style={{ cursor: 'pointer' }} />
                                                                        )
                                                                    }
                                                                    {(canView && (file.mimeType === "application/zip" || file.mimeType === "application/x-rar") && file.isActive === true) && (
                                                                        <div
                                                                            className="fileName"
                                                                            style={{ cursor: 'pointer', color: '#000000', marginRight: '10px', flexGrow: 1 }}
                                                                        >
                                                                            {file.fileName}
                                                                        </div>
                                                                    )}

                                                                    {(canView && (file.mimeType !== "application/zip" && file.mimeType !== "application/x-rar") && file.isActive === true) && (
                                                                        <div
                                                                            className="fileName"
                                                                            onClick={() => handleFileOpen(file.fileKeyId)}
                                                                            style={{ cursor: 'pointer', color: '#207698', marginRight: '10px', flexGrow: 1 }}
                                                                        >
                                                                            {file.fileName}
                                                                        </div>
                                                                    )}

                                                                    {(canDownload && file.isActive === true) && (
                                                                        <VerticalAlignBottom className="download-icon" onClick={() => handleDownloadFile(file.fileKeyId, file.fileName)} style={{ cursor: 'pointer' }} />
                                                                    )}

                                                                </div>
                                                            )
                                                        })}
                                                    </div>
                                                ) : (
                                                    <p>No file available</p>
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style={{ marginTop: '200px' }}></div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ItemDetails;
