import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from '../../components/Header/Header';
import { Helmet } from 'react-helmet';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import Divider from '@mui/material/Divider';
import Typography from '@mui/material/Typography';

const ListUser = () => {
    const [users, setUsers] = useState([]);
    const [searchQuery, setSearchQuery] = useState('');
    const [searchType, setSearchType] = useState('');
    const [error, setError] = useState(null);
    const [currentPage, setCurrentPage] = useState(1);
    const itemsPerPage = 12;
    const token = localStorage.getItem('Token');
    const header = `Bearer ${token}`;

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await axios.get(`${process.env.REACT_APP_BASE_URL}/api/People/getListOfPeople`, {
                    headers: { Authorization: header }
                });
                setUsers(response.data);
            } catch (error) {
                setError(error.message);
                console.error("Error fetching data:", error);
            }
        };

        fetchData();
    }, []);

    const handleSearchChange = (e) => {
        setSearchQuery(e.target.value);
    };

    const handleSearchTypeChange = (e) => {
        setSearchType(e.target.value);
        setSearchQuery('');
    };

    const filteredUsers = users.filter(user => {
        if (!searchQuery.trim()) return true;

        const normalizedSearchQuery = searchQuery.replace(/\s+/g, '').toLowerCase();
        switch (searchType) {
            case 'fullName':
                return (
                    (user.firstName && user.firstName.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery)) ||
                    (user.lastName && user.lastName.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery))
                );
            case 'address':
                return (user.address && user.address.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery));
            case 'phoneNumber':
                return (user.phoneNumber && user.phoneNumber.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery));
            case 'email':
                return (user.email && user.email.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery));
            default:
                return (
                    (user.firstName && user.firstName.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery)) ||
                    (user.lastName && user.lastName.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery)) ||
                    (user.address && user.address.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery)) ||
                    (user.phoneNumber && user.phoneNumber.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery)) ||
                    (user.email && user.email.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery))
                );
        }
    });

    const indexOfLastItem = currentPage * itemsPerPage;
    const indexOfFirstItem = indexOfLastItem - itemsPerPage;
    const currentUsers = filteredUsers.slice(indexOfFirstItem, indexOfLastItem);
    const totalPages = Math.ceil(filteredUsers.length / itemsPerPage);

    const handlePageChange = (pageNumber) => {
        setCurrentPage(pageNumber);
    };

    const handleNextPage = () => {
        if (currentPage < totalPages) {
            setCurrentPage(prevPage => prevPage + 1);
        }
    };

    const handlePreviousPage = () => {
        if (currentPage > 1) {
            setCurrentPage(prevPage => prevPage - 1);
        }
    };

    return (
        <div>
            <Header />
            <Helmet>
                <meta charSet="utf-8" />
                <title>List of Users - dSPACE</title>
            </Helmet>
            <div className="container mt-4 d-flex flex-column align-self-end">
                <div className="d-flex align-self-start">
                    <h1 className="mb-4">Users List</h1>
                </div>
                <div className="d-flex flex-row align-items-end justify-content-between mb-4 w-100">
                    <div className="w-50">
                        <label htmlFor="searchInput" className="form-label fw-bold">Search</label>
                        <input
                            id="searchInput"
                            type="text"
                            className="form-control"
                            placeholder="Search"
                            value={searchQuery}
                            onChange={handleSearchChange}
                        />
                    </div>
                    <div className="w-25">
                        <label htmlFor="searchTypeSelect" className="form-label fw-bold">Search By</label>
                        <select
                            id="searchTypeSelect"
                            className="form-select"
                            value={searchType}
                            onChange={handleSearchTypeChange}
                        >
                            <option value="">All</option>
                            <option value="fullName">Full Name</option>
                            <option value="address">Address</option>
                            <option value="phoneNumber">Phone Number</option>
                            <option value="email">Email</option>
                        </select>
                    </div>

                    <Link to="/Dspace/User/CreateUser" className="btn btn-primary">Create new User</Link>
                </div>
                {error && <div className="alert alert-danger">Error fetching data: {error}</div>}
                <List sx={{ width: '100%', maxWidth: '100%', bgcolor: 'background.paper' }}>
                    {currentUsers.length > 0 ? (
                        currentUsers.map((user, index) => (
                            <React.Fragment key={user.peopleId}>
                                <ListItem alignItems="flex-start" sx={{ margin: 0 }}>
                                    <ListItemButton component={Link} to={`/Dspace/User/UserDetail/${user.peopleId}`}>
                                        <ListItemAvatar>
                                            <Avatar sx={{ width: 60, height: 60, marginRight: 5 }} alt="User" src="/user.png" />
                                        </ListItemAvatar>
                                        <ListItemText
                                            primary={
                                                <Typography variant="h6" component="div">
                                                    {`${user.firstName} ${user.lastName}`}
                                                </Typography>
                                            }
                                            secondary={
                                                <React.Fragment>
                                                    <Typography variant="body2" color="text.primary">
                                                        Address: {user.address}
                                                    </Typography>
                                                    <Typography variant="body2" color="text.primary">
                                                        Phone: {user.phoneNumber}
                                                    </Typography>
                                                    <Typography variant="body2" color="text.primary">
                                                        Email: {user.email}
                                                    </Typography>
                                                    <Typography variant="body2" color="text.primary">
                                                        Role: {user.role}
                                                    </Typography>
                                                </React.Fragment>
                                            }
                                        />
                                    </ListItemButton>
                                </ListItem>
                                <Divider component="li" />
                            </React.Fragment>
                        ))
                    ) : (
                        <ListItem>
                            <ListItemText
                                primary="No users available"
                                primaryTypographyProps={{ align: 'center' }}
                            />
                        </ListItem>
                    )}
                </List>
                <div className="d-flex justify-content-center">
                    <nav>
                        <ul className="pagination">
                            <li className={`page-item ${currentPage === 1 ? 'disabled' : ''}`}>
                                <button className="page-link" onClick={handlePreviousPage}>Previous</button>
                            </li>
                            {[...Array(totalPages)].map((_, i) => (
                                <li key={i} className={`page-item ${currentPage === i + 1 ? 'active' : ''}`}>
                                    <button className="page-link" onClick={() => handlePageChange(i + 1)}>
                                        {i + 1}
                                    </button>
                                </li>
                            ))}
                            <li className={`page-item ${currentPage === totalPages ? 'disabled' : ''}`}>
                                <button className="page-link" onClick={handleNextPage}>Next</button>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    );
};

export default ListUser;
