import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useParams, Link, useNavigate } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from '../../components/Header/Header';
import { Helmet } from 'react-helmet';
import { Button, Modal } from 'react-bootstrap';
import { toast } from 'react-toastify';

const UserDetails = () => {
    const { peopleId } = useParams();
    const [user, setUser] = useState(null);
    const [error, setError] = useState(null);
    const [message, setMessage] = useState(null); // State for success or error messages
    const [messageType, setMessageType] = useState(''); // State for message type (success or error)
    const navigate = useNavigate();
    const token = localStorage.getItem('Token');
    const header = `Bearer ${token}`;
    useEffect(() => {
        const fetchUserDetails = async () => {
            try {
                const response = await axios.get(`${process.env.REACT_APP_BASE_URL}/api/People/getPeopleById/${peopleId}`, {
                    headers: { Authorization: header }
                });
                setUser(response.data);
            } catch (error) {
                setError(error.message);
                console.error('Error fetching user details:', error);
                addNotification('Error fetching user details', 'error');
            }
        };

        fetchUserDetails();
    }, [peopleId]);
    const addNotification = (message, type) => {
        toast(message, { type });
    };
    const deleteUser = async () => {
        try {
            await axios.delete(`${process.env.REACT_APP_BASE_URL}/api/People/DeletePeople/${peopleId}`, {
                headers: { Authorization: header }
            });
            setMessage('User deleted successfully.');
            addNotification('User deleted successfully!', 'success');
            setMessageType('success');
            setTimeout(() => navigate('/Dspace/User/UserList'), 2000); // Redirect after 2 seconds
        } catch (error) {
            setMessage('Error deleting user.');
            setMessageType('danger');
            addNotification(error.response.data, 'error');
            console.error('Error deleting user:', error);
        }
    };
    
    const DeleteButtonWithConfirmation = ({ deleteUser }) => {
        const [show, setShow] = useState(false);
    
        const handleClose = () => setShow(false);
        const handleShow = () => setShow(true);
    
        const handleConfirmDelete = () => {
            deleteUser();
            handleClose();
        };
    
        return (
            <>
                <Button variant="danger" onClick={handleShow} className="me-2">
                    Delete
                </Button>
    
                <Modal show={show} onHide={handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Confirm Deletion</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>Are you sure you want to delete this user?</Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Cancel
                        </Button>
                        <Button variant="danger" onClick={handleConfirmDelete}>
                            Delete
                        </Button>
                    </Modal.Footer>
                </Modal>
            </>
        );
    };
    if (!user) {
        return (
            <div>
                <Header />
                <div className="container mt-5">
                    <h1 className="mb-4">User Details</h1>
                    <p>Loading...</p>
                </div>
            </div>
        );
    }

    if (error) {
        return (
            <div>
                <Header />
                <div className="container mt-5">
                    <h1 className="mb-4">User Details</h1>
                    <div className="alert alert-danger">{error}</div>
                    <Link to="/Dspace/User/UserList">Back to List</Link>
                </div>
            </div>
        );
    }

    return (
        <div>
            <Header />
            <Helmet>
                <meta charSet="utf-8" />
                <title>User {`${user?.firstName} ${user?.lastName}`} - dSPACE</title>
            </Helmet>
            <div className="container mt-5">
                {/* <div className="py-1 bg-light">
                    <Link to="/HomepageAdmin" className="text-decoration-none">Home </Link>
                    <span className="text-dark"><strong>| User Detail</strong></span>
                </div> */}
                <h1 className="mb-4">User Details</h1>
                {/* {message && <div className={`alert alert-${messageType}`}>{message}</div>} */}
                <div className="card">
                    <div className="card-body">
                    <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">First Name:</div>
                            <div className="col-sm-6">{user.firstName}</div>
                        </div>
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Last Name:</div>
                            <div className="col-sm-6">{user.lastName}</div>
                        </div>
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Address:</div>
                            <div className="col-sm-6">{user.address}</div>
                        </div>
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Phone Number:</div>
                            <div className="col-sm-6">{user.phoneNumber}</div>
                        </div>
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Email:</div>
                            <div className="col-sm-6">{user.email}</div>
                        </div>
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Role:</div>
                            <div className="col-sm-6">{user.role}</div>
                        </div>
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Created By:</div>
                            <div className="col-sm-6">{user.createdBy}</div>
                        </div>
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Created Date:</div>
                            <div className="col-sm-6">{new Date(user.createdDate).toLocaleString()}</div>
                        </div>
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Last Modified By:</div>
                            <div className="col-sm-6">{user.lastModifiedBy}</div>
                        </div>
                        <div className="row pb-2 my-3">
                            <div className="col-sm-2 text-end">Last Modified Date:</div>
                            <div className="col-sm-6">{new Date(user.lastModifiedDate).toLocaleString()}</div>
                        </div>
                    </div>
                </div>
                <div className="mt-4">
                    <button onClick={() => navigate(`/Dspace/User/EditUser/${user.peopleId}`)} className="btn btn-primary me-2">Edit</button>
                    <DeleteButtonWithConfirmation deleteUser={deleteUser} />
                    {/* <button onClick={deleteUser} className="btn btn-danger me-2">Delete</button> */}
                    <button onClick={() => navigate('/Dspace/User/UserList')} className="btn btn-secondary me-2">Back to List</button>
                    
                </div>
            </div>
        </div>
    );
};

export default UserDetails;
