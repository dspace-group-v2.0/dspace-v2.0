import React, { useState } from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from '../../components/Header/Header';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { toast } from 'react-toastify';

const AddUser = () => {
    const [userData, setUserData] = useState({
        firstName: '',
        lastName: '',
        address: '',
        phoneNumber: '',
        email: '',
        role: 'ADMIN', // Default selection
    });

    const token = localStorage.getItem('Token');
    const header = `Bearer ${token}`;
    const [successMessage, setSuccessMessage] = useState('');
    const [errors, setErrors] = useState({}); // State for errors

    const handleChange = (e) => {
        const { name, value } = e.target;
        setUserData({
            ...userData,
            [name]: value,
        });
    };
    const addNotification = (message, type) => {
        toast(message, { type });
    };
    const validate = () => {
        let newErrors = {};

        if (!userData.firstName.trim()) newErrors.firstName = 'First Name is required';
        if (!userData.lastName.trim()) newErrors.lastName = 'Last Name is required';
        if (!userData.address.trim()) newErrors.address = 'Address is required';
        if (!userData.phoneNumber.trim()) {
            newErrors.phoneNumber = 'Phone Number is required';
        } else if (!/^\d{10,}$/.test(userData.phoneNumber)) {
            newErrors.phoneNumber = 'Phone Number must be at least 10 digits';
        }
        if (!userData.email.trim()) {
            newErrors.email = 'Email is required';
        } else if (!/\S+@\S+\.\S+/.test(userData.email)) {
            newErrors.email = 'Email address is invalid';
        }

        setErrors(newErrors);
        return Object.keys(newErrors).length === 0;
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        if (validate()) {
            axios.post(`${process.env.REACT_APP_BASE_URL}/api/People/createPeople`, userData, {
                headers: { Authorization: header }
            })
                .then(response => {
                    console.log('User created successfully!', response.data);
                    setSuccessMessage('User added successfully!');
                    addNotification('User added successfully!', 'success');
                    // Reset form after successful submission
                    setUserData({
                        firstName: '',
                        lastName: '',
                        address: '',
                        phoneNumber: '',
                        email: '',
                        role: 'ADMIN', // Reset to default
                    });
                    setErrors({}); // Clear errors after successful submission
                })
                .catch(error => {
                    console.error('There was an error creating the user!', error);
                    addNotification(error.response.data, 'error');
                });
        }
    };

    return (
        <div>
            <Header />
            <Helmet>
                <meta charSet="utf-8" />
                <title>Create new User - dSPACE</title>
            </Helmet>
            <div className="container mt-5">
                {/* <div className="py-1 bg-light">
                    <Link to="/HomepageAdmin" className="text-decoration-none">Home </Link>
                    <span className="text-dark"><strong>| Add User</strong></span>
                </div> */}
                <h1 className="mb-4">Create New User</h1>
                {/* {successMessage && <div className="alert alert-success">{successMessage}</div>} */}
                <div className="row">
                    <div className="col-md-12">
                        <form onSubmit={handleSubmit}>
                            <div className="form-group row w-75">
                                <div className="col">
                                    <label>First Name</label>
                                    <input
                                        type="text"
                                        className={`form-control ${errors.firstName ? 'is-invalid' : ''}`}
                                        name="firstName"
                                        value={userData.firstName}
                                        onChange={handleChange}
                                    />
                                    {errors.firstName && <div className="invalid-feedback">{errors.firstName}</div>}
                                </div>
                                <div className="col">
                                    <label>Last Name</label>
                                    <input
                                        type="text"
                                        className={`form-control ${errors.lastName ? 'is-invalid' : ''}`}
                                        name="lastName"
                                        value={userData.lastName}
                                        onChange={handleChange}

                                    />
                                    {errors.lastName && <div className="invalid-feedback">{errors.lastName}</div>}
                                </div>
                            </div>
                            <div className="form-group row w-75">
                                <div className="col">
                                    <label>Address</label>
                                    <input
                                        type="text"
                                        className={`form-control ${errors.address ? 'is-invalid' : ''}`}
                                        name="address"
                                        value={userData.address}
                                        onChange={handleChange}

                                    />
                                    {errors.address && <div className="invalid-feedback">{errors.address}</div>}
                                </div>
                                <div className="col">
                                    <label>Phone Number</label>
                                    <input
                                        type="text"
                                        className={`form-control ${errors.phoneNumber ? 'is-invalid' : ''}`}
                                        name="phoneNumber"
                                        value={userData.phoneNumber}
                                        onChange={handleChange}

                                    />
                                    {errors.phoneNumber && <div className="invalid-feedback">{errors.phoneNumber}</div>}
                                </div>
                            </div>
                            <div className="form-group row w-75">
                                <div className="col">
                                    <label>Email</label>
                                    <input
                                        type="email"
                                        className={`form-control ${errors.email ? 'is-invalid' : ''}`}
                                        name="email"
                                        value={userData.email}
                                        onChange={handleChange}

                                    />
                                    {errors.email && <div className="invalid-feedback">{errors.email}</div>}
                                </div>
                                <div className="col">
                                    <label>Role</label>
                                    <select
                                        className="form-control"
                                        name="role"
                                        value={userData.role}
                                        onChange={handleChange}

                                    >
                                        <option value="ADMIN">ADMIN</option>
                                        <option value="STAFF">STAFF</option>
                                    </select>
                                </div>
                            </div>
                            <button type="submit" className="btn btn-primary mt-3">Add</button>
                        </form>
                    </div>
                </div>
                <div className="mt-3">
                    <Link to="/Dspace/User/UserList">Back to List</Link>
                </div>
            </div>
        </div>
    );
};

export default AddUser;
