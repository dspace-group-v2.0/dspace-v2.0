import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useParams, useNavigate, Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from '../../components/Header/Header';
import { Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@mui/material';
import { Helmet } from 'react-helmet';

const MetadataDetails = () => {
    const { itemId } = useParams();
    const [metadata, setMetadata] = useState([]);
    const [item, setItem] = useState([]);
    const [message, setMessage] = useState(null);
    const [messageType, setMessageType] = useState('');
    const navigate = useNavigate();

    useEffect(() => {
        axios.get(`${process.env.REACT_APP_BASE_URL}/api/Item/GetItemSimpleById/${itemId}`) // Replace with your actual API endpoint
            .then(({ data }) => {
                setItem(data);
            })
            .catch(error => {
                console.error('There was an error fetching the item!', error);
            });

        axios.get(`${process.env.REACT_APP_BASE_URL}/api/Item/GetItemFullById/${itemId}`)
            .then(({ data }) => {
                setMetadata(data.listMetadataValueDTOForSelect)
            })
            .catch(error => {
                console.error('There was an error fetching the metadata!', error);
            });
    }, [itemId]);

    // Conditionally render the JSX based on whether metadata is null or not
    if (!metadata) {
        return (
            <div>
                <Header />
                <div className="container mt-5">
                    <h1 className="mb-4">Metadata Details</h1>
                    <p>Loading...</p>
                </div>
            </div>
        );
    }

    return (
        <div>
            <Header />
            <Helmet>
                <meta charSet="utf-8" />
                <title>Metadata for {`${item?.title}`} - dSPACE</title>
            </Helmet>
            <div className="container mt-5">
                <h1 className="mb-4">Metadata Details</h1>
                <TableContainer component={Paper}>
                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Field</TableCell>
                                <TableCell align="left">Value</TableCell>
                                <TableCell align="right">Lang</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {metadata.map((row) => (
                                <TableRow
                                    key={row.metadataFieldName}
                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                >
                                    <TableCell component="th" scope="row">
                                        {row.metadataFieldName}
                                    </TableCell>
                                    <TableCell align="left">{row.textValue}</TableCell>
                                    <TableCell align="right">{row.textLang}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <div className="mt-4">
                    <button
                        onClick={() => {
                            if (localStorage.getItem('Token') !== null) {
                                navigate(`/DSpace/ItemDetail/${itemId}`)
                            } else {
                                navigate('/Login');
                            }
                        }}
                        className="btn btn-secondary"
                    >
                        Back
                    </button>
                </div>
            </div>
        </div>
    );
};

export default MetadataDetails;
