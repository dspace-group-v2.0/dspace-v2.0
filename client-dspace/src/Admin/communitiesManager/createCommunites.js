import React, { useEffect, useState, useCallback } from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';
import Header from '../../components/Header/Header';
import { useDropzone } from 'react-dropzone';
import { Helmet } from 'react-helmet';
import { ToastContainer, toast } from 'react-toastify';
import { Autocomplete, TextField } from '@mui/material';
import { jwtDecode } from 'jwt-decode';


const CreateCommunity = () => {
    const [selectedValue, setSelectedValue] = useState('');
    const [communityData, setCommunityData] = useState({
        logoUrl: 'nothing',
        communityName: '',
        shortDescription: '',
        isActive: true,
    });
    const token = localStorage.getItem('Token');
    const header = `Bearer ${token}`;
    const [parentCommunities, setParentCommunities] = useState([]); // State for parent communities
    const [successMessage, setSuccessMessage] = useState(''); // State for success message
    const [errors, setErrors] = useState({}); // State for errors
    const role = token ? jwtDecode(token)["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"] : null;

    useEffect(() => {
        if (role === "ADMIN") {
            axios.get(`${process.env.REACT_APP_BASE_URL}/api/Community/getListOfCommunities`)
                .then(response => {
                    setParentCommunities(response.data);
                })
                .catch(error => {
                    console.error('There was an error fetching the parent communities!', error);
                });
        } else if (role === "STAFF") {
            axios.get(`${process.env.REACT_APP_BASE_URL}/api/CommunityStaff/GetAllCommunities`, {
                headers: {
                    Authorization: header
                }
            })
                .then(response => {
                    setParentCommunities(response.data);
                })
                .catch(error => {
                    console.error('There was an error fetching the parent communities!', error);
                });
        }

    }, []);
    const addNotification = (message, type) => {
        toast(message, { type });
    };
    const handleChange = (e) => {
        const { name, value, type, checked } = e.target;
        const newValue = type === 'checkbox' ? checked : value;
        if (name === 'parentCommunityId') {
            setSelectedValue(newValue.communityId);
            setCommunityData({
                ...communityData,
                parentCommunityId: newValue === '' ? null : newValue,
            });
        } else {
            setCommunityData({
                ...communityData,
                [name]: newValue,
            });
        }
    };

    const validate = () => {
        let newErrors = {};

        if (!communityData.logoUrl.trim()) newErrors.logoUrl = 'Logo URL is required';
        if (!communityData.communityName.trim()) newErrors.communityName = 'Name is required';
        if (!communityData.shortDescription.trim()) newErrors.shortDescription = 'Short description is required';

        setErrors(newErrors);
        return Object.keys(newErrors).length === 0;
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const formData = new FormData();
        formData.append('file', file);
        Object.keys(communityData).forEach(key => {
            formData.append(key, communityData[key]);
        });
        if(selectedCommunity !== null){
            formData.append('parentCommunityId', selectedCommunity.communityId)
        }
        if (validate()) {
            if (role === "ADMIN") {
                await axios.post(`${process.env.REACT_APP_BASE_URL}/api/Community/createCommunity`, formData, {
                    'Content-Type': 'multipart/form-data',
                    headers: { Authorization: header }
                })
                    .then(response => {
                        console.log('Community created successfully!', response.data);
                        console.log(formData)
                        setSuccessMessage('Community created successfully!');
                        addNotification('Community created successfully!', 'success');
                    })
                    .catch(error => {
                        console.error('There was an error creating the community!', error);
                        addNotification('There was an error creating the community!', 'error');
                    });
            } else if (role === "STAFF"){
                await axios.post(`${process.env.REACT_APP_BASE_URL}/api/CommunityStaff/createCommunity`, formData, {
                    'Content-Type': 'multipart/form-data',
                    headers: { Authorization: header }
                })
                    .then(response => {
                        console.log('Community created successfully!', response.data);
                        console.log(formData)
                        setSuccessMessage('Community created successfully!');
                        addNotification('Community created successfully!', 'success');
                    })
                    .catch(error => {
                        console.error('There was an error creating the community!', error);
                        addNotification('There was an error creating the community!', 'error');
                    });
            }
        } 
    };

    const [file, setFile] = useState(null);
    const [preview, setPreview] = useState(null);

    const onDrop = useCallback((acceptedFiles) => {
        const file = acceptedFiles[0];
        setFile(file);

        const reader = new FileReader();
        reader.onloadend = () => {
            setPreview(reader.result);
        };
        reader.readAsDataURL(file);
        console.log(file)
    }, []);

    const { getRootProps, getInputProps, isDragActive } = useDropzone({
        onDrop,
        multiple: false,
        accept: 'image/*',
    });
    const [selectedCommunity, setSelectedCommunity] = useState(null);
    const handleCommunityChange = (event, newValue) => {
        setSelectedCommunity(newValue);
    };

    return (
        <div>
            <Header />
            <Helmet>
                <meta charSet="utf-8" />
                <title>Create new Community - dSPACE</title>
            </Helmet>
            <div className="container mt-5">
                <h1 className="mb-4 text-center">Create New Community</h1>
                {/* {successMessage && <div className="alert alert-success">{successMessage}</div>}  */}
                <div className="row justify-content-center">
                    <div className="col-md-11">
                        <form onSubmit={handleSubmit}>
                            <div className='d-flex justify-content-between'>
                                <div className='row w-50'>
                                    <div
                                        {...getRootProps()}
                                        className={`dropzone border d-flex flex-column rounded p-5 ${isDragActive ? 'bg-light' : ''}`}
                                        style={{ cursor: 'pointer' }}
                                    >
                                        <input {...getInputProps()} />
                                        {isDragActive ? (
                                            <p className="text-center">Drop the files here...</p>
                                        ) : (
                                            <p className="text-center">Drag 'n' drop a file here, or click to select one</p>
                                        )}
                                        {preview ? (
                                            <div className="mt-3">
                                                <h5>Preview:</h5>
                                                <img src={preview} alt="Preview" className="img-thumbnail" />
                                            </div>
                                        ) : (
                                            <img src='../../dropbox.png' className="img-thumbnail" style={{ width: 300, height: 300, margin: 'auto' }} alt="Background" />
                                        )}
                                    </div>
                                </div>
                                <div className="row w-50 flex-column justify-content-center">
                                    <div className="col-md-5 w-100">
                                        <div className="form-group mb-3">
                                            <label>Name</label>
                                            <input
                                                type="text"
                                                className={`form-control ${errors.communityName ? 'is-invalid' : ''}`}
                                                name="communityName"
                                                value={communityData.communityName}
                                                onChange={handleChange}
                                            />
                                            {errors.communityName && <div className="invalid-feedback">{errors.communityName}</div>}
                                        </div>
                                        <div className="form-group mb-3">
                                            <label>Short Description</label>
                                            <textarea
                                                style={{ height: 250 }}
                                                className={`form-control ${errors.shortDescription ? 'is-invalid' : ''}`}
                                                name="shortDescription"
                                                value={communityData.shortDescription}
                                                onChange={handleChange}
                                            />
                                            {errors.shortDescription && <div className="invalid-feedback">{errors.shortDescription}</div>}
                                        </div>
                                        <div className="form-group mb-3">
                                            <label>Parent Community</label>
                                            <Autocomplete
                                                options={parentCommunities}
                                                getOptionLabel={(option) => option.communityName}
                                                onChange={handleCommunityChange}
                                                renderInput={(params) => (
                                                    <TextField
                                                        {...params}
                                                        variant="outlined"
                                                        placeholder="Select Parent Community"
                                                    />
                                                )}
                                            />
                                        </div>
                                        <button type="submit" className="btn btn-primary mt-4">Add</button>
                                    </div>
                                </div>
                            </div>
                            <div className="d-flex justify-content-between mt-4">
                                <Link to="/Dspace/Community/CommunityList">Back to List</Link>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CreateCommunity;
