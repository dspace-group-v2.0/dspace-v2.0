import React, { useCallback, useEffect, useState } from 'react';
import axios from 'axios';
import { useParams, Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from '../../components/Header/Header';
import { Helmet } from 'react-helmet';
import { useDropzone } from 'react-dropzone';
import { Autocomplete, TextField } from '@mui/material';
import { toast } from 'react-toastify';
import { jwtDecode } from 'jwt-decode';

const EditCommunity = () => {
    const { communityId } = useParams(); // Get the community ID from the URL

    const [communityData, setCommunityData] = useState({
        logoUrl: '',
        communityName: '',
        shortDescription: '',
        isActive: true,
    });
    const [parentCommunities, setParentCommunities] = useState([]);
    const token = localStorage.getItem('Token');
    const header = `Bearer ${token}`;
    const [successMessage, setSuccessMessage] = useState('');
    const [communityNameHeader, setCommunityNameHeader] = useState('');
    const [errors, setErrors] = useState({}); // State for errors
    const [parentCommunityId, setParentCommunityId] = useState(0);
    const [parentCommunity, setParentCommunity] = useState({});
    const role = token ? jwtDecode(token)["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"] : null;

    useEffect(() => {
        if (role === "ADMIN") {
            axios.get(`${process.env.REACT_APP_BASE_URL}/api/Community/getCommunity/${communityId}`)
                .then(async (response) => {
                    const data = response.data;
                    setCommunityData({
                        logoUrl: data.logoUrl || '',
                        communityName: data.communityName || '',
                        shortDescription: data.shortDescription || '',
                        createTime: data.createTime || '',
                        updateTime: data.updateTime || '',
                        createBy: data.createBy || '',
                        updateBy: data.updateBy || '',
                        isActive: data.isActive,
                    });
                    if (data.logoUrl !== '') {
                        const relativePath = getRelativePath(data.logoUrl);
                        try {
                            const responseFile = await fetch(`/${relativePath}`);
                            const blob = await responseFile.blob();
                            const fileName = relativePath.split('/').pop();
                            const fileObject = new File([blob], fileName, { type: blob.type });
                            console.log(responseFile)
                            setFile(fileObject);
                            setPreview(`/${relativePath}`);
                        } catch (error) {
                            console.error('Error fetching the logo file:', error);
                        }
                    }
                    setCommunityNameHeader(data.communityName);
                    setParentCommunityId(data.parentCommunityId)
                })
                .catch(error => {
                    console.error('There was an error fetching the community!', error);
                    addNotification('There was an error fetching the community!', 'error');
                });

            axios.get(`${process.env.REACT_APP_BASE_URL}/api/Community/getListOfCommunities`)
                .then(response => {
                    setParentCommunities(response.data);
                })
                .catch(error => {
                    console.error('There was an error fetching the parent communities!', error);
                    addNotification('There was an error fetching the parent communities!', 'error');
                });
        } else if (role === "STAFF") {
            axios.get(`${process.env.REACT_APP_BASE_URL}/api/CommunityStaff/GetCommunityById/${communityId}`, {
                headers: {
                    Authorization: header
                }
            })
                .then(async (response) => {
                    const data = response.data;
                    setCommunityData({
                        logoUrl: data.logoUrl || '',
                        communityName: data.communityName || '',
                        shortDescription: data.shortDescription || '',
                        createTime: data.createTime || '',
                        updateTime: data.updateTime || '',
                        createBy: data.createBy || '',
                        updateBy: data.updateBy || '',
                        isActive: data.isActive,
                    });
                    if (data.logoUrl !== '') {
                        const relativePath = getRelativePath(data.logoUrl);
                        try {
                            const responseFile = await fetch(`/${relativePath}`);
                            const blob = await responseFile.blob();
                            const fileName = relativePath.split('/').pop();
                            const fileObject = new File([blob], fileName, { type: blob.type });
                            console.log(fileObject)
                            setFile(fileObject);
                            setPreview(`/${relativePath}`);
                        } catch (error) {
                            console.error('Error fetching the logo file:', error);
                        }
                    }
                    setCommunityNameHeader(data.communityName);
                    setParentCommunityId(data.parentCommunityId)
                    setParentCommunity(data.communityDTOParentStaff)
                })
                .catch(error => {
                    console.error('There was an error fetching the community!', error);
                    addNotification('There was an error fetching the community!', 'error');
                });

            axios.get(`${process.env.REACT_APP_BASE_URL}/api/CommunityStaff/GetAllCommunities`, {
                headers: {
                    Authorization: header
                }
            })
                .then(response => {
                    setParentCommunities(response.data);
                })
                .catch(error => {
                    console.error('There was an error fetching the parent communities!', error);
                    addNotification('There was an error fetching the parent communities!', 'error');
                });
        }
    }, []);
    const addNotification = (message, type) => {
        toast(message, { type });
    };

    useEffect(() => {
        var checkCommunity = parentCommunities.find(c => c.communityId === parentCommunityId)
        if (checkCommunity !== undefined) {
            console.log(checkCommunity)
            setSelectedCommunity(checkCommunity)
        }
    }, [parentCommunityId]);


    const handleChange = (e) => {
        const { name, value, type, checked } = e.target;
        setCommunityData({
            ...communityData,
            [name]: type === 'checkbox' ? checked : value,
        });
    };
    function getRelativePath(fullPath) {
        const splitPath = fullPath.split('public\\');
        return splitPath[1].replace(/\\/g, '/');
    }
    const validate = () => {
        let newErrors = {};

        if (!communityData.communityName.trim()) newErrors.communityName = 'Community name is required';
        if (!communityData.shortDescription.trim()) newErrors.shortDescription = 'Short description is required';

        setErrors(newErrors);
        return Object.keys(newErrors).length === 0;
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        if (validate()) {
            const formData = new FormData();
            formData.append('file', file);
            Object.keys(communityData).forEach(key => {
                formData.append(key, communityData[key]);
            });
            if (selectedCommunity !== null) {
                if (role === "ADMIN" || (role === "STAFF" && parentCommunity.canEdit)) {
                    formData.append('parentCommunityId', selectedCommunity.communityId)
                } else {
                    formData.append('parentCommunityId', parentCommunityId)
                }
            }
            if (role === "ADMIN") {
                axios.put(`${process.env.REACT_APP_BASE_URL}/api/Community/updateCommunity/${communityId}`, formData, {
                    'Content-Type': 'multipart/form-data',
                    headers: { Authorization: header }
                })
                    .then(response => {
                        console.log('Community updated successfully!', response.data);
                        setSuccessMessage('Community updated successfully!');
                        addNotification('Community updated successfully!', 'success');
                    })
                    .catch(error => {
                        console.error('There was an error updating the community!', error);
                        addNotification('There was an error updating the community!', 'error');
                    });
            } else if (role === "STAFF") {
                axios.put(`${process.env.REACT_APP_BASE_URL}/api/CommunityStaff/UpdateCommunity/${communityId}`, formData, {
                    'Content-Type': 'multipart/form-data',
                    headers: { Authorization: header }
                })
                    .then(response => {
                        console.log('Community updated successfully!', response.data);
                        setSuccessMessage('Community updated successfully!');
                        addNotification('Community updated successfully!', 'success');
                    })
                    .catch(error => {
                        console.error('There was an error updating the community!', error);
                        addNotification('There was an error updating the community!', 'error');
                    });
            }
        }
    };

    const [file, setFile] = useState(null);
    const [preview, setPreview] = useState(null);

    const onDrop = useCallback((acceptedFiles) => {
        const file = acceptedFiles[0];
        setFile(file);

        const reader = new FileReader();
        reader.onloadend = () => {
            setPreview(reader.result);
        };
        reader.readAsDataURL(file);
        console.log(file)
    }, []);


    const { getRootProps, getInputProps, isDragActive } = useDropzone({
        onDrop,
        multiple: false,
        accept: 'image/*',
    });
    const [selectedCommunity, setSelectedCommunity] = useState(null);
    const handleCommunityChange = (event, newValue) => {
        setSelectedCommunity(newValue);
        console.log(newValue);
    };

    return (
        <div>
            <Header />
            <Helmet>
                <meta charSet="utf-8" />
                <title>Modify Community - {`${communityData?.communityName}`} - dSPACE</title>
            </Helmet>
            <div className="container mt-5">
                {/* <div className="py-1 bg-light">
                    <Link to="/HomepageAdmin" className="text-decoration-none">Home </Link>
                    <span className="text-dark"><strong>| Edit Community</strong></span>
                </div> */}
                <h1 className="mb-4 text-center">Edit Community - {communityNameHeader}</h1>
                {/* {successMessage && <div className="alert alert-success text-center">{successMessage}</div>} */}
                <div className="row justify-content-center">
                    <div className="col-md-11">
                        <form onSubmit={handleSubmit}>
                            <div className='d-flex justify-content-between'>
                                <div className='row w-50'>
                                    <div
                                        {...getRootProps()}
                                        className={`dropzone border d-flex flex-column rounded p-5 ${isDragActive ? 'bg-light' : ''}`}
                                        style={{ cursor: 'pointer' }}
                                    >
                                        <input {...getInputProps()} />
                                        {isDragActive ? (
                                            <p className="text-center">Drop the files here...</p>
                                        ) : (
                                            <p className="text-center">Drag 'n' drop a file here, or click to select one</p>
                                        )}
                                        {preview ? (
                                            <div className="mt-3">
                                                <h5>Preview:</h5>
                                                <img src={preview} alt="Preview" className="img-thumbnail" />
                                            </div>
                                        ) : (
                                            <img src='/dropbox.png' className="img-thumbnail" style={{ width: 300, height: 300, margin: 'auto' }} alt="Background" />
                                        )}
                                    </div>
                                </div>
                                <div className="row w-50 flex-column justify-content-center">
                                    <div className="col-md-5 w-100">
                                        <div className="form-group mb-3">
                                            <label>Name</label>
                                            <input
                                                type="text"
                                                className={`form-control ${errors.communityName ? 'is-invalid' : ''}`}
                                                name="communityName"
                                                value={communityData.communityName}
                                                onChange={handleChange}
                                            />
                                            {errors.communityName && <div className="invalid-feedback">{errors.communityName}</div>}
                                        </div>
                                        <div className="form-group mb-3">
                                            <label>Short Description</label>
                                            <textarea
                                                style={{ height: 250 }}
                                                className={`form-control ${errors.shortDescription ? 'is-invalid' : ''}`}
                                                name="shortDescription"
                                                value={communityData.shortDescription}
                                                onChange={handleChange}
                                            />
                                            {errors.shortDescription && <div className="invalid-feedback">{errors.shortDescription}</div>}
                                        </div>
                                        <div className="form-group mb-3">
                                            <label>Parent Community</label>
                                            {(role === "ADMIN" || (role === "STAFF" && parentCommunity.canEdit)) ? (

                                                <Autocomplete
                                                    options={parentCommunities}
                                                    getOptionLabel={(option) => option.communityName}
                                                    value={selectedCommunity}
                                                    onChange={handleCommunityChange}
                                                    getOptionDisabled={(option) => option.communityId == communityId}
                                                    renderInput={(params) => (
                                                        <TextField
                                                            {...params}
                                                            variant="outlined"
                                                            placeholder="Select Parent Community"
                                                        // required={true}
                                                        />
                                                    )}
                                                />
                                            ) :
                                                <Autocomplete
                                                    readOnly
                                                    options={parentCommunities}
                                                    getOptionLabel={(option) => option.communityName}
                                                    value={parentCommunity}
                                                    onChange={handleCommunityChange}
                                                    renderInput={(params) => (
                                                        <TextField
                                                            {...params}
                                                            variant="outlined"
                                                            placeholder="Select Parent Community"
                                                            required={true}
                                                        />
                                                    )}
                                                />
                                            }
                                        </div>
                                        <div className="form-group mb-3">
                                            <label>Active</label>
                                            <input
                                                type="checkbox"
                                                className="form-check-input ms-2"
                                                name="isActive"
                                                checked={communityData.isActive}
                                                onChange={handleChange}
                                            />
                                        </div>
                                        <button type="submit" className="btn btn-primary mt-4">Update</button>
                                    </div>
                                </div>
                            </div>
                            <div className="d-flex justify-content-between mt-4">
                                <Link to="/Dspace/Community/CommunityList">Back to List</Link>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default EditCommunity;
