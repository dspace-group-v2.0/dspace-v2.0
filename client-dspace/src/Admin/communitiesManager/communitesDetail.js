import React, { useCallback, useEffect, useState, useRef } from 'react';
import axios from 'axios';
import { useParams, useNavigate, Link, useSearchParams, useLocation } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import AssessmentIcon from '@mui/icons-material/Assessment';
import Header from '../../components/Header/Header';
import Counter from '../../components/Statistics/counterView';
import { Button } from '@mui/material';
import Badge from '@mui/material/Badge';
import { AddRounded } from '@material-ui/icons';

import { Search } from '@material-ui/icons';
import { Helmet } from 'react-helmet';
import {
    ExpandLess, ExpandMore
} from '@material-ui/icons';
import { Modal } from 'react-bootstrap';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css'
import DangerousIcon from '@mui/icons-material/Dangerous';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import Tooltip from '@mui/material/Tooltip';
import IconButton from '@mui/material/IconButton';
import { jwtDecode } from 'jwt-decode';

const CommunityDetails = () => {
    const { communityId } = useParams();
    const [community, setCommunity] = useState({});
    const [collections, setCollections] = useState([]);
    const [message, setMessage] = useState(null);
    const [messageType, setMessageType] = useState('');
    const navigate = useNavigate();
    const [communities, setCommunities] = useState([]);
    const [originalCollections, setOriginalCollections] = useState([]);
    const [originalCommunities, setOriginalCommunities] = useState([]);
    const [currentPageCommunities, setCurrentPageCommunities] = useState(1);
    const [itemsPerPageCommunities, setItemsPerPageCommunities] = useState(5);
    const [currentPageCollections, setCurrentPageCollections] = useState(1);
    const [itemsPerPageCollections, setItemsPerPageCollections] = useState(5);
    const [searchParams, setSearchParams] = useSearchParams();
    const location = useLocation();
    let communityIdRef = useRef(searchParams.getAll('cid'));
    const queryParams = new URLSearchParams(location.search);
    const [searchQuery, setSearchQuery] = useState('');
    const [subscriptions, setSubscriptions] = useState({});
    const initialButton = queryParams.get('button') || 'All';
    const [activeButton, setActiveButton] = useState(initialButton);
    const [activeButtonCollection, setActiveButtonCollection] = useState(initialButton);
    const token = localStorage.getItem('Token');
    const userRole = token ? jwtDecode(token)["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"] : null;
    const header = `Bearer ${token}`;
    const [expandedAbstracts, setExpandedAbstracts] = useState({});
    const [loading, setLoading] = useState(false);
    const toggleAbstract = (itemId) => {
        setExpandedAbstracts(prevState => ({
            ...prevState,
            [itemId]: !prevState[itemId]
        }));
    };

    const addNotification = (message, type) => {
        toast(message, { type });
    };
    useEffect(() => {
        communityIdRef.current = searchParams.getAll('cid');
    }, [location.search, searchParams]);

    useEffect(() => {
        const fetchCommunityData = async () => {
            try {
                if (userRole == 'STUDENT' || userRole == 'LECTURER') {
                    const communityResponse = await axios.get(`${process.env.REACT_APP_BASE_URL}/api/CommunityUser/GetCommunity/${communityId}`, { headers: { Authorization: header } });
                    if(communityResponse.status === 204){
                        setLoading(true)
                    }else{
                        const communityData = communityResponse.data;
                        setCommunity(communityResponse.data);
                        setCommunities(communityResponse.data.subCommunities);
                        setOriginalCommunities(communityResponse.data.subCommunities);
                        setCollections(communityResponse.data.collections);
                        setOriginalCollections(communityResponse.data.collections);
                        const initialSubscriptions = communityData.collections.reduce((acc, collection) => {
                            acc[collection.collectionId] = collection.isSubcribe;
                            return acc;
                        }, {});
                        setSubscriptions(initialSubscriptions);
                    }

                } else if (userRole == 'STAFF') {
                    const communityResponse = await axios.get(`${process.env.REACT_APP_BASE_URL}/api/CommunityStaff/GetCommunityById/${communityId}`, { headers: { Authorization: header } });
                    if(communityResponse.status === 204){
                        setLoading(true)
                    }else{
                        setCommunity(communityResponse.data);
                        setCommunities(communityResponse.data.subCommunities);
                        setOriginalCommunities(communityResponse.data.subCommunities);
                        setCollections(communityResponse.data.collections);
                        setOriginalCollections(communityResponse.data.collections);
                    }
                } else {
                    const communityResponse = await axios.get(`${process.env.REACT_APP_BASE_URL}/api/Community/getCommunity/${communityId}`);
                    if(communityResponse.status === 204){
                        setLoading(true)
                    }else{
                        setCommunity(communityResponse.data);
                        setCommunities(communityResponse.data.subCommunities);
                        setOriginalCommunities(communityResponse.data.subCommunities);
                        setCollections(communityResponse.data.collections);
                        setOriginalCollections(communityResponse.data.collections);
                    }
                }

            } catch (error) {
                console.error('There was an error fetching the community or collections!', error);
            }
        };

        fetchCommunityData();
    }, [communityId]);


    const DeleteButtonWithModal = ({ handleDelete }) => {
        const [show, setShow] = useState(false);

        const handleClose = () => setShow(false);
        const handleShow = () => setShow(true);

        const handleConfirmDelete = () => {
            handleDelete();
            handleClose();
        };

        return (
            <>
                <button variant="danger" className="btn btn-danger me-2" onClick={handleShow}>
                    Delete
                </button>

                <Modal show={show} onHide={handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Confirm Deletion</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>Are you sure you want to delete this community?</Modal.Body>
                    <Modal.Footer>
                        <button className="btn btn-secondary me-2" variant="secondary" onClick={handleClose}>
                            Cancel
                        </button>
                        <button className="btn btn-danger me-2" variant="danger" onClick={handleConfirmDelete}>
                            Delete
                        </button>
                    </Modal.Footer>
                </Modal>
            </>
        );
    };
    const handleDelete = () => {
        if (userRole === "ADMIN") {
        axios.delete(`${process.env.REACT_APP_BASE_URL}/api/Community/DeleteCommunity/${communityId}`, {
            headers: {
                Authorization: header
            }
        })
            .then(response => {
                setMessage('Community deleted successfully!');
                setMessageType('success');
                // addNotification(message, 'success');
                addNotification('Community deleted successfully!', 'success');
                setTimeout(() => navigate('/Dspace/Community/CommunityList'), 2000);
            })
            .catch(error => {
                console.error('There was an error deleting the community!', error);
                setMessage('Community has collections or sub-communities. Please delete all collections or sub-communities in the community before deleting it.');
                setMessageType('danger');
                // addNotification(message, 'error');
                addNotification('Community has collections or sub-communities. Please delete all collections or sub-communities in the community before deleting it.', 'error');

            });
        }else if (userRole === "STAFF") {
            axios.delete(`${process.env.REACT_APP_BASE_URL}/api/CommunityStaff/DeleteCommunity/${communityId}`, {
                headers: {
                    Authorization: header
                }
            })
            .then(response => {
                setMessage('Community deleted successfully!');
                setMessageType('success');
                // addNotification(message, 'success');
                addNotification('Community deleted successfully!', 'success');
                setTimeout(() => navigate('/Dspace/Community/CommunityList'), 2000);
            })
            .catch(error => {
                console.error('There was an error deleting the community!', error);
                setMessage('Community has collections or sub-communities. Please delete all collections or sub-communities in the community before deleting it.');
                setMessageType('danger');
                // addNotification(message, 'error');
                addNotification('Community has collections or sub-communities. Please delete all collections or sub-communities in the community before deleting it.', 'error');

            });
        }
    };

    const indexOfLastCommunity = currentPageCommunities * itemsPerPageCommunities;
    const indexOfFirstCommunity = indexOfLastCommunity - itemsPerPageCommunities;
    const currentCommunitiesPaginated = communities.slice(indexOfFirstCommunity, indexOfLastCommunity);
    const totalPagesCommunities = Math.ceil(communities.length / itemsPerPageCommunities);

    const handleNextPageCollections = () => {
        setCurrentPageCollections(prevPage => prevPage + 1);
    };

    const handlePrevPageCollections = () => {
        setCurrentPageCollections(prevPage => prevPage - 1);
    };

    const indexOfLastCollection = currentPageCollections * itemsPerPageCollections;
    const indexOfFirstCollection = indexOfLastCollection - itemsPerPageCollections;
    const currentCollectionsPaginated = collections.slice(indexOfFirstCollection, indexOfLastCollection);
    const totalPagesCollections = Math.ceil(collections.length / itemsPerPageCollections);

    const buttons = [
        'All',
        'By Name',
        'By Description'
    ];
    const buttonCollection = [
        'All',
        'By Name',
        'By Description'
    ];

    const [collectionSearchQuery, setCollectionSearchQuery] = useState('');

    const handleCollectionSearchChange = (e) => {
        setCollectionSearchQuery(e.target.value);
    };

    const performCollectionSearch = () => {
        const normalizedSearchQuery = collectionSearchQuery.replace(/\s+/g, '').toLowerCase();

        if (activeButtonCollection === 'By Name') {
            setCollections(originalCollections.filter(collection =>
                collection.collectionName.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery)
            ));
        } else if (activeButtonCollection === 'By Description') {
            setCollections(originalCollections.filter(collection =>
                collection.shortDescription.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery)
            ));
        }
    };

    const performSearch = () => {
        const normalizedSearchQuery = searchQuery.replace(/\s+/g, '').toLowerCase();

        if (activeButton === 'By Name') {
            setCommunities(originalCommunities.filter(community =>
                community.communityName.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery)
            ));

        } else if (activeButton === 'By Description') {
            setCommunities(originalCommunities.filter(community =>
                community.shortDescription.replace(/\s+/g, '').toLowerCase().includes(normalizedSearchQuery)
            ));
        }
    };

    const handleSearchChange = (e) => {
        setSearchQuery(e.target.value);
    };


    const handleButtonClick = (buttonName) => {
        setActiveButton(buttonName);
        navigate(`?cid=${community.communityId}&button=${buttonName}`);
        setSearchQuery('');
        setCurrentPageCommunities(1);
        setCommunities(originalCommunities);

    };

    const handleButtonColClick = (buttonName) => {
        setActiveButtonCollection(buttonName);
        navigate(`?cid=${community.communityId}&button=${buttonName}`);
        setCollectionSearchQuery('');
        setCurrentPageCollections(1);
        setCollections(originalCollections);
    };

    const handleCommunityClick = (community) => {
        navigate(`/Dspace/Community/CommunityDetail/${community.communityId}`);
    };

    const handleCollectionClick = useCallback((collection) => {
        {
            (navigate(`/Dspace/Collection/CollectionDetail/${collection.collectionId}`))
        }

    }, []);

    const handleNextPageCommunities = () => {
        setCurrentPageCommunities(prevPage => prevPage + 1);
    };

    const handlePrevPageCommunities = () => {
        setCurrentPageCommunities(prevPage => prevPage - 1);
    };

    function getRelativePath(fullPath) {
        const splitPath = fullPath.split('public\\');
        return splitPath[1];
    }


    const handleRedirectStats = () => {
        navigate(`/Dspace/StatisticDetail?communityId=${communityId}`);
    };
    const handleSubscribe = (collectionId) => {
        axios.post(`${process.env.REACT_APP_BASE_URL}/api/SubcribeUser/SubcribeCollection?collectionId=${collectionId}`, null, {
            headers: { Authorization: header }
        })
            .then(response => {
                console.log('Subscribed successfully!', response.data);
                setSubscriptions(prev => ({ ...prev, [collectionId]: true }));
                // alert('Subscribed successfully');
                addNotification(response.data, 'success');
            })
            .catch(error => {
                console.error('Error subscribing to collection:', error);
                // alert('Failed to subscribe');
                addNotification(error.response.data, 'error');
                // console.log(error)
            });
    };
    
    function Status({ status }) {
        const getStatusText = (status) => {
            if (status == true) {
                return "Published";
            } else {
                return "Undiscoverable";
            }
        };

        const getStatusColor = (status) => {
            if (status == true) {
                return "#28A745";
            } else {
                return "#DC3545";
            }
        };
        const getBoxShadow = (status) => {
            const color = getStatusColor(status);
            return `0 0 10px 2px ${color}`;
        };
        return (
            <p style={{ margin: '0' }}>
                <span style={{
                    margin: '0',
                    backgroundColor: getStatusColor(status),
                    borderRadius: '3px',
                    color: 'white',
                    padding: '2px',
                    fontSize: 'small',
                    boxShadow: getBoxShadow(status)

                }}>
                    {getStatusText(status)}
                </span>
            </p>
        );
    }
    const handleUnsubscribe = (collectionId) => {
        axios.post(`${process.env.REACT_APP_BASE_URL}/api/SubcribeUser/UnSubcribeCollection?collectionId=${collectionId}`, null, {
            headers: { Authorization: header }
        })
            .then(response => {
                console.log('Unsubscribed successfully!', response.data);
                setSubscriptions(prev => ({ ...prev, [collectionId]: false }));
                // alert('Unsubscribed successfully');
                addNotification(response.data, 'success');
            })
            .catch(error => {
                console.error('Error unsubscribing to collection:', error);
                // alert('Failed to unsubscribe');
                addNotification(error.response.data, 'error');
            });
    };

    if (loading) {
        return (
            <div>
                <Header />
                <div className="container mt-5 d-flex justify-content-center align-items-center" style={{ height: '60vh' }}>
                    <div class="load-4">
                        {/* <p>Loading 4</p> */}
                        <div class="ring-1"></div>
                    </div>
                </div>
            </div>
        );
    }

    return (
        <div>
            <Header />
            <Helmet>
                <meta charSet="utf-8" />
                <title>{`${community?.communityName}`} - dSPACE</title>
            </Helmet>
            {community !== null && (
                <Counter communityId={community.communityId} />
            )}
            <div className="container mt-5">
                {/* <div className="py-1 bg-light">
                    <Link to="/HomepageAdmin" className="text-decoration-none">Home </Link>
                    <span className="text-dark"><strong>| Community Detail</strong></span>
                </div> */}

                {/* {message && <div className={`alert alert-${messageType}`}>{message}</div>} */}
                <h1 className="mb-4">Community Details
                    <Button
                        onClick={handleRedirectStats}
                        className='ms-2'
                        startIcon={<AssessmentIcon />}
                        sx={{
                            color: 'primary',
                            border: 'none',
                            boxShadow: 'none',
                            '&:hover': {
                                border: 'none',
                                boxShadow: 'none',
                            }
                        }}
                    >
                        Statistics
                    </Button>
                    {/* <Noti /> */}

                </h1>
                {/* <ToastContainer style={{width: '600px'}} position="top-right" newestOnTop /> */}

                <div className="card">
                    <div className="card-body">
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Name:</div>
                            <div className="col-sm-6">{community?.communityName}</div>
                        </div>
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Short Description:</div>
                            <div className="col-sm-6">{community?.shortDescription}</div>
                        </div>
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Parent community:</div>
                            <div className="col-sm-6">{community?.parentCommunityName}</div>
                        </div>
                        {(userRole === 'STAFF' || userRole === 'ADMIN') && (
                            <>
                                {community?.communityGroupDTOForDetails?.length > 0 && (
                                    <div className="row border-bottom pb-2 my-3">
                                        <div className="col-sm-2 text-end">Group Names:</div>
                                        <div className="col-sm-6">
                                            {community.communityGroupDTOForDetails.map(group => (
                                                <div key={group.id}>{group.groupName}</div>
                                            ))}
                                        </div>
                                    </div>
                                )}
                                <div className="row border-bottom pb-2 my-3">
                                    <div className="col-sm-2 text-end">Created By:</div>
                                    <div className="col-sm-6 ">{community?.createBy}</div>
                                </div>
                                <div className="row border-bottom pb-2 my-3">
                                    <div className="col-sm-2 text-end">Updated By:</div>
                                    <div className="col-sm-6">{community?.updateBy}</div>
                                </div>
                                <div className="row border-bottom pb-2 my-3">
                                    <div className="col-sm-2 text-end">Create Time:</div>
                                    <div className="col-sm-6 ">{new Date(community?.createTime).toLocaleString()}</div>
                                </div>
                                <div className="row border-bottom pb-2 my-3">
                                    <div className="col-sm-2 text-end">Update Time:</div>
                                    <div className="col-sm-6 ">{new Date(community?.updateTime).toLocaleString()}</div>
                                </div>
                                <div className="row pb-2 my-3">
                                    <div className="col-sm-2 text-end">Active:</div>
                                    <div className="col-sm-6"><Status status={community.isActive} /></div>

                                </div>
                            </>


                        )}
                        <div className="row border-bottom pb-2 my-3">
                            <div className="col-sm-2 text-end">Logo:</div>
                            <div className="col-sm-6">

                                {community.logoUrl ? (
                                    <img
                                        src={`/${getRelativePath(community.logoUrl)}`}
                                        alt={community.communityName}
                                        style={{ width: '200px', marginRight: '20px', boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1)' }}
                                        onError={(e) => {
                                            e.target.onerror = null;
                                            e.target.src = '/No-Image-Placeholder.png';
                                        }}
                                    />
                                ) : (
                                    <img
                                        src={`/No-Image-Placeholder.png`}
                                        alt={community.communityName}
                                        style={{ width: '150px', height: '200px', objectFit: 'cover', marginRight: '20px', boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1)' }}

                                    />
                                )}
                            </div>
                        </div>

                    </div>
                </div>
                <div className="mt-4">
                    {(userRole === "ADMIN" || (userRole === "STAFF" && community?.canEdit)) && (
                        <>
                            <button className="btn btn-primary me-2" onClick={() => navigate(`/Dspace/Community/EditCommunity/${community?.communityId}`)}>Edit</button>
                            {/* <button className="btn btn-danger me-2" onClick={handleDelete}>Delete</button> */}
                            <DeleteButtonWithModal handleDelete={handleDelete} />
                        </>
                    )}

                    <button className="btn btn-secondary" onClick={() => navigate('/Dspace/Community/CommunityList')}>Back to List</button>
                </div>
                <br></br>
                <div className='d-flex mb-4'>
                    {buttons.map((buttonName, index) => (
                        <div
                            key={index}
                            className="btn"
                            style={{
                                border: '1px solid black',
                                borderRadius: index === 0 ? '4px 0 0 4px' : index === buttons.length - 1 ? '0 4px 4px 0' : '0',
                                padding: '13px',
                                fontWeight: 'bolder',
                                backgroundColor: activeButton === buttonName ? '#43515F' : 'white',
                                color: activeButton === buttonName ? 'white' : '#207698',
                                cursor: 'pointer'
                            }}
                            onClick={() => handleButtonClick(buttonName)}
                        >
                            {buttonName}
                        </div>
                    ))}
                </div>
                {activeButton === 'By Name' && (
                    <div className="d-flex mb-4">
                        <input
                            type="text"
                            className="form-control w-50"
                            placeholder="Search By Name"
                            style={{ borderRadius: '0 0 0 0', flex: '0 1 auto' }}
                            value={searchQuery}
                            onChange={handleSearchChange}
                        />
                        <button
                            style={{ backgroundColor: '#43515F', color: 'white', borderRadius: '0 6px 6px 0' }}
                            className="btn"
                            onClick={performSearch}
                        >
                            <Search />
                        </button>
                    </div>
                )}
                {activeButton === 'By Description' && (
                    <div className="d-flex mb-4">
                        <input
                            type="text"
                            className="form-control w-50"
                            placeholder="Search By Description"
                            style={{ borderRadius: '0 0 0 0', flex: '0 1 auto' }}
                            value={searchQuery}
                            onChange={handleSearchChange}
                        />
                        <button
                            style={{ backgroundColor: '#43515F', color: 'white', borderRadius: '0 6px 6px 0' }}
                            className="btn"
                            onClick={performSearch}
                        >
                            <Search />
                        </button>
                    </div>
                )}

                <h3>Sub-Communities of {community.communityName}</h3>
                {((userRole === "STAFF" && community.canEdit) || (userRole === "ADMIN")) && (
                    <div>
                        <span className="add-field-container mb-2" onClick={() => navigate(`/Dspace/Community/CreateCommunity`)}>
                            <AddRounded className="add-rounded-icon" />
                            <span className="add-field-text">Create new sub-community</span>
                        </span>
                    </div>
                )}
                <ul className="list-group">
                    {currentCommunitiesPaginated.map((community, index) => (
                        <li key={index} className="list-group-item clickable-row border-0">
                            <div className="d-flex justify-content-between align-items-center">
                                <div className="d-flex">
                                    {community.logoUrl ? (
                                        // <img src={`/${getRelativePath(community.logoUrl)}`} alt="logo" className='img-thumbnail' style={{ width: '200px' }} />
                                        <img
                                            src={`/${getRelativePath(community.logoUrl)}`}
                                            alt={community.communityName}
                                            style={{ width: '150px', height: '200px', objectFit: 'cover', marginRight: '20px', boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1)' }}
                                            onError={(e) => {
                                                e.target.onerror = null;
                                                e.target.src = '/No-Image-Placeholder.png';
                                            }}
                                        />
                                    ) : (
                                        <img
                                            src={`/No-Image-Placeholder.png`}
                                            alt={community.communityName}
                                            style={{ width: '150px', height: '200px', objectFit: 'cover', marginRight: '20px', boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1)' }}

                                        />
                                    )}
                                    <div style={{}}>
                                        <div className='d-flex' style={{ alignItems: 'center' }}>
                                            {(userRole === "STAFF" || userRole === "ADMIN") && (
                                                <Status status={community?.isActive} style={{ display: 'flex', alignItems: 'center' }} />
                                            )}
                                            {(userRole === "STAFF" && community.canEdit) && (
                                                <Tooltip color='success' placement="top" title="You are assigned to this community">
                                                    <IconButton style={{ display: 'flex', alignItems: 'center' }}>
                                                        <CheckCircleIcon />
                                                    </IconButton>
                                                </Tooltip>
                                            )}
                                            {(userRole === "STAFF" && !community.canEdit) && (
                                                <Tooltip placement="top" title="You are not assigned to this community">
                                                    <IconButton color='error' style={{ display: 'flex', alignItems: 'center' }}>
                                                        <DangerousIcon />
                                                    </IconButton>
                                                </Tooltip>
                                            )}
                                        </div>


                                        <h3 style={{ margin: '0' }}>
                                            <span className="author-item" style={{ cursor: 'pointer' }} onClick={() => handleCommunityClick(community)}>{community.communityName}</span>
                                        </h3>
                                        {/* <p className="mb-0">{community.shortDescription}</p> */}
                                        <div>
                                            {expandedAbstracts[`community_${community.communityId}`] ? (
                                                <div>
                                                    <div>{community.shortDescription}</div>
                                                    <button onClick={() => toggleAbstract(`community_${community.communityId}`)} style={{ background: 'none', color: '#207f9e', border: 'none', padding: '0', cursor: 'pointer', fontWeight: 'bolder' }}>
                                                        <ExpandLess /> Collapse
                                                    </button>
                                                </div>
                                            ) : (
                                                <div>
                                                    <div style={{ overflow: 'hidden', textOverflow: 'ellipsis', display: '-webkit-box', WebkitLineClamp: 5, WebkitBoxOrient: 'vertical' }}>
                                                        {community.shortDescription}
                                                    </div>
                                                    {community.shortDescription.split(' ').length > 20 && (
                                                        <button onClick={() => toggleAbstract(`community_${community.communityId}`)} style={{ background: 'none', color: '#207f9e', border: 'none', padding: '0', cursor: 'pointer', fontWeight: 'bolder' }}>
                                                            <ExpandMore /> Show more
                                                        </button>
                                                    )}
                                                </div>
                                            )}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    ))}
                </ul>

                <div className="d-flex justify-content-center align-items-center">
                    <button
                        className="btn btn-outline-primary mx-2"
                        onClick={handlePrevPageCommunities}
                        disabled={currentPageCommunities === 1}
                    >
                        Prev
                    </button>
                    <span className="btn btn-outline-primary mx-2">
                        {currentPageCommunities}
                    </span>
                    <button
                        className="btn btn-outline-primary mx-2"
                        onClick={handleNextPageCommunities}
                        disabled={indexOfLastCommunity >= communities.length}
                    >
                        Next
                    </button>
                </div>
                <div className="d-flex justify-content-center mt-2">
                    Page {currentPageCommunities} of {totalPagesCommunities}
                </div>
            </div>


            <div className="container mt-5">
                <div className='d-flex mb-4'>
                    {buttonCollection.map((buttonName, index) => (
                        <div
                            key={index}
                            className="btn"
                            style={{
                                border: '1px solid black',
                                borderRadius: index === 0 ? '4px 0 0 4px' : index === buttonCollection.length - 1 ? '0 4px 4px 0' : '0',
                                padding: '13px',
                                fontWeight: 'bolder',
                                backgroundColor: activeButtonCollection === buttonName ? '#43515F' : 'white',
                                color: activeButtonCollection === buttonName ? 'white' : '#207698',
                                cursor: 'pointer'
                            }}
                            onClick={() => handleButtonColClick(buttonName)}
                        >
                            {buttonName}
                        </div>
                    ))}
                </div>
                {activeButtonCollection === 'By Name' && (
                    <div className="d-flex mb-4">
                        <input
                            type="text"
                            className="form-control w-50"
                            placeholder="Search By Name"
                            style={{ borderRadius: '0 0 0 0', flex: '0 1 auto' }}
                            value={collectionSearchQuery}
                            onChange={handleCollectionSearchChange}
                        />
                        <button
                            style={{ backgroundColor: '#43515F', color: 'white', borderRadius: '0 6px 6px 0' }}
                            className="btn"
                            onClick={performCollectionSearch}
                        >
                            <Search />
                        </button>
                    </div>
                )}

                {activeButtonCollection === 'By Description' && (
                    <div className="d-flex mb-4">
                        <input
                            type="text"
                            className="form-control w-50"
                            placeholder="Search By Description"
                            style={{ borderRadius: '0 0 0 0', flex: '0 1 auto' }}
                            value={collectionSearchQuery}
                            onChange={handleCollectionSearchChange}
                        />
                        <button
                            style={{ backgroundColor: '#43515F', color: 'white', borderRadius: '0 6px 6px 0' }}
                            className="btn"
                            onClick={performCollectionSearch}
                        >
                            <Search />
                        </button>
                    </div>
                )}
                <div className="container mt-5">
                    <h3>Collections of {community.communityName}</h3>
                    {((userRole === "STAFF" && community.canEdit) || (userRole === "ADMIN")) && (
                        // <div className="">
                        //     <Link to="/Dspace/Community/CreateCommunity" className="btn btn-primary">Create new community</Link>
                        // </div>
                        <div>
                            <span className="add-field-container mb-2" onClick={() => navigate(`/Dspace/Collection/CreateCollection`)}>
                                <AddRounded className="add-rounded-icon" />
                                <span className="add-field-text">Create new collection</span>
                            </span>
                        </div>
                    )}
                    <ul className="list-group">
                        {currentCollectionsPaginated.map((collection, index) => (
                            <li key={collection.collectionId} className="list-group-item clickable-row border-0">
                                <div className="d-flex justify-content-between align-items-center">
                                    <div className="d-flex">

                                        {collection.logoUrl ? (
                                            // <img src={`/${getRelativePath(community.logoUrl)}`} alt="logo" className='img-thumbnail' style={{ width: '200px' }} />
                                            <img
                                                src={`/${getRelativePath(collection.logoUrl)}`}
                                                alt={collection.collectionName}
                                                style={{ width: '150px', height: '200px', objectFit: 'cover', marginRight: '20px', boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1)' }}
                                                onError={(e) => {
                                                    e.target.onerror = null;
                                                    e.target.src = '/No-Image-Placeholder.png';
                                                }}
                                            />
                                        ) : (
                                            <img
                                                src={`/No-Image-Placeholder.png`}
                                                alt={collection.collectionName}
                                                style={{ width: '150px', height: '200px', objectFit: 'cover', marginRight: '20px', boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1)' }}

                                            />
                                        )}

                                        <div style={{}}>
                                        <div className='d-flex' style={{ alignItems: 'center' }}>
                                            {(userRole === "STAFF" || userRole === "ADMIN") && (
                                                <Status status={collection?.isActive} style={{ display: 'flex', alignItems: 'center' }} />
                                            )}
                                            {(userRole === "STAFF" && collection.canEdit) && (
                                                <Tooltip color='success' placement="top" title="You are assigned to this collection">
                                                    <IconButton style={{ display: 'flex', alignItems: 'center' }}>
                                                        <CheckCircleIcon />
                                                    </IconButton>
                                                </Tooltip>
                                            )}
                                            {(userRole === "STAFF" && !collection.canEdit) && (
                                                <Tooltip placement="top" title="You are not assigned to this collection">
                                                    <IconButton color='error' style={{ display: 'flex', alignItems: 'center' }}>
                                                        <DangerousIcon />
                                                    </IconButton>
                                                </Tooltip>
                                            )}
                                        </div>

                                            <h3 style={{ margin: '0' }}>
                                                <Badge sx={{
                                                    '& .MuiBadge-badge': {
                                                        transform: 'translateX(25px)',
                                                    },
                                                }} color="primary" badgeContent={collection.totalItems} showZero max={999}>
                                                    <span className="author-item" style={{ cursor: 'pointer' }} onClick={() => handleCollectionClick(collection)}>{collection.collectionName}</span>
                                                </Badge>
                                            </h3>
                                            {/* <h1>{collection.totalItems}</h1> */}
                                            {/* <p className="mb-0">{community.shortDescription}</p> */}

                                            <div>
                                                {expandedAbstracts[`collection_${collection.collectionId}`] ? (
                                                    <div>
                                                        <div>{collection.shortDescription}</div>
                                                        <button onClick={() => toggleAbstract(`collection_${collection.collectionId}`)} style={{ background: 'none', color: '#207f9e', border: 'none', padding: '0', cursor: 'pointer', fontWeight: 'bolder' }}>
                                                            <ExpandLess /> Collapse
                                                        </button>
                                                    </div>
                                                ) : (
                                                    <div>
                                                        <div style={{ overflow: 'hidden', textOverflow: 'ellipsis', display: '-webkit-box', WebkitLineClamp: 3, WebkitBoxOrient: 'vertical' }}>
                                                            {collection.shortDescription}
                                                        </div>
                                                        {collection.shortDescription.split(' ').length > 20 && (
                                                            <button onClick={() => toggleAbstract(`collection_${collection.collectionId}`)} style={{ background: 'none', color: '#207f9e', border: 'none', padding: '0', cursor: 'pointer', fontWeight: 'bolder' }}>
                                                                <ExpandMore /> Show more
                                                            </button>
                                                        )}
                                                    </div>
                                                )}
                                            </div>
                                            {(userRole === 'STUDENT' || userRole === 'LECTURER') && (
                                                <>
                                                    <div>
                                                        {subscriptions[collection.collectionId] ? (
                                                            <button className="btn btn-secondary mt-2" onClick={() => handleUnsubscribe(collection.collectionId)}>Unsubscribe</button>
                                                        ) : (
                                                            <button className="btn btn-danger mt-2" onClick={() => handleSubscribe(collection.collectionId)}>Subscribe</button>
                                                        )}
                                                    </div>
                                                </>
                                            )}
                                        </div>
                                    </div>
                                </div>
                            </li>
                        ))}
                    </ul>
                    <div className="d-flex justify-content-center align-items-center">
                        <button
                            className="btn btn-outline-primary mx-2"
                            onClick={handlePrevPageCollections}
                            disabled={currentPageCollections === 1}
                        >
                            Prev
                        </button>
                        <span className="btn btn-outline-primary mx-2">
                            {currentPageCollections}
                        </span>
                        <button
                            className="btn btn-outline-primary mx-2"
                            onClick={handleNextPageCollections}
                            disabled={indexOfLastCollection >= collections.length}
                        >
                            Next
                        </button>
                    </div>
                    <div className="d-flex justify-content-center mt-2">
                        Page {currentPageCollections} of {totalPagesCollections}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CommunityDetails;
