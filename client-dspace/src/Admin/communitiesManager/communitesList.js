import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from '../../components/Header/Header';
import { Helmet } from 'react-helmet';
import {
    ExpandLess, ExpandMore
} from '@material-ui/icons';
import { toast } from 'react-toastify';
import { jwtDecode } from 'jwt-decode';

const CommunitiesList = () => {
    const [communities, setCommunities] = useState([]);
    const [searchQuery, setSearchQuery] = useState('');
    const [searchType, setSearchType] = useState(''); // State to track the current search type
    const [error, setError] = useState(null);
    const [currentPage, setCurrentPage] = useState(1);
    const communitiesPerPage = 12;
    const token = localStorage.getItem('Token');
    const userRole = token ? jwtDecode(token)["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"] : null;
    const header = `Bearer ${token}`;
    const [expandedAbstracts, setExpandedAbstracts] = useState({});
    const toggleAbstract = (itemId) => {
        setExpandedAbstracts(prevState => ({
            ...prevState,
            [itemId]: !prevState[itemId]
        }));
    };
    const addNotification = (message, type) => {
        toast(message, { type });
    };
    useEffect(() => {
        if (userRole === "ADMIN") {
            // Fetch data from the API
            axios.get(`${process.env.REACT_APP_BASE_URL}/api/Community/getListOfCommunities`)
                .then(response => {
                    setCommunities(response.data);
                })
                .catch(error => {
                    console.error('There was an error fetching the communities!', error);
                    setError(error.message);
                    addNotification('There was an error fetching the communities!', 'error');
                });
        } else if (userRole === "STAFF") {
            axios.get(`${process.env.REACT_APP_BASE_URL}/api/CommunityStaff/GetAllCommunities`, {
                headers: {
                    Authorization: header
                }
            })
                .then(response => {
                    setCommunities(response.data);
                    if(response.data.length == 0){
                        addNotification('You are not assigned any community', 'error');
                    }
                })
                .catch(error => {
                    setError("You are not assigned any community");
                    addNotification('You are not assigned any community', 'error');
                });
        }
    }, []);

    const handleSearchChange = (e) => {
        setSearchQuery(e.target.value);
        setCurrentPage(1); // Reset to the first page when searching
    };

    const handleSearchTypeChange = (e) => {
        setSearchType(e.target.value);
        setSearchQuery(''); // Clear search query when changing search type
    };

    const filteredCommunities = communities.filter(community => {
        const normalizedSearchQuery = searchQuery.trim().toLowerCase(); // Normalize and trim search query

        if (!normalizedSearchQuery) return true; // Return all communities if search query is empty

        const searchKeywords = normalizedSearchQuery.split(/\s+/); // Split search query by whitespace

        switch (searchType) {
            case 'communityName':
                return searchKeywords.every(keyword =>
                    community.communityName.toLowerCase().includes(keyword)
                );
            case 'shortDescription':
                return searchKeywords.every(keyword =>
                    community.shortDescription.toLowerCase().includes(keyword)
                );
            default:
                return (
                    searchKeywords.some(keyword =>
                        community.communityName.toLowerCase().includes(keyword)
                    ) ||
                    searchKeywords.some(keyword =>
                        community.shortDescription.toLowerCase().includes(keyword)
                    )
                );
        }
    });

    // Pagination logic
    const indexOfLastCommunity = currentPage * communitiesPerPage;
    const indexOfFirstCommunity = indexOfLastCommunity - communitiesPerPage;
    const currentCommunities = filteredCommunities.slice(indexOfFirstCommunity, indexOfLastCommunity);

    const totalPages = Math.ceil(filteredCommunities.length / communitiesPerPage);

    const paginate = (pageNumber) => setCurrentPage(pageNumber);

    const handleNext = () => {
        if (currentPage < totalPages) {
            setCurrentPage(currentPage + 1);
        }
    };

    const handlePrevious = () => {
        if (currentPage > 1) {
            setCurrentPage(currentPage - 1);
        }
    };

    function getRelativePath(fullPath) {
        const splitPath = fullPath.split('public\\');
        return splitPath[1];
    }
    function Status({ status }) {
        const getStatusText = (status) => {
            if (status == true) {
                return "Published";
            } else {
                return "Undiscoverable";
            }
        };

        const getStatusColor = (status) => {
            if (status == true) {
                return "#28A745";
            } else {
                return "#DC3545";
            }
        };
        const getBoxShadow = (status) => {
            const color = getStatusColor(status);
            return `0 0 10px 2px ${color}`;
        };
        return (
            <p style={{ margin: '0' }}>
                <span style={{
                    margin: '0',
                    backgroundColor: getStatusColor(status),
                    borderRadius: '3px',
                    color: 'white',
                    padding: '2px',
                    fontSize: 'small',
                    boxShadow: getBoxShadow(status)

                }}>
                    {getStatusText(status)}
                </span>
            </p>
        );
    }
    return (
        <div>
            <Header />
            <Helmet>
                <meta charSet="utf-8" />
                <title>List of Communities - dSPACE</title>
            </Helmet>
            <div className="container mt-5">
                {/* <div className="py-1 bg-light">
                    <Link to="/HomepageAdmin" className="text-decoration-none">Home </Link>
                    <span className="text-dark" ><strong>| Community</strong></span>
                </div> */}
                <h1 className="mb-4">Communities List</h1>
                <div className="row mb-4">
                    <div className="col-md-5">
                        <label htmlFor="searchInput" className="form-label fw-bold">Search</label>
                        <input
                            id="searchInput"
                            type="text"
                            className="form-control"
                            placeholder="Search by community name"
                            value={searchQuery}
                            onChange={handleSearchChange}
                        />
                    </div>
                    <div className="col-md-4">
                        <label htmlFor="searchTypeSelect" className="form-label fw-bold">Search By</label>
                        <select
                            id="searchTypeSelect"
                            className="form-select"
                            value={searchType}
                            onChange={handleSearchTypeChange}
                        >
                            <option value="">Select...</option>
                            <option value="communityName">Community Name</option>
                            <option value="shortDescription">Short Description</option>
                        </select>
                    </div>
                    {((userRole === "STAFF" && communities.length > 0) || (userRole === "ADMIN")) && (
                        <div className="col-md-3 d-flex align-items-end justify-content-end">
                            <Link to="/Dspace/Community/CreateCommunity" className="btn btn-primary">Create new community</Link>
                        </div>)}
                </div>
                <ul className="list-group">
                    {currentCommunities.map((community, index) => (
                        <li key={index} className="list-group-item clickable-row border-0">
                            <div className="d-flex justify-content-between align-items-center">
                                <div className="d-flex">
                                    {community.logoUrl ? (
                                        // <img src={`/${getRelativePath(community.logoUrl)}`} alt="logo" className='img-thumbnail' style={{ width: '200px' }} />
                                        <img
                                            src={`/${getRelativePath(community.logoUrl)}`}
                                            alt={community.communityName}
                                            style={{ width: '150px', height: '200px', objectFit: 'cover', marginRight: '20px', boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1)' }}
                                            onError={(e) => {
                                                e.target.onerror = null;
                                                e.target.src = '/No-Image-Placeholder.png';
                                              }}
                                        />
                                    ) : (
                                        <img
                                            src={`/No-Image-Placeholder.png`}
                                            alt={community.communityName}
                                            style={{ width: '150px', height: '200px', objectFit: 'cover', marginRight: '20px', boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1)' }}

                                        />
                                    )}
                                    <div style={{}}>
                                    <Status status={community.isActive} />
                                        <h3 style={{ margin: '0' }}>
                                            <Link style={{ textDecoration: 'none' }} to={`/Dspace/Community/CommunityDetail/${community.communityId}`}>

                                                <span className="author-item" style={{ cursor: 'pointer' }} >{community.communityName}</span>
                                            </Link>
                                        </h3>
                                        {/* <p className="mb-0">{community.shortDescription}</p> */}
                                        <div>
                                            {expandedAbstracts[`community_${community.communityId}`] ? (
                                                <div>
                                                    <div>{community.shortDescription}</div>
                                                    <button onClick={() => toggleAbstract(`community_${community.communityId}`)} style={{ background: 'none', color: '#207f9e', border: 'none', padding: '0', cursor: 'pointer', fontWeight: 'bolder' }}>
                                                        <ExpandLess /> Collapse
                                                    </button>
                                                </div>
                                            ) : (
                                                <div>
                                                    <div style={{ overflow: 'hidden', textOverflow: 'ellipsis', display: '-webkit-box', WebkitLineClamp: 5, WebkitBoxOrient: 'vertical' }}>
                                                        {community.shortDescription}
                                                    </div>
                                                    {community.shortDescription.split(' ').length > 20 && (
                                                        <button onClick={() => toggleAbstract(`community_${community.communityId}`)} style={{ background: 'none', color: '#207f9e', border: 'none', padding: '0', cursor: 'pointer', fontWeight: 'bolder' }}>
                                                            <ExpandMore /> Show more
                                                        </button>
                                                    )}
                                                </div>
                                            )}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    ))}
                </ul>
                <nav>
                    <ul className="pagination justify-content-center">
                        <li className={`page-item ${currentPage === 1 ? 'disabled' : ''}`}>
                            <button className="page-link" onClick={handlePrevious}>
                                Previous
                            </button>
                        </li>
                        {Array.from({ length: totalPages }, (_, index) => (
                            <li key={index + 1} className={`page-item ${index + 1 === currentPage ? 'active' : ''}`}>
                                <button className="page-link" onClick={() => paginate(index + 1)}>
                                    {index + 1}
                                </button>
                            </li>
                        ))}
                        <li className={`page-item ${currentPage === totalPages ? 'disabled' : ''}`}>
                            <button className="page-link" onClick={handleNext}>
                                Next
                            </button>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    );
};

export default CommunitiesList;
