import React, { useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import './homepage.css';
import HeaderAdmin from '../components/Header/Header-admin';
import { Search } from '@material-ui/icons'; // Assuming you're using Material-UI icons
import { Helmet } from 'react-helmet';
import { jwtDecode } from 'jwt-decode';

const HomepageAdmin = () => {
    const [communities, setCommunities] = useState([]);
    const [searchQuery, setSearchQuery] = useState('');
    const [decodedRole, setDecodedRole] = useState(null)
    const navigate = useNavigate();

    const token = localStorage.getItem('Token');
    

    useEffect(() => {
        if(token !== null){
            setDecodedRole(jwtDecode(token)["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"]);
        }

        const fetchCommunities = async () => {
            try {
                const responseCommunities = await fetch(`${process.env.REACT_APP_BASE_URL}/api/Community/GetCommunityParent`);
                const dataCommunities = await responseCommunities.json();
                setCommunities(dataCommunities);
            } catch (error) {
                console.error('Error fetching communities:', error);
            }
        };

        fetchCommunities();
    }, []);

    const handleSearchChange = (event) => {
        setSearchQuery(event.target.value);
    };

    const handleSearchSubmit = () => {
        navigate(`/Dspace/Collection/ItemList/0?search=${searchQuery}`);
    };

    return (
        <div>
            <Helmet>
                <meta charSet="utf-8" />
                <title>Homepage for Admin - dSPACE</title>
            </Helmet>
            <HeaderAdmin />
            <img
                src="FPT.jpg"
                alt="Background"
                className="background-image"
            />

            <div className="content-container">
                <div className="container">
                    <div className="row">
                        <div className="col-12 mt-5">
                            <p style={{ margin: '0', fontWeight: 'bold' }}>Search items by title</p>
                            <div className="search-container d-flex align-items-center mb-4">

                                <input
                                    type="text"
                                    className="form-control w-50"
                                    placeholder="Search items by title"
                                    style={{ borderRadius: '0', flex: '0 1 auto' }}
                                    value={searchQuery}
                                    onChange={handleSearchChange}
                                />
                                <button
                                    style={{ backgroundColor: '#43515F', color: 'white', borderRadius: '0 6px 6px 0' }}
                                    className="btn"
                                    onClick={handleSearchSubmit}
                                >
                                    <Search />
                                </button>
                            </div>

                            <h1>Communities in DSpace</h1>
                            <h4>Select a community to browse its collections</h4>
                            <ul>
                                {communities.map((community, index) => (
                                    <li key={index}>
                                        <Link to={`/Dspace/Community/CommunityDetail/${community.communityId}`} className="community-link">
                                            {community.communityName}
                                        </Link>
                                    </li>
                                ))}
                            </ul>
                        </div>
                    </div>

                    <hr className="separator-line" />

                </div>
            </div>

            <div className="ft">
                <div className="container">
                    <div className="row">
                        <div className="col-12 text-center">
                            <span>&copy; 2023 - FPT Education | <Link to="#">About us</Link></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default HomepageAdmin;
