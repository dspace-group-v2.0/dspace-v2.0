import Login from "./components/login";
import HomepageAdmin from "./Admin/homepage";
import HomepageStaff from "./Staff/homepageStaff";
import HomepageUser from "./User/homepageUser";
import ListItems from "./Admin/Items/listItem";
import ListItemsReader from "./StudentAndLecture/itemListReader.js";
import ListItemsStaff from "./Staff/ItemStaff/listItemStaff.js";
import ItemDetails from "./Admin/Items/itemDetails";
import ItemDetailReader from "./StudentAndLecture/itemDetailReader.js";
import ItemDetailsStaff from "./Staff/ItemStaff/itemDetailsStaff.js";
import CreateItem from "./Admin/Items/createItem";
import CreateItemStaff from "./Staff/ItemStaff/createItemStaff.js";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import CollectionList from "./Admin/collectionManager/collectionList";
import CreateCollection from "./Admin/collectionManager/createCollection";
import CommunitiesList from "./Admin/communitiesManager/communitesList";
import CreateCommunity from "./Admin/communitiesManager/createCommunites";
import GroupList from "./Admin/groupManager/groupList";
import CreateGroup from "./Admin/groupManager/createGroup";
import GroupDetails from "./Admin/groupManager/groupDetail";
import GroupStaffDetails from "./Staff/groupStaffDetails.js";
import CollectionDetails from "./Admin/collectionManager/collectionDetail";
import EditGroup from "./Admin/groupManager/editGroup";
import EditCollection from "./Admin/collectionManager/editCollection";
import CommunityDetails from "./Admin/communitiesManager/communitesDetail";
import EditCommunity from "./Admin/communitiesManager/editCommunities";
import MetadataDetails from "./Admin/metadataManager/metadataDetail";
import ListAuthor from "./Admin/authorManager/listAuthor.js";
import Addauthor from "./Admin/authorManager/addAuthor.js";
import EditAuthor from "./Admin/authorManager/editAuthor.js";
import AuthorDetails from "./Admin/authorManager/detailAuthor.js";
import ListUser from "./Admin/peopleManager/listPeople.js";
import AddUser from "./Admin/peopleManager/addPeople.js";
import EditUser from "./Admin/peopleManager/editPeople.js";
import UserDetails from "./Admin/peopleManager/detailsPeople.js";
import AddUserToGroup from "./Admin/groupManager/addPeopletoGroup.js";
import PdfViewer from "./components/pdfViewer.js";
import ModifyItem from "./Admin/Items/modifyItemWithMetadata.js";
import ModifyItemStaff from "./Staff/ItemStaff/modifiItemStaff.js";
import GroupListStaff from "./Staff/groupStaff.js";
import React, { useEffect, useState } from 'react';
import HomepageReader from "./StudentAndLecture/homepageReader.js";
import AddCollectionGroup from "./Admin/groupManager/addCollectionGroup.js";
import SubscribedCollectionList from "./StudentAndLecture/SubscribedCollectionList.js";
import CollectionDetailsStaff from "./Staff/CollectionStaff/colecctionDetails.js";
import CollectionListStaff from "./Staff/CollectionStaff/collectionList.js";
import NotFound from "./components/notFound.js";
import StatisticPage from "./components/Statistics/statisticPage.js";
import CommunityDetailStaff from "./Staff/CommunityStaff/communityDetail.js";
import AddCommunityGroup from "./Admin/groupManager/addCommunityGroup.js";
import CommunityTree from "./components/communityTree.js";
import AdvanceSearch from "./Admin/Items/advanceSearch.js";
import { jwtDecode } from "jwt-decode";
function App() {
  const [token, setToken] = useState(localStorage.getItem('Token'));

  useEffect(() => {
    const handleStorageChange = () => {
      setToken(localStorage.getItem('Token'));
    };

    window.addEventListener("storageChanged", handleStorageChange);
  }, []);

  const role = token ? jwtDecode(token)["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"] : null;
  if (!token) {
    return (
      <Router>
        <Routes>
          <Route path="/" element={<HomepageUser />} />
          <Route path="/HomepageAdmin" element={<HomepageAdmin />} />
          <Route path="/HomepageStaff" element={<HomepageStaff />} />
          <Route path="/Homepage" element={<HomepageReader />} />
          <Route path="/Login" element={<Login />} />
          <Route path="*" element={<NotFound />} />
        </Routes>
      </Router>
    )
  }
  return (
    <Router>
      <Routes>
        {(role === "STUDENT" || role === "LECTURER") ? (
          <>
            <Route path="/" element={<HomepageUser />} />
            <Route path="/Homepage" element={<HomepageReader />} />
            <Route path="/Dspace/Community/CommunityDetail/:communityId" element={<CommunityDetails />} />
            <Route path="/DSpace/ItemDetail/:itemId" element={<ItemDetails />} />
            <Route path="/Dspace/Collection/ItemListReader/:collectionId" element={<ListItemsReader />} />
            <Route path="/Dspace/Collection/CollectionDetail/:collectionId" element={<CollectionDetails />} />
            <Route path="/Dspace/Metadata/MetadataDetail/:itemId" element={<MetadataDetails />} />
            <Route path="/DSpace/PdfViewer/:fileId" element={<PdfViewer />} />
            <Route path="/Dspace/Author/AuthorDetail/:authorId" element={<AuthorDetails />} />
            <Route path="/Dspace/SubscribedCollectionList" element={<SubscribedCollectionList />} />
            <Route path="/Dspace/StatisticDetail/" element={<StatisticPage />} />
            <Route path="/Dspace/AdvanceSearch/" element={<AdvanceSearch />} />
            <Route path="*" element={<NotFound />} />
          </>
        ) : role === "STAFF" ? (
          <>
            <Route path="/" element={<HomepageUser />} />
            <Route path="/HomepageStaff" element={<HomepageStaff />} />
            <Route path="/Dspace/Collection/ItemStaffList/:collectionId" element={<ListItemsStaff />} />
            <Route path="/DSpace/ItemDetail/:itemId" element={<ItemDetails />} />
            <Route path="/DSpace/ModifyItem/:itemId" element={<ModifyItem />} />
            <Route path="/DSpace/CreateItem/:collectionId" element={<CreateItem />} />
            <Route path='/Dspace/Collection/CollectionList' element={<CollectionList />} />
            <Route path='/Dspace/Collection/CreateCollection' element={<CreateCollection />} />
            <Route path="/Dspace/Collection/CollectionDetail/:collectionId" element={<CollectionDetails />} />
            <Route path="/Dspace/Collection/EditCollection/:collectionId" element={<EditCollection />} />
            <Route path="/Dspace/Community/CommunityList" element={<CommunitiesList />} />
            <Route path="/Dspace/Community/DirectoryTree" element={<CommunityTree />} />
            <Route path="/Dspace/Community/CreateCommunity" element={<CreateCommunity />} />
            <Route path="/Dspace/Community/CommunityDetail/:communityId" element={<CommunityDetails />} />
            <Route path="/Dspace/Community/EditCommunity/:communityId" element={<EditCommunity />} />
            <Route path="/Dspace/Metadata/MetadataDetail/:itemId" element={<MetadataDetails />} />
            <Route path="/Dspace/Group/GroupStaffList" element={<GroupListStaff />} />
            <Route path="/Dspace/Group/GroupStaffDetail/:groupId" element={<GroupStaffDetails />} />
            <Route path="/Dspace/Author/AuthorList" element={<ListAuthor />} />
            <Route path="/Dspace/Author/AddAuthor" element={<Addauthor />} />
            <Route path="/Dspace/Author/EditAuthor/:authorId" element={<EditAuthor />} />
            <Route path="/Dspace/Author/AuthorDetail/:authorId" element={<AuthorDetails />} />
            <Route path="/DSpace/PdfViewer/:fileId" element={<PdfViewer />} />
            <Route path="/Dspace/StatisticDetail/" element={<StatisticPage />} />
            <Route path="/Dspace/AdvanceSearch/" element={<AdvanceSearch />} />
            <Route path="*" element={<NotFound />} />
          </>
        ) : role === "ADMIN" ? (
          <>
            <Route path="/" element={<HomepageUser />} />
            <Route path="/HomepageAdmin" element={<HomepageAdmin />} />
            <Route path="/Dspace/Collection/ItemList/:collectionId" element={<ListItems />} />
            <Route path="/DSpace/ItemDetail/:itemId" element={<ItemDetails />} />
            <Route path="/DSpace/CreateItem/:collectionId" element={<CreateItem />} />
            <Route path='/Dspace/Collection/CollectionList' element={<CollectionList />} />
            <Route path='/Dspace/Collection/CreateCollection' element={<CreateCollection />} />
            <Route path="/Dspace/Collection/CollectionDetail/:collectionId" element={<CollectionDetails />} />
            <Route path="/Dspace/Collection/EditCollection/:collectionId" element={<EditCollection />} />
            <Route path="/Dspace/Community/CommunityList" element={<CommunitiesList />} />
            <Route path="/Dspace/Community/CreateCommunity" element={<CreateCommunity />} />
            <Route path="/Dspace/Community/DirectoryTree" element={<CommunityTree />} />
            <Route path="/Dspace/Community/CommunityDetail/:communityId" element={<CommunityDetails />} />
            <Route path="/Dspace/Community/EditCommunity/:communityId" element={<EditCommunity />} />
            <Route path="/Dspace/Metadata/MetadataDetail/:itemId" element={<MetadataDetails />} />
            <Route path="/Dspace/Group/GroupList" element={<GroupList />} />
            <Route path="/Dspace/Group/CreateGroup" element={<CreateGroup />} />
            <Route path="/Dspace/Group/GroupDetail/:groupId" element={<GroupDetails />} />
            <Route path="/Dspace/Group/EditGroup/:groupId" element={<EditGroup />} />
            <Route path="/Dspace/Group/UserToGroup/:groupId" element={<AddUserToGroup />} />
            <Route path="/Dspace/Group/CollectionToGroup/:groupId" element={<AddCollectionGroup />} />
            <Route path="/Dspace/Group/CommunityToGroup/:groupId" element={<AddCommunityGroup />} />
            <Route path="/Dspace/Author/AuthorList" element={<ListAuthor />} />
            <Route path="/Dspace/Author/AddAuthor" element={<Addauthor />} />
            <Route path="/Dspace/Author/EditAuthor/:authorId" element={<EditAuthor />} />
            <Route path="/Dspace/Author/AuthorDetail/:authorId" element={<AuthorDetails />} />
            <Route path="/Dspace/User/UserList" element={<ListUser />} />
            <Route path="/Dspace/User/CreateUser" element={<AddUser />} />
            <Route path="/Dspace/User/UserDetail/:peopleId" element={<UserDetails />} />
            <Route path="/Dspace/User/EditUser/:peopleId" element={<EditUser />} />
            <Route path="/DSpace/ModifyItem/:itemId" element={<ModifyItem />} />
            <Route path="/DSpace/PdfViewer/:fileId" element={<PdfViewer />} />
            <Route path="/Dspace/StatisticDetail/" element={<StatisticPage />} />
            <Route path="/Dspace/AdvanceSearch/" element={<AdvanceSearch />} />
            <Route path="*" element={<NotFound />} />
          </>
        ) : (
          <Route path="*" element={<NotFound />} />
        )}
      </Routes>
    </Router>
  )

}
export default App;