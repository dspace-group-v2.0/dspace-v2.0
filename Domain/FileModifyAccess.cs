using Shared.Enums;

namespace Domain;

public class FileModifyAccess
{
   public int FileAccessId { get; set; }
   public int FileId { get; set; }
   public FileAccessType AccessType { get; set; }
   public string Role { get; set; }
   public FileUpload File { get; set; }
}