namespace Domain;

public class CollectionGroup
{
   public int Id { get; set; }
   public int CollectionId { get; set; }
   public int GroupId { get; set; }
   public Group Group { get; set; }
   public Collection Collection { get; set; }
}