namespace Domain;

public class CommunityGroup
{
   public int Id { get; set; }
   public int CommunityId { get; set; }
   public int GroupId { get; set; }
   public Group Group { get; set; }
   public Community Community { get; set; }
}