namespace Domain;

public class Statistic
{
   public int StatisticId { get; set; }
   public int Year { get; set; }
   public int Month { get; set; }
   public int Day { get; set; }
   public int ViewOnDay { get; set; }
   public int? ItemId { get; set; }
   public int? CollectionId { get; set; }
   public int? CommunityId { get; set; }
   public string UserId { get; set; }
   public Collection Collection { get; set; }
   public Community Community { get; set; }
   public User User { get; set; }
   public Item Item { get; set; }
}