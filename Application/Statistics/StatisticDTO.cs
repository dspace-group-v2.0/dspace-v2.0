namespace Application.Statistics;

public class StatisticDTO
{
    public int? CommunityId { get; set; }
    public int? CollectionId { get; set; }
    public int? ItemId { get; set; }
    public int Month { get; set; }
    public int Year { get; set; }
    public int TotalCount { get; set; }
}