namespace Application.Statistics;

public class StatisticDTOForCollection
{
    public List<StatisticDTO> Collection { get; set; }
    public List<StatisticDTO> Item { get; set; }
}