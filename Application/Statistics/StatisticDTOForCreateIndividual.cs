namespace Application.Statistics;

public class StatisticDTOForCreateIndividual
{
    public int Year { get; set; }
    public int Month { get; set; }
    public int Day { get; set; }
    public int ViewOnDay { get; set; }
    public int? ItemId { get; set; }
    public int? CollectionId { get; set; }
    public int? CommunityId { get; set; }
    public string UserId { get; set; }
}