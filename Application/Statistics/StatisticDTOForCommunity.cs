namespace Application.Statistics;

public class StatisticDTOForCommunity
{
    public List<StatisticDTO> Community { get; set; }
    public List<StatisticDTO> Collection { get; set; }
    public List<StatisticDTO> Item { get; set; }
}