﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Collections
{
   public class CollectionDTOForSelectListOfUser
   {
      public int CollectionId { get; set; }
      public string LogoUrl { get; set; }
      public string CollectionName { get; set; }

      //public int? CommunityId { get; set; }

      //public string CommunityName { get; set; }
      public string ShortDescription { get; set; }

      public bool IsSubcribe {get;set; }
      public int TotalItems { get; set; }
   }
}
