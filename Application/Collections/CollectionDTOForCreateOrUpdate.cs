﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Application.Collections
{
   public class CollectionDTOForCreateOrUpdate
   {
      public string? LogoUrl { get; set; }
      [RegularExpression(@"^(?:(?!\b(ALTER|CREATE|DELETE|DROP|EXEC(UTE)?|INSERT(\s+INTO)?|MERGE|SELECT(\s+\*)?|UPDATE|UNION(\s+ALL)?)\b)[\S\s])*$",
        ErrorMessage = "Characters are not allowed.")]
      public string CollectionName { get; set; }
      [RegularExpression(@"^(?:(?!\b(ALTER|CREATE|DELETE|DROP|EXEC(UTE)?|INSERT(\s+INTO)?|MERGE|SELECT(\s+\*)?|UPDATE|UNION(\s+ALL)?)\b)[\S\s])*$",
        ErrorMessage = "Characters are not allowed.")]
      public string ShortDescription { get; set; }
      public int CommunityId { get; set; }
      public bool isActive { get; set; }
      public string? License { get; set; }
      public int EntityTypeId { get; set; }
      public IFormFile? File { get; set; }
   }
}
