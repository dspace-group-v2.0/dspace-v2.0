﻿using Shared.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Items
{
   public class ItemDTOForSearchAuthor
   {
      public List<string> ListAuthor { get; set; }
      public int CollectionId { get; set; }

      public bool? Discoverable { get; set; }

      public SortType SortType { get; set; }

   }
}
