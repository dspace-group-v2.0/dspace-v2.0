﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Metadatas;
using Microsoft.AspNetCore.Http;

namespace Application.Items
{
   public class ItemDTOForCreateSimple
   {
      public bool Discoverable { get; set; }
      public int CollectionId { get; set; }
      public string RoleFile { get; set; }
      public List<MetadataValueDTOForCreate> metadataValueDTO { get; set; }
      public List<IFormFile> multipleFiles { get; set; }
   }
}
