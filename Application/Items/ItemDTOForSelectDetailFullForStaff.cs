﻿using Application.Metadatas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Authors;
using Application.Collections;
using Application.FileUploads;
using Domain;
using Shared.Enums;

namespace Application.Items
{
   public class ItemDTOForSelectDetailFullForStaff
   {
      public int ItemId { get; set; }
      public DateTime LastModified { get; set; }
      public bool Discoverable { get; set; }
      public int? SubmitterId { get; set; }
      public int CollectionId { get; set; }
      public string CollectionName { get; set; }

      public bool CanEdit { get; set; }

      public CollectionDTOForSelectOfUser Collection { get; set; }
      public List<MetadataValueDTOForSelect> ListMetadataValueDTOForSelect { get; set; }
      public List<FileDTO> ListFilesDTOForSelect { get; set; }
      public List<AuthorDTOForSelectItem> ListAuthorDtoForSelect { get; set; }
      public List<AuthorDTOForSelectItem> ListAdvisorDtoForSelect { get; set; }
   }
}
