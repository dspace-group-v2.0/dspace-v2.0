﻿using Shared.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Application.Authors;
using Application.FileUploads;

namespace Application.Items
{
   public class ItemDTOForSelectDetailSimpleForStaff
   {
      public int ItemId { get; set; }
      //no 1
      public DateTime LastModified { get; set; }
      public bool Discoverable { get; set; }
      public int SubmitterId { get; set; }

      public string Email { get; set; }
      public List<string> Authors { get; set; }
      // 57
      public string Title { get; set; }
      //12
      public string  DateOfIssue { get; set; }
      //36
      public string? Publisher { get; set; }
      //51 
      public List<string>? SubjectKeywords { get; set; }
      //15
      public string? Abstract { get; set; }
    //34
      public string? IdentifierUri { get; set; }
      //14
      public string? Description { get; set; }
      public int CommunityId { get; set; }
      public int CollectionId { get; set; }
      public string? CollectionName { get; set; }

      public bool CanEdit { get; set; }
      public List<FileDTO> File { get; set; }
      public List<AuthorDTOForSelectItem>? AuthorItems  { get; set; }

   }
}
