﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Shared.Enums;

namespace Application.Items
{
   public class ItemDtoForSearch
   {
      public int CollectionId { get; set; }

      public bool? Discoverable { get; set; }
      public string? Title { get; set; }
      public int? StartDateIssue { get; set; }

      public int? EndDateIssue { get; set; }

      public string? Publisher { get; set; }

      public List<string> Authors { get; set; }
      public List<string> Keywords { get; set; }

      public List<string> Types { get; set; }
      public SortType SortType { get; set; }

   }
}
