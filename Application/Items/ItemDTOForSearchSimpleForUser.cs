﻿using Shared.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Items
{
   public class ItemDTOForSearchSimpleForUser
   {
      public int? CollectionId { get; set; }
      public string? Title { get; set; }
      public List<string>? Authors { get; set; }
      public string? Year { get; set; }
      public string? Month { get; set; }
      public string? Day { get; set; }
      public List<string>? SubjectKeywords { get; set; }
      public SortType SortType { get; set; }
   }
}
