namespace Application.FileUploads;

public class PdfFileIndexDto
{
    public string FileKey { get; set; }
    public string FileName { get; set; }
    public string FullText { get; set; }
}