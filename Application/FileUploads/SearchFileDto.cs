namespace Application.FileUploads;

public class SearchFileDto
{
    public string FileId { get; set; }
    public string FileName { get; set; }
    public IList<string> FileParents { get; set; }
}