using Microsoft.AspNetCore.Http;
using Shared.Enums;

namespace Application.FileUploads;

public class ModifyFileWithAccessDto
{
    public int FileId { get; set; }
    public FileAccessType AccessType { get; set; }
    public string Role { get; set; }
    public IFormFile? File { get; set; }
}