﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.FileModifyAccesses;
using Domain;

namespace Application.FileUploads
{
    public class FileDTO
    {
      public int FileId { get; set; }
      public string FileUrl { get; set; }
      public string FileKeyId { get; set; }
      public string MimeType { get; set; }
      public string Kind { get; set; }
      public string FileName { get; set; }
      public DateTime CreationTime { get; set; }
      public bool isActive { get; set; }
      public int DownloadCount { get; set; }
      public List<FileModifyAccessDTO> FileAccess { get; set; }
   }
}
