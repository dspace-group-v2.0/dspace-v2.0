﻿using Application.Collections;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Communities
{
   public class CommunityDTO
   {
      public int CommunityId { get; set; }
      public string LogoUrl { get; set; }
      public string CommunityName { get; set; }
      public string ShortDescription { get; set; }
      public bool isActive { get; set; }
      public int? ParentCommunityId { get; set; }

      public double TotalItem { get; set; }
      //public CommunityDTO ParentCommunity { get; set; }
      //public People People { get; set; }
      public List<CommunityDTO> SubCommunities { get; set; }
      public List<CollectionDTOForSelectList> CollectionDTOs { get; set; }
      //public List<CommunityGroup> CommunityGroups { get; set; }
      
   }
}
