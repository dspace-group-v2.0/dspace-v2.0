﻿using Application.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Communities
{
   public class CommunityDTOForDetailForUser
   {
      public int CommunityId { get; set; }
      public string LogoUrl { get; set; }
      public string CommunityName { get; set; }
      public string ShortDescription { get; set; }
      public int? ParentCommunityId { get; set; }
      public string? ParentCommunityName { get; set; }

      public List<CommunityDTOForDetailForUser> SubCommunities { set; get; }

      public List<CollectionDTOForSelectListOfUser> Collections { set; get; }

   }
}