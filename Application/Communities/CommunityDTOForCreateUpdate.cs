﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Application.Communities
{
   public class CommunityDTOForCreateOrUpdate
   {
      public string? LogoUrl { get; set; }
      [RegularExpression(@"^(?:(?!\b(ALTER|CREATE|DELETE|DROP|EXEC(UTE)?|INSERT(\s+INTO)?|MERGE|SELECT(\s+\*)?|UPDATE|UNION(\s+ALL)?)\b)[\S\s])*$",
        ErrorMessage = "Characters are not allowed.")]
      public string CommunityName { get; set; }
      [RegularExpression(@"^(?:(?!\b(ALTER|CREATE|DELETE|DROP|EXEC(UTE)?|INSERT(\s+INTO)?|MERGE|SELECT(\s+\*)?|UPDATE|UNION(\s+ALL)?)\b)[\S\s])*$",
        ErrorMessage = "Characters are not allowed.")]
      public string ShortDescription { get; set; }
      public bool isActive { get; set; }
      public int? ParentCommunityId { get; set; }

      public IFormFile? File { get; set; }
   }
}
