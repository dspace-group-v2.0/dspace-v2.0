using Shared.Enums;

namespace Application.FileModifyAccesses;

public class FileModifyAccessDTO
{
    public int FileAccessId { get; set; }
    public int FileId { get; set; }
    public FileAccessType AccessType { get; set; }
    public string Role { get; set; }
}