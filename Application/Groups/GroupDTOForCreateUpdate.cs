﻿
using System.ComponentModel.DataAnnotations;

namespace Application.Groups
{
   public class GroupDTOForCreateUpdate
   {
      [RegularExpression(@"^(?:(?!\b(ALTER|CREATE|DELETE|DROP|EXEC(UTE)?|INSERT(\s+INTO)?|MERGE|SELECT(\s+\*)?|UPDATE|UNION(\s+ALL)?)\b)[\S\s])*$",
       ErrorMessage = "Characters are not allowed.")]
      public string Title { get; set; }
      [RegularExpression(@"^(?:(?!\b(ALTER|CREATE|DELETE|DROP|EXEC(UTE)?|INSERT(\s+INTO)?|MERGE|SELECT(\s+\*)?|UPDATE|UNION(\s+ALL)?)\b)[\S\s])*$",
       ErrorMessage = "Characters are not allowed.")]
      public string Description { get; set; }
      public bool isActive { get; set; }
   }
}
