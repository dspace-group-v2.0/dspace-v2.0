﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.LoginRequest
{
   public class LoginRequest
   {
      [Required]
      [EmailAddress]
      public string Email { get; set; }
      [Required]
      public string Given_Name { get; set; }
      [Required]
      public string Family_Name { get; set; }
      public string? Hd { get; set; }
   }
}
