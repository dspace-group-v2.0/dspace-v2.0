﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Authors
{
   public class AuthorDTOForSelectItem
   {
      public int AuthorId { get; set; }
      public string FullName { get; set; }
   }
}
