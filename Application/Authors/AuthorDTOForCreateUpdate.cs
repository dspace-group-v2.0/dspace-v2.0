﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Authors
{
   public class AuthorDTOForCreateUpdate
   {
      [RegularExpression(@"^(?:(?!\b(ALTER|CREATE|DELETE|DROP|EXEC(UTE)?|INSERT(\s+INTO)?|MERGE|SELECT(\s+\*)?|UPDATE|UNION(\s+ALL)?)\b)[\S\s])*$",
       ErrorMessage = "Characters are not allowed.")]
      public string FullName { get; set; }
      [RegularExpression(@"^(?:(?!\b(ALTER|CREATE|DELETE|DROP|EXEC(UTE)?|INSERT(\s+INTO)?|MERGE|SELECT(\s+\*)?|UPDATE|UNION(\s+ALL)?)\b)[\S\s])*$",
       ErrorMessage = "Characters are not allowed.")]
      public string JobTitle { get; set; }
      [DataType(DataType.DateTime)]
      public DateTime DateAccessioned { get; set; }
      [DataType(DataType.DateTime)]
      public DateTime DateAvailable { get; set; }
      public string Uri { get; set; }
      public string Type { get; set; }
   }
}