﻿using System.ComponentModel.DataAnnotations;

namespace Application.Peoples
{
   public class PeopleDTOForCreateUpdate
   {
      [RegularExpression(@"^(?:(?!\b(ALTER|CREATE|DELETE|DROP|EXEC(UTE)?|INSERT(\s+INTO)?|MERGE|SELECT(\s+\*)?|UPDATE|UNION(\s+ALL)?)\b)[\S\s])*$",
       ErrorMessage = "Characters are not allowed.")]
      public string FirstName { get; set; }
      [RegularExpression(@"^(?:(?!\b(ALTER|CREATE|DELETE|DROP|EXEC(UTE)?|INSERT(\s+INTO)?|MERGE|SELECT(\s+\*)?|UPDATE|UNION(\s+ALL)?)\b)[\S\s])*$",
       ErrorMessage = "Characters are not allowed.")]
      public string LastName { get; set; }
      [RegularExpression(@"^(?:(?!\b(ALTER|CREATE|DELETE|DROP|EXEC(UTE)?|INSERT(\s+INTO)?|MERGE|SELECT(\s+\*)?|UPDATE|UNION(\s+ALL)?)\b)[\S\s])*$",
       ErrorMessage = "Characters are not allowed.")]
      public string Address { get; set; }
      [RegularExpression(@"^[0-9\-]{10,}$",
       ErrorMessage = "Characters are not allowed.")]
      public string PhoneNumber { get; set; }
      [DataType(DataType.EmailAddress)]
      public string Email { get; set; }
      [RegularExpression(@"^[A-Z]{1,10}$",
      ErrorMessage = "Characters are not allowed.")]
      public string Role { get; set; }
   }
}